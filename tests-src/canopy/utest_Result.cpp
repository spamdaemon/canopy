#include <iostream>
#include <canopy/Result.h>
#include <cassert>

using namespace canopy;

static int checkFailed = 0;

static void fail(const char* m)
{
   ::std::cerr << "Failed : " << (m ? m : "") << ::std::endl;
   ++checkFailed;
}

static Result< int> makeResult(int n)
{
   return n;
}

static void assertNotChecked()
{
   assert(checkFailed>0);
   checkFailed = 0;
}
static void assertChecked()
{
   assert(checkFailed==0);
   checkFailed = 0;
}

void test_Constructor_NotChecked()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   checkFailed = 0;
   {
      Result< int> r = makeResult(4);
   }
   assertNotChecked();
   {
      Result< int> r(5);
   }
   assertNotChecked();

}

void test_Copying_NotChecked()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   checkFailed = 0;
   {
      Result< int> r = makeResult(4);
      assert( r.check() == 4);
      assertChecked();
      ::std::cerr << __LINE__ << ::std::endl;
      {
         r = makeResult(9);
      }
      assertChecked();
      ::std::cerr << __LINE__ << ::std::endl;
   }
   assertNotChecked();
}

void test_Checked()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   checkFailed = 0;
   {
      Result< int> r = makeResult(4);
      assert( r.check() == 4);
   }
   assertChecked();

}

int main()
{
   // set a new result not checked function so we can intercept
   ::canopy::setResultNotChecked(fail);

   try {
      test_Constructor_NotChecked();
      test_Copying_NotChecked();
      test_Checked();
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "Exception caught " << e.what() << ::std::endl;
      return 1;
   }
   catch (...) {
      ::std::cerr << "Unknown exception caught " << ::std::endl;
      return 1;
   }
   return 0;
}
