#include <canopy/time/Time.h>
#include <cassert>
#include <cstring>
#include <cstdio>

using namespace ::canopy;
using namespace ::canopy::time;

void testFormat()
{
  Time t(0);
  char buf[Time::BUFFER_SIZE];
  
  t.format(buf,3);
  ::std::fprintf(stderr,"Time : %s\n",buf);
  assert(::std::strcmp(buf,"1970-01-01T00:00:00.000Z")==0);

  t = Time(1000000000);
  t.format(buf,3);
  ::std::fprintf(stderr,"Time : %s\n",buf);

  assert(::std::strcmp(buf,"1970-01-01T00:00:01.000Z")==0);
  
  t.formatTime(buf,3);
  assert(::std::strcmp(buf,"00:00:01.000Z")==0);
  t.formatDate(buf);
  assert(::std::strcmp(buf,"1970-01-01")==0);
}

void testTime()
{
  Time t(123456789);
  char buf[Time::BUFFER_SIZE];
  const char* bufs[] = {
    "1970-01-01T00:00:00Z",
    "1970-01-01T00:00:00.1Z",
    "1970-01-01T00:00:00.12Z",
    "1970-01-01T00:00:00.123Z",
    "1970-01-01T00:00:00.1234Z",
    "1970-01-01T00:00:00.12345Z",
    "1970-01-01T00:00:00.123456Z",
    "1970-01-01T00:00:00.1234567Z",
    "1970-01-01T00:00:00.12345678Z",
    "1970-01-01T00:00:00.123456789Z"
  };
  for (UInt32 i=0;i<10;++i) {
    t.format(buf,i);
    ::std::fprintf(stderr,"Time : %s\n",buf);
    assert(::std::strcmp(buf,bufs[i])==0);
  }
}

void testTimeOnly()
{
  Time t(123456789);
  char buf[Time::BUFFER_SIZE];
  const char* bufs[] = {
    "00:00:00Z",
    "00:00:00.1Z",
    "00:00:00.12Z",
    "00:00:00.123Z",
    "00:00:00.1234Z",
    "00:00:00.12345Z",
    "00:00:00.123456Z",
    "00:00:00.1234567Z",
    "00:00:00.12345678Z",
    "00:00:00.123456789Z"
  };
  for (UInt32 i=0;i<10;++i) {
    t.formatTime(buf,i);
    ::std::fprintf(stderr,"Time : %s\n",buf);
    assert(::std::strcmp(buf,bufs[i])==0);
  }
}

void testParseTime()
{
  char buf[Time::BUFFER_SIZE];
  const char* bufs[] = {
    "1970-01-02T00:00:00Z",
    "1997-01-01T00:00:00.1Z",
    "1970-01-01T00:00:00.12Z",
    "2000-01-01T23:59:59.999Z",
    "1970-01-01T00:00:00.1234Z",
    "1971-01-01T00:00:00.12345Z",
    "2006-01-01T00:00:00.123456Z",
    "1970-01-01T00:00:00.1234567Z",
    "2008-02-02T00:00:00.12345678Z",
    "1970-12-31T00:00:00.123456789Z"
  };
  for (UInt32 i=0;i<10;++i) {
    Time t = Time::parse(bufs[i]);
    t.format(buf,i);
    ::std::fprintf(stderr,"Time : %s\n",buf);
    assert(::std::strcmp(buf,bufs[i])==0);
  }
}

void testParseTime2()
{
  char buf[Time::BUFFER_SIZE];
  Time t = Time::parse("1970-01-02T23:50:59.+00:00");
  t.format(buf,2);
  ::std::fprintf(stderr,"Time : %s\n",buf);
  assert(::std::strcmp(buf,"1970-01-02T23:50:59.00Z")==0);
}

void testTimeWithTZ()
{
  const char* est = "1994-11-05T08:15:30-05:00";
  const char* utc = "1994-11-05T13:15:30Z";

  char buf[Time::BUFFER_SIZE];
  Time tm = Time::parse(est);
  tm.format(buf,0);
  assert(::std::strcmp(buf,utc)==0);
}



int main()
{
  testFormat();
  testTime();
  testTimeOnly();
  testParseTime();
  testParseTime2();
  testTimeWithTZ();
  return 0;
}
