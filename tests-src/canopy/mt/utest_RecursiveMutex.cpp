#include <canopy/mt/RecursiveMutex.h>

using namespace canopy;
using namespace ::canopy::mt;



void test1()
{
  RecursiveMutex x;
  x.lock();
  x.lock();
  x.unlock();
  x.unlock();
}

int main()
{
  test1();
}
