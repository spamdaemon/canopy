#include <canopy/mt.h>
#include <string>
#include <iostream>
#include <cassert>

using namespace ::canopy;
using namespace ::canopy::mt;

// the thread function will change the name of the 
// a given thread to that of the current thread
static void setName (Thread t)
{
  // get a handle to the currently executing thread
  Thread currentThread;
  
  ::std::cerr << "Changing name of thread from" 
	      << "'" << t.name() << "'"
	      << " to "
	      << "'" << currentThread.name() << "'" 
	      << ::std::endl;  
  
  t.setName(currentThread.name());             
}


int main()
{
  Thread cur;
  // change the name of this thread
  cur.setName("MAIN");
  assert(cur.name()=="MAIN");
  
  // create a new thread. use the createThread function
   // so that an arbitrary object can be transfered to the 
   // new thread. The standard thread constructor is too
   // simplistic for this.
  Thread newThread = createThread("THREAD-2",setName,cur);
  
  // wait for the thread to finish
  Thread::join(newThread);
  
  // the name of this thread should have been changed
  assert (cur.name()==newThread.name());
  
  return 0;
}
