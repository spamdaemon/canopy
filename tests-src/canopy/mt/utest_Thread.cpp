#include <canopy/mt/Thread.h>
#include <canopy/mt.h>
#include <canopy/time/Time.h>
#include <canopy/mt/Condition.h>
#include <iostream>
#include <sstream>
#include <cassert>

using namespace canopy;
using namespace ::canopy::mt;
using namespace ::canopy::time;

static bool uncaughtException = false;
static void uncaughtExceptionThrown(Thread t, const char* msg)
{
   uncaughtException = true;
}

struct foo
{

};

struct bar : public foo
{
};

static void foobar(foo& xfoo)
{
}

static void sleep(void* tm)
{
   Thread currentThread;
   ::std::cerr << "Current thread : " << currentThread.name() << ::std::endl;
   assert(currentThread.name() == ::std::string("test2"));
   Condition cond;
   cond.lock();
   cond.waitUntil(Time(Time::now().time() + Time::Duration(1000000000 * (UInt64) tm)));
   cond.unlock();
}

static void sleep2(void* tm)
{
   Thread currentThread;
   ::std::cerr << "Current thread : " << currentThread.name() << ::std::endl;
   Condition cond;
   cond.lock();
   cond.waitUntil(Time(Time::now().time() + Time::Duration(1000000000 * (UInt64) tm)));
   cond.unlock();
}

static void testUncaughtExceptionHandler()
{
   throw 5;
}

static void failWithException(void*)
{
   Thread currentThread;
   ::std::cerr << "Current thread : " << currentThread.name() << ::std::endl;
   throw ::std::runtime_error("ABORT THREAD");
}

static void failWithAnyException(void*)
{
   Thread currentThread;
   ::std::cerr << "Current thread : " << currentThread.name() << ::std::endl;
   throw 1;
}

static void printString(::std::string tmp)
{
   ::std::cerr << "Transferred data to thread " << tmp << ::std::endl;
   assert(tmp == Thread().name());
}

static void testCancelled()
{
   while (true) {
      Thread::suspend(1);
      Thread::testCancelled();
   }
}

static void testCancelSelf()
{
   while (true) {
      Thread::testCancelled();
      Thread::suspend(1000000000);
      Thread::cancel(Thread::currentThread());
   }
}

static void cancel(Thread t)
{
   Condition cond;
   cond.lock();
   ::std::cerr << "Waiting " << ::std::endl;
   cond.waitUntil(Time(Time::now().time() + Time::Duration(INT64_C(1000000000) * (UInt64) 1)));
   cond.unlock();
   ::std::cerr << "Cancelling " << ::std::endl;
   Thread::cancel(t);
}

void test1()
{
   Thread currentThread;
   currentThread.setName("MAIN");
   try {
      Thread::join(currentThread);
      assert(false);
   }
   catch (const ::std::logic_error& e) {
   }
   ::std::vector< Thread> activeThreads(Thread::activeThreads());

   // must be 11, because the current thread will be in there
   ::std::cerr << "Active threads " << activeThreads.size() << ::std::endl;
}

void test2()
{
   Thread newThread("test2", sleep, (void*) 2);
   Thread::join(newThread);
   ::std::vector< Thread> activeThreads(Thread::activeThreads());

   // must be 11, because the current thread will be in there
   ::std::cerr << "Active threads " << activeThreads.size() << ::std::endl;
}

void test3()
{
   Thread newThread("test3", failWithException, 0);
   Thread::join(newThread);
   newThread = Thread("test4", failWithAnyException, 0);
   Thread::join(newThread);
   ::std::vector< Thread> activeThreads(Thread::activeThreads());

   // must be 11, because the current thread will be in there
   ::std::cerr << "Active threads " << activeThreads.size() << ::std::endl;
}

void test4()
{
   for (int i = 0; i < 10; ++i) {
      ::std::stringstream ss;
      ss << "test4-" << i;
      Thread newThread(ss.str(), sleep2, (void*) 4);
   }
   sleep2((void*) 1);
   ::std::vector< Thread> activeThreads(Thread::activeThreads());

   // must be 11, because the current thread will be in there
   ::std::cerr << "Active threads " << activeThreads.size() << ::std::endl;
   assert(activeThreads.size() == 11);

   Thread cur;
   cur.setName("MAIN2");
   for (size_t i = 0; i < activeThreads.size(); ++i) {
      if (cur != activeThreads[i]) {
         Thread::join(activeThreads[i]);
      }
   }
   activeThreads = Thread::activeThreads();
   assert(activeThreads.size() == 1);
   assert(activeThreads[0] == cur);
}

void test5()
{
   ::std::string tmp("deadbeef");
   Thread t1 = createThread(tmp, printString, tmp);
   Thread::join(t1);
}

void test6()
{
   Thread t1 = createThread("testCancelled", testCancelled);
   Thread t2 = createThread< Thread>("cancel", cancel, t1);
   Thread::join(t1);
   Thread::join(t2);
}

void test7()
{
   Thread t1 = createThread("testCancelSelf", testCancelSelf);
   Thread::join(t1);
}

void test8()
{
   uncaughtException = false;
   Thread::UncaughtExceptionHandler old = Thread::setUncaughtExceptionHandler(uncaughtExceptionThrown);
   Thread t1 = createThread("uncaughtException", testUncaughtExceptionHandler);
   Thread::join(t1);
   Thread::setUncaughtExceptionHandler(old);
   assert(uncaughtException);
}

void test9()
{
   bar xbar;
   Thread t1 = createThread("foobar", foobar, xbar);
   Thread::join(t1);
}

int main()
{
   Thread::setLogLevel(Thread::ALL);
   try {
      test1();
      test2();
      test3();
      test4();
      test5();
      test6();
      test7();
      test8();
      test9();
   }
   catch (::std::exception& e) {
      ::std::cerr << "Exception " << e.what() << ::std::endl;
      return 1;
   }
   catch (...) {
      ::std::cerr << "Unknown exception" << ::std::endl;
      return 1;
   }
   return 0;
}
