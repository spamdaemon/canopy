#include <canopy/mt/Condition.h>
#include <canopy/mt.h>

#include <cstdio>

using namespace canopy;
using namespace ::canopy::mt;

// the class that provides access to the data that will be shared
// between the consumer and the producer
struct Shared {
  
  Condition condition;
  volatile bool haveData;
  volatile int data;
};

// this function will be executed by consumer
// and it will read one piece of data at a time
void consume(Shared* ptr)
{
  // lock the mutex
  ptr->condition.lock();
  do {
    // first, check if there is any data; if this is not done,
    // then the first time through this loop might end up 
    // causing a deadlock
    if (ptr->haveData) {

      ptr->haveData= false;
      int i = ptr->data;
      ::std::printf("->%d\n",i);
      if (i==1000) {
	break;
      }

      // notify the producer that we've picked up the data
      ptr->condition.notify();
    }
    // once we wait, the producer can create more data
    ptr->condition.wait();


  } while(true); 
  
  // don't forget to unlock the mutex. 
  ptr->condition.unlock();

}


void produce(Shared* ptr)
{
  // lock the mutex
  int i=0;
  ptr->condition.lock();
  while(true) {
    // test if we need to produce data
    if (!ptr->haveData) {
      // produce the data and 
      ptr->data = i++;
      ptr->haveData= true;
      ::std::printf("%d",ptr->data);

      // notify the consumer that data is available
      ptr->condition.notify();

      // stop after 1001 iterations
      if (i>1000) {
        break;
      }
    }
    // wait until the consumer has picked up the data
    ptr->condition.wait(); 
  }
  ptr->condition.unlock();
}

int main()
{
  // create the object that will be concurently accessed
  Shared shared;
  // initialize the shared 
  shared.haveData=false;

  // kick of the two threads
  Thread producer = createThread<Shared*>("producer",produce,&shared);
  Thread consumer = createThread<Shared*>("consumer",consume,&shared);

  // wait for each to finish
  Thread::join(producer);
  Thread::join(consumer);

  return 0;
}
