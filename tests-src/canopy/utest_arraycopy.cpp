#include <canopy/arraycopy.h>
#include <cassert>
#include <cstdlib>
#include <iostream>

using namespace canopy;

class A {
};

template <typename T>
void testArrayCopyMove ()
{
  T a[2];
  T b[2];

  ::std::cerr << "PointerDiff " << ((char*)&a[1] - (char*)&a[0]) << ::std::endl << ::std::flush;

  arraycopy(a,b,2);
  arraymove(a,b,2);
}


void testarraycopymove()
{
  testArrayCopyMove<double>();
  testArrayCopyMove<float>();
  testArrayCopyMove<wchar_t>();
  testArrayCopyMove<char>();
  testArrayCopyMove<signed char>();
  testArrayCopyMove<unsigned char>();
  testArrayCopyMove<short>();
  testArrayCopyMove<unsigned short>();
  testArrayCopyMove<int>();
  testArrayCopyMove<unsigned int>();
  testArrayCopyMove<long>();
  testArrayCopyMove<unsigned long>();
  testArrayCopyMove<long long>();
  testArrayCopyMove<unsigned long long>();
  testArrayCopyMove<bool>();
  testArrayCopyMove<A>();
}


int main()
{
  testarraycopymove();
  return 0;
}
