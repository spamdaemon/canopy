#include <canopy/io/Selector.h>
#include <canopy/net/Socket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/mt/Thread.h>
#include <canopy/time/Time.h>
#include <canopy/Process.h>
#include <iostream>
#include <cassert>
#include <map>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::io;
using namespace ::canopy::net;
using namespace ::canopy::mt;
using namespace ::canopy::time;

static string detectBroadcastAddress()
{
   ::std::string result;
   try {
      auto proc = Process::execute(Process::Command("test-bin/get_broadcastaddr"));
      ::std::getline(proc.cout(), result);
      proc.closeCout();
      proc.closeCerr();
      proc.waitFor();
   }
   catch (...) {
      cerr << "Failed to detect the broadcast address" << endl;
      return "";
   }
   return result;
}

void testSelector()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   ::std::string bcastAddr = detectBroadcastAddress();
   if (bcastAddr.empty()) {
      ::std::cerr << __FUNCTION__ << ": cannot test selected since broadcast address could not be determined"
            << ::std::endl;
      return;
   }

   const char* ipaddr = bcastAddr.c_str();

   Socket dummy(DATAGRAM, INET_4);
   Socket receiver(DATAGRAM, INET_4);
   Socket sender(DATAGRAM, INET_4);
   cerr << __LINE__ << endl;
   Selector selector;
   sender.setBroadcastEnabled(true);

   cerr << __LINE__ << endl;
   receiver.bind(*(Endpoint(ipaddr, "54321", DATAGRAM, INET_4).begin()));

   cerr << __LINE__ << endl;
   assert(selector.size() == 0);
   selector.add(receiver.descriptor(), ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   assert(selector.getRegisteredEvents(receiver.descriptor()) == (::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE));
   selector.remove(receiver.descriptor(), ::canopy::io::IOEvents::WRITE);
   assert(selector.getRegisteredEvents(receiver.descriptor()) == ::canopy::io::IOEvents::READ);
   selector.remove(receiver.descriptor(), ::canopy::io::IOEvents::READ);
   assert(selector.size() == 0);
   selector.add(receiver.descriptor(), ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   selector.add(sender.descriptor(), ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   selector.add(sender.descriptor(), ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   assert(selector.size() == 2);
   selector.setAutoWakeupEnabled(!selector.isAutoWakeupEnabled());
   selector.wakeup();
   selector.clearPendingWakeup();
   assert(selector.isSelectable(receiver.descriptor()));
   assert(selector.isSelectable(sender.descriptor()));
   assert(!selector.isSelectable(dummy.descriptor()));

   cerr << "Selecting " << endl;
   Selector::Events events = selector.select(makeTimeout(Timeout(100)),Selector::Events());
   assert(events.size() == 2);
   for (Selector::Events::iterator i = events.begin(); i != events.end(); ++i) {
      assert(i->second == ::canopy::io::IOEvents::WRITE);
   }

   const string hello("Hello, World");
   sender.writeTo(hello.c_str(), hello.length() + 1, *(Endpoint(ipaddr, "54321", DATAGRAM, INET_4).begin()));

   cerr << "Sleeping " << endl;
   Thread::suspend(1000);
   cerr << "Selecting " << endl;
   events = selector.select(makeTimeout(::std::chrono::microseconds(1)), ::std::move(events));
   assert(events.size() == 2);
   for (Selector::Events::iterator i = events.begin(); i != events.end(); ++i) {
      if (i->first == receiver.descriptor()) {
         ::std::cerr << "Receiver has events : " << i->second << ::std::endl;
      }
      else {
         ::std::cerr << "Sender has events : " << i->second << ::std::endl;
      }
   }
   for (Selector::Events::iterator i = events.begin(); i != events.end(); ++i) {
      if (i->first == receiver.descriptor()) {
         assert((i->second & ::canopy::io::IOEvents::READ) == ::canopy::io::IOEvents::READ);
      }
      assert((i->second & ::canopy::io::IOEvents::WRITE) == ::canopy::io::IOEvents::WRITE);
   }

   assert(
         Selector::select(receiver.descriptor(), ::canopy::io::IOEvents::WRITE | ::canopy::io::IOEvents::READ, Timeout(1))
               == (::canopy::io::IOEvents::WRITE | ::canopy::io::IOEvents::READ));

}

void testSelection()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 54321, DATAGRAM, INET_4);
   Selector selector;
   Socket sock(DATAGRAM, INET_4);
   sock.bind(*xaddr.begin());
   selector.add(sock.descriptor(), ::canopy::io::IOEvents::READ);
   Selector::Events events;
   events = selector.select(Timeout::zero(), ::std::move(events));

   Time then = Time::now();
   for (int i = 0; i < 10; ++i) {
      events = selector.select(makeTimeout(::std::chrono::milliseconds(100)), ::std::move(events));
   }
   Time now = Time::now();
   cerr << "Delta time : " << (now.time() - then.time()).count() << endl;

   assert(now.time()-then.time() > Time::Duration(1000*INT64_C(1000000)));
   assert(now.time()-then.time() < Time::Duration(1100*INT64_C(1000000)));
}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {
      testSelector();
      testSelection();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
