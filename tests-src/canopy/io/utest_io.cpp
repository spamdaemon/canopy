#include <canopy/canopy.h>
#include <canopy/io/io.h>
#include <iostream>

using namespace ::std;
using namespace ::canopy::io;

void test_Timeouts()
{
   Timeout blocking=BLOCKING_TIMEOUT;
   Timeout non_blocking=NON_BLOCKING_TIMEOUT;
   Timeout finite(56);

   Timeout blocking2(-5);

   assert(isBlockingTimeout(blocking));
   assert(isBlockingTimeout(blocking2));
   assert(isNonBlockingTimeout(non_blocking));
   assert(isFiniteTimeout(finite));

}

int main()
{

   try {
      test_Timeouts();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
