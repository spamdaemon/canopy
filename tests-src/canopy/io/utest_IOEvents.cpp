#include <canopy/canopy.h>
#include <canopy/io/IOEvents.h>
#include <iostream>

using namespace ::std;
using namespace ::canopy::io;

void test_events()
{
   IOEvents events;
   events.addEvents(IOEvents::ALL);
   assert(!events.empty() && "removeEvents addEvents");
   events.removeEvents(IOEvents::ALL);
   assert(events.empty() && "removeEvents failed");
   events.addEvents(IOEvents::ALL);
   assert(!events.empty());
   events.clearEvents();
   assert(events.empty() && "clearEvents failed");
}

int main()
{

   try {
      test_events();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
