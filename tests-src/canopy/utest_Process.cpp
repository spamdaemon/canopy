#include <canopy/Process.h>
#include <canopy/User.h>
#include <iostream>
#include <sstream>
#include <cassert>
#include <map>

using namespace ::std;
using namespace ::canopy;

static string toLowerCase(const string& str)
{
   // an empty argument vector
   vector< string> args;
   args.push_back("ABCDEFGHIJKLMNOPQURSTUVWXYZ");
   args.push_back("abcdefghijklmnopqurstuvwxyz");
   // the process to be executed is the tr process
   string cat("/usr/bin/tr");

   // create the child process
   Process child = Process::execute(Process::Command(cat, args));
   // send the string that we want echoed to the child
   child.cin() << str << endl << flush;
   // close the input stream for the child (this will also
   // flush the output buffer)
   child.closeCin();

   // at this point, wait until the child has exited
   child.waitFor();

   if (child.state() != Process::EXITED) {
      throw runtime_error("Child did not exit normally");
   }

   // consume any data that the child had written
   string echoedString;
   getline(child.cout(), echoedString);

   return echoedString;
}

void testProcess()
{
   cerr << "Current process " << Process::currentProcessID() << endl;
}

void testExecve()
{
   vector< string> args;
   args.push_back("1");
   Process forked = Process::execute(Process::Command("/bin/sleep", args));
   forked.waitFor();
   assert (forked.state()==Process::EXITED);
   assert (forked.exitCode()==0);
}

void testStreamReadWrite()
{
   const string expected("Hello, World!");
   Process forked = Process::execute(Process::Command("/bin/cat", {}));
   forked.cin() << expected << endl << flush;
   assert(forked.cin().good());
   forked.closeCin();
   forked.waitFor();
   string out;
   getline(forked.cerr(), out);
   cerr << "Error stream : " << out << endl;
   getline(forked.cout(), out);
   cerr << "Output : " << out << endl;
   assert(expected==out);
   assert (forked.state()==Process::EXITED);
   assert (forked.exitCode()==0);
}

void testExample()
{
   string res = toLowerCase("Hello,World");
   cerr << "RES : " << res << endl;
   assert(res=="hello,world");
}

void testFindExecutable()
{
   string exec = Process::findExecutable();
   cerr << "The executable is " << exec << endl;
   assert (exec.find("utest_Process") != string::npos);
}

void testEnvironment()
{
   map< string, string> env;
   env["message"] = "Hello, World!";

   Process p = Process::execute(Process::Command("/usr/bin/env", env,{}));
   while (!p.cout().eof()) {
      string tmp;
      ::std::getline(p.cout(), tmp);
      if (!p.cout().fail()) {
         cerr << tmp << endl << flush;
         assert(tmp=="message=Hello, World!");
      }
   }
   cerr.flush();
   p.waitFor();
}

void testName()
{
   ::std::cerr << "Process::name() = " << Process::name() << ::std::endl;
}

void testArgv()
{
   vector< string> argv = Process::argv();
   ::std::cerr << "Process Arguments :";
   for (size_t i = 0; i < argv.size(); ++i) {
      ::std::cerr << " \"" << argv[i] << '"';
   }
   ::std::cerr << ::std::endl;
}

void testEnv()
{
   bool isSet;
   string user = Process::getEnv("USER", 0, isSet);
   if (isSet) {
      unique_ptr< User> curUser = User::currentUser();
      ::std::cerr << "User: " << user << ::std::endl;
      assert(user==curUser->name());
   }
   else {
      ::std::cerr << "No environment variable USER is defined" << ::std::endl;
   }

   // highly unlikely that there is such an environment variable
   string var = Process::getEnv("ASF(&(&)_)(*123948732*(&k235tr2", "1234", isSet);
   assert(var=="1234");
   assert(!isSet);
}

void testEnv_default()
{
   size_t n=1;
   ::std::string str = Process::getEnv("FOOOOOOARRR","1");
   ::std::istringstream in(str);
   in >> n;
   if (in.fail()) {
      ::std::cerr << "READ " << n << ::std::endl;
      assert(false && "Read operation failed ");
   }
   assert(n==1);
}

void testEnv_parsed()
{
   size_t n= Process::getEnv("FOOOOOOARRR",2);
   assert(n==2);
}

void testEnv_parsed_invalid()
{
   Process::setEnv("XYWS11223","-1");
   size_t n= Process::getEnv("FOOOOOOARRR",2);
   assert(n==2);
}

// a process returns -1 as a byte via both streams
void testProcessReturningEOF()
{
   const char expected[] = { (char)(unsigned char) 0xff, 1, 2, 3, 4, 5, 6, 7,0 };

   Process forked = Process::execute(Process::Command("/bin/cat", {}));
   forked.cin() << expected << endl << flush;
   assert(forked.cin().good());
   forked.closeCin();
   forked.waitFor();
   string errout;
   getline(forked.cerr(), errout);
   cerr << "Error stream : " << errout << endl;
   char out[13] = { 0,0,0,0,0,0,0,0,0,0,0,0,0 };
   for (int i = 0; i < 12 && !forked.cout().eof(); ++i) {
      assert(i>=8 || forked.cout().peek()!= ::std::istream::traits_type::eof());
      int x = forked.cout().get();
      if (::std::istream::traits_type::eof() != x) {
         out[i] = (char) x;
         out[i + 1] = '\n';
         ::std::cerr << "out[" << i<<"]=" << (int)out[i] << ::std::endl;
      }
   }
   cerr << "Output : " << out << endl;

   for (int i = 0; i < 8; ++i) {
      assert(expected[i]==out[i]);
   }
   assert(out[8]=='\n');

   assert (forked.state()==Process::EXITED);
   assert (forked.exitCode()==0);
}

int main()
{
   try {
      testExample();
      testProcess();
      testExecve();
      testStreamReadWrite();
      testFindExecutable();
      testEnvironment();
      testName();
      testArgv();
      testEnv();
      testEnv_default();
      testEnv_parsed();
      testEnv_parsed_invalid();
      testProcessReturningEOF();
      return 0;
   }
   catch (const exception& e) {
      cerr << "Exception " << e.what() << endl;
   }
   catch (...) {
      cerr << "unknown exception " << endl;
   }
   return 1;
}

