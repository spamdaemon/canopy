#include <canopy/dso/DSO.h>
#include <canopy/Process.h>
#include <cassert>
#include <cstdio>
#include <string>
#include <iostream>
#include <vector>

using namespace ::std;
using namespace canopy;
using namespace canopy::dso;

static const char* SIMPLE_NAME="canopyTest";
static const char* DSO_NAME="libcanopyTest.so";
static const char* FULLNAME="lib/libcanopyTest.so";

static ::std::vector< ::std::string> loadSearchPaths()
{
   ::std::vector< ::std::string> result;
   result.push_back("/tmp");
   result.push_back("/lib");
   result.push_back("lib");
   return result;
}

void utest_test1()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   typedef double (*SQR)(double);

   ::std::unique_ptr < DSO > dso = DSO::load(FULLNAME, true);

   SQR dynSQR = reinterpret_cast< SQR>(castToFunction((dso->symbol("sqr"))));
   double tmp = dynSQR(5);
   ::std::printf("TMP : %lf\n", tmp);
   assert(tmp == 25);

   // closing more than once must be supported
   dso->close();
   dso->close();
}

void utest_test2()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   typedef double (*SQR)(double);

   ::std::unique_ptr < DSO > dso = DSO::load(FULLNAME, false);

   SQR dynSQR = reinterpret_cast< SQR>(castToFunction((dso->symbol("sqr"))));
   double tmp = dynSQR(5);
   ::std::printf("TMP : %lf\n", tmp);
   assert(tmp == 25);

   // closing more than once must be supported
   try {
      dso->close();
      assert(false);
   }
   catch (const std::logic_error& e) {
      ::std::printf("ERROR %s\n", e.what());
   }
}

void utest_test3()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   ::std::string dso = DSO::makeDSOPath(nullptr, SIMPLE_NAME);
   assert(dso == DSO_NAME);
}

void utest_test4()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   typedef double (*SQR)(double);

   DSO::Spec dsoSpec(SIMPLE_NAME);
   ::std::unique_ptr < DSO > dso;
   for (const ::std::string& p : loadSearchPaths()) {
      ::std::string foundAt;
      try {
         dso = DSO::load(p.c_str(), dsoSpec, true, &foundAt);
      }
      catch (...) {

      }
      if (dso) {
         ::std::cerr << "Found DSO at " << foundAt << ::std::endl;
         assert(!foundAt.empty());
         break;
      }
      assert(foundAt.empty());
   }
   assert(dso.get());

   SQR dynSQR = reinterpret_cast< SQR>(castToFunction((dso->symbol("sqr"))));
   double tmp = dynSQR(5);
   ::std::printf("TMP : %lf\n", tmp);
   assert(tmp == 25);

   // closing more than once must be supported
   dso->close();
   dso->close();
}

void utest_test5()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   DSO::Spec spec;
}

void utest_testSearchPath()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   typedef double (*SQR)(double);
   vector< string> spath = loadSearchPaths();
   if (spath.empty()) {
      ::std::cerr << "No search paths found" << ::std::endl;
      return;
   }
   spath.insert(spath.begin(), "/root/bin");
   spath.insert(spath.begin(), "/tmp");
   string found("FOO");
   DSO::Spec dsoSpec(SIMPLE_NAME);

   ::std::unique_ptr < DSO > dso = DSO::load(spath, dsoSpec, true, &found);

   assert(dso.get());
   ::std::cerr << "Found at : " << found << ::std::endl;

   SQR dynSQR = reinterpret_cast< SQR>(castToFunction((dso->symbol("sqr"))));
   double tmp = dynSQR(5);
   ::std::printf("TMP : %lf\n", tmp);
   assert(tmp == 25);
}

int main()
{
   try {
      utest_test1();
      utest_test2();
      utest_test3();
      utest_test4();
      utest_test5();
      utest_testSearchPath();
      return 0;
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "exception : " << e.what() << ::std::endl;

   }
   catch (...) {
   }
   return 1;
}
