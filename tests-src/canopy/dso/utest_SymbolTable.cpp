#include <canopy/dso/SymbolTable.h>
#include <canopy/Process.h>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <vector>

using namespace canopy;
using namespace canopy::dso;

//static const char* SIMPLE_NAME="canopyTest";
static const char* DSO_NAME="libcanopyTest.so";
//static const char* FULLNAME="lib/libcanopyTest.so";

static ::std::vector< ::std::string> loadSearchPaths()
{
   ::std::vector< ::std::string> result;
  result.push_back("/tmp");
  result.push_back("/lib");
  result.push_back("lib");
  return result;
}

void test1()
{
   ::std::vector< ::std::string> searchPath = loadSearchPaths();
   if (searchPath.empty()) {
      ::std::cerr << "No search paths found" << ::std::endl;
      return;
   }
   try {
      ::std::unique_ptr < SymbolTable > dso;
      for (const ::std::string& p : searchPath) {
         try {
            ::std::cerr << "Looking in " << p << ::std::endl;
  	    dso = SymbolTable::load(((p + "/") + DSO_NAME).c_str());
            break;
         }
         catch (const UnsupportedException&) {
            throw;
         }
         catch (...) {
            // ignore, and try again
         }
      }
      if (!dso) {
         throw ::std::runtime_error("Failed to load symbold tables");
      }

      ::std::set< ::std::string> names = dso->names();
      for (::std::set< ::std::string>::const_iterator i = names.begin(); i != names.end(); ++i) {
         ::std::cout << "Symbol " << *i << " ";
         switch (dso->symbolType(*i)) {
            case SymbolTable::FUNCTION_SYMBOL:
               ::std::cout << "F";
               break;
            case SymbolTable::DATA_SYMBOL:
               ::std::cout << "D";
               break;
            default:
               ::std::cout << "U";
               break;
         }
         ::std::cout << ::std::endl;
      }
   }
   catch (const UnsupportedException&) {
      ::std::cerr << "SymbolTable does not support symbols; no result" << ::std::endl;
   }
}

int main()
{
   try {
      test1();
      return 0;
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "exception : " << e.what() << ::std::endl;

   }
   catch (...) {
   }
   return 1;
}
