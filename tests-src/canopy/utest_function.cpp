#include <canopy/function.h>
#include <cassert>
#include <sstream>

using namespace canopy::function;

static int FUNC(int x)
{
   return x * x;
}

static void test_lambda()
{
   auto f = makeFunction([] (int x) {return x*x;});
   assert(f(2) == 4);
}

static void test_function_pointer()
{

   auto f = makeFunction(FUNC);
   assert(f(2) == 4);
}

static void test_conversion()
{
   auto f = makeFunction([] (int i) {return i;});
   auto conv = makeFunction([] ( int i) {::std::ostringstream s; s<< i; return s.str();});
   auto g = changeReturnType(f,conv);
   assert(g(1) == "1");
}

int main()
{
   test_lambda();
   test_function_pointer();
   test_conversion();
   return 0;
}
