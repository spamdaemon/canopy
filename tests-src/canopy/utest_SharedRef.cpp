#include <canopy/SharedRef.h>
#include <cassert>
#include <cstdlib>
#include <iostream>

using namespace std;
using namespace canopy;

struct A
{
      virtual ~A()
      {
      }

      int x() { return 1; }

};

struct B : public A
{
   ~B()
   {
   }
};

struct C
{
      virtual ~C()
      {
      }
};

struct D : public C, public A
{
      ~D()
      {
      }
};

void test_NPE()
{
   ::std::shared_ptr<A> nil;
   try {
      SharedRef<A> a0(nil);
      assert(false);
   }
   catch (...) {

   }
}

void test_constructors()
{
   SharedRef< A> a0(make_shared< A>());
   SharedRef< A> a1(SharedRef<B>(make_shared< B>()));
   SharedRef< A> a2(make_unique< B>());
   SharedRef< A> a3(make_unique< B>());

   try {
      SharedRef< A> x (make_shared< C>());
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
   try {
      SharedRef< A> x(make_unique< C>());
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
}

void test_assignment()
{
   SharedRef<A > a(make_shared< A>());

   a= SharedRef< A> (make_shared<A>());
   a= SharedRef< B> (make_shared<B>());
   a= make_shared< A>();
   a= make_shared< B>();
   a= make_unique< B>();

   try {
      a =  make_shared< C>();
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
   try {
      a =  make_unique< C>();
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
}

void test_operator()
{
   SharedRef<A > a(make_shared< A>());
   assert(a->x()==1);
}

void test_comparison()
{
   SharedRef<A > a(make_shared< A>());
   SharedRef<B > b0(make_shared< B>());
   SharedRef<A > b1(b0);
   ::std::shared_ptr<B> b2(b1);

   assert(a!=b0);
   assert(b1==b0);
   assert(a<b0 || b0<a);
   assert(b2==b0.get());
}

int main()
{
   test_constructors();
   test_assignment();
   test_comparison();
   test_operator();
   test_NPE();
   return 0;
}
