#include <canopy/lambda.h>
#include <cassert>

using namespace canopy::lambda;

static void test_function()
{
   auto f = toFunction([] (int x) {return x*x;});
   assert(f(2) == 4);
}
static void test_function2()
{
   auto L = [] (int x) {return x*x;};
   auto f = toFunction(L);
   assert(f(2) == 4);
}

static void test_function_pointer()
{

   auto f = toFunctionPointer([] (int x) {return x*x;});
   assert(f(2) == 4);
}

int main()
{
   test_function();
   test_function2();
   test_function_pointer();
   return 0;
}
