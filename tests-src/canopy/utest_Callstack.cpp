#include <canopy/canopy.h>
#include <canopy/Callstack.h>

#include <string>
#include <cassert>
#include <sstream>
#include <iostream>

using namespace std;
using namespace canopy;

#define TEST_FUNCTION thisisaveryuniquefunction

void TEST_FUNCTION()
{
   ::std::unique_ptr< Callstack> stk = Callstack::create();
   ::std::ostringstream out;
   stk->print(out);
   ::std::string trace=out.str();
   ::std::cerr << "callstack : " << endl << trace << endl;

   assert(trace.find(CANOPY_TO_STRING(TEST_FUNCTION))!=::std::string::npos);
}

int main()
{
   if (!Callstack::isSupported()) {
      ::std::cerr << "Callstacks are not supported on this system; test passes" << ::std::endl;
      return 0;
   }
   TEST_FUNCTION();
   return 0;
}
