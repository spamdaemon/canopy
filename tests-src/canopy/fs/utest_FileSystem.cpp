#include <canopy/fs/FileSystem.h>
#include <canopy/time/Time.h>
#include <canopy/mt/Thread.h>
#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;
using namespace canopy;
using namespace canopy::fs;
using namespace canopy::time;

static void assertRelativePath(const string& a, const string& b, const string& expected)
{
   string path = FileSystem().getRelativePath(a, b);
   if (path != expected) {
      cerr << "Expected '" << expected << "' but got '" << path << "'" << endl;
   }
   assert(path == expected);
}

static void assertNormalizedPath(const string& str, const string& expected)
{
   cerr << "Normalize " << str << " : " << flush;
   string n = FileSystem().normalizePath(str);
   if (n != expected) {
      cerr << "Expected '" << expected << "' but got '" << n << "'" << endl;
   }
   assert(n == expected);
}

static const char DIR[] = { "utest_filesystem" };

void test1()
{

   FileSystem fs;
   try {
      fs.rmdir(DIR, true);
   }
   catch (...) {
   }
   assert(!fs.exists(DIR));

   string fname(DIR + string(fs.directorySeparator()) + "file");
   ofstream failOpen(fname.c_str());

   assert(failOpen.fail());

   fs.mkdir(DIR);
   assert(fs.exists(DIR));
   assert(fs.isDirectory(DIR));
   ofstream file(fname.c_str());
   assert(!file.fail());
   assert(fs.isFile(fname));
   vector< string> entries;
   UInt32 count = fs.listEntries(DIR, entries);
   assert(count == 1);
   assert(entries.size() == count);
   assert(entries[0] == fname);
   string rname(DIR + string(fs.directorySeparator()) + "rfile");
   fs.rename(fname, rname);
   assert(fs.isFile(rname));
   assert(fs.exists(rname));
   assert(!fs.exists(fname));
   entries.clear();
   assert(fs.listEntries(DIR, entries) == 1);
   try {
      fs.remove(DIR);
      assert("Directory should be empty for this to work" == 0);
   }
   catch (...) {
   }
   bool wasFile = fs.remove(rname);
   assert(wasFile);
   fs.rmdir(DIR, false);

   // test symbolic link creation
   fs.mkdir(DIR);
   fs.createSymbolicLink("/etc", string(DIR) + fs.directorySeparator() + "linkfile");
   fs.createSymbolicLink("linkfile", string(DIR) + fs.directorySeparator() + "linkfile2");
   assert(fs.findLinkTarget(string(DIR) + fs.directorySeparator() + "linkfile") == "/etc");
   assert(fs.findLinkTarget(string(DIR) + fs.directorySeparator() + "linkfile2") == "linkfile");
   assert(fs.followLinks(string(DIR) + fs.directorySeparator() + "linkfile2") == "/etc");
   fs.rmdir(DIR, true);

}

void test2()
{
   cerr << "Testing path normalization" << endl;

   FileSystem fs;
   assertNormalizedPath("a/b/c", "a/b/c");
   assertNormalizedPath("/a/b/c", "/a/b/c");
   assertNormalizedPath("/a/b/c/", "/a/b/c");
   assertNormalizedPath("/a//b/c/", "/a/b/c");
   assertNormalizedPath("/a/b/././/./c/./././/", "/a/b/c");
   assertNormalizedPath("/a//b/./c/", "/a/b/c");
   assertNormalizedPath("/a//b/c/.", "/a/b/c");
   assertNormalizedPath("/a/b/c/..", "/a/b");
   assertNormalizedPath("/a/b/../c", "/a/c");
   assertNormalizedPath("a/../c", "c");
   assertNormalizedPath("a/..", ".");
   assertNormalizedPath("/../a/b/../c", "/a/c");
   assertNormalizedPath("/../a/./b/./../c", "/a/c");
   assertNormalizedPath("/../", "/");
   assertNormalizedPath("/..", "/");
   assertNormalizedPath("/./", "/");
   assertNormalizedPath("/.", "/");
   assertNormalizedPath("///////a///////b////////c////////", "/a/b/c");
   assertNormalizedPath(".", ".");
   assertNormalizedPath("./a", "a");
   assertNormalizedPath("././a", "a");
   assertNormalizedPath("../a", "../a");
   assertNormalizedPath(".///../././../a", "../../a");
   assertNormalizedPath("../../../../a", "../../../../a");
   assertNormalizedPath("../../a/../../", "../../..");
   assertNormalizedPath("../../a/../../../", "../../../..");
   assertNormalizedPath("a/../..", "..");

}

void testMkdirs()
{
   FileSystem fs;
   fs.mkdirs("a/b/c");
   assert(fs.isDirectory("a/b/c"));
   fs.rmdir("a", true);
}

void testDirname()
{
   FileSystem fs;
   assert(fs.directoryName("a/b/c///") == "a/b");
   assert(fs.directoryName("a/./b/.///c") == "a/./b/.");
}

void testBasename()
{
   FileSystem fs;
   cerr << endl << fs.baseName("a/./b/./c/////") << endl << fs.baseName("///") << endl << fs.baseName("//./") << endl;
   assert(fs.baseName("a/./b/./c/////") == "c");
   assert(fs.baseName("///") == "/");
   assert(fs.baseName("//./") == ".");
}

void testSetTime()
{
   const string fname("foobar");
   FileSystem fs;
   ofstream f(fname.c_str());
   f << "HAHA" << endl;
   f.close();

   Time mtm = fs.modificationTime(fname);
   Time ctm = fs.timeOfLastChange(fname);
   Time atm = fs.accessTime(fname);

   fs.setTime(fname.c_str(), Time::now().time() - Time::Duration(2000000000L));

   assert(fs.modificationTime(fname) < mtm);
   fs.remove(fname);
}

void testFileTimeAccuracy()
{
   FileSystem fs;
   const string fname("times");

   {
      ofstream f(fname.c_str());
      f << "HAHA" << endl;
   }
   Time mtm = fs.modificationTime(fname);

   ::std::cerr << "Timestamp accuracy : " << fs.timeStampAccuracy() << ::std::endl;

   ::canopy::mt::Thread::suspend(2 * fs.timeStampAccuracy());

   {
      ofstream f(fname.c_str(), ios::app);
      f << mtm.time().time_since_epoch().count() << endl;
   }

   Time tmp = fs.timeOfLastChange(fname);
   assert(tmp.time() - mtm.time() >= Time::Duration(fs.timeStampAccuracy()));

   fs.remove(fname);

}

void testCreateFile()
{
   const string fname("foobar");
   FileSystem fs;
   try {
      fs.remove(fname);
   }
   catch (...) {
   }

   bool ok = fs.createFile(fname);
   assert(ok);
   ok = fs.createFile(fname);
   assert(!ok);
   fs.remove(fname);
}

void testRelativePath()
{
   assertRelativePath("/a/b/c", "/a/b/c/d", "d");
   assertRelativePath("/a/b/c", "/a/b/d", "../d");
   assertRelativePath("/a/b/c", "/a/b/d/", "../d/");
   assertRelativePath("/w/x/y/z", "/z", "../../../../z");
}

void testListEntries()
{
   FileSystem fs;
   vector< string> entries;
   int n = fs.listEntries(".", entries);
   cerr << "There are " << n << " entries" << endl;
}
void testPermissions()
{
   FileSystem fs;
   string path;
   if (fs.exists("/usr/bin/cat")) {
      path = "/usr/bin/cat";
   }
   if (fs.exists("/bin/cat")) {
      path = "/bin/cat";
   }
   if (path.empty()) {
      cerr << "Failed to locate 'cat' program; test is inconclusive";
      return;
   }
   assert(fs.hasPermissions(path,FileSystem::READ));
   assert(fs.hasPermissions(path,FileSystem::EXECUTE));
   assert(fs.hasPermissions(path,FileSystem::READ|FileSystem::EXECUTE));
}

int main()
{
   try {
      test1();
      test2();
      testMkdirs();
      testDirname();
      testBasename();
      testSetTime();
      testFileTimeAccuracy();
      testCreateFile();
      testRelativePath();
      testListEntries();
      testPermissions();
   }
   catch (const exception& e) {
      cerr << "Exception caught " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception caught " << endl;
      return 1;
   }
   return 0;
}
