#include <canopy/fs/TempFile.h>
#include <cassert>
#include <iostream>

using namespace std;
using namespace canopy;
using namespace canopy::fs;

void test_makeTempFile()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   FileSystem fs;
   string name;

   {
      TempFile f;
      name = f.path();
      assert(fs.exists(name,false));
   }
   assert(!fs.exists(name,false));
}

void test_fixTempFile()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   FileSystem fs;
   string name;

   {
      TempFile f;
      name = f.path();
      f.fix();
      assert(fs.exists(name,false));
   }
   assert(fs.exists(name,false));
   fs.remove(name);
   assert(!fs.exists(name,false));
}

void test_renameTempFile()
{
   ::std::cerr << __FUNCTION__ << ::std::endl;
   FileSystem fs;
   string name;
   string newName;
   {
      TempFile f;
      name = f.path();
      ::std::cerr << "Dirname : " << fs.directoryName(name) << ::std::endl;
      newName = fs.normalizePath(name+"/../foo");
      bool ok = f.rename(newName);
      assert(ok);
      assert(!fs.exists(name,false));
      assert(fs.exists(newName,false));
   }
   assert(fs.exists(newName,false));
   fs.remove(newName);
}


int main()
{
   try {
      test_makeTempFile();
      test_fixTempFile();
      test_renameTempFile();
   }
   catch (const exception& e) {
      cerr << "Exception caught " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception caught " << endl;
      return 1;
   }
   return 0;
}
