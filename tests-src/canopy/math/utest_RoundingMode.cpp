#include <iostream>
#include <cassert>
#include <canopy/math/RoundingMode.h>
#include <canopy/canopy.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

void utest_test1()
{
  RoundingMode rnd(RoundingMode::NEAREST);
  assert(RoundingMode::testFPU(RoundingMode::NEAREST));
  rnd.setRoundingMode(RoundingMode::UP);
  assert(RoundingMode::testFPU(RoundingMode::UP));
  rnd.setRoundingMode(RoundingMode::DOWN);
  assert(RoundingMode::testFPU(RoundingMode::DOWN));
  rnd.setRoundingMode(RoundingMode::ZERO);
  assert(RoundingMode::testFPU(RoundingMode::ZERO));
}

int main()
{
  try {
      utest_test1();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
