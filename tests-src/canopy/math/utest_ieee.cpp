#include <iostream>
#include <cassert>
#include <sstream>

#include <canopy/canopy.h>
#include <canopy/math/ieee.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

void utest_test1()
{
  assert(precision<double>()==53);
  assert(precision<float>()==24);
  assert(precision<long double>()==0); // unknown precision
}

void utest_test2()
{
  double doubleEPS = eps(double(1.0));
  double floatEPS  = eps(float(1.0));

  ::std::cerr << "double eps: " << doubleEPS << endl
	      << "float eps: " << floatEPS << endl;
}

void utest_test3()
{
  testAndClearErrors();

  ::std::istringstream in("1.0 0.0");
  double x,y;
  in >> x >> y;
  
  ::std::cerr << "x / y" << (x/y) << ::std::endl;
  
  assert(testAndClearErrors()!=0 && "Expected a divide by 0 error");
  assert(testAndClearErrors()==0 && "Error should have been cleared");

}

void utest_test4()
{
  {
    RoundingMode rnd(RoundingMode::NEAREST);
    assert(isRoundToNearest() && "Rounding mode was not set to nearest float");
  }
}

void utest_test5()
{
}


int main()
{
  try {
      utest_test1();
      utest_test2();
      utest_test3();
      utest_test4();
      utest_test5();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
