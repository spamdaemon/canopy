#include <iostream>
#include <cassert>
#include <sstream>

#include <canopy/canopy.h>
#include <canopy/math/exact.h>
#include <canopy/math/RoundingMode.h>
#include <canopy/math/Precision.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

static double parseDouble (const char* x)
{
  istringstream in(x);
  double xx;
  in >> xx;
  return xx;
}

void utest_1()
{
  Precision pmode(Precision::DOUBLE);
  RoundingMode rmode(RoundingMode::NEAREST);
  testAndClearErrors();

  double value,error;

  twoSum(parseDouble("1"),parseDouble("3"),value,error);
  assert(value==4.0);
  assert(error==0.0);

  twoSum(parseDouble("3"),parseDouble("1"),value,error);
  assert(value==4.0);
  assert(error==0.0);

}

void utest_2()
{
  Precision pmode(Precision::DOUBLE);
  RoundingMode rmode(RoundingMode::NEAREST);
  testAndClearErrors();

  double value,error;
  fastTwoSum(parseDouble("3"),parseDouble("1"),value,error);
  assert(value==4.0);
  assert(error==0.0);
}

void utest_3()
{
  Precision pmode(Precision::DOUBLE);
  RoundingMode rmode(RoundingMode::NEAREST);
  double hi,lo;
  testAndClearErrors();
  split(parseDouble("5"),hi,lo);
  assert(hi==5);
  assert(lo==0);
}

void utest_4()
{
  Precision pmode(Precision::DOUBLE);
  RoundingMode rmode(RoundingMode::NEAREST);
  double value,error;
  testAndClearErrors();
  twoProduct(parseDouble("5"),parseDouble("9"),value,error);
  assert(value==45);
  assert(error==0);
}

void utest_5()
{
  Precision pmode(Precision::DOUBLE);
  RoundingMode rmode(RoundingMode::NEAREST);
  double res[4];
  scaleSum(parseDouble("5"),parseDouble("2"),parseDouble("7"),res);
  double value,error=0;
  arraySum(4,res,value,error);
  ::std::cerr << "Error bounds " << error << ::std::endl;
  assert(value==45);
}


void utest_6()
{
  testAndClearErrors();
  {
    double a[9];

    a[0] = 0; a[1] = 0; a[2] = 1;
    a[3] = 1; a[4] = 0; a[5] = 1;
    a[6] = 0; a[7] = 1; a[8] = 1;
    
    double value,error;
    
    int ok = determinant3x3(a,value,error);
    assert(ok==0);
    assert(value==1);
    
    int s;
    ok = determinantSign3x3(a,s);
    assert(ok==0);
    assert(s>0);
  }

  {
    double a[9];

    a[0] = 0; a[1] = 1; a[2] = 1;
    a[3] = 1; a[4] = 0; a[5] = 1;
    a[6] = 0; a[7] = 0; a[8] = 1;
    
    double value,error;
    
    int ok = determinant3x3(a,value,error);
    assert(ok==0);
    assert(value==-1);

    int s;
    ok = determinantSign3x3(a,s);
    assert(ok==0);
    assert(s<0);
  }

}

void utest_7()
{
  Precision pmode(Precision::DOUBLE);
  RoundingMode rmode(RoundingMode::NEAREST);
  double value,error;

  double p1[] = { 0X0.00079C08CA373P-1022, -0X1.6B5708C076FD2P+9 };
  double p2[] = { -0X1.CBF290E1B331CP+9, 0X1.02B827542201AP+9  };

  testAndClearErrors();
 // generates an underflow error
   twoProduct(p1[0],p2[1],value,error);
  assert(testErrors!=0 && "Expected code to generate an underflow; is this not a 64bit FPU?");
  testAndClearErrors();
}


int main()
 {
  try {
      utest_7();
      utest_1();
      utest_2();
      utest_3();
      utest_4();
      utest_5();
      utest_6();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
