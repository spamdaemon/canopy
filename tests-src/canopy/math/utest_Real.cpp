#include <iostream>
#include <cassert>
#include <sstream>

#include <canopy/canopy.h>
#include <canopy/math/Real.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

static double parseDouble (const char* x)
{
  istringstream in(x);
  double xx;
  in >> xx;
  return xx;
}

void utest_1()
{
  double p1[] = { 0X0.00079C08CA373P-1022, -0X1.6B5708C076FD2P+9 };
  double p2[] = { -0X1.CBF290E1B331CP+9, 0X1.02B827542201AP+9  };

  Real r1(p1[0]);
  Real r2(p2[1]);

  Real p = r1*r2;
  Real diff = p - Real(p1[0]*p2[1]);
  
  ::std::cerr << r1.toDouble() << " * " << r2.toDouble() << " = " << p.toDouble() << ", error = " << diff.toDouble() << ::std::endl;

}

void utest_2()
{
  double p1[] = { 0X0.00079C08CA373P-1022, -0X1.6B5708C076FD2P+9 };
  double p2[] = { -0X1.CBF290E1B331CP+9, 0X1.02B827542201AP+9  };

  Real r1(p1[0]);
  Real r2(p2[1]);

  Real p = 0;
  p += r1;
  p+= r2;

  Real diff = p - Real(p1[0]+p2[1]);

   ::std::cerr << r1.toDouble() << " + " << r2.toDouble() << " = " << p.toDouble() << ", error = " << diff.toDouble() << ::std::endl;
 
}

void utest_3()
{
  double p1[] = { 0X0.00079C08CA373P-1022, -0X1.6B5708C076FD2P+9 };
  double p2[] = { -0X1.CBF290E1B331CP+9, 0X1.02B827542201AP+9  };

  Real r1(p1[0]);
  Real r2(p2[1]);

  Real p = 0;
  p += r1*r2;

  Real diff = p - Real(p1[0]*p2[1]);

   ::std::cerr << r1.toDouble() << " * " << r2.toDouble() << " = " << p.toDouble() << ", error = " << diff.toDouble() << ::std::endl;
 
}

void utest_4()
{
  Real sum;
  
  const Real p1x = Real(1.0);
  const Real p1y = Real(2.0);
  const Real p2x = Real(3.0);
  const Real p2y = Real(4.0);
  const Real p3x = Real(5.0);
  const Real p3y = Real(6.0);
  
  sum += p1x*p2y;
  sum += p1y*p3x;
  sum += p2x*p3y;
  
  sum -= p3x*p2y;
  sum -= p3y*p1x;
  sum -= p2x*p1y;
  

}

int main()
 {
  try {
      utest_1();
      utest_2();
      utest_3();
      utest_4();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
