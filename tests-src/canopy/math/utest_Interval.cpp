#include <iostream>
#include <canopy/math/Interval.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

#define VALIDATE_INTERVAL(X,Y) assert(X.lowerBound()==Y.lowerBound()&&X.upperBound()==Y.upperBound())

static const Double  MAX_DOUBLE (INT64_C(0x001fffffffffffff));

void utest_1()
{
  Interval one(1);
  assert((one-one)== Interval::zero());
  assert(one*one == one);
  assert(one+Interval::zero() == one);
  assert(one*Interval::zero() == Interval::zero());
  assert(Interval::zero() < one);
  assert(one >Interval::zero());
  assert(Interval::zero() <= one);
  assert(Interval::zero() <= Interval::zero());
  assert(one >= Interval::zero());
  assert(Interval::zero() >= Interval::zero());
}

// test addition and subtraction
void utest_2()
{
  assert (Interval::INF == ::std::numeric_limits<double>::infinity());

  const Interval neg(-2,-1);
  const Interval zero(-1,1);
  const Interval pos(1,2);
  const Interval ZERO(0);
  const Interval INF(-Interval::INF,Interval::INF);

  // add a scalar to each interval
  {
    const Interval neg3(3+neg);
    const Interval zero3(zero+3);
    const Interval pos3(pos+3);
    const Interval ZERO3(3+ZERO);
    const Interval INF3(3+INF);

    VALIDATE_INTERVAL(neg3,Interval(1,2));
    VALIDATE_INTERVAL(zero3,Interval(2,4));
    VALIDATE_INTERVAL(pos3,Interval(4,5));
    VALIDATE_INTERVAL(ZERO3,Interval(3,3));
    VALIDATE_INTERVAL(INF3,INF);
  }

  // subtract a scalar to each interval
  {
    const Interval neg3(-3+neg);
    const Interval zero3(zero-3);
    const Interval pos3(pos-3);
    const Interval ZERO3(-3+ZERO);
    const Interval INF3(-3+INF);

    VALIDATE_INTERVAL(neg3,Interval(-5,-4));
    VALIDATE_INTERVAL(zero3,Interval(-4,-2));
    VALIDATE_INTERVAL(pos3,Interval(-2,-1));
    VALIDATE_INTERVAL(ZERO3,Interval(-3,-3));
    VALIDATE_INTERVAL(INF3,INF);
  }

  // add intervals to intervals
  {
    const Interval neg_neg(neg+neg);
    const Interval neg_zero(neg+zero);
    const Interval neg_pos(neg+pos);
    const Interval neg_ZERO(neg+ZERO);
    const Interval neg_INF(neg+INF);

    VALIDATE_INTERVAL(neg_neg,Interval(-4,-2));
    VALIDATE_INTERVAL(neg_zero,Interval(-3,0));
    VALIDATE_INTERVAL(neg_pos,Interval(-1,1));
    VALIDATE_INTERVAL(neg_ZERO,Interval(-2,-1));
    VALIDATE_INTERVAL(neg_INF,INF);

    const Interval zero_neg(zero+neg);
    const Interval zero_zero(zero+zero);
    const Interval zero_pos(zero+pos);
    const Interval zero_ZERO(zero+ZERO);
    const Interval zero_INF(zero+INF);

    VALIDATE_INTERVAL(zero_neg,Interval(-3,0));
    VALIDATE_INTERVAL(zero_zero,Interval(-2,2));
    VALIDATE_INTERVAL(zero_pos,Interval(0,3));
    VALIDATE_INTERVAL(zero_ZERO,Interval(-1,1));
    VALIDATE_INTERVAL(zero_INF,INF);

    const Interval pos_neg(pos+neg);
    const Interval pos_zero(pos+zero);
    const Interval pos_pos(pos+pos);
    const Interval pos_ZERO(pos+ZERO);
    const Interval pos_INF(pos+INF);

    VALIDATE_INTERVAL(pos_neg,Interval(-1,1));
    VALIDATE_INTERVAL(pos_zero,Interval(0,3));
    VALIDATE_INTERVAL(pos_pos,Interval(2,4));
    VALIDATE_INTERVAL(pos_ZERO,Interval(1,2));
    VALIDATE_INTERVAL(pos_INF,INF);

    const Interval ZERO_neg(ZERO+neg);
    const Interval ZERO_zero(ZERO+zero);
    const Interval ZERO_pos(ZERO+pos);
    const Interval ZERO_ZERO(ZERO+ZERO);
    const Interval ZERO_INF(zero+INF);

    VALIDATE_INTERVAL(ZERO_neg,Interval(-2,-1));
    VALIDATE_INTERVAL(ZERO_zero,Interval(-1,1));
    VALIDATE_INTERVAL(ZERO_pos,Interval(1,2));
    VALIDATE_INTERVAL(ZERO_ZERO,Interval(0,0));
    VALIDATE_INTERVAL(ZERO_INF,INF);

    VALIDATE_INTERVAL(INF+INF,INF);
  }
  
  // subtract intervals from intervals
  {
    const Interval neg_neg(neg-neg);
    const Interval neg_zero(neg-zero);
    const Interval neg_pos(neg-pos);
    const Interval neg_ZERO(neg-ZERO);

    VALIDATE_INTERVAL(neg_neg,Interval(-1,1));
    VALIDATE_INTERVAL(neg_zero,Interval(-3,0));
    VALIDATE_INTERVAL(neg_pos,Interval(-4,-2));
    VALIDATE_INTERVAL(neg_ZERO,Interval(-2,-1));

    const Interval zero_neg(zero-neg);
    const Interval zero_zero(zero-zero);
    const Interval zero_pos(zero-pos);
    const Interval zero_ZERO(zero-ZERO);

    VALIDATE_INTERVAL(zero_neg,Interval(0,3));
    VALIDATE_INTERVAL(zero_zero,Interval(-2,2));
    VALIDATE_INTERVAL(zero_pos,Interval(-3,0));
    VALIDATE_INTERVAL(zero_ZERO,Interval(-1,1));

    const Interval pos_neg(pos-neg);
    const Interval pos_zero(pos-zero);
    const Interval pos_pos(pos-pos);
    const Interval pos_ZERO(pos-ZERO);

    VALIDATE_INTERVAL(pos_neg,Interval(2,4));
    VALIDATE_INTERVAL(pos_zero,Interval(0,3));
    VALIDATE_INTERVAL(pos_pos,Interval(-1,1));
    VALIDATE_INTERVAL(pos_ZERO,Interval(1,2));

    const Interval ZERO_neg(ZERO-neg);
    const Interval ZERO_zero(ZERO-zero);
    const Interval ZERO_pos(ZERO-pos);
    const Interval ZERO_ZERO(ZERO-ZERO);

    VALIDATE_INTERVAL(ZERO_neg,Interval(1,2));
    VALIDATE_INTERVAL(ZERO_zero,Interval(-1,1));
    VALIDATE_INTERVAL(ZERO_pos,Interval(-2,-1));
    VALIDATE_INTERVAL(ZERO_ZERO,Interval(0,0));

    VALIDATE_INTERVAL(INF-INF,INF);
  }
}

// test multiplication
void utest_3()
{
  const Interval neg(-2,-1);
  const Interval zero(-1,1);
  const Interval pos(1,2);
  const Interval ZERO(0);
  const Interval INF(-Interval::INF,Interval::INF);

  // multiply each interval by a positive scalar 
  {
    const Interval neg3(3*neg);
    const Interval zero3(zero*3);
    const Interval pos3(pos*3);
    const Interval ZERO3(3*ZERO);
    const Interval INF3(3*INF);

    VALIDATE_INTERVAL(neg3,Interval(-6,-3));
    VALIDATE_INTERVAL(zero3,Interval(-3,3));
    VALIDATE_INTERVAL(pos3,Interval(3,6));
    VALIDATE_INTERVAL(ZERO3,Interval(0,0));
    VALIDATE_INTERVAL(INF3,INF);
  }

  // multiply each interval by a negative scalar 
  {
    const Interval neg3(-3*neg);
    const Interval zero3(zero*(-3));
    const Interval pos3(pos*(-3));
    const Interval ZERO3(-3*ZERO);
    const Interval INF3(-3*INF);

    VALIDATE_INTERVAL(neg3,Interval(3,6));
    VALIDATE_INTERVAL(zero3,Interval(-3,3));
    VALIDATE_INTERVAL(pos3,Interval(-6,-3));
    VALIDATE_INTERVAL(ZERO3,Interval(0,0));
    VALIDATE_INTERVAL(INF3,INF);
  }

  // multiply each interval by each interval
  {
    const Interval neg_neg(neg*neg);
    const Interval neg_zero(neg*zero);
    const Interval neg_pos(neg*pos);
    const Interval neg_ZERO(neg*ZERO);
    const Interval neg_INF(neg*INF);

    VALIDATE_INTERVAL(neg_neg,Interval(1,4));
    VALIDATE_INTERVAL(neg_zero,Interval(-2,2));
    VALIDATE_INTERVAL(neg_pos,Interval(-4,-1));
    VALIDATE_INTERVAL(neg_ZERO,Interval(0,0));
    VALIDATE_INTERVAL(neg_INF,INF);

    const Interval zero_neg(zero*neg);
    const Interval zero_zero(zero*zero);
    const Interval zero_pos(zero*pos);
    const Interval zero_ZERO(zero*ZERO);
    const Interval zero_INF(zero*INF);

    VALIDATE_INTERVAL(zero_neg,Interval(-2,2));
    VALIDATE_INTERVAL(zero_zero,Interval(-1,1));
    VALIDATE_INTERVAL(zero_pos,Interval(-2,2));
    VALIDATE_INTERVAL(zero_ZERO,Interval(0,0));
    VALIDATE_INTERVAL(zero_INF,INF);

    const Interval pos_neg(pos*neg);
    const Interval pos_zero(pos*zero);
    const Interval pos_pos(pos*pos);
    const Interval pos_ZERO(pos*ZERO);
    const Interval pos_INF(pos*INF);

    VALIDATE_INTERVAL(pos_neg,Interval(-4,-1));
    VALIDATE_INTERVAL(pos_zero,Interval(-2,2));
    VALIDATE_INTERVAL(pos_pos,Interval(1,4));
    VALIDATE_INTERVAL(pos_ZERO,Interval(0,0));
    VALIDATE_INTERVAL(pos_INF,INF);

    const Interval ZERO_neg(ZERO*neg);
    const Interval ZERO_zero(ZERO*zero);
    const Interval ZERO_pos(ZERO*pos);
    const Interval ZERO_ZERO(ZERO*ZERO);
    const Interval ZERO_INF(ZERO*INF);

    VALIDATE_INTERVAL(ZERO_neg,ZERO);
    VALIDATE_INTERVAL(ZERO_zero,ZERO);
    VALIDATE_INTERVAL(ZERO_pos,ZERO);
    VALIDATE_INTERVAL(ZERO_ZERO,ZERO);
    VALIDATE_INTERVAL(ZERO_INF,ZERO);
  }

}

// test division 
void utest_4()
{
  const Interval neg(-2,-1);
  const Interval zero(-1,1);
  const Interval pos(1,2);
  const Interval ZERO(0);
  const Interval INF(-Interval::INF,Interval::INF);

  // compute the inverse of the specified interval
  {
    const Interval invNeg = neg.inv();
    const Interval invZero = zero.inv();
    const Interval invPos = pos.inv();
    const Interval invZERO = ZERO.inv();
    const Interval invINF = INF.inv();

    VALIDATE_INTERVAL(invNeg,Interval(-1,-0.5));
    VALIDATE_INTERVAL(invZero,Interval(-Interval::INF,Interval::INF));
    VALIDATE_INTERVAL(invPos,Interval(0.5,1.0));
    assert(invZERO.isUndefined());
    VALIDATE_INTERVAL(invINF,INF);
  }

  // divide an interval by a scalar
  {
    Interval neg2 = neg / 2;
    Interval zero2 = zero / 2;
    Interval pos2 = pos /2 ;
    Interval ZERO2 = ZERO / 2;
    Interval INF2 = INF/2;
    VALIDATE_INTERVAL(neg2,Interval(-1,-0.5));
    VALIDATE_INTERVAL(zero2,Interval(-0.5,0.5));
    VALIDATE_INTERVAL(pos2,Interval(0.5,1.0));
    VALIDATE_INTERVAL(ZERO2,Interval(0));
    VALIDATE_INTERVAL(INF2,INF);

    neg2 = neg / -2;
    zero2 = zero / -2;
    pos2 = pos /-2 ;
    ZERO2 = ZERO / -2;
    INF2 = INF / -2;
    
    VALIDATE_INTERVAL(neg2,Interval(0.5,1));
    VALIDATE_INTERVAL(zero2,Interval(-0.5,0.5));
    VALIDATE_INTERVAL(pos2,Interval(-1.0,-0.5));
    VALIDATE_INTERVAL(ZERO2,Interval(0));
    VALIDATE_INTERVAL(INF2,INF);
  }
  
  // division by 0
  {
    Interval neg0 = neg / 0;
    Interval zero0 = zero / 0;
    Interval pos0 = pos /0 ;
    Interval ZERO0 = ZERO / 0;
    Interval INF0 = INF / 0;
    assert(neg0.isUndefined());
    assert(zero0.isUndefined());
    assert(pos0.isUndefined());
    assert(ZERO0.isUndefined());
    assert(INF0.isUndefined());
  }

  // divide an interval by another interval
  {
    Interval neg_neg(neg/neg);
    Interval neg_zero(neg/zero);
    Interval neg_pos(neg/pos);
    Interval neg_ZERO(neg/ZERO);
    Interval neg_INF(neg/INF);

    VALIDATE_INTERVAL(neg_neg,Interval(0.5,2));
    VALIDATE_INTERVAL(neg_zero,Interval(-Interval::INF,Interval::INF));
    VALIDATE_INTERVAL(neg_pos,Interval(-2,-0.5));
    assert(neg_ZERO.isUndefined());
    VALIDATE_INTERVAL(neg_INF,INF);

    Interval zero_neg(zero/neg);
    Interval zero_zero(zero/zero);
    Interval zero_pos(zero/pos);
    Interval zero_ZERO(zero/ZERO);
    Interval zero_INF(zero/INF);

    VALIDATE_INTERVAL(zero_neg,Interval(-1,1));
    VALIDATE_INTERVAL(zero_zero,Interval(-Interval::INF,Interval::INF));
    VALIDATE_INTERVAL(zero_pos,Interval(-1,1));
    assert(zero_ZERO.isUndefined());
    VALIDATE_INTERVAL(zero_INF,INF);

    Interval pos_neg(pos/neg);
    Interval pos_zero(pos/zero);
    Interval pos_pos(pos/pos);
    Interval pos_ZERO(pos/ZERO);
    Interval pos_INF(pos/INF);
    
    VALIDATE_INTERVAL(pos_neg,Interval(-2,-0.5));
    VALIDATE_INTERVAL(pos_zero,Interval(-Interval::INF,Interval::INF));
    VALIDATE_INTERVAL(pos_pos,Interval(0.5,2));
    assert(pos_ZERO.isUndefined());
    VALIDATE_INTERVAL(pos_INF,INF);

    Interval ZERO_neg(ZERO/neg);
    Interval ZERO_zero(ZERO/zero);
    Interval ZERO_pos(ZERO/pos);
    Interval ZERO_ZERO(ZERO/ZERO);
    Interval ZERO_INF(ZERO/INF);

    VALIDATE_INTERVAL(ZERO_neg,ZERO);
    VALIDATE_INTERVAL(ZERO_zero,ZERO);
    VALIDATE_INTERVAL(ZERO_pos,ZERO);
    assert(ZERO_ZERO.isUndefined());
    VALIDATE_INTERVAL(ZERO_INF,ZERO);
  }

}

// test square root
void utest_5()
{
  const Interval neg(-2,-1);
  const Interval zero(-1,1);
  const Interval pos(1,4);
  const Interval ZERO(0);

  assert(neg.sqrt().isUndefined());
  VALIDATE_INTERVAL(zero.sqrt(),Interval(0,1));
  VALIDATE_INTERVAL(pos.sqrt(),Interval(1,2));
  VALIDATE_INTERVAL(ZERO.sqrt(),Interval(0,0));
}

// test log
void utest_6()
{
  const Interval neg(-2,-1);
  const Interval zero(-1,1);
  const Interval pos(1,4);
  const Interval ZERO(0);

  assert(neg.log().isUndefined());
  assert(zero.log().lowerBound()==-Interval::INF);
  assert(!pos.log().isUndefined());
  assert(ZERO.log().isUndefined());
}

// test exp
void utest_7()
{
  const Interval neg(-2,-1);
  const Interval zero(-1,1);
  const Interval pos(1,4);
  const Interval ZERO(0);

  assert(!neg.exp().isUndefined());
  assert(!zero.exp().isUndefined());
  assert(!pos.exp().isUndefined());
  assert(!ZERO.exp().isUndefined());
}

void utest_8()
{
  {
    Interval x(MAX_DOUBLE);
    x += 1.0;
    assert(x.lowerBound()==x.upperBound());
    Interval y(x+1); // inexact calculation
    assert(y.upperBound()>y.lowerBound());
    Interval yExact(x+2); // exact calculation
    assert(yExact.lowerBound()==yExact.upperBound());
    assert(x.upperBound()==y.lowerBound());
    assert(yExact.lowerBound()==y.upperBound());
    
    y += 1;
    assert(x.upperBound()==y.lowerBound());
    assert((yExact+2).lowerBound()==y.upperBound());
  }

  {
    Interval x(0,1);
    Interval y(-1,1);
    Interval z(x/y);
    assert(z.lowerBound()==-Interval::INF);
    assert(z.upperBound()==Interval::INF);
    assert(z.compare(x)==0);
    
    z = z/y;
    assert(z.lowerBound()==-Interval::INF);
    assert(z.upperBound()==Interval::INF);
  }

  {
    Interval x(0,1);
    Interval y(0,1);
    Interval z(x/y);
    assert(z.lowerBound()==0.0);
    assert(z.upperBound()==Interval::INF);
    assert(z.compare(x)==0);
    
    z = z/y;
    assert(z.lowerBound()==0.0);
    assert(z.upperBound()==Interval::INF);
  }

  {
    Interval x(0,1);
    Interval y(-1,1);
    Interval z(x/y);
    assert(z.lowerBound()==-Interval::INF);
    assert(z.upperBound()==Interval::INF);
    assert(z.compare(x)==0);
    
    z = z + z;
    assert(z.lowerBound()==-Interval::INF);
    assert(z.upperBound()==Interval::INF);

    z += Interval(Interval::INF,Interval::INF);
    assert(z.isUndefined());
    z += Interval(-1,1);
    assert(z.isUndefined());
    z *= Interval(0,0);
    assert(z.isUndefined());
  }

   
}

void utest_9()
{
  Interval add = Interval::addDouble(MAX_DOUBLE,0.25);
  assert(add.lowerBound()<add.upperBound());
  Interval sub = Interval::subDouble(0.25,MAX_DOUBLE);
  assert(sub.lowerBound()<sub.upperBound());
  Interval mul = Interval::mulDouble(1.25,MAX_DOUBLE);
  assert(mul.lowerBound()<mul.upperBound());
  Interval div = Interval::divDouble(1.25,MAX_DOUBLE);
  assert(div.lowerBound()<div.upperBound());
  div = Interval::divDouble(MAX_DOUBLE,0);
  assert(div.isUndefined());
}

// boundary conditions
int main()
{
  utest_1(); // comparison
  utest_2(); // addition/subtraction
  utest_3(); // multiplication
  utest_4(); // division
  utest_5(); // sqrt
  utest_6(); // log
  utest_7(); // exp
  utest_8(); // boundary conditions
  utest_9(); // utest_ double add,sub,mul, and div
  return 0;
}
 
