#include <iostream>
#include <cassert>

#include <canopy/canopy.h>
#include <canopy/math/ComplexNumber.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

void utest_test1()
{
  ComplexNumber c;
  assert(c.r()==0);
  assert(c.i()==0);
}

void utest_test2()
{
  {
    ComplexNumber a(1);
    ComplexNumber b(1);
    ComplexNumber c=a*b;
    assert(c.r()==1);
    assert(c.i()==0);
  }
  {
    ComplexNumber a(0,1);
    ComplexNumber b(0,1);
    ComplexNumber c=a*b;
    assert(c.r()==-1);
    assert(c.i()==0);
  }

  {
    // multiplication by i is a rotation by 90 degrees
    ComplexNumber a(1);
    ComplexNumber b(0,1);
    ComplexNumber c=a*b;
    assert(c.r()==0);
    assert(c.i()==1);
  }
}

void utest_test3()
{
  {
    ComplexNumber a(1,2);
    ComplexNumber b(10,11);
    ComplexNumber c=a+b;
    assert(c.r()==11 && c.i()==13);
  }
  
  {
    ComplexNumber a(1,2);
    ComplexNumber b(10,11);
    ComplexNumber c=a-b;
    assert(c.r()==-9 && c.i()==-9);
  }
}

void utest_test4()
{
   {
    // multiplication by i is a rotation by 90 degrees
     ComplexNumber a(0,1);
    ComplexNumber b(0,1);
    ComplexNumber c=a/b;
    assert(c.r()==1);
    assert(c.i()==0);
  }
   
}

void utest_test5()
{
}


int main()
{
  try {
      utest_test1();
      utest_test2();
      utest_test3();
      utest_test4();
      utest_test5();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
