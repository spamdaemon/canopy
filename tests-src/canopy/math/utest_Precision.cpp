#include <iostream>
#include <cassert>
#include <canopy/math/Precision.h>
#include <canopy/canopy.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;

void utest_test1()
{
  {
  Precision rnd(Precision::DOUBLE);
  assert(Precision::testFPU(Precision::DOUBLE));
  assert(!Precision::testFPU(Precision::SINGLE));
  assert(!Precision::testFPU(Precision::EXTENDED));
  }
{
  Precision rnd(Precision::EXTENDED);
  assert(Precision::testFPU(Precision::EXTENDED));
  assert(!Precision::testFPU(Precision::SINGLE));
  assert(!Precision::testFPU(Precision::DOUBLE));
}
{
  Precision rnd(Precision::SINGLE);
  assert(Precision::testFPU(Precision::SINGLE));
  assert(!Precision::testFPU(Precision::DOUBLE));
  assert(!Precision::testFPU(Precision::EXTENDED));
}
}

int main()
{
  try {
      utest_test1();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
