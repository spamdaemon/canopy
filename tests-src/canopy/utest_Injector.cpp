#include <canopy/Injector.h>
#include <cassert>
#include <iostream>

using namespace ::canopy;

struct NoDefaultConstructor
{

      NoDefaultConstructor(int& v)
            : value(v)
      {
      }

      int value;
};

class Private
{
      friend class MyProvider;
      Private(int n)
            : value(n)
      {
      }
      int value;
};

struct MyProvider
{
      MyProvider()
            : _private(13)
      {
      }

      int get(InjectedType< int>)
      {
         return 3;
      }

      double get(InjectedType< double>)
      {
         return 2.5;
      }

      NoDefaultConstructor get(InjectedType< NoDefaultConstructor>)
      {
         int v = 17;
         return NoDefaultConstructor(v);
      }

      ::std::istream& get(InjectedType< ::std::istream>)
      {
         return ::std::cin;
      }

      const Private& get(InjectedType< Private>)
      {
         return _private;

      }

      template<typename T> T get(InjectedType< T>)
      {
         assert(false);
         return T();
      }

   public:
      Private _private;
};

struct MyClass
{

      double sum(int x, double y)
      {
         return x + y;
      }
};

void test_1()
{
   MyProvider provider;
   Injector< MyProvider> injector;

   ::std::function< double(int, double)> fn0 = [] (int x, double y) {return (double)(x*y);};

   auto fn = injector.injectFunction(::std::move(fn0));
   double product = fn(provider);
   ::std::cerr << "The product is " << product << ::std::endl;
   assert(product == 7.5);
}

void test_member()
{
   Injector< MyProvider> injector;
   MyProvider provider;
   ::std::function< double(int, double)> fn0 = ::std::bind(&MyClass::sum, MyClass(), ::std::placeholders::_1,
         ::std::placeholders::_2);
   auto fn = injector.injectFunction(::std::move(fn0));
   double sum = fn(provider);
   ::std::cerr << "The sum is " << sum << ::std::endl;
   assert(sum == 5.5);

}

void test_noDefaultConstructor()
{
   Injector< MyProvider> injector;
   MyProvider provider;
   int value = 0;
   ::std::function< void(NoDefaultConstructor)> fn0 = [&value] (NoDefaultConstructor v) {value=v.value;};
   auto fn = injector.injectFunction(::std::move(fn0));
   fn(provider);
   ::std::cout << value << ::std::endl;
   assert(value == 17);
}

void test_returnByRef()
{
   Injector< MyProvider> injector;
   MyProvider provider;
   ::std::istream* value = nullptr;
   ::std::function< void(::std::istream&)> fn0 = [&value] (::std::istream& v) {value=&v;};
   auto fn = injector.injectFunction(::std::move(fn0));
   fn(provider);
   assert(value == &::std::cin);
}

void test_returnByConstRef()
{
   Injector< MyProvider> injector;
   MyProvider provider;
   const Private* value = nullptr;
   ::std::function< void(const Private&)> fn0 = [&value] (const Private& v) {value=&v;};
   auto fn = injector.injectFunction(::std::move(fn0));
   fn(provider);
   assert(value == &provider._private);
}

int main()
{
   test_1();
   test_member();
   test_noDefaultConstructor();
   test_returnByRef();
   test_returnByConstRef();
}
