#include <canopy/net/socket/MulticastSocket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/time/Time.h>
#include <iostream>
#include <cassert>
#include <map>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::net;
using namespace ::canopy::net::socket;
using namespace ::canopy::time;

static Endpoint mcEndpoint()
{
   Endpoint xaddr("224.0.0.2", 12345);
   return xaddr.filter(DATAGRAM);
}

static Address mcAddress()
{
   return mcEndpoint().address();
}

void test_TTL()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   MulticastSocket mc(INET_4);
   mc.setMulticastTTL(247);
   assert(mc.getMulticastTTL() == 247);
}

void test_MulticastLoop()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   MulticastSocket mc(INET_4);
   assert(mc.isMulticastLoopEnabled());
   mc.setMulticastLoopEnabled(false);
   assert(!mc.isMulticastLoopEnabled());
}

void test_join()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   MulticastSocket mc(INET_4);
   mc.bind(mcAddress());
   mc.join(mcAddress());
}
void test_leave()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   MulticastSocket mc(INET_4);
   mc.bind(mcAddress());
   mc.join(mcAddress());
   mc.leave(mcAddress());
}
void test_send_receive()
{
   char buf[5];
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   auto receiver = MulticastSocket::joinGroup(mcAddress());
   auto sender = MulticastSocket::createConnectedSocket(mcAddress());
   buf[4] = '\0';
   sender->write("wxyz", 4, ::canopy::io::BLOCKING_TIMEOUT);
   int n = receiver->read(buf, sizeof(buf), ::canopy::io::BLOCKING_TIMEOUT);
   assert(n == 4);
   assert(::std::string(buf) == "wxyz");
}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {
      test_TTL();
      test_MulticastLoop();
      test_join();
      test_leave();
      test_send_receive();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
