#include <canopy/mt.h>
#include <canopy/net/socket/SslSocket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/mt/Thread.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::net;
using namespace ::canopy::net::socket;

static const char* SERVER = "www.openssl.org";
static const int SSL_PORT = 443;

SslSocket::Options createServerOptions()
{
   SslSocket::Options opts;

//   opts.setKeyFile("test-data/ssl/ssl.key");
//   opts.setCertificateFile("test-data/ssl/ssl.crt");
   opts.setOwnCertificateFile("test-data/ssl/server.pem");

   opts.setCertificateRepositoryFile("test-data/ssl/client.crt");
   return opts;
}

SslSocket::Options createClientOptions()
{
   SslSocket::Options opts;
   opts.setCertificateRepositoryFile("test-data/ssl/server.crt");
   opts.setCertificateRepositoryPath("/etc/ssl/certs");
   opts.setOwnCertificateFile("test-data/ssl/client.pem");
   return opts;
}

void write_socket(::canopy::net::socket::Socket& tcp, const ::std::string& data)
{
   size_t nWritten = 0;
   while (nWritten < data.size()) {
      int n = tcp.write(data.c_str() + nWritten, data.size() - nWritten, ::canopy::io::BLOCKING_TIMEOUT);
      if (n == 0) {
         ::std::cerr << "Nothing written" << ::std::endl;
         ::canopy::mt::Thread::suspend(::std::chrono::milliseconds(20));
      }
      ::std::cerr << "Wrote \"" << ::std::string(data, nWritten, n) << '"' << ::std::endl;
      nWritten += n;
   }
}

::std::string read_socket(::canopy::net::socket::Socket& tcp, int max = -1)
{
   char buf[128];
   int totalRead = 0;
   ::std::ostringstream sbuf;
   while (max < 0 || totalRead < max) {
      int n = tcp.read(buf, sizeof(buf), ::canopy::io::BLOCKING_TIMEOUT);
      if (n == 0) {
         ::std::cerr << "Nothing read" << ::std::endl;
         ::canopy::mt::Thread::suspend(::std::chrono::milliseconds(20));
         continue;
      }
      if (n < 0) {
         break;
      }
      sbuf.write(buf, n);
      totalRead += n;
      ::std::cout.write(buf, n);
      ::std::cout.flush();
   }
   ::std::cout << ::std::endl;
   return sbuf.str();
}

void test_Client()
{
   ::std::string nl("\r\n");
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr(SERVER, SSL_PORT, STREAM);
   assert(xaddr.begin() != xaddr.end());
   SslSocket::Options opts = createClientOptions();
   opts.setCertificateChainLength(5);
   SslSocket tcp(opts, INET_4);
   ::std::cerr << "Connecting" << ::std::endl;
   tcp.connect(xaddr.address());
   ::std::cerr << "Connection successful" << ::std::endl;

   ::std::string write("GET / HTTP/1.1" + nl);
   write += "Host: " + (SERVER + nl);
   write += nl + nl;
   write_socket(tcp, write);
   ::std::cerr << "Write successful" << ::std::endl;
   ::std::string result = read_socket(tcp, 512);
   ::std::cerr << "Read successful" << ::std::endl;
   assert(result.find("<html") != ::std::string::npos);
}

void test_NonBlocking_Client()
{
   ::std::string nl("\r\n");
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr(SERVER, SSL_PORT, STREAM);
   assert(xaddr.begin() != xaddr.end());
   SslSocket::Options opts = createClientOptions();
   opts.setCertificateChainLength(5);
   SslSocket tcp(opts, INET_4);
   tcp.setBlockingEnabled(false);
   ::std::cerr << "Connecting" << ::std::endl;
   tcp.connect(xaddr.address());
   ::std::cerr << "Connection successful" << ::std::endl;

   ::std::string write("GET / HTTP/1.1" + nl);
   write += "Host: " + (SERVER + nl);
   write += nl + nl;
   write_socket(tcp, write);
   ::std::cerr << "Write successful" << ::std::endl;
   ::std::string result = read_socket(tcp, 512);
   ::std::cerr << "Read successful" << ::std::endl;
   assert(result.find("<html") != ::std::string::npos);
}

static const char* HELLO_WORLD = "Hello, World!";
static void server_connect(Endpoint ep)
{
   ::std::string result;
   try {
      SslSocket::Options opts = createClientOptions();
//      opts.setCertificateValidationDisabled(true);
//      opts.setPeerCertificateOptional(true);
      SslSocket client(opts, INET_4);
      client.connect(ep.address());
      result = read_socket(client);
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "Failed on client : " << e.what() << ::std::endl;
   }
   ::std::cerr << "Server finished" << ::std::endl;
   assert(result == HELLO_WORLD);
}

void test_Server()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint ep("localhost", (int) 55453, STREAM, INET_4);
   SslSocket::Options opts = createServerOptions();
//   opts.setCertificateValidationDisabled(true);
//   opts.setPeerCertificateOptional(true);
   SslSocket server(opts, INET_4);
   server.setReuseAddressEnabled(true);
   server.bind(ep.address());
   server.listen(1);

   ::canopy::mt::Thread t = ::canopy::createThread("connector", server_connect, ep);
   Address remote;
   ::std::unique_ptr< ::canopy::net::socket::Socket> client = server.accept(remote);
   write_socket(*client, HELLO_WORLD);
   client->close();
   ::canopy::mt::Thread::join(t);
}
int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {
      test_NonBlocking_Client();
      test_Client();
      test_Server();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
