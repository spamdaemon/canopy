#include <canopy/net/socket/PosixSocket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/mt/Thread.h>
#include <canopy/time/Time.h>
#include <iostream>
#include <cassert>
#include <map>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::net;
using namespace ::canopy::net::socket;
using namespace ::canopy::mt;
using namespace ::canopy::time;

static const char* bcastAddr = "192.168.1.255";

void test1()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 60600, STREAM);
   assert(xaddr.begin() != xaddr.end());
   PosixSocket tcp(STREAM, xaddr.begin()->domain());
   tcp.setReuseAddressEnabled(true);
   tcp.bind(*xaddr.begin());
   tcp.listen(1);
}
void test4()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 60600, STREAM, INET_4);
   PosixSocket s(STREAM, INET_4);
   s.bind(xaddr.address());
   s.listen(1);
}

// test socket options
void test2()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   PosixSocket udp(DATAGRAM, INET_4);

   udp.setKeepAliveEnabled(true);
   udp.setKeepAliveEnabled(false);
   assert(!udp.isKeepAliveEnabled());
   udp.setKeepAliveEnabled(true);
   assert(udp.isKeepAliveEnabled());

   udp.setBroadcastEnabled(false);
   udp.setBroadcastEnabled(true);
   assert(udp.isBroadcastEnabled());
   udp.setBroadcastEnabled(false);
   assert(!udp.isBroadcastEnabled());

   udp.setReuseAddressEnabled(false);
   udp.setReuseAddressEnabled(true);
   assert(udp.isReuseAddressEnabled());
   udp.setReuseAddressEnabled(false);
   assert(!udp.isReuseAddressEnabled());

   udp.setSendBufferSize(64000);
   udp.setSendBufferSize(127000);
   cerr << "Send buffer size " << udp.getSendBufferSize() << endl;
   assert(127000 <= udp.getSendBufferSize());

   udp.setReceiveBufferSize(64000);
   udp.setReceiveBufferSize(128000);
   cerr << "Receive buffer size " << udp.getReceiveBufferSize() << endl;
   assert(128000 <= udp.getReceiveBufferSize());

   udp.enableLinger(10);
   udp.enableLinger(5);
   assert(udp.isLingerEnabled());
   assert(5 == udp.getLingerTime());
   udp.disableLinger();
   assert(!udp.isLingerEnabled());
}

// test non-blocking IO
void test3()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 60600, STREAM);
   assert(xaddr.begin() != xaddr.end());
   PosixSocket tcp(STREAM, xaddr.begin()->domain());
   tcp.setReuseAddressEnabled(true);
   tcp.bind(*xaddr.begin());
   tcp.listen(1);
   tcp.setBlockingEnabled(false);
   assert(!tcp.isBlockingEnabled());
   Address other;
   ::std::unique_ptr< ::canopy::net::socket::Socket> s = tcp.accept(other);
   assert(!s);
   tcp.setBlockingEnabled(true);
   assert(tcp.isBlockingEnabled());
}

// test non-blocking IO
void testPeek()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 43215, DATAGRAM, INET_4);
   PosixSocket receiver(DATAGRAM, INET_4);
   receiver.setReuseAddressEnabled(true);
   receiver.bind(*xaddr.begin());
   PosixSocket sender(DATAGRAM, INET_4);
   sender.connect(xaddr.address());

   const string hello("Hello, World");
   sender.write(hello.c_str(), hello.length() + 1, ::canopy::io::NON_BLOCKING_TIMEOUT);

   char r1Hello[100];
   receiver.peek(r1Hello, sizeof(r1Hello), ::canopy::io::NON_BLOCKING_TIMEOUT);
   assert(hello == r1Hello);
   receiver.read(r1Hello, sizeof(r1Hello), ::canopy::io::NON_BLOCKING_TIMEOUT);
   assert(hello == r1Hello);
}

void testDatagram()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 43215, DATAGRAM, INET_4);
   PosixSocket receiver(DATAGRAM, INET_4);
   receiver.setReuseAddressEnabled(true);
   receiver.bind(*xaddr.begin());
   PosixSocket sender(DATAGRAM, INET_4);
   sender.connect(xaddr.address());

   const string hello("Hello, World");
   sender.write(hello.c_str(), hello.length() + 1, ::canopy::io::NON_BLOCKING_TIMEOUT);

   char r1Hello[100];
   receiver.read(r1Hello, sizeof(r1Hello), ::canopy::io::NON_BLOCKING_TIMEOUT);
   assert(hello == r1Hello);
}

void testDatagramWriteTo()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 43215, DATAGRAM, INET_4);
   PosixSocket receiver(DATAGRAM, INET_4);
   PosixSocket sender(DATAGRAM, INET_4);
   receiver.setReuseAddressEnabled(true);
   receiver.bind(*xaddr.begin());

   const string hello("Hello, World");
   sender.writeTo(hello.c_str(), hello.length() + 1, *xaddr.begin(), ::canopy::io::NON_BLOCKING_TIMEOUT);

   char r1Hello[100];
   Address otherSide;
   receiver.readFrom(r1Hello, sizeof(r1Hello), otherSide, ::canopy::io::NON_BLOCKING_TIMEOUT);
   assert(hello == r1Hello);
   cerr << "Other side " << otherSide.nodeName() << " at " << otherSide.serviceName() << endl;
}

void testBroadcast()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   PosixSocket recv1(DATAGRAM, INET_4);
   PosixSocket recv2(DATAGRAM, INET_4);
   PosixSocket sender(DATAGRAM, INET_4);

   try {
      recv1.setReuseAddressEnabled(true);
      recv2.setReuseAddressEnabled(true);
      cerr << "Binding 1" << endl;
      recv1.bind(*(Endpoint(bcastAddr, "43215", DATAGRAM, INET_4).begin()));
      cerr << "Binding 2" << endl;
      recv2.bind(*(Endpoint(bcastAddr, "43215", DATAGRAM, INET_4).begin()));
      cerr << "Enable broadcast on sender" << endl;
      sender.setBroadcastEnabled(true);
      cerr << "Connecting" << endl;
      sender.connect(*(Endpoint(bcastAddr, "43215", DATAGRAM, INET_4).begin()));
      cerr << "Sending" << endl;
   }
   catch (...) {
      cerr << "Cannot bind " << bcastAddr << "; try to discover the proper interface" << endl;
      return;
   }

   const string hello("Hello, World");
   UInt32 cnt = sender.write(hello.c_str(), hello.length() + 1, ::canopy::io::NON_BLOCKING_TIMEOUT);
   assert(cnt == hello.length() + 1);

   // suspend thread for .1 second
   Thread::suspend(INT64_C(10000000));

   char r1Hello[100], r2Hello[100];

   cerr << "Trying to read now recv1" << endl;
   recv1.read(r1Hello, sizeof(r1Hello), ::canopy::io::NON_BLOCKING_TIMEOUT);
   cerr << "Trying to read now recv2" << endl;
   recv2.read(r2Hello, sizeof(r2Hello), ::canopy::io::NON_BLOCKING_TIMEOUT);

   assert(hello == r1Hello);
   assert(hello == r2Hello);
}

void testNonBlockingRead()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 43215, DATAGRAM, INET_4);
   PosixSocket sock(DATAGRAM, INET_4);
   sock.setReuseAddressEnabled(true);
   sock.bind(*xaddr.begin());
   char x;
   assert(sock.read(&x, 1, ::canopy::io::NON_BLOCKING_TIMEOUT) == 0);
}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {
      testBroadcast();
      test1();
      test2();
      test3();
      test4();
      testPeek();
      testDatagram();
      testDatagramWriteTo();
      testNonBlockingRead();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
