#include <canopy/net/Endpoint.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::net;

static Address newAddress(const char* addr)
{
   Endpoint ep = Endpoint::createEndpoint(addr, "0");
   assert(ep.addressCount() > 0);
   return ep.address();
}

void testMulticastAddresses()
{

   assert(newAddress("224.0.0.0").isMulticastAddress());
   assert(newAddress("239.255.255.255").isMulticastAddress());
   assert(!newAddress("223.255.255.255").isMulticastAddress());
   assert(!newAddress("240.0.0.0").isMulticastAddress());
}

int main()
{
   try {
      testMulticastAddresses();
   }
   catch (const ::std::exception& e) {
      cerr << "Exception " << e.what() << endl;
      return 1;
   }

   return 0;
}
