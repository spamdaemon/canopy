#include <canopy/net/Socket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/mt/Thread.h>
#include <canopy/time/Time.h>
#include <iostream>
#include <cassert>
#include <map>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::net;
using namespace ::canopy::mt;
using namespace ::canopy::time;

void testSocketEquality()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   // test that socket equality is invariant under close()
   map< Socket, int> sockets;
   Socket s1(DATAGRAM, INET_4);
   Socket s2(DATAGRAM, INET_4);
   Socket s3(DATAGRAM, INET_4);

   sockets.insert(map< Socket, int>::value_type(s1, 1));
   sockets.insert(map< Socket, int>::value_type(s2, 2));
   sockets.insert(map< Socket, int>::value_type(s3, 3));

   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());

   s1.close();
   s2.close();
   s3.close();

   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());

}

void testConnectEndpoint()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Socket s = Socket::createConnectedSocket(Endpoint("google.com", "http").filter(STREAM));

   Address peer(s.getPeerAddress());
   cerr << "Connected to " << peer.nodeName() << " at " << peer.serviceName() << endl;

   try {
      Socket tmo = Socket::createConnectedSocket(Endpoint());
      assert(false);
   }
   catch (const exception& e) {
      cerr << __FUNCTION__ << ":" << e.what() << endl;
   }

}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {

      testSocketEquality();
      testConnectEndpoint();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
