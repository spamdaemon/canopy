#include <canopy/ByteOrder.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace canopy;

static bool isLittleEndian()
{
  return !isBigEndian();
}


void utest_convertInt64()
{
  {
    volatile UInt64 x = INT64_C(0x414c14c5d49de743);
    if (isLittleEndian()) {
      ::std::cerr << ::std::hex << fromNetworkByteOrder(x) << ::std::endl;
      assert(fromNetworkByteOrder(x)==INT64_C(0x43e79dd4c5144c41));
    }
    else {
      assert(fromNetworkByteOrder(x)==INT64_C(0x414c14c5d49de743));
    }
  }

  {
    volatile UInt64 x = INT64_C(0x414ab3f08b2a5b33);
    if (isLittleEndian()) {
      ::std::cerr << ::std::hex << fromNetworkByteOrder(x) << ::std::endl;
      assert(fromNetworkByteOrder(x)==INT64_C(0x335b2a8bf0b34a41));
    }
    else {
      assert(fromNetworkByteOrder(x)==INT64_C(0x414ab3f08b2a5b33));
    }
  }

  {
    volatile UInt64 x = INT64_C(0x1f85eb51b81e4140);
    if (isLittleEndian()) {
      ::std::cerr << ::std::hex << fromNetworkByteOrder(x) << ::std::endl;
      assert(fromNetworkByteOrder(x)==INT64_C(0x40411eb851eb851f));
    }
    else {
      assert(fromNetworkByteOrder(x)==INT64_C(0x1f85eb51b81e4140));
    }
  }

  {
    volatile UInt64 x = INT64_C(0x1122334455667788);
    
    if (isLittleEndian()) {
      ::std::cerr << ::std::hex << toNetworkByteOrder(x) << ::std::endl;
      assert(toNetworkByteOrder(x)==INT64_C(0x8877665544332211));
    }
    else {
      assert(toNetworkByteOrder(x)==INT64_C(0x1122334455667788));
    }
  }

  {
    union {
      char x[8];
      double y;
    }  z;
    z.x[0] = 0x88;
    z.x[1] = 0x77;
    z.x[2] = 0x66;
    z.x[3] = 0x55;
    z.x[4] = 0x44;
    z.x[5] = 0x33;
    z.x[6] = 0x22;
    z.x[7] = 0x11;
    
    if (isLittleEndian()) {
      ::std::cerr << ::std::hex << toNetworkByteOrder(z.y) << ::std::endl;
      if(reinterpret_type<UInt64>(toNetworkByteOrder(z.y))!=INT64_C(0x8877665544332211)) {
	abort();
      }
    }
    else {
      assert(reinterpret_type<UInt64>(toNetworkByteOrder(z.y))==INT64_C(0x1122334455667788));
    }
  }
}

void utest_convertDouble()
{
  {
    double x = 34.24;
    
    if (isLittleEndian()) {
      double y  = toNetworkByteOrder(x);
      ::std::cerr << ::std::hex << y << " vs. " << ::std::hex << fromNetworkByteOrder(y) << ::std::endl;
      assert(fromNetworkByteOrder(y) == x);
    }
  }

  {
    ::std::cerr.setf(::std::ios::fixed);
    ::std::cerr.precision(8);

    union {
      char x[8];
      double y;
    }  z;
    z.x[0] = 65;
    z.x[1] = 77;
    z.x[2] = 90;
    z.x[3] = 81;
    z.x[4] = 74;
    z.x[5] = -9;
    z.x[6] = 62;
    z.x[7] = -44;

    if (isLittleEndian()) {
      double tmp =  toNetworkByteOrder(z.y);
      ::std::cerr << tmp << ::std::endl;
      assert(::std::abs(tmp-3847330.58567033) < .0001);
    }
  }
}

int main()
{
  utest_convertInt64(); 
  utest_convertDouble();
  return 0;
}
