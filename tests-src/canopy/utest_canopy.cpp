#define CANOPY_ASSERT_ON_CHECKED_CAST 0

#include <canopy/canopy.h>
#include <string>
#include <cassert>
#include <iostream>

using namespace std;
using namespace canopy;

void testFileLocationMacro()
{
  cerr << "File location " << CANOPY_FILE_LOCATION << endl;
}

void testVersionAndDate()
{
  cerr << "Version    : " << ::canopy::VERSION << endl;
  cerr << "Build date : " << ::canopy::DATE << endl;
}

void testCheckedCast()
{
  int x = 0x10000;
  
  cerr << "X: " << checked_cast<unsigned short>(x-1) << endl << flush;
  try {
    cerr << "X: " << checked_cast<short>(x) << endl << flush;
    assert(false);
  }
  catch (...) {
  }
  cerr << "X: " << checked_cast<short>(x>>2) << endl << flush;
}


int main()
{
  testFileLocationMacro();
  testVersionAndDate();
  testCheckedCast();
  return 0;
}
