#include <canopy/Static.h>
#include <cassert>

using namespace canopy;

struct A {
};

static A* theInstance()
{
  static A* volatile _foo;
  static Static<A> _fooGuard(new A(),_foo);
  return _foo;
}


void testStatic()
{
  assert(theInstance()!=0);
}

class Foo {
  ~Foo() 
  { 
    assert(theInstance()==0);
  }

};

static Foo theFoo();

int main()
{
  testStatic();
  return 0;
}
