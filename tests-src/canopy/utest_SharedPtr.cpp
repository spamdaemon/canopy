#include <canopy/SharedPtr.h>
#include <cassert>
#include <cstdlib>
#include <iostream>

using namespace std;
using namespace canopy;

struct A
{
      virtual ~A()
      {
      }

      int x()
      {
         return 1;
      }

};

struct B : public A
{
      ~B()
      {
      }
};

struct C
{
      virtual ~C()
      {
      }
};

struct D : public C, public A
{
      ~D()
      {
      }
};

void test_constructors()
{
   SharedPtr< A> a0(make_shared< A>());
   SharedPtr< A> a1(SharedPtr< B>(make_shared< B>()));
   SharedPtr< A> a2(make_unique< B>());
   SharedPtr< A> a3(make_unique< B>());

   try {
      SharedPtr< A> x(make_shared< C>());
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
   try {
      SharedPtr< A> x(make_unique< C>());
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
}

void test_assignment()
{
   SharedPtr< A> a(make_shared< A>());

   a = SharedPtr< A>(make_shared< A>());
   a = SharedPtr< B>(make_shared< B>());
   a = make_shared< A>();
   a = make_shared< B>();
   a = make_unique< B>();

   try {
      a = make_shared< C>();
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
   try {
      a = make_unique< C>();
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {

   }
}

void test_operator()
{
   SharedPtr< A> a(make_shared< A>());
   assert(a->x() == 1);
}

void test_comparison()
{
   SharedPtr< A> a(make_shared< A>());
   SharedPtr< B> b0(make_shared< B>());
   SharedPtr< A> b1(b0);
   ::std::shared_ptr< B> b2(b1);

   assert(a != b0);
   assert(b1 == b0);
   assert(a < b0 || b0 < a);
   assert(b2 == b0.get());
}

void test_invalid_cast()
{
   SharedPtr< A> a(make_shared< A>());
   try {
      SharedPtr< B> b(a);
      assert(false);
   }
   catch (...) {

   }
   SharedPtr< B> b0(make_shared< B>());
   try {
      b0 = a;
      assert(false);
   }
   catch (...) {

   }

}

int main()
{
   test_constructors();
   test_assignment();
   test_comparison();
   test_operator();
   test_invalid_cast();
   return 0;
}
