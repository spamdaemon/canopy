#include <iostream>
#include <canopy/User.h>
#include <cassert>

using namespace canopy;

void test1()
{
  ::std::unique_ptr<User> user = User::currentUser();
  
  ::std::cerr << "User name " << user->name() << ::std::endl;
  ::std::cerr << "User home " << user->documentRoot() << ::std::endl;
  
  ::std::unique_ptr<User> cur = User::findUser(user->name());
  assert(cur.get()!=0);
  assert(cur->documentRoot()==user->documentRoot());
}
void test2()
{
  ::std::unique_ptr<User> user = User::findUser("rw45897dshv978234");
  assert(user.get()==0 && "Should not find a user");
}

int main()
{
  try {
    test1();
    test2();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
