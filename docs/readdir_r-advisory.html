<html><head><title>readdir_r considered harmful</title></head>
  <body>

    <h1><code>readdir_r</code> considered harmful</h1>

    <p>
      Issued by Ben Hutchings &lt;<a href="mailto:ben@decadentplace.org.uk">ben@decadentplace.org.uk</a>&gt;,
      2005-11-02.
    </p>

    <p>
      This is revision 4, which makes the following change:
    </p>
    <ul>
      <li>Changed size calculation to allow for <code>d_name</code> not being the last
      member of <code>struct dirent</code>.</li>
    </ul>
    <p>
      Thanks to Kevin Bracey of Broadcom for pointing out this
      possibility.
    </p>
    <p>
      Revision 3 made the following change:
    </p>
    <ul>
      <li>Corrected typo in netwib fixed version.<!--/li-->
    </li></ul>
    <p>
      Revision 2 made the following changes:
    </p>
    <ul>
      <li>Added note about lack of dirfd on HP-UX and Tru64.</li>
      <li>Amended example code to work around too-small definitions of
      NAME_MAX.</li>
      <li>Added mitigating factors for insight.</li>
    </ul>
    <p>
      Thanks to Dave Butenhof of HP for the information on HP-UX and Tru64.
    </p>

    <h2>Background</h2>

    <p>
      The POSIX <code><a href="http://www.opengroup.org/onlinepubs/009695399/functions/readdir.html">readdir_r</a></code>
      function is a thread-safe version of the <code>readdir</code>
      function used to read directory entries.  Whereas
      <code>readdir</code> returns a pointer to a system-allocated
      buffer and may use global state without mutual exclusion,
      <code>readdir_r</code> uses a user-supplied buffer and is
      guaranteed to be reentrant.  Its use is therefore preferable or
      even essential in portable multithreaded programs.
    </p>

    <h2>Problem Description</h2>

    <p>
      The length of the user-supplied buffer passed to
      <code>readdir_r</code> is implicit; it is assumed to be long
      enough to hold any directory entry read from the given directory
      stream.  The length of a directory entry obviously depends on
      the length of the name, and the maximum name length may vary
      between filesystems.  The standard means to determine the
      maximum name length within a directory is to call <code><a href="http://www.opengroup.org/onlinepubs/009695399/functions/fpathconf.html">pathconf</a>(</code><var>dir_name</var><code>,
      _PC_NAME_MAX)</code>.  This method unfortunately results in a
      race condition between the <code>opendir</code> and
      <code>pathconf</code> calls, which could in some cases be
      exploited to cause a buffer overflow.  For example, suppose a
      setuid program "<code>rd</code>" includes code like this:
    </p>

<blockquote><pre>#include &lt;dirent.h&gt;
#include &lt;unistd.h&gt;

int main(int argc, char ** argv)
{
    DIR * dir;
    long name_max;
    struct dirent * buf, * de;

    if ((dir = opendir(argv[1]))
        &amp;&amp; (name_max = pathconf(argv[1], _PC_NAME_MAX)) &gt; 0
        &amp;&amp; (buf = (struct dirent *)malloc(
                offsetof(struct dirent, d_name) + name_max + 1))
    {
        while (readdir_r(dir, buf, &amp;de) == 0 &amp;&amp; de)
        {
            /* process entry */
        }
    }
}</pre></blockquote>

    <p>Then an attacker could run:</p>

<blockquote><pre>ln -sf exploit link &amp;&amp; (rd link &amp;; ln -sf /fat link)</pre></blockquote>

    <p>
      where the "<code>exploit</code>" directory is on a filesystem
      that allows a maximum of 255 bytes in a name whereas the
      "<code>/fat</code>" directory is the root of a FAT filesystem
      that allows a maximum of 12 byes.
    </p>

    <p>
      Depending on the timing of operations, "<code>rd</code>" may
      open the "<code>exploit</code>" directory but allocate a buffer
      only long enough for names in the "<code>/fat</code>"
      directory. Then names of entries in the "<code>exploit</code>"
      directory may overflow the allocated buffer by up to 243 bytes.
      Depending on the heap allocation behaviour of the target
      program, it may be possible to construct a name that will
      overwrite sensitive data following the buffer.  If the target
      program uses <code>alloca</code> or a variable length array to create the
      buffer, a classic stack overflow exploit is possible.
    </p>

    <p>
      A similar attack could be mounted on a daemon that reads
      user-controllable directories, for example a web server.
    </p>

    <p>
      Attacks are easier where a program assumes that all directories
      will have the same or smaller maximum name length than, for
      instance, its initial current directory.
    </p>

    <h2>Impact</h2>

    <p>
      This depends greatly on how an application uses
      <code>readdir_r</code> and on the configuration of the host
      system.  At the worst, a user with limited access to the local
      filesystem could cause a privileged process to execute arbitrary
      code.  However there are no known exploits.
    </p>

    <h2>Mitigation</h2>

    <p>
      Many systems don't have any variation in maximum name lengths
      among mounted and user-mountable filesystems.
    </p>

    <p>
      Directory entry buffers for <code>readdir_r</code> are usually
      allocated on the heap, and it is relatively hard to inject code
      into a process through a heap buffer overflow, though
      denial-of-service may be more easily achievable.
    </p>

    <p>
      Many programmers that use <code>readdir_r</code> erroneously
      calculate the buffer size as <code>sizeof(struct dirent) +
      pathconf(</code><var>dir_name</var><code>, _PC_NAME_MAX) +
      1</code> or similarly.  On Linux (with glibc) and most versions
      of Unix, <code>struct dirent</code> is large enough to hold
      maximum-length names from most filesystems, so this is safe
      (though wasteful).  This is not true of Solaris and BeOS, where
      the <code>d_name</code> member is an array of length 1.
    </p>

    <h2>Affected software</h2>

    <p>
      The following software appears to be exploitable when compiled
      for a system that defines <code>struct dirent</code> with a
      short <code>d_name</code> array, such as Solaris or BeOS:
    </p>

    <ul>
      <li>
	gcj (all versions to date)
	<p>
	  The run-time library functions
	  <code>java.io.File.list</code> and
	  <code>java.io.File.listFiles</code> call a private function
	  written in C++ that calls <code>readdir_r</code> using a
	  stack buffer and has a race condition as described above.
	</p>
      </li>
      <li>
	KDE (versions 3.3.0 to 3.3.2 inclusive; not present in version 3.4.0)
	<p>
	  The library function
	  <code>KURLCompletion::listDirectories</code>, used for
	  interactive URL completion, may start a thread that calls
	  <code>readdir_r</code> using a stack buffer of type
	  <code>struct dirent</code> (no extra bytes).  This behaviour
	  can be disabled by defining the environment variable
	  <code>KURLCOMPLETION_LOCAL_KIO</code>.
	</p>
      </li>
      <li>
	libwww (at least versions 3.1 to 5.3.2 inclusive; not yet fixed)
	<p>
	  The library functions <code>HTMulti</code>,
	  <code>HTBrowseDirectory</code> (version 3.1) and
	  <code>HTLoadFile</code> (version 4.0 onwards, when called
	  for a directory) indirectly call <code>readdir_r</code>
	  using a stack buffer of type <code>struct dirent</code> (no
	  extra bytes).  These functions are used in the process of
	  loading <code>file:</code> URLs.
	</p>
      </li>
      <li>
	Rudiments library (versions 0.27 to 0.28.2 inclusive; not yet fixed)
	<p>
	  The library function <code>directory::getChildName</code>
	  calls <code>readdir_r</code> using a stack buffer of type
	  <code>struct dirent</code> (no extra bytes).
	</p>
      </li>
      <li>
	teTeX (versions 1.0 to 2.0 inclusive; not present in version 3.0)
	<p>
	  The <code>xdvi</code> program included in these versions of
	  teTeX use libwww to read resources specified by URLs.
	</p>
      </li>
      <li>
	xmail (at least versions 1.0 to 1.21 inclusive; fixed in version 1.22)
	<p>
	  Uses <code>readdir_r</code> with variously allocated buffers
	  of type <code>struct dirent</code> (no extra bytes) when
	  listing mail directories.
	</p>
      </li>
    </ul>

    <p>The following software may also be exploitable:</p>

    <ul>
      <li>
	bfbtester (versions 2.0 and 2.0.1; not fixed)
	<p>
	  Uses <code>readdir_r</code> with a stack buffer of size
	  <code>struct dirent</code> (no extra bytes) to list the
	  contents of <code>/tmp</code> (or a specified temporary
	  directory) and directories in <code>$PATH</code>.  (Oh, the
	  irony.)
	</p>
      </li>
      <li>
	insight (if run using an exploitable version of Tcl)
	<p>
	  Uses Tcl, but includes its own copy which has never been
	  one of the vulnerable versions.
	</p>
      </li>
      <li>
	ncftp (at least versions 3.1.8 and 3.1.9, but not version 2.4.3;
	not fixed)
	<p>
	  Uses <code>readdir_r</code> with a heap buffer with
	  <code>min(pathconf(gLogfileName, _PC_NAME_MAX), 512) +
	  8</code> extra bytes (where <code>gLogFileName</code> is the
	  path to the log file).
	</p>
      </li>
      <li>
	netwib (versions 5.1.0 to 5.30.0 inclusive; fixed in version 5.31.0)
	<p>
	  Uses <code>readdir_r</code> with a heap buffer with extra
	  bytes: if <code>pathconf</code> is available,
	  <code>pathconf("/", _PC_NAME_MAX)+1</code>; otherwise, if
	  <code>NAME_MAX</code> is available, <code>NAME_MAX+1</code>;
	  otherwise 256.
	</p>
      </li>
      <li>
	OpenOffice.org (at least version 1.1.3)
	<p>
	  The code that enumerates fonts and plugins in the
	  appropriate directories uses a stack buffer of type
	  <code>long[sizeof(struct dirent) + _PC_NAME_MAX + 1]</code>.
	  I can only assume this is the result of a programmer cutting
	  his crack with aluminium filings.
	</p>
      </li>
      <li>
	Pike (versions 0.4pl8 to 7.4.327, 7.6.0 to 7.6.35, 7.7.0 to 7.7.21,
        all inclusive; fixed in versions 7.4.328, 7.6.36 and 7.7.22)
	<p>
	  Uses <code>readdir_r</code> with a heap buffer with
	  <code>max(pathconf(path, _PC_NAME_MAX), 1024) + 1</code> or
	  <code>NAME_MAX + 1025</code>, or 2049 extra bytes, depending
	  on which of these functions and macros are available.  In
	  addition to the race condition described above, there is a
	  second race condition in the evaluation of the greater of
	  <code>pathconf(...)</code> or 1024.
	</p>
      </li>
      <li>
	reprepro
	<p>
	  Uses <code>readdir_r</code> with a stack buffer of type
	  <code>struct dirent</code> (no extra bytes).  (Also misuses
	  <code>errno</code> following the call.)
	</p>
      </li>
      <li>
	Roxen (versions 1.1.1a2 to 4.0.402 inclusive; fixed in version
	4.0.403)
	<p>
	  Uses Pike.
	</p>
      </li>
      <li>
	saods9
	<p>
	  Uses Tcl.
	</p>
      </li>
      <li>
	Tcl (versions 8.4.2 to 8.5a2 inclusive; fixed in version 8.5a3)
	<p>
	  Uses <code>readdir_r</code> with a thread-specific heap
	  buffer padded to a size of at least <code>MAXNAMLEN+1</code>
	  bytes.  This can be a few bytes too short, though the heap
	  manager may pad the allocation sufficiently to make up for
	  this.
	</p>
      </li>
      <li>
	xgsmlib
	<p>
	  Uses stack buffer with no extra bytes when listing device
	  directories.
	</p>
      </li>
    </ul>

    <p>
      Some proprietary software may also be vulnerable, but I have no
      way of testing this.  I provided a draft of this advisory to Sun
      Security earlier this year on the basis that applications
      running on Solaris are most likely to be exploitable, but I have
      not received any substantive response.  A brief search through
      the OpenSolaris source code suggests that it may include
      exploitable applications, but apparently no-one at Sun could
      spare the time to investigate this.
    </p>

    <h2>Recommendations</h2>

    <p>
      Many POSIX systems implement the <code><a href="http://www.freebsd.org/cgi/man.cgi?query=dirfd">dirfd</a></code>
      function from BSD, which returns the file descriptor used by a
      directory stream.  However, current versions of HP-UX and Tru64
      do not implement this function.  This allows
      <code>pathconf(</code><var>dir_name</var><code>,
      _PC_NAME_MAX)</code> to be replaced by
      <code>fpathconf(dirfd(</code><var>dir</var><code>),
      _PC_NAME_MAX)</code>, eliminating the race condition.
    </p>

    <p>
      Some systems, including Solaris, implement the <code><a href="http://docs.sun.com/app/docs/doc/816-5168/6mbb3hr5r?a=view">fdopendir</a></code>
      function which creates a directory stream from a given file
      descriptor.  This allows the
      <code>opendir</code>,<code>pathconf</code> sequence to be
      replaced by
      <code>open</code>,<code>fpathconf</code>,<code>fdopendir</code>.
      However this function is much less widely available than
      <code>dirfd</code>.
    </p>

    <p>
      Programs using <code>readdir_r</code> may be able to use
      <code>readdir</code>.  According to POSIX the buffer
      <code>readdir</code> uses is not shared between directory
      streams.  However <code>readdir</code> is not guaranteed to be
      thread-safe and some implementations may use global state, so
      for portability the use of <code>readdir</code> in a
      multithreaded program should be controlled using a mutex.
    </p>

    <p>
      Suggested code for calculating the required buffer size for
      <code>readdir_r</code> follows:
    </p>

<blockquote><pre>#include &lt;sys/types.h&gt;
#include &lt;dirent.h&gt;
#include &lt;limits.h&gt;
#include &lt;stddef.h&gt;
#include &lt;unistd.h&gt;

/* Calculate the required buffer size (in bytes) for directory       *
 * entries read from the given directory handle.  Return -1 if this  *
 * this cannot be done.                                              *
 *                                                                   *
 * This code does not trust values of NAME_MAX that are less than    *
 * 255, since some systems (including at least HP-UX) incorrectly    *
 * define it to be a smaller value.                                  *
 *                                                                   *
 * If you use autoconf, include fpathconf and dirfd in your          *
 * AC_CHECK_FUNCS list.  Otherwise use some other method to detect   *
 * and use them where available.                                     */

size_t dirent_buf_size(DIR * dirp)
{
    long name_max;
    size_t name_end;
#   if defined(HAVE_FPATHCONF) &amp;&amp; defined(HAVE_DIRFD) \
       &amp;&amp; defined(_PC_NAME_MAX)
        name_max = fpathconf(dirfd(dirp), _PC_NAME_MAX);
        if (name_max == -1)
#           if defined(NAME_MAX)
                name_max = (NAME_MAX &gt; 255) ? NAME_MAX : 255;
#           else
                return (size_t)(-1);
#           endif
#   else
#       if defined(NAME_MAX)
            name_max = (NAME_MAX &gt; 255) ? NAME_MAX : 255;
#       else
#           error "buffer size for readdir_r cannot be determined"
#       endif
#   endif
    name_end = (size_t)offsetof(struct dirent, d_name) + name_max + 1;
    return (name_end &gt; sizeof(struct dirent)
            ? name_end : sizeof(struct dirent));
}</pre></blockquote>

<p>An example of how to use the above function:</p>

<blockquote><pre>#include &lt;errno.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int main(int argc, char ** argv)
{
    DIR * dirp;
    size_t size;
    struct dirent * buf, * ent;
    int error;

    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s path\n", argv[0]);
        return 2;
    }

    dirp = opendir(argv[1]);
    if (dirp == NULL)
    {
        perror("opendir");
        return 1;
    }
    size = dirent_buf_size(dirp);
    printf("size = %lu\n" "sizeof(struct dirent) = %lu\n",
           (unsigned long)size, (unsigned long)sizeof(struct dirent));
    if (size == -1)
    {
        perror("dirent_buf_size");
        return 1;
    }
    buf = (struct dirent *)malloc(size);
    if (buf == NULL)
    {
        perror("malloc");
        return 1;
    }
    while ((error = readdir_r(dirp, buf, &amp;ent)) == 0 &amp;&amp; ent != NULL)
        puts(ent-&gt;d_name);
    if (error)
    {
        errno = error;
        perror("readdir_r");
        return 1;
    }
    return 0;
}</pre></blockquote>

    <p>
      The Austin Group should amend POSIX and the SUS in one or more
      of the following ways:
    </p>

    <ol>
      <li>
	Standardise the <code>dirfd</code> function from BSD and
	recommend its use in determining the buffer size for
	<code>readdir_r</code>.
      </li>
      <li>
	Specify a new variant of <code>readdir</code> in which the
	buffer size is explicit and the function returns an error code
	if the buffer is too small.
      </li>
      <li>
	Specify that <code>NAME_MAX</code> must be defined as the
	length of the longest name that can be used on any filesystem.
	(This seems to be what many or most implementations attempt to
	do at present, although POSIX currently specifies otherwise.)
      </li>
    </ol>

  </body></html>