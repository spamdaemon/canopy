#ifndef _CANOPY_DESTRUCTOR_H
#define _CANOPY_DESTRUCTOR_H

#include <functional>

namespace canopy {
   /**
    * The destructor is object invokes when destructed.
    */
   class Destructor
   {
         Destructor() = delete;

         Destructor(const Destructor&) = delete;
         Destructor&operator=(const Destructor&) = delete;

         /**
          * Create a new destructor.
          * @param cb the destruction callback
          */
      public:
         inline Destructor(::std::function< void()> cb) noexcept
         : _destructor(::std::move(cb)) {}

            /** The destructor */
            public:
            ~Destructor() noexcept;

            /** The destructor function */
            private:
            ::std::function< void() > _destructor;
         };
      }
#endif
