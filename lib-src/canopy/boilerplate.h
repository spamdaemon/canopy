#ifndef _CANOPY_BOILERPLATE_H
#define _CANOPY_BOILERPLATE_H

#include <functional>
#include <iosfwd>

/**
 * The purpose of this file is to introduce some macros to define
 * and boilerplate functions, classes, etc.
 */

/**
 * Prevent copying. This simple declare the copy constructor and operator as deleted
 */
#define CANOPY_BOILERPLATE_PREVENT_COPYING(X) \
   X(const X&) = delete; \
   X& operator=(const X&) = delete


/**
 * Define a instantiation of a ::std::hash object for a given type X. The type
 * X is assumed to have a function of this signature:
 * size_t hash() const;
 */
#define CANOPY_BOILERPLATE_HASH(X) \
   namespace std {\
   template <> struct hash<X> {\
    inline size_t operator() (const X& x) const {\
      return x.hash();\
     }\
   };\
}

/**
 * This defines the boilerplate hash function to be used with CANOPY_BOILERPLATE_HASH.
 */
#define CANOPY_BOILERPLATE_DECLARE_HASH size_t hash() const

/**
 * Define a simple boilerplate hash function using an expression.
 */
#define CANOPY_BOILERPLATE_DEFINE_HASH(X,EXPR) size_t X::hash() const { return (EXPR); }


/**
 * Define a boilerplate operator<< for a type x. The type
 * X is assumed to have a function of this signature:
 * void print(::std::ostream&  out);
 */
#define CANOPY_BOILERPLATE_PRINT(X) \
   inline ::std::ostream& operator<<(::std::ostream& out, const X& x) {\
    x.print(out); return out;\
   }

/**
 * This defines the boilerplate print function to be used with CANOPY_BOILERPLATE_PRINT.
 */
#define CANOPY_BOILERPLATE_DECLARE_PRINT void print(::std::ostream& out) const;

/**
 * Define boiler plate print function outside of a class using a simple body
 */
#define CANOPY_BOILERPLATE_DEFINE_PRINT(X,BODY) void X::print(::std::ostream& out) const { BODY; }



#endif
