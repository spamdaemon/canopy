#include <canopy/canopy.h>
#include <canopy/time/Time.h>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <string>
#include <canopy/version.h>

namespace canopy {

   const char DATE[] = { __DATE__ };

   static void logMessage(int level, const char* message, int len) throws()
   {
      char buf[::canopy::time::Time::BUFFER_SIZE];
      size_t n = ::canopy::time::Time::now().format(buf, 4);
      ::std::cerr.write(buf, n) << " [canopy-" << level << "] ";
      if (len < 0) {
         ::std::cerr << message;
      }
      else {
         ::std::cerr.write(message, len);
      }
      ::std::cerr << ::std::endl << ::std::flush;
   }

   static Logger& getLogger()
   {
      static Logger _log = logMessage;
      return _log;
   }

   void abortProgram(int err, const char* file, int line) throws()
   {
      ::std::fprintf(stderr, "Internal error %d at %s:%d: aborting program\n", err, file, line);
      ::std::fflush(stderr);
      ::std::abort();
   }

   ::std::string getSystemError(int xerr, const char* msg)
   {
      ::std::string res;
      char tmp[128];
      char* m;
#if CANOPY_OS == CANOPY_SUNOS
      m = "";
#else
      m = strerror_r(xerr, tmp, sizeof(tmp));
#endif
      res.assign(m);
      if (msg) {
         res += " : ";
         res += msg;
      }
      return res;
   }

   bool printSystemError(int xerr, const char* msg) throws()
   {
      if (xerr == 0) {
         return false;
      }
      const ::std::string xmsg = getSystemError(xerr, msg);
      getLogger()(10, xmsg.c_str(), xmsg.length());
      return true;
   }

   void throwSystemError(int xerr, const char* msg) throws (::std::exception)
   {
      const ::std::string xmsg = getSystemError(xerr, msg);
      getLogger()(10, xmsg.c_str(), xmsg.length());
      throw ::std::runtime_error(xmsg);
   }

   ::std::string getSystemError(const ::std::string& msg)
   {
      return getSystemError(msg.c_str());
   }

   ::std::string getSystemError(int xerr, const ::std::string& msg)
   {
      return getSystemError(xerr, msg.c_str());
   }

   ::std::string getSystemError(const char* msg)
   {
      return getSystemError(errno, msg);
   }

   void throwSystemError(int xerr, const ::std::string& msg) throws (::std::exception)
   {
      throwSystemError(xerr, msg.c_str());
   }

   void throwSystemError(const ::std::string& msg) throws (::std::exception)
   {
      throwSystemError(errno, msg.c_str());
   }

   void throwSystemError(const char* msg) throws (::std::exception)
   {
      throwSystemError(errno, msg);
   }

   bool printSystemError(int xerr, const ::std::string& msg) throws()
   {
      return printSystemError(xerr, msg.c_str());
   }

   bool printSystemError(const ::std::string& msg) throws()
   {
      return printSystemError(errno, msg.c_str());
   }

   bool printSystemError(const char* msg) throws()
   {
      return printSystemError(errno, msg);
   }

   void printErrorMessage(const ::std::string& msg) throws()
   {
      getLogger()(5, msg.c_str(), msg.length());
   }

   void printErrorMessage(const char* msg, int len) throws()
   {
      getLogger()(5, msg, len);
   }

   void printDebugMessage(const ::std::string& msg) throws()
   {
      getLogger()(0, msg.c_str(), msg.length());
   }

   void printDebugMessage(const char* msg, int len) throws()
   {
      getLogger()(0, msg, len);
   }

   Logger setLogger(Logger logger) throws()
   {
      Logger& _log = getLogger();
      Logger old = _log;
      if (logger == nullptr) {
         _log = logMessage;
      }
      else {
         _log = logger;
      }
      return old;
   }

#if CANOPY_COMPILER != CANOPY_GCC
   size_t countOnes(::std::uint32_t x)
   {
      size_t n = 0;
      for (size_t i = 0; i < 32; ++i) {
         if ((x >> i & 1)) {
            ++n;
         }
      }
      return n;
   }
   size_t countOnes(::std::uint64_t x)
   {
      size_t n = 0;
      for (size_t i = 0; i < 64; ++i) {
         if ((x >> i & 1)) {
            ++n;
         }
      }
      return n;
   }
#endif

}
