#include <canopy/InterruptedException.h>
#include <memory>

namespace canopy {
  
   InterruptedException::InterruptedException(::std::string msg) throws()
     : ::std::runtime_error(::std::move(msg)) {}

   InterruptedException::InterruptedException() throws()
     : ::std::runtime_error("Interrupted") {}

}

