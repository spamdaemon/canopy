#include <canopy/SystemError.h>
#include <canopy/canopy.h>
#include <string>

namespace canopy {
  
  SystemError::SystemError() throws()
    : ::std::runtime_error(getSystemError("System Error")) 
  {}


  SystemError::SystemError(::std::string msg) throws()
    : ::std::runtime_error(getSystemError(msg))
  {}

  SystemError::~SystemError() throw()
  {}

}

