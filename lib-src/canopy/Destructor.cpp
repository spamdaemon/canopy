#include <canopy/Destructor.h>
#include <canopy/canopy.h>

namespace canopy {


   Destructor::~Destructor() noexcept
   {
      try {
         if (_destructor) {
            _destructor();
         }
      }
      catch (...) {
         ::canopy::printErrorMessage("Destructor threw exception");
      }
   }
}
