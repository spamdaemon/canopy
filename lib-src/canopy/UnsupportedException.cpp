#include <canopy/UnsupportedException.h>

namespace canopy {

   UnsupportedException::UnsupportedException(::std::string msg)
throws         ()
         : ::std::runtime_error(::std::move(msg)) {}

   UnsupportedException::UnsupportedException()
throws      ()
      : ::std::runtime_error("Unsupported") {}

   }

