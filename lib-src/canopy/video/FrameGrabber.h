#ifndef _CANOPY_VIDEO_FRAMEGRABBER_H
#define _CANOPY_VIDEO_FRAMEGRABBER_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_VIDEO_FORMAT_H
#include <canopy/video/Format.h>
#endif

#include <memory>
#include <iosfwd>
#include <functional>

namespace canopy {
   namespace video {
      class Format;

      /**
       * The FrameGrabber allows clients to access the raw video
       * data produced by some VideoSource.
       */
      class FrameGrabber
      {
            FrameGrabber(const FrameGrabber&);
            FrameGrabber&operator=(const FrameGrabber&);

            /** Details about a captured frame. */
         public:
            struct FrameInfo
            {
                  /** The default constructor */
               public:
                  FrameInfo()
                        : frameNo(0), x(0), y(0), w(0), h(0), format(Format::UNKNOWN), buf(0), bufSize(0)
                  {
                  }

                  /** the frame number (starts at 0) */
                  size_t frameNo;

                  /** @param x the left side of the frame within the videosource's available image frame */
                  size_t x;

                  /** @param y the right side of the frame within the videosource's available image frame */
                  size_t y;

                  /** @param w the width of the captured frame (in pixels) */
                  size_t w;

                  /** @param h the height of the captured frame (in pixels) */
                  size_t h;

                  /** @param enc the encoding of the captured frame */
                  Format format;

                  /** @param buf the video buffer itself */
                  const char* buf;

                  /** @param bufSize the size of the video buffer.
                   * @todo perhaps instead of bufSize use bytesPerLine?
                   */
                  size_t bufSize;
            };

            /**
             * A notification callback to indicate that an image has been captured.
             * It is possible to capture
             * a subimage of the actual image that the VideoSource produces, and so the
             * location of the captured frame is available. <br>
             * The size of the video buffer is provided for convenience but can also be
             * computed via the w, h, and encoding fields.
             * @param info about the frame
             * @return true or false if capturing should continue
             */
         public:
            typedef ::std::function< bool(const FrameInfo&)> FrameCallback;

            /** Default constructor */
         public:
            FrameGrabber() throws();

            /** Destructor */
         public:
            virtual ~FrameGrabber() throws() = 0;

            /**
             * Get the height of the images being produced.
             * @return the height in pixels of the images produced
             * @throws ::std::exception if the some kind of error occurred
             */
         public:
            virtual size_t imageHeight() const throws (::std::exception) = 0;

            /**
             * Get the width of the images being produced.
             * @return the width in pixels of the images produced
             * @throws ::std::exception if the some kind of error occurred
             */
         public:
            virtual size_t imageWidth() const throws (::std::exception) = 0;

            /**
             * Set the width and height for the image frames to be grabbed. The default
             * implementation simply returns false. This method may not be invoked
             * if frames are being captured.
             * @param width the width of the images (pixels)
             * @param height the height of the images (pixels)
             * @return true if this frame grabber supports the specified dimension
             * @throws ::std::exception if the some kind of error occurred
             */
         public:
            virtual bool setImageSize(size_t width, size_t height) throws (::std::exception);

            /**
             * Wait until a new frame is captured. A callback is invoked
             * for the captured frame.
             * @param cb a callback
             * @return the return value of the callback
             */
         public:
            virtual bool captureFrame(FrameCallback& cb) throws (::std::exception)= 0;

            /**
             * Capture frames until the callback returns false.
             * @param cb a callback
             * @return the number of frames captured
             */
         public:
            virtual size_t captureFrames(FrameCallback& cb) throws (::std::exception);
      };
   }
}
#endif
