#include <canopy/video/Format.h>

namespace canopy {
  namespace video {
    namespace {
      // these strings must match the enum 
      static const char* STRINGS[] = { 
	"RGB332",
	"RGB444",
	"RGB555",
	"RGB565",
	"RGB555X",
	"RGB565X",
	"BGR24",
	"RGB24",
	"BGR32",
	"RGB32",
	"GREY",
	"YVU410",
	"YVU420",
	"YUYV",
	"UYVY",
	"YUV422P",
	"YUV411P",
	"Y41P",
	"NV12",
	"NV21",
	"YUV410",
	"YUV420",
	"YYUV",
	"HI240",
	"SBGGR8",
	"MJPEG",
	"JPEG",
	"DV",
	"MPEG",
	"WNVA",
	"UNKNOWN"
      };
    }
    
    const char* Format::string() const throws()
    {
      // return unknown if not found
      return STRINGS[(int)_encoding];
    }

  }
}
