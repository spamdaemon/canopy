#ifndef _CANOPY_VIDEO_FORMAT_H
#define _CANOPY_VIDEO_FORMAT_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace canopy {
   namespace video {

      /** 
       * This class contains information about various video
       * formats.
       */
      class Format
      {
            /**
             * The various raw image encodings.
             */
         public:
            enum Encoding
            {
               RGB332, RGB444, RGB555, RGB565, RGB555X, RGB565X, BGR24, RGB24, BGR32, RGB32, GREY, YVU410,

               /**
                * All N Y values are sent first, followed by N/4 V values, followed by N/4 U values.
                * For any given pixel (i,j) in an image with w pixels per row, the offset into
                * the respective arrays are:<br>
                * <code>
                * y = address of first Y value<br>
                * v = y + N<br>
                * u = v + w/2;<br>
                *
                * Y = y[i*w + j]<br>
                * V = v[(i/2)*w/2 + j/2]<br>
                * U = u[(i/2)*w/2 + j/2]<br>
                * </code>
                */
               YVU420,

               /**
                * The Y-sample period is every pixel
                * The U sample period is every other pixel horizontally and each pixel vertically
                * The V sample period is every other pixel horizontally and each pixel vertically
                */
               YUYV, UYVY, YUV422P, YUV411P, Y41P,

               NV12, NV21,

               YUV410,

               /**
                * This is the same as YVU420, but with V and U reversed
                */
               YUV420, YYUV, HI240,

               SBGGR8,

               MJPEG, JPEG, DV, MPEG,

               WNVA,

               /** Unknown is used when the detailed format information is not available */
               UNKNOWN
            };

            /**
             * Create a format object
             * @param enc the encoding
             */
         public:
            inline Format(Encoding enc)
                  : _encoding(enc)
            {
            }

            /** Destructor */
         public:
            inline ~Format() throws()
            {
            }

            /**
             * Test if two formats are equal.
             * @param fmt a format
             * @return true if the two formats are the same
             */
         public:
            inline bool operator==(const Format& fmt) throws()
            {
               return _encoding == fmt._encoding;
            }

            /**
             * Get the encoding
             * @return the format encoding
             */
         public:
            inline Encoding encoding() const throws()
            {
               return _encoding;
            }

            /** Get the format for the string */
         public:
            const char* string() const throws();

            /** The encoding */
         private:
            Encoding _encoding;
      };

   }
}

#endif
