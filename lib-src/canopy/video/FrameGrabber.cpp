#include <canopy/video/FrameGrabber.h>

namespace canopy {
   namespace video {

      FrameGrabber::FrameGrabber() throws()
      {
      }
      FrameGrabber::~FrameGrabber() throws()
      {
      }

      size_t FrameGrabber::captureFrames(FrameCallback& cb) throws (::std::exception)
      {
         size_t nFrames = 0;
         while (captureFrame(cb)) {
            ++nFrames;
         }
         return nFrames;
      }

      bool FrameGrabber::setImageSize(size_t, size_t) throws (::std::exception)
      {
         return false;
      }

   }
}
