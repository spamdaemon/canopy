#ifndef _CANOPY_VIDEO_LINUXOS_V4L2SOURCE_H
#define _CANOPY_VIDEO_LINUXOS_V4L2SOURCE_H

#ifndef _CANOPY_VIDEO_LINUXOS_LINUXVIDEOSOURCE_H
#include <canopy/video/linuxos/LinuxVideoSource.h>
#endif

#if CANOPY_OS == CANOPY_LINUX

// the video 4 linux api
#include <sys/time.h>
#include <linux/videodev2.h>

#include <memory>

namespace canopy {
   namespace video {
      namespace linuxos {
         /**
          * The VideoSource provides access to a source
          * of video from a web-cam, a TV tuners or other
          * kinds of sources. It is the starting point
          * for interacting with the video.
          * <p>
          * Information about V4L2 can be found here:
          * <a href="http://v4l2spec.bytesex.org/spec/book1.htm">V4L2 API</a>/
          * or locally <a href="canopy/docs/v4l2.pdf">here</a>.
          * @todo The local pdf link is not working
          */
         class V4L2Source : public LinuxVideoSource
         {
               V4L2Source&operator=(const V4L2Source&);
               V4L2Source(const V4L2Source&);

               /**
                * Default constructor.
                * @param dev the video device
                */
            private:
               V4L2Source(int dev) throws();

               /**
                * The destructor.
                */
            public:
               ~V4L2Source() throws();

               /**
                * Open a video device. The src parameter may be 0,
                * in which case an attempt is made to locate a default source.
                * @param src a descriptor for the location of the source.
                * @return a video source or 0 if the device is not a V4L2 device.
                * @throws ::std::exception if the video source exists, but could not be opened
                */
            public:
               static ::std::unique_ptr< VideoSource> open(const char* src) throws (::std::exception);

               /**
                * Get a frame grabber
                * @return a frame grabber
                * @throws ::std::exception if this source does not support frame capture
                */
            public:
               FrameGrabber& frameGrabber() throws (::std::exception);

               /** The source device */
            private:
               int _device;

               /**
                * A frame grabber
                */
            private:
               ::std::unique_ptr< FrameGrabber> _frameGrabber;
         };
      }
   }
}
#endif
#endif
