#ifndef _CANOPY_VIDEO_LINUXOS_LINUXVIDEOSOURCE_H
#define _CANOPY_VIDEO_LINUXOS_LINUXVIDEOSOURCE_H

#ifndef _CANOPY_VIDEO_VIDEOSOURCE_H
#include <canopy/video/VideoSource.h>
#endif

#if CANOPY_OS == CANOPY_LINUX

namespace canopy {
  namespace video {
    namespace linuxos {
      /**
       * The VideoSource provides access to a source
       * of video from a web-cam, a TV tuners or other
       * kinds of sources. It is the starting point
       * for interacting with the video.
       */
      class LinuxVideoSource : public VideoSource {
	LinuxVideoSource&operator=(const LinuxVideoSource&);
	LinuxVideoSource(const LinuxVideoSource&);

	/** 
	 * The default video source
	 */
      public:
	static const char* DEFAULT_DEVICE;

	/**
	 * Default constructor.
	 */
      protected:
	LinuxVideoSource() throws();

	/**
	 * The destructor.
	 */
      public:
	virtual ~LinuxVideoSource() throws() = 0;

	/**
	 * Open a video device. The src parameter may be 0, 
	 * in which case an attempt is made to locate a default source.
	 * @param src a descriptor for the location of the source.
	 * @return a video source 
	 * @throws ::std::exception if the source could not be opened
	 */
      public:
	static ::std::unique_ptr<VideoSource> open(const char* src) throws (::std::exception);
      };
    }
  }
}
#endif
#endif
