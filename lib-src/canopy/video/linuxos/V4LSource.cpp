#include <canopy/video/linuxos/V4LSource.h>
#if CANOPY_OS == CANOPY_LINUX
#if HAVE_V4L == 1

#include <canopy/mt/Mutex.h>
#include <canopy/video/FrameGrabber.h>
#include <canopy/video/Format.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <cerrno>
#include <cstring>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

using namespace ::std;

namespace canopy {
  namespace video {
    namespace linuxos {

      namespace {
	
	inline static Format palette2Format (int palette)
	{
	  switch (palette) {
	  case VIDEO_PALETTE_GREY: return Format::GREY; 
	  case VIDEO_PALETTE_HI240: return Format::HI240; 
	  case VIDEO_PALETTE_RGB565: return Format::RGB565; 
	  case VIDEO_PALETTE_RGB555: return Format::RGB555; 
	  case VIDEO_PALETTE_RGB24: return Format::RGB24; 
	  case VIDEO_PALETTE_RGB32: return Format::RGB32; 
	  case VIDEO_PALETTE_YUV420: return Format::YUV420; 
	  case VIDEO_PALETTE_YUV420P: return Format::YUV420; 
	  case VIDEO_PALETTE_YUV422: return Format::YUYV; 
	  case VIDEO_PALETTE_YUYV: return Format::YUYV;  
	  case VIDEO_PALETTE_UYVY: return Format::UYVY; 
	  case VIDEO_PALETTE_YUV411: return Format::Y41P; 
	  case VIDEO_PALETTE_YUV422P: return Format::YUV422P; 
	  case VIDEO_PALETTE_YUV411P: return Format::YUV411P; 
	  case VIDEO_PALETTE_YUV410P: return Format::YVU410; 
	    
	  default:
	    ::std::ostringstream out;
	    out << "Format not understood " << palette << " (  0x" << hex << palette << " )";
	    printDebugMessage(out.str());
	    return Format::UNKNOWN;
	  }
	}

	inline size_t imageSize(size_t w, size_t h, const Format& f)
	{
	  if (f.encoding()==Format::YUV420) {
	    return (3*w*h)/2;
	  }
	  return 0;
	}

	struct FrameGrabberBase : public FrameGrabber {
	  FrameGrabberBase() throws() 
	  {}
	  ~FrameGrabberBase() throws()
	  {}
	};

	/** This framegrabber processed the input image via the read() system call */
	struct FrameGrabber_Read : public FrameGrabberBase {
	  FrameGrabber_Read(int device) 
	    : _capturing(false),
	      _device(device),_frameBuf(0),_frameNo(0)
	  {
	    // query the video device capabilities
	    video_capability caps;
	    if (::ioctl(device,VIDIOCGCAP,&caps) == -1) {
	      throwSystemError("[canopy.video.linuxos.V4LSource] Could not query video device capabilities");
	    }
	    if ((caps.type & VID_TYPE_CAPTURE)==0) {
	      throw ::std::runtime_error("Capture is unsupported");
	    }

	    if (-1 == ::ioctl(device,VIDIOCGWIN,&_origWin)) {
	      throwSystemError("Could not get VIDIOCGWIN");
	    }
	    setSize(_origWin.width,_origWin.height);
	  }

	  ~FrameGrabber_Read() throws () 
	  {
	    reset();
	  }

	private:
	  void reset() throws()
	  {
	    assert(!_capturing);
	    resetWin();
	    resetPict();
	    delete[] _frameBuf;
	    _frameBuf =0;
	  }

	public:
	  size_t imageWidth() const throws (::std::exception)
	  { return _win.width; }

	  size_t imageHeight() const throws (::std::exception)
	  { return _win.height; }

	  bool setImageSize (size_t w, size_t h) throws (::std::exception)
	  {
	    if (_capturing) {
	      throw ::std::runtime_error("setImageSize: concurrent modification");
	    }
	    size_t oldWidth = _win.width;
	    size_t oldHeight = _win.height;

	    reset();
	    
	    // we're cannot really do this without resetting everying on the device
	    try {
	      setSize(w,h);
	      return true;
	    }
	    catch (...) {
	      //  this may still fail!!
	      setSize(oldWidth,oldHeight);
	      return false;
	    }
	  }

	private:
	  void setSize(int width, int height) throws (::std::exception)
	  {
	    assert(!_capturing);
	    // first, acquire the current settings which are known to work
	    if (-1 == ::ioctl(_device,VIDIOCGWIN,&_origWin)) {
	      throwSystemError("Could not get VIDIOCGWIN");
	    }
	    if (-1 == ::ioctl(_device,VIDIOCGPICT,&_origPict)) {
	      resetWin();
	      throwSystemError("Could not get VIDIOCGPICT");
	    }
	    
	    _win = _origWin;
	    _pict = _origPict;

	    // this camera doesn't really support anything useful
	    _win.width = checked_cast<int>(width);
	    _win.height = checked_cast<int>(height);
	    if (-1 == ::ioctl(_device,VIDIOCSWIN,&_win)) {
	      resetWin();
	      throwSystemError("Could not set VIDIOCSWIN");
	    }
	    const Format fmt(palette2Format(_pict.palette));
	    size_t nBytes = imageSize(_win.width,_win.height,fmt);
	    if (nBytes==0) {
	      ::std::ostringstream out;
	      out << "Video format not understood : " << fmt.string();
	      throw ::std::runtime_error(out.str());
	    }
	    
	    _frameSize = nBytes;
	    _frameBuf = new char[_frameSize];
	  }

	public:
	  bool captureFrame (Callback& cb) throws (::std::exception)
	  {
	    _capturing = true;
	    if (::read(_device,_frameBuf,_frameSize)==-1) {
	      _capturing=false;
	      throwSystemError("Could not capture next frame");
	    }
	    const Format fmt(palette2Format(_pict.palette));
	    bool res = cb.frameCaptured(_frameNo++,0,0,_win.width,_win.height,fmt,_frameBuf,_frameSize);
	    _capturing = false;
	    return res;
	  }

	private:
	  void resetWin()
	  {
	    int tmp = errno;
	    if (-1 == ::ioctl(_device,VIDIOCSWIN,&_origWin)) {
	      printSystemError("Could not set VIDIOCSWIN");
	    }
	    errno = tmp;
	  }

	  void resetPict()
	  {
	    int tmp = errno;
	    if (-1 == ::ioctl(_device,VIDIOCSPICT,&_origPict)) {
	      printSystemError("Could not set VIDIOCSPICT");
	    }
	    errno = tmp;
	  }

	private:
	  bool _capturing;
	  int _device;
	  video_window _origWin,_win;
	  video_picture _origPict,_pict;
	  size_t _frameSize;
	  char* _frameBuf;
	  size_t _frameNo;
	};

	/** This framegrabber processed the input image via the read() system call */
	struct FrameGrabber_MMap : public FrameGrabberBase {
	  FrameGrabber_MMap(int device) 
	    : _capturing(false),
	      _device(device),_frameBuf(0),_frameNo(0)
	  {
	    video_capability caps;
	    if (::ioctl(device,VIDIOCGCAP,&caps) == -1) {
	      throwSystemError("[canopy.video.linuxos.V4LSource] Could not query video device capabilities");
	    }
	    if ((caps.type & VID_TYPE_CAPTURE)==0) {
	      throw ::std::runtime_error("Capture is unsupported");
	    }
	    if (-1 == ::ioctl(device,VIDIOCGWIN,&_origWin)) {
	      throwSystemError("Could not get VIDIOCGWIN");
	    }
	    setSize(_origWin.width,_origWin.height);
	  }

	  ~FrameGrabber_MMap() throws () 
	  {
	    reset();
	  }

	  void reset() throws ()
	  {
	    assert(!_capturing);
	    resetWin();
	    resetPict();
	    if (_frameBuf!=0) {
	      ::munmap(_frameBuf,_videobuf.size);
	      _frameBuf = 0;
	    }
	  }

	  
	  size_t imageWidth() const throws (::std::exception)
	  {
	    return _captureBuf.width;
	  }

	  size_t imageHeight() const throws (::std::exception)
	  {	
	    return _captureBuf.height;
	  }

	  void setSize(int width, int height) throws (::std::exception)
	  {
	    assert(!_capturing);
	    // first, acquire the current settings which are known to work
	    if (-1 == ::ioctl(_device,VIDIOCGWIN,&_origWin)) {
	      throwSystemError("Could not get VIDIOCGWIN");
	    }
	    if (-1 == ::ioctl(_device,VIDIOCGPICT,&_origPict)) {
	      resetWin();
	      throwSystemError("Could not get VIDIOCGPICT");
	    }
	    
	    _win = _origWin;
	    _pict = _origPict;

	    // this camera doesn't really support anything useful
	    _win.width = width;
	    _win.height = height;
	    if (-1 == ::ioctl(_device,VIDIOCSWIN,&_win)) {
	      resetWin();
	      throwSystemError("Could not set VIDIOCSWIN");
	    }

	    if (-1 == ::ioctl(_device,VIDIOCGMBUF,&_videobuf)) {
	      resetWin();
	      resetPict();
	      throw ::std::runtime_error("Cannot allocate mmap");
	    }
	    
	    // create a memory map
	    _frameBuf = (char*)::mmap(0,_videobuf.size,PROT_READ|PROT_WRITE,MAP_SHARED,_device,0);
	    if (_frameBuf== (void*)-1) {
	      resetWin();
	      resetPict();
	      throwSystemError("MMap failed");
	    }

	    // setup the template capture buffer
	    _captureBuf.frame = 0;
	    _captureBuf.width = _win.width;
	    _captureBuf.height = _win.height;
	    _captureBuf.format = _pict.palette;

	  }

	  bool setImageSize (size_t w, size_t h) throws (::std::exception)
	  {
	    if (_capturing) {
	      throw ::std::runtime_error("setImageSize: concurrent modification");
	    }

	    const size_t oldWidth  = _captureBuf.width;
	    const size_t oldHeight = _captureBuf.height;
	    if (w==oldWidth && h==oldHeight) {
	      return true;
	    }
	    reset();
	    
	    // we're cannot really do this without resetting everying on the device
	    try {
	      setSize(w,h);
	      return true;
	    }
	    catch (...) {
	      //  this may still fail!!
	      setSize(oldWidth,oldHeight);
	      return false;
	    }
	  }

	private:
	  void startCapture ()
	  {
	    _capturing = true;
	    video_mmap capturebuf = _captureBuf;
	    capturebuf.frame = _frameNo % _videobuf.frames;
	    if (-1 == ::ioctl(_device,VIDIOCMCAPTURE,&capturebuf)) {
	      throwSystemError("Could not capture next frame");
	    }
#if 0
	    ::std::cerr << "VIDIOCMCAPTURE (" 
			<< capturebuf.frame << ";" 
			<< capturebuf.width << ";"
			<< capturebuf.height << ";"
			<< capturebuf.format << ")"
			<< ::std::endl;
#endif
	  }
	  
	public:
	  bool captureFrame (Callback& cb) throws (::std::exception)
	  {
	    // reset this here; it will be set to true if another capture is done
	    _capturing=false;

	    if (_frameNo==0) {
	      startCapture(); // do first capture
	    }
	    int curFrame = _frameNo % _videobuf.frames;
	    ++_frameNo;
	    if (_videobuf.frames > 1) {
	      startCapture(); 
	    }
	    // wait for the frame
	    if (-1 == ::ioctl(_device,VIDIOCSYNC,&curFrame)) {
	      throwSystemError("Could not wait for frame to finish");
	    }
	    char* buf = _frameBuf+_videobuf.offsets[curFrame];
	    const Format fmt(palette2Format(_pict.palette));
	    bool doMore = cb.frameCaptured(_frameNo-1,0,0,_captureBuf.width,_captureBuf.height,fmt,buf,imageSize(_captureBuf.width,_captureBuf.height,fmt));
	    if (doMore && _videobuf.frames==1) {
	      startCapture();
	    }
	    else if (!doMore && _videobuf.frames>1) {
	      _frameNo = 0;
	      _capturing = false; // for sure, we've given up on capturing
	      // wait for the last frame to finish
	      if (-1 == ::ioctl(_device,VIDIOCSYNC,&curFrame)) {
		throwSystemError("Could not wait for frame to finish");
	      }
	    }
	    return doMore;
	  }

	private:
	  void resetWin()
	  {
	    int tmp = errno;
	    if (-1 == ::ioctl(_device,VIDIOCSWIN,&_origWin)) {
	      printSystemError("Could not set VIDIOCSWIN");
	    }
	    errno = tmp;
	  }

	  void resetPict()
	  {
	    int tmp = errno;
	    if (-1 == ::ioctl(_device,VIDIOCSPICT,&_origPict)) {
	      printSystemError("Could not set VIDIOCSPICT");
	    }
	    errno = tmp;
	  }

	private:
	  bool _capturing;
	  int _device;
	  video_window _origWin,_win;
	  video_picture _origPict,_pict;
	  char* _frameBuf;
	  video_mbuf _videobuf;
	  video_mmap _captureBuf;
	  video_capture _captureWin;
	  size_t _frameNo;
	};
      }

      V4LSource::V4LSource(int dev) throws()
      : _device(dev)
      {}
      
      V4LSource::~V4LSource() throws()
      {
	_frameGrabber.reset(0);

	int err = ::close(_device);
	if (err) {
          printSystemError("[canopy.video.linuxos.V4LSource] Could not close the video device");
	}
      }
      
      unique_ptr<VideoSource> V4LSource::open(const char* src) throws (exception)
      {
	// use the default device if none specified
	if (src==0) {
	  src = DEFAULT_DEVICE;
	}
	// try to open the device
	int vdev = ::open(src,O_RDWR);
	if (vdev<0) {
	  throwSystemError(string("[canopy.video.linuxos.V4LSource] Could not open the video device ")+src);
	}
	try {
	  return unique_ptr<VideoSource>(new V4LSource(vdev));
	}
	catch (...) {
	  ::close(vdev);
	  throw;
	}
      }

      void V4LSource::printDescription (::std::ostream& out) throws ()
      {
	// query the video device capabilities
	video_capability caps;
	if (::ioctl(_device,VIDIOCGCAP,&caps) == -1) {
	  throwSystemError("[canopy.video.linuxos.V4LSource] Could not query video device capabilities");
	}
	out<< "Device driver     : " << caps.name << endl
	   << "Capabilities      :" 
	   <<   ((caps.type & VID_TYPE_CAPTURE) ? " CAPTURE" : "")
	   <<   ((caps.type & VID_TYPE_TUNER) ? " TUNER" : "")
	   <<   ((caps.type & VID_TYPE_TELETEXT) ? " TELETEXT" : "")
	   <<   ((caps.type & VID_TYPE_OVERLAY) ? " OVERLAY" : "")
	   <<   ((caps.type & VID_TYPE_CHROMAKEY) ? " CHROMAKEY" : "")
	   <<   ((caps.type & VID_TYPE_CLIPPING) ? " CLIPPING" : "")
	   <<   ((caps.type & VID_TYPE_FRAMERAM) ? " FRAMERAM" : "")
	   <<   ((caps.type & VID_TYPE_SCALES) ? " SCALES" : "")
	   <<   ((caps.type & VID_TYPE_MONOCHROME) ? " MONOCHROME" : "")
	   <<   ((caps.type & VID_TYPE_SUBCAPTURE) ? " SUBCAPTURE" : "")
	   << endl;
      }

      FrameGrabber& V4LSource::frameGrabber() throws (::std::exception)
      {
	// not threadsafe!
	if (_frameGrabber.get()==0) {
	  try {
	    // try to use mmap first
	    _frameGrabber.reset(new FrameGrabber_MMap(_device));
	  }
	  catch (...) {
	    printDebugMessage("Could not use mmap for video: trying simple read interface");
	    // use regular read call (slower!)
	    _frameGrabber.reset(new FrameGrabber_Read(_device));
	  }
	}
	return *_frameGrabber;
      }

    }
  }
}
#endif
#endif
