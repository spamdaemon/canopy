#ifndef _CANOPY_VIDEO_LINUXOS_V4LSOURCE_H
#define _CANOPY_VIDEO_LINUXOS_V4LSOURCE_H

#ifndef _CANOPY_VIDEO_LINUXOS_LINUXVIDEOSOURCE_H
#include <canopy/video/linuxos/LinuxVideoSource.h>
#endif

#if CANOPY_OS == CANOPY_LINUX
#if HAVE_V4L == 1
// the video device api
#include <sys/time.h>
#include <linux/videodev.h>

#include <memory>

namespace canopy {
  namespace video {
    namespace linuxos {
      /**
       * The VideoSource provides access to a source
       * of video from a web-cam, a TV tuners or other
       * kinds of sources. It is the starting point
       * for interacting with the video.
       * <p>
       * Information about V4L can be found here: 
       * <a href="http://www.linuxtv.org/downloads/video4linux/API/V4L1_API.html">V4L API</a>/
       */
      class V4LSource : public LinuxVideoSource {
	V4LSource&operator=(const V4LSource&);
	V4LSource(const V4LSource&);

	/**
	 * Default constructor.
	 * @param dev the video device
	 */
      private:
	V4LSource(int dev) throws();

	/**
	 * The destructor.
	 */
      public:
	~V4LSource() throws();

	/**
	 * Open a video device. The src parameter may be 0, 
	 * in which case an attempt is made to locate a default source.
	 * @param src a descriptor for the location of the source.
	 * @return a video source or 0 if the device is not a V4L device.
	 * @throws ::std::exception if the video source exists, but could not be opened
	 */
      public:
	static ::std::unique_ptr<VideoSource> open(const char* src) throws (::std::exception);

	/** Implementation */
      public:
	FrameGrabber& frameGrabber() throws (::std::exception);
	void printDescription (::std::ostream& out) throws ();
	
	/** The source device */
      private:
	int _device;

	/**
	 * A frame grabber
	 */
      private:
	::std::unique_ptr<FrameGrabber> _frameGrabber;
      };
    }
  }
}
#endif
#endif
#endif
