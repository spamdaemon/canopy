#include <canopy/video/linuxos/V4L2Source.h>
#if CANOPY_OS == CANOPY_LINUX

#include <canopy/video/FrameGrabber.h>
#include <canopy/video/Format.h>

#include <string>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>

#include <libv4lconvert.h>

using namespace ::std;

#define PTZ_CONTROLS_EXPERIMENTAL 0

namespace canopy {
   namespace video {
      namespace linuxos {
         namespace {
            inline static Format palette2Format(size_t fmt) throws (exception)
            {
               switch (fmt) {
                  case V4L2_PIX_FMT_GREY:
                     return Format::GREY;
                  case V4L2_PIX_FMT_RGB332:
                     return Format::RGB332;
                  case V4L2_PIX_FMT_RGB444:
                     return Format::RGB444;
                  case V4L2_PIX_FMT_RGB555:
                     return Format::RGB555;
                  case V4L2_PIX_FMT_RGB565:
                     return Format::RGB565;
                  case V4L2_PIX_FMT_RGB24:
                     return Format::RGB24;
                  case V4L2_PIX_FMT_RGB32:
                     return Format::RGB32;
                  case V4L2_PIX_FMT_BGR24:
                     return Format::BGR24;
                  case V4L2_PIX_FMT_BGR32:
                     return Format::BGR32;
                  case V4L2_PIX_FMT_YUYV:
                     return Format::YUYV;
                  case V4L2_PIX_FMT_UYVY:
                     return Format::UYVY;
                  case V4L2_PIX_FMT_YUV420:
                     return Format::YUV420;
                  case V4L2_PIX_FMT_YUV422P:
                     return Format::YUV422P;
                  case V4L2_PIX_FMT_YUV411P:
                     return Format::YUV411P;
                  case V4L2_PIX_FMT_MJPEG:
                     return Format::MJPEG;
                  case V4L2_PIX_FMT_JPEG:
                     return Format::JPEG;
                  case V4L2_PIX_FMT_DV:
                     return Format::DV;
                  case V4L2_PIX_FMT_MPEG:
                     return Format::MPEG;
                  case V4L2_PIX_FMT_WNVA:
                     return Format::WNVA;
                  case V4L2_PIX_FMT_OV511: {
                     ::std::ostringstream out;
                     out << "V4L2 format not understood  V4L2_PIX_FMT_OV511";
                     printDebugMessage(out.str());
                     return Format::UNKNOWN;
                  }
                  case V4L2_PIX_FMT_OV518: {
                     ::std::ostringstream out;
                     out << "V4L2 format not understood  V4L2_PIX_FMT_OV518";
                     printDebugMessage(out.str());
                     return Format::UNKNOWN;
                  }
                  default:
                     ::std::ostringstream out;
                     out << "V4L2 format not understood " << fmt << " (  0x" << hex << fmt << " )";
                     printDebugMessage(out.str());
                     return Format::UNKNOWN;
               }
            }

            inline static Format palette2Format(const v4l2_pix_format& palette)
            {
               return palette2Format(palette.pixelformat);
            }

            static v4l2_capability queryCapabilities(int device) throws(::std::exception)
            {
               v4l2_capability caps;
               ::memset(&caps, 0, sizeof(caps));
               if (-1 == ::ioctl(device, VIDIOC_QUERYCAP, &caps)) {
                  throwSystemError("Could not query video capabilities");
               }
               return caps;
            }

#if PTZ_CONTROLS_EXPERIMENTAL == 1
            struct PTZControls {
               PTZControls(int dev)
               : _device(dev)
               {
                  // check if ptz controls are available
                  v4l2_queryctrl ctrl;
                  ::memset(&ctrl,0,sizeof(ctrl));
                  ctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;
                  while(0== ::ioctl(_device,VIDIOC_QUERYCTRL, &ctrl)) {
                     cerr << "Control class " << hex << V4L2_CTRL_ID2CLASS(ctrl.id) << " id " << ctrl.id << endl;
                     if (false && V4L2_CTRL_ID2CLASS(ctrl.id) != V4L2_CTRL_CLASS_CAMERA) {
                        // not interpreted in non-camera control
                        break;
                     }
                     cerr
                     //<< "Control class " << hex << (ctrl.id & (~V4L2_CID_CAMERA_CLASS_BASE)) << endl
                     << "  type : " << ctrl.type << endl
                     << "  range: " << ctrl.minimum << "  to  " << ctrl.maximum << endl
                     << "  step : " << ctrl.step << endl
                     << endl;

                     ctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
                  };
                  printSystemError("Done enumerating device controls");
               }

               ~PTZControls() throws()
               {}

               private:
               int _device;
            };
#endif

            struct FrameGrabberBase : public FrameGrabber
            {
                  FrameGrabberBase() throws()
                  {
                  }
                  ~FrameGrabberBase() throws()
                  {
                  }
            };

            /** This framegrabber processed the input image via the read() system call */
            struct FrameGrabber_Stream : public FrameGrabberBase
            {

                  // the maximum number of memory mapped buffers
                  static const size_t MAX_BUFFER_COUNT = 2;

                  // some info to be remember for munmap
                  struct MappedBuffer
                  {
                        void* addr; // address of a mapped buffer
                        size_t length; // size of the mapped buffer
                  };

                  FrameGrabber_Stream(int device)
                        : _format(Format::UNKNOWN), _capturing(false), _device(device), _conv(0)
                  {
                     // query the video device capabilities
                     v4l2_capability caps = queryCapabilities(device);

                     // list the driver formats
                     {
                        ostringstream sout;
                        sout << "Supported formats :";

                        v4l2_fmtdesc desc;
                        ::memset(&desc, 0, sizeof(desc));
                        desc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                        desc.index = 0;
                        while (::ioctl(device, VIDIOC_ENUM_FMT, &desc) != -1) {
                           Format fmt = palette2Format(desc.pixelformat);
                           sout << ' ' << fmt.string();
                           ++desc.index;
                        }
                        printDebugMessage(sout.str());
                     }

                     // query the image format
                     v4l2_format format;
                     ::memset(&format, 0, sizeof(format));

                     format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                     if (-1 == ::ioctl(device, VIDIOC_G_FMT, &format)) {
                        throwSystemError("Could not get image format");
                     }

                     // set the format to YUYV
                     format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;

                     // normalize to smaller image width
//                     format.fmt.pix.width = 0;
//                     format.fmt.pix.height = 0;

                     _outputFormat = format;

                     if (-1 == ::ioctl(device, VIDIOC_S_FMT, &format)) {
                        throwSystemError("Could not place video into YUYV mode");
                     }

                     if (-1 == ::ioctl(device, VIDIOC_G_FMT, &format)) {
                        throwSystemError("Could not get image format");
                     }
                     _inputFormat = format;
                     _pix = format.fmt.pix;
                     _format = palette2Format(_pix);

                     {
                        ostringstream sout;
                        sout << "Video4Linux2: image capture size : " << _pix.width << " X " << _pix.height << endl
                              << "  format : " << _format.string() << " ( raw value = " << _pix.pixelformat << ") "
                              << endl << "  bytesperline : " << _pix.bytesperline << endl << "  image memory size : "
                              << _pix.sizeimage;
                        printDebugMessage(sout.str());
                     }

                     allocateBuffers();

                     // create a converter if the format is unknown
                     if (_format == Format::UNKNOWN) {
                        _conv = v4lconvert_create(device);
                        _conversionBufferSize = 1024 * 1024 * 4;
                        _conversionBuffer.reset(new char[_conversionBufferSize]);
                     }
                  }

                  ~FrameGrabber_Stream() throws ()
                  {
                     reset();
                     if (_conv) {
                        v4lconvert_destroy(_conv);
                     }
                  }

               private:
                  void allocateBuffers() throws (::std::exception)
                  {
                     ::memset(&_bufs, 0, sizeof(_bufs));
                     _bufs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                     _bufs.memory = V4L2_MEMORY_MMAP;
                     _bufs.count = MAX_BUFFER_COUNT;

                     if (-1 == ioctl(_device, VIDIOC_REQBUFS, &_bufs)) {
                        if (errno == EINVAL) {
                           throwSystemError("Streaming video not supported");
                        }
                        else {
                           throwSystemError("Could not setup streaming data");
                        }
                     }

                     {
                        ostringstream sout;
                        sout << "Allocated " << _bufs.count
                              << " device buffers for memory-mapped streaming from V4L2 device";
                        printDebugMessage(sout.str());
                     }

                     // clear the buffers array
                     ::memset(_buffers, 0, sizeof(_buffers));

                     // need to mmap each buffer
                     for (size_t i = 0; i < _bufs.count; ++i) {
                        v4l2_buffer buffer;
                        memset(&buffer, 0, sizeof(buffer));
                        buffer.type = _bufs.type;
                        buffer.memory = V4L2_MEMORY_MMAP;
                        buffer.index = i;

                        if (-1 == ioctl(_device, VIDIOC_QUERYBUF, &buffer)) {
                           unmapBuffers();
                           throwSystemError("Could not query V4L2 device buffers");
                        }

                        if (false) {
                           ostringstream sout;
                           sout << "Queried buffer " << buffer.index << ": flags = " << hex << buffer.flags;
                           printDebugMessage(sout.str());
                        }

                        _buffers[i].length = buffer.length;
                        _buffers[i].addr = ::mmap(NULL, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, _device,
                              buffer.m.offset);
                        if (_buffers[i].addr == MAP_FAILED) {
                           unmapBuffers();
                           throwSystemError("Could not memory map V4L2 device buffers");
                        }
                        ::memset(_buffers[i].addr, 128, _buffers[i].length);

                        // push the buffer into the image buffer queue
                        _free.push_back(buffer);
                     }
                  }

                  void unmapBuffers()
                  {
                     // unmap all buffers (if they are used)
                     for (size_t i = 0; i < MAX_BUFFER_COUNT; ++i) {
                        if (_buffers[i].length != 0) {
                           ::munmap(_buffers[i].addr, _buffers[i].length);
                           _buffers[i].addr = 0;
                           _buffers[i].length = 0;
                        }
                     }
                     _free.clear();
                     _used.clear();
                  }

                  void reset() throws()
                  {
                     // turn off streaming
                     if (_capturing) {
                        if (-1 == ::ioctl(_device, VIDIOC_STREAMOFF, &_bufs.type)) {
                           // ignore any message here
                        }
                        _capturing = false;
                     }

                     unmapBuffers();

#if PTZ_CONTROLS_EXPERIMENTAL==1
                     ::memset(&_bufs,0,sizeof(_bufs));
                     _bufs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                     _bufs.memory = V4L2_MEMORY_MMAP;
#endif
                     _bufs.count = 0; // free's the buffers

                     if (-1 == ioctl(_device, VIDIOC_REQBUFS, &_bufs)) {
                        printSystemError("Could not free streaming data");
                     }
                  }

               public:
                  size_t imageWidth() const throws (::std::exception)
                  {
                     return _pix.width;
                  }

                  size_t imageHeight() const throws (::std::exception)
                  {
                     return _pix.height;
                  }

                  bool setImageSize(size_t w, size_t h) throws (::std::exception)
                  {
                     if (_capturing) {
                        throw ::std::runtime_error("setImageSize: concurrent modification");
                     }
                     reset();

                     // request a new size
                     // query the image format
                     v4l2_format format;
                     ::memset(&format, 0, sizeof(format));

                     format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

                     // get the current format
                     if (-1 == ::ioctl(_device, VIDIOC_G_FMT, &format)) {
                        throwSystemError("Could not get image format");
                     }

                     // set the size
                     format.fmt.pix.width = w;
                     format.fmt.pix.height = h;

                     // the set-format call returns the format actually
                     // set in the device
                     if (-1 == ::ioctl(_device, VIDIOC_S_FMT, &format)) {
                        throwSystemError("Could not get image format");
                     }

                     // need to reset the formats
                     _pix = format.fmt.pix;
                     _format = palette2Format(_pix);

                     // reallocate the buffers
                     allocateBuffers();

                     return true;
                  }

               private:
                  void setSize(size_t width, size_t height) throws (::std::exception)
                  {
                     assert(!_capturing);

                  }

               private:
                  bool enqueueBuffer() throws (::std::exception)
                  {
                     if (_free.empty()) {
                        return false;
                     }
                     if (-1 == ::ioctl(_device, VIDIOC_QBUF, &_free.back())) {
                        throwSystemError("Could not enqueue buffer for V4L2 device");
                     }
                     _used.push_back(_free.back());
                     _free.pop_back();
                     return true;
                  }

                  /**
                   * Dequeue a buffer and place it at the end of the _free list
                   */
               private:
                  void dequeueBuffer() throws (::std::exception)
                  {
                     assert(!_used.empty());
                     if (-1 == ::ioctl(_device, VIDIOC_DQBUF, &_used[0])) {
                        throwSystemError("Could not dequeue buffer for V4L2 device");
                     }
                     _free.push_back(_used[0]);
                     _used.erase(_used.begin());
                  }

               public:
                  bool captureFrame(FrameCallback& cb) throws (::std::exception)
                  {
                     bool res = false;
                     if (!_capturing) {

                        // enqueue all buffers initially
                        while (!_free.empty()) {
                           enqueueBuffer();
                        }

                        if (-1 == ::ioctl(_device, VIDIOC_STREAMON, &_bufs.type)) {
                           throwSystemError("Could not initiate image capture");
                        }

                        _capturing = true;
                     }

                     try {
                        // if there is a buffer to be enqueued, do so now
                        if (!_free.empty()) {
                           enqueueBuffer();
                        }

                        // dequeue the next buffer and place it at the end() of
                        // the free list; this call will block!
                        dequeueBuffer();

                        // the dequeue buffer is at the back of the array
                        v4l2_buffer& buf = _free.back();
                        MappedBuffer& mmbuf = _buffers[buf.index];

                        char* frameBuf = (char*) mmbuf.addr;
                        int frameSize = mmbuf.length;
                        Format fmt = _format;

                        // we've got the buffer, but may need to convert to a format we understand
                        if (_conv) {
                           ostringstream sout;
                           sout << "Converting format : " << frameSize;
                           printDebugMessage(sout.str());
                           if (v4lconvert_needs_conversion(_conv, &_inputFormat, &_outputFormat)) {
                              int n = v4lconvert_convert(_conv, &_inputFormat, &_outputFormat,
                                    (unsigned char*) frameBuf, frameSize, (unsigned char*) _conversionBuffer.get(),
                                    _conversionBufferSize);
                              if (n >= 0) {
                                 frameBuf = _conversionBuffer.get();
                                 frameSize = n;
                                 fmt = palette2Format(_outputFormat.fmt.pix);
                                 printDebugMessage("Conversion successful");
                              }
                              else {
                                 printDebugMessage("Failed to convert");
                              }
                           }
                           else {
                              printDebugMessage("Conversion not necessary");
                           }
                        }
                        if (frameSize > 0) {
                           _info.frameNo++;
                           _info.x = _info.y = 0;
                           _info.w = _pix.width;
                           _info.h = _pix.height;
                           _info.format = fmt;
                           _info.buf = frameBuf;
                           _info.bufSize = frameSize;
                           try {
                              res = cb(_info);
                           }
                           catch (...) {
                              _capturing = false;
                              return false;
                           }
                        }
                        else {
                           return true;
                        }
                     }
                     catch (const ::std::exception& e) {
                        _capturing = false;
                        throw;
                     }

                     return res;
                  }

               private:
                  MappedBuffer _buffers[MAX_BUFFER_COUNT];

                  /** The image buffer queue */
               private:
                  vector< v4l2_buffer> _free;

                  /** The queue of buffers currently containing images */
               private:
                  vector< v4l2_buffer> _used;

                  /** The video format */
               private:
                  FrameInfo _info;

                  Format _format;

                  v4l2_pix_format _pix;
                  v4l2_requestbuffers _bufs;

                  bool _capturing;
                  int _device;
                  size_t _frameSize;

               private:
                  v4l2_format _inputFormat;
                  v4l2_format _outputFormat;
                  v4lconvert_data* _conv;
                  unique_ptr< char[]> _conversionBuffer;
                  size_t _conversionBufferSize;
            };

         }
         V4L2Source::V4L2Source(int dev) throws()
               : _device(dev)
         {
         }

         V4L2Source::~V4L2Source() throws()
         {
            _frameGrabber.reset(0);
            int err = ::close(_device);
            if (err) {
               printSystemError("[canopy.video.linuxos.V4L2Source] Could not close the video device");
            }
         }

         unique_ptr< VideoSource> V4L2Source::open(const char* src) throws (exception)
         {
            // use the default device if none specified
            if (src == 0) {
               src = DEFAULT_DEVICE;
            }
            // try to open the device
            int vdev = ::open(src, O_RDWR);
            if (vdev < 0) {
               throwSystemError(string("[canopy.video.linuxos.V4L2Source] Could not open the video device ") + src);
            }
            try {
               unique_ptr< V4L2Source> source(new V4L2Source(vdev));

               // query the video device capabilities
               v4l2_capability caps;
               if (::ioctl(vdev, VIDIOC_QUERYCAP, &caps) == -1) {
                  return unique_ptr< VideoSource>();
               }

#if PTZ_CONTROLS_EXPERIMENTAL==1
               try {
                  PTZControls ptz(vdev);
               }
               catch (...) {
                  // ignore;
               }
#endif
               return ::std::move(source);
            }
            catch (...) {
               //FIXME: would this not mean we're trying to close twice? May also affect V4LSource
               ::close(vdev);

               throw;
            }

         }

         FrameGrabber& V4L2Source::frameGrabber() throws (::std::exception)
         {
            // not threadsafe!
            if (_frameGrabber.get() == 0) {

               // query capabilities
               v4l2_capability caps = queryCapabilities(_device);

               if ((caps.capabilities & V4L2_CAP_VIDEO_CAPTURE) == 0) {
                  throw ::std::runtime_error("Video capture not supported by V4L2 device");
               }

               // check for read/write support
               if (_frameGrabber.get() == 0 && (caps.capabilities & V4L2_CAP_READWRITE)) {
                  printDebugMessage("V4L2 device support read/write access; not implemented");
               }
               if (_frameGrabber.get() == 0 && (caps.capabilities & V4L2_CAP_ASYNCIO)) {
                  printDebugMessage("V4L2 device supports asynchronous access; not implemented");
               }
               if (_frameGrabber.get() == 0 && (caps.capabilities & V4L2_CAP_STREAMING)) {
                  printDebugMessage("V4L2 device supports streaming access");
                  _frameGrabber.reset(new FrameGrabber_Stream(_device));
               }

               if (_frameGrabber.get() == 0) {
                  throw ::std::runtime_error("Could not get a framegrabber for V4L2 device");
               }
            }
            return *_frameGrabber;
         }

      }
   }
}
#endif
