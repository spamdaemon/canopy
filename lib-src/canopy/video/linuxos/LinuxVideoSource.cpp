#include <canopy/video/linuxos/LinuxVideoSource.h>
#if CANOPY_OS == CANOPY_LINUX
#if HAVE_V4L == 1
#include <canopy/video/linuxos/V4LSource.h>
#endif
#include <canopy/video/linuxos/V4L2Source.h>

#include <string>
#include <sstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

using namespace ::std;

namespace canopy {
   namespace video {
      namespace linuxos {

         const char* LinuxVideoSource::DEFAULT_DEVICE = "/dev/video0";

         LinuxVideoSource::LinuxVideoSource() throws()
         {
         }

         LinuxVideoSource::~LinuxVideoSource() throws()
         {
         }

         unique_ptr< VideoSource> LinuxVideoSource::open(const char* src) throws (exception)
         {
            unique_ptr< VideoSource> res;
            try {
               res = V4L2Source::open(src);
               if (res.get()) {
                  // ok, we've got a v4l2 device
                  return res;
               }
            }
            catch (...) {
               // ignore and try V4L
            }
#if HAVE_V4L == 1
            res = V4LSource::open(src);
            if (res.get()) {
               // ok, we've got a v4l device
               return res;
            }
#endif
            throw ::std::runtime_error("Could not obtain a linux video source");
         }

      }
   }
}
#endif
