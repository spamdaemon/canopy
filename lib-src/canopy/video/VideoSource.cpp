#include <canopy/video/VideoSource.h>
#include <canopy/video/linuxos/LinuxVideoSource.h>

namespace canopy {
  namespace video {
    
    VideoSource::VideoSource() throws()
    {}

    VideoSource::~VideoSource() throws()
    {}

    ::std::unique_ptr<VideoSource> VideoSource::open(const char* src) throws (::std::exception)
    {
      ::std::unique_ptr<VideoSource> source;
#if CANOPY_OS == CANOPY_LINUX
      source = canopy::video::linuxos::LinuxVideoSource::open(src);
#else
      throw ::std::runtime_error("VideoSource not supported");
#endif
      return source;
    }

    FrameGrabber& VideoSource::frameGrabber() throws (::std::exception)
    { throw ::std::runtime_error("VideoSource::frameGrabber unsupported operation"); }

    void VideoSource::printDescription (::std::ostream&) throws ()
    { return; }

    
  }
}
