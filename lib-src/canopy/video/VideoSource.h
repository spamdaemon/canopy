#ifndef _CANOPY_VIDEO_VIDEOSOURCE_H
#define _CANOPY_VIDEO_VIDEOSOURCE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif


#include <memory>
#include <iosfwd>

namespace canopy {
  namespace video {
    class FrameGrabber;

    /**
     * The VideoSource provides access to a source
     * of video from a web-cam, a TV tuners or other
     * kinds of sources. It is the starting point
     * for interacting with the video.
     */
    class VideoSource {
      VideoSource&operator=(const VideoSource&);
      VideoSource(const VideoSource&);
      /**
       * Default constructor.
       */
    protected:
      VideoSource() throws();

      /**
       * The destructor.
       */
    public:
      virtual ~VideoSource() throws() = 0;

      /**
       * Open video source. The src parameter may be 0, 
       * in which case an attempt is made to locate a default source.
       * @param src a descriptor for the location of the source.
       * @return a video source 
       * @throws ::std::exception if the video source exists, but could not be opened
       */
    public:
      static ::std::unique_ptr<VideoSource> open(const char* src) throws (::std::exception);

      /**
       * Print a description onto the specified stream.
       * @param out a description of the video source
       */
    public:
      virtual void printDescription (::std::ostream& out) throws ();

      /**
       * Get a frame grabber for this source. The default is to throw an exception.
       * @return a frame grabber
       * @throws ::std::exception if this operation is not supported.
       */
    public:
      virtual FrameGrabber& frameGrabber() throws (::std::exception);
    };
  }
}
#endif
