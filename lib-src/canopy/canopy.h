#ifndef _CANOPY_H
#define _CANOPY_H

#ifndef _CANOPY_DEFINES_H
#include <canopy/defines.h>
#endif

#ifndef _CANOPY_TYPES_H
#include <canopy/types.h>
#endif

#ifndef _CANOPY_COMPILER_H
#include <canopy/compiler.h>
#endif

#ifndef _CANOPY_DESTRUCTOR_H
#include <canopy/Destructor.h>
#endif

#ifndef _CANOPY_BOILERPLATE_H
#include <canopy/boilerplate.h>
#endif

#include <sstream>
#include <stdexcept>
#include <cassert>
#include <cstddef>
#include <string>

namespace canopy {

   /** The version of canopy */
   extern const char VERSION[];

   /** The build date */
   extern const char DATE[];

   /**
    * A generic function pointer.
    */
   typedef void (*FunctionPointer)();

   /**
    * A generic void* pointer for completeness
    */
   typedef void* ObjectPointer;

   /**
    * A logger to print a message.
    * @param priority the priority
    * @param message the message itself
    * @param len the message length or -1 if message is simply null-terminated
    */
   typedef void (*Logger)(int level, const char* message, int len);

   /**
    * Set a log handler. This function can be used to intercept log messages from canopy
    * into a more sophisticated logging framework.
    * @param logger a new logger or nullptr to reset the logger
    * @return the previously set logger
    */
   Logger setLogger(Logger logger) throws();

   /**
    * Cast a function pointer to a void pointer. This is actually
    * not allowed by the C/C++ standards.
    * @param fp a function pointer
    * @return an object pointer
    */
   inline ObjectPointer castToVoid(FunctionPointer fp)
   {
      union
      {
            ObjectPointer objP;
            FunctionPointer funcP;
      } c;
      c.funcP = fp;
      return c.objP;
   }

   /**
    * Cast an object pointer to a function pointer. This is actually
    * not allowed by the C/C++ standards.
    * @param op an object pointer
    * @return a function pointer
    */
   inline FunctionPointer castToFunction(ObjectPointer op)
   {
      union
      {
            ObjectPointer objP;
            FunctionPointer funcP;
      } c;
      c.objP = op;
      return c.funcP;
   }

   /**
    * Create a string from the last system error. This method
    * is threadsafe.
    * @param error the error number (errno)
    * @param msg a message or 0
    * @return a string for the current value of errno
    * This method may throw an out-of-memory exception.
    */
   ::std::string getSystemError(int error, const ::std::string& msg);

   /**
    * Create a string from the last system error. This method
    * is threadsafe.
    * @param error the error number (errno)
    * @param msg a message or 0
    * @return a string for the current value of errno
    * This method may throw an out-of-memory exception.
    */
   ::std::string getSystemError(int error, const char* msg);

   /**
    * Create a string from the last system error. This method
    * is threadsafe.
    * @param msg a message or 0
    * @return a string for the current value of errno
    * This method may throw an out-of-memory exception.
    */
   ::std::string getSystemError(const ::std::string& msg);

   /**
    * Create a string from the last system error. This method
    * is threadsafe.
    * @param msg a message or 0
    * @return a string for the current value of errno
    * This method may throw an out-of-memory exception.
    */
   ::std::string getSystemError(const char* msg);

   /**
    * Print a message based on the current state of errno. An optional
    * message can be specified which will be printed in addition to
    * the message detail associated with errno.
    * If there was no error, then no message is printed and false is returned
    * @param error the value of errno
    * @param msg a message.
    * @return true if errno was not 0 and a message was printed
    */
   bool printSystemError(int error, const char* msg) throws();

   /**
    * Print a message based on the current state of errno. An optional
    * message can be specified which will be printed in addition to
    * the message detail associated with errno.
    * If there was no error, then no message is printed and false is returned
    * @param error the value of errno
    * @param msg a message.
    * @return true if errno was not 0 and a message was printed
    */
   bool printSystemError(int error, const ::std::string& msg) throws();

   /**
    * Print a message based on the current state of errno. An optional
    * message can be specified which will be printed in addition to
    * the message detail associated with errno.
    * If there was no error, then no message is printed and false is returned
    * @param msg a message.
    * @return true if errno was not 0 and a message was printed
    */
   bool printSystemError(const char* msg) throws();

   /**
    * Print a message based on the current state of errno. An optional
    * message can be specified which will be printed in addition to
    * the message detail associated with errno.
    * If there was no error, then no message is printed and false is returned
    * @param msg a message.
    * @return true if errno was not 0 and a message was printed
    */
   bool printSystemError(const ::std::string& msg) throws();

   /**
    * Print a message based on the current state of errno. An optional
    * message can be specified which will be printed in addition to
    * the message detail associated with errno.
    * If there was no error, then no message is printed and false is returned
    * @param error the value of errno
    * @param msg a message.
    * @return true if errno was not 0 and a message was printed
    */
   void throwSystemError(int error, const char* msg) throws (::std::exception);

   /**
    * Throw a system error. This will use getSystemError() to generate
    * an error message which will be used as the message for the exception.
    * @param error the value of errno
    * @param msg a message.
    * @throws ::std::exception always throws
    */
   void throwSystemError(int error, const ::std::string& msg) throws (::std::exception);

   /**
    * Print a message based on the current state of errno. An optional
    * message can be specified which will be printed in addition to
    * the message detail associated with errno.
    * If there was no error, then no message is printed and false is returned
    * @param msg a message.
    * @return true if errno was not 0 and a message was printed
    */
   void throwSystemError(const char* msg) throws (::std::exception);

   /**
    * Throw a system error. This will use getSystemError() to generate
    * an error message which will be used as the message for the exception.
    * @param msg a message.
    * @throws ::std::exception always throws
    */
   void throwSystemError(const ::std::string& msg) throws (::std::exception);

   /**
    * Print an error message
    * @param msg a message.
    */
   void printErrorMessage(const ::std::string& msg) throws();

   /**
    * Print an error message.
    * @param msg a message.
    * @param len the length of the message or -1 if it is null-terminated
    */
   void printErrorMessage(const char* msg, int len = -1) throws();

   /**
    * Print a debug message.
    * @param msg a message.
    */
   void printDebugMessage(const ::std::string& msg) throws();

   /**
    * Print a debug message.
    * @param msg a message.
    * @param len the length of the message or -1 if it is null-terminated
    */
   void printDebugMessage(const char* msg, int len = -1) throws();

   /**
    * Abort the program.
    * @param err an error code
    * @param file the file from which this function was called
    * @param line the line in the file
    */
   void abortProgram(int err, const char* file, int line) throws ();

   /**
    * Abort the program if an error has occurred.
    * @param err if not 0, then program will be aborted
    * @param file the file from which this function was called
    * @param line the line in the file
    */
   inline void abortOnError(int err, const char* file, int line) throws ()
   {
      if (unlikely(err != 0)) {
         abortProgram(err, file, line);
      }
   }

   /**
    * This method provides a simple and clean way to reinterpret
    * the type of a variable. This method may be used, for example,
    * to convert an integer into a floating point number.
    * @param from the value from which to transform
    * @return the value into which the object is mapped
    */
   template<class TO, class FROM>
   inline TO reinterpret_type(const FROM& from)
   {
      union
      {
            TO _to;
            FROM _from;
      } x;
      x._from = from;
      return x._to;
   }

   /**
    * The checked cast perform static_casts between
    * objects, but asserts or throws an exception if
    * the value of the casted object is changed by the operation
    * @param from the value to be casted
    * @return the casted value
    * @throws ::std::logic_error if the cast fails
    */
   template<typename TO, typename FROM>
   inline TO checked_cast(FROM from)
   {
#ifdef NDEBUG
      return static_cast<TO>(from);
#else
      // the compiler should be smart enough to
      // optimize nicely if constants are involved.
      TO res = static_cast< TO>(from);
#if CANOPY_ASSERT_ON_CHECKED_CAST != 0
      assert(static_cast< FROM>(res) == from && "Static cast failed");
#else
      if (unlikely(static_cast<FROM>(res) != from)) {
         ::std::ostringstream oss;
         oss << "Invalid static_cast from " << from << " to " << res;
         throw ::std::logic_error(oss.str());
      }
#endif 
      return res;
#endif
   }

   /**
    * Count the number of bits in an integer
    * @param x a value
    * @return the number of 1 bits set in x
    */
#if CANOPY_COMPILER == CANOPY_GCC
   inline size_t countOnes(::std::uint32_t x)
   {
      return __builtin_popcount(x);
   }
#else
   size_t countOnes(::std::uint32_t x);
#endif

   /**
    * Count the number of bits in an integer
    * @param x a value
    * @return the number of 1 bits set in x
    */
#if CANOPY_COMPILER == CANOPY_GCC
   inline size_t countOnes(::std::uint64_t x)
   {
      return __builtin_popcountl(x);
   }
#else
   size_t countOnes(::std::uint64_t x);
#endif


   /**
    * A static assertion type.
    */
#if __cplusplus >= 201103L
#define STATIC_ASSERT(X,MESSAGE) static_assert((X),(MESSAGE))
#else
#define STATIC_ASSERT(X,MESSAGE) ::canopy::compile_time_assert<(X)>(MESSAGE)
   template <bool X> struct compile_time_assert {};
   template <> struct compile_time_assert<true> { compile_time_assert(const char*) {} };
#endif
}

#endif
