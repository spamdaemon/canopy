#ifndef _CANOPY_MT_H
#define _CANOPY_MT_H

#ifndef _CANOPY_MT_THREAD_H
#include <canopy/mt/Thread.h>
#endif

namespace canopy {
  /**
   * Create a thread that invokes a function with an argument of arbitrary type. The data
   * may be copied multiple times to transfer it from one thread to another.
   * @param name the name for the new tread
   * @param func the function to be executed in the new thread
   * @param data the data that is passed to the function
   * @return a thread 
   * @throws ::std::exception if the thread could not be created
   * @throws ::std::invalid_argument if no function was specified.
   */
  template <class T,class U>
    ::canopy::mt::Thread createThread (const ::std::string& name, void (*func) (U ), const T& data) throws (::std::exception, ::std::invalid_argument)
    {
      struct Data {
	Data (void (*f)(U), const T& val) throws()
	  : _func(f), _data(val) {}
	    
	~Data() throws () {}
	    
	static void run(void* x)
	{
	  Data* ptr = reinterpret_cast<Data*>(x);
	      
	  try {
	    // just call the function directly
	    (*ptr->_func)(ptr->_data);

	    // don't forget to delete the temporary data structure
	    delete ptr;
	  }
	  catch (...) {
	    // ok, something happened, delete the data
	    delete ptr;
	    throw;
	  }
	}
	
	void (*_func)(U);
	T _data;
      };
	  
      if (func==0) { 
	throw ::std::invalid_argument("Missing thread function");
      }

      Data* theData  = new Data(func,data);
      try {
	return ::canopy::mt::Thread(name,Data::run,theData);
      }
      catch (const ::std::exception& e) {
	delete theData;
	throw;
      }
    }

  /**
   * Create a thread that invokes a function with no arguments
   * @param name the name for the new tread
   * @param func the function to be executed in the new thread
   * @return a thread 
   * @throws ::std::exception if the thread could not be created
   * @throws ::std::invalid_argument if no function was specified.
   */
  ::canopy::mt::Thread createThread (const ::std::string& name, void (*func) ()) throws (::std::exception, ::std::invalid_argument);
}
#endif
