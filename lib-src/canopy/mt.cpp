#include <canopy/mt.h>

namespace canopy {
  namespace {
    typedef void (*FUNC)();
    
    static void runIt(void* f)
    {
      (*(FUNC)f)();
      }
  }
  
  ::canopy::mt::Thread createThread (const ::std::string& name, void (*func) ()) throws (::std::exception, ::std::invalid_argument)
  {
    if (func==0) { 
      throw ::std::invalid_argument("Missing thread function");
    }
    
    return ::canopy::mt::Thread(name,runIt,castToVoid(func));
  }
}
