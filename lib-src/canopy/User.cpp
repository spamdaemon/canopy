#include <canopy/User.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <canopy/SystemError.h>

namespace canopy {

  namespace {
    struct UnixUser : public User {
      
      UnixUser(const ::std::string& nm) throws (::std::exception)
      {
	struct ::passwd *entryP = 0;
	_stringsSize = 1+::sysconf(_SC_GETPW_R_SIZE_MAX);
	_strings = new char[_stringsSize];

	int ok = getpwnam_r(nm.c_str(),&_entry,_strings,_stringsSize,&entryP);
        // we need to check both ok and entryP because even though the user
        // may not exit, the function still returns ok
	if (ok!=0) {
	  delete[] _strings;
	  throw SystemError("Could not find user "+nm);
	}
	if (entryP==0) {
	  delete[] _strings;
	  throw ::std::runtime_error("Could not find user "+nm);
	}
      }

      UnixUser(uid_t id) throws (::std::exception)
      {
	struct ::passwd *entryP = 0;
	_stringsSize = 1+::sysconf(_SC_GETPW_R_SIZE_MAX);
	_strings = new char[_stringsSize];

        // we need to check both ok and entryP because even though the user
        // may not exit, the function still returns ok
	int ok = getpwuid_r(id,&_entry,_strings,_stringsSize,&entryP);
	if (ok!=0) {
	  delete[] _strings;
	  throw SystemError("Could not find user");
	}
	if (entryP==0) {
	  delete[] _strings;
	  throw ::std::runtime_error("Could not find user");
	}
      }

      ~UnixUser() throws() 
      {
	delete[] _strings;
      }
      
      ::std::string name() const throws()
      { return _entry.pw_name; }
      
      ::std::string documentRoot() const throws() 
      { return _entry.pw_dir; }
      
    private:
      ::passwd _entry;
      char*    _strings;
      long      _stringsSize;
    };
  }

  User::User() throws()
  {}

  User::~User() throws()
  {}

  ::std::unique_ptr<User> User::currentUser()throws (::std::exception)
  {
    UnixUser* uu = new UnixUser(::getuid());
    return ::std::unique_ptr<User>(uu);
  }

  ::std::unique_ptr<User> User::effectiveUser()throws (::std::exception)
  {
    UnixUser* uu = new UnixUser(::geteuid());
    return ::std::unique_ptr<User>(uu);
  }

  ::std::unique_ptr<User> User::findUser(const ::std::string& n) throws (::std::exception)
  {
    try {
      UnixUser* uu = new UnixUser(n);
      return ::std::unique_ptr<User>(uu);
    }
    catch (const SystemError& e) {
      // something went wrong, other than not finding the user
      throw;
    }
    catch (const ::std::exception& e) {
      return ::std::unique_ptr<User>();
    }
  }

  
}
