#ifndef _CANOPY_USER_H
#define _CANOPY_USER_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <memory>

namespace canopy {

  /**
   * The User class provides information about the user logged in.
   * @todo need a way to authenticate the user
   */
  class User {
    /** Disable copying */
  private:
    CANOPY_BOILERPLATE_PREVENT_COPYING(User);

    /**
     * Default constructor.
     */
  protected:
    User() throws();
    
    /**
     * Destroy this user
     */
  public:
    virtual ~User() throws() = 0;

    /**
     * Get the currently logged in user. The name of
     * the currently logged in user may be different
     * from that of the user effectively running the 
     * currently executing program.
     * @return the current user
     * @throws ::std::exception if some error occurred
     */
  public:
    static ::std::unique_ptr<User> currentUser() throws (::std::exception);

    /**
     * Get the id of the user in whose name the current program
     * is running. The effective user is normally the same as the
     * current user, but on Unix it is possible to execute programs
     * as a different user. If the operating system does not make
     * a distinction between the current user and the effective user,
     * then both function must return the same user.
     * @return the effective user
     * @throws ::std::exception if some error occurred
     */
  public:
    static ::std::unique_ptr<User> effectiveUser() throws (::std::exception);
    
    /**
     * Find a user by the specified name.
     * @param name the name of user
     * @return the user or a null pointer if not found
     * @throws ::std::exception if some error occurred
    */
  public:
    static ::std::unique_ptr<User> findUser(const ::std::string& name) throws (::std::exception);

    /**
     * Get the name of the user. Since user names are unique, this method
     * may be used to check for equality or for sorting purposes.
     * @return the name of the user
     */
  public:
    virtual ::std::string name() const throws() = 0;

    /**
     * Get the directory where documents are stored by default. 
     * On Unix-like systems, this is known as the home directory,
     * and on Windows it is the <em>Documents And Settings</em> 
     * directory.
     * @return the path to the user's home directory or an empty string if not available
     */
  public:
    virtual ::std::string documentRoot() const throws() = 0;
  };
  
  
}
#endif
