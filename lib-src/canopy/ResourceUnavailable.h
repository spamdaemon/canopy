#ifndef _CANOPY_RESOURCEUNAVAILABLE_H
#define _CANOPY_RESOURCEUNAVAILABLE_H

#include <canopy/canopy.h>
#include <stdexcept>

namespace canopy {

  /**
   * The unsupported exception is thrown by function and methods
   * that are not supported by the version of the operating
   * system.
   */
  class ResourceUnavailable : public ::std::runtime_error {

    /**
     * Create a new exception.
     * @param tryAgain if true then the operation that failed should be retried after some time.
     */
  public:
    ResourceUnavailable(bool tryAgain=false) throws ();

     /**
     * Create a new unsupported exception
     * @param msg a message
     * @param tryAgain if true then the operation that failed should be retried after some time.
     */
  public:
    ResourceUnavailable(::std::string msg, bool tryAgain=false) throws ();

    /** Destructor */
  public:
    ~ResourceUnavailable() throw() {}


    /**
     * Check if the operation that failed should be tried again after some time.
     * @return true if the operation should be tried again
     */
  public:
    inline bool tryAgain() const throws()
    { return _tryAgain; }

    /** The try again flag */
  private:
    bool _tryAgain;
  };
   
}

#endif
