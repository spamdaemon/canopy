#ifndef _CANOPY_BYTEORDER_H
#define _CANOPY_BYTEORDER_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <netinet/in.h>

namespace canopy {
  
  /**
   * Determine if this host is little endian or big endian. If this
   * host is big endian, then its host byte order is also network
   * byte order, otherwise they are different.
   * @return true if integers are in network byte order on this host
   */
  inline bool isBigEndian() throws()
  { return htons(0xff00)==0xff00; }

  /**
   * Convert an integer from host byte order into network byte order
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline UInt16 toNetworkByteOrder (UInt16 t) throws()
  { return static_cast<UInt16>(htons(static_cast<Int16>(t))); }
  
  /**
   * Convert an integer from host byte order into network byte order
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline Int16 toNetworkByteOrder (Int16 t) throws()
  { return htons(t); }
  
  /**
   * Convert an integer from network byte order into host byte order
   * @param t value in network byte order
   * @return a value in host byte order
   */
  inline UInt16 fromNetworkByteOrder (UInt16 t) throws()
  { return static_cast<UInt16>(ntohs(static_cast<Int16>(t))); }
  
  /**
   * Convert an integer from network byte order into host byte order
   * @param t value in network byte order
   * @return a value in host byte order
   */
  inline Int16 fromNetworkByteOrder (Int16 t) throws()
  { return ntohs(t); }
  
  /**
   * Convert an integer from host byte order into network byte order
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline UInt32 toNetworkByteOrder (UInt32 t) throws()
  { return static_cast<UInt32>(htonl(static_cast<UInt32>(t))); }
  
  /**
   * Convert an integer from host byte order into network byte order
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline Int32 toNetworkByteOrder (Int32 t) throws()
  { return htonl(t); }

  /**
   * Convert an integer from network byte order into host byte order
   * @param t value in network byte order
   * @return a value in host byte order
   */
  inline UInt32 fromNetworkByteOrder (UInt32 t) throws()
  { return static_cast<UInt32>(ntohl(static_cast<UInt32>(t))); }
  
  /**
   * Convert an integer from hnetwork byte order into host byte order
   * @param t value in network byte order
   * @return a value in host byte order
   */
  inline Int32 fromNetworkByteOrder (Int32 t) throws()
  { return ntohl(t); }

  /**
   * Convert an integer from host byte order into network byte order
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline UInt64 toNetworkByteOrder (UInt64 t) throws()
  {
    volatile UInt64 res=-1;
    unsigned char* bytes = reinterpret_cast<unsigned char*>(const_cast<UInt64*>(&res));
    bytes[0] = static_cast<unsigned char>((t >> 56) & 0xff);
    bytes[1] = static_cast<unsigned char>((t >> 48) & 0xff);
    bytes[2] = static_cast<unsigned char>((t >> 40) & 0xff);
    bytes[3] = static_cast<unsigned char>((t >> 32) & 0xff);
    bytes[4] = static_cast<unsigned char>((t >> 24) & 0xff);
    bytes[5] = static_cast<unsigned char>((t >> 16) & 0xff);
    bytes[6] = static_cast<unsigned char>((t >> 8) & 0xff);
    bytes[7] = static_cast<unsigned char>((t >> 0) & 0xff);
    
    return res;
  }

  /**
   * Convert an integer from network byte order into host byte order
   * @param t value in network byte order
   * @return a value in host byte order
   */
  inline UInt64 fromNetworkByteOrder (UInt64 t) throws()
  { 
    // need to use unsigned chars here to avoid problems with 
    // with sign extension
    unsigned char* bytes = reinterpret_cast<unsigned char*>(&t);
    UInt32 hi,lo;
    hi = 
      (bytes[0] << 24) | 
      (bytes[1] << 16) |
      (bytes[2] << 8)|
      (bytes[3] << 0);
    lo = 
      (bytes[4] << 24) | 
      (bytes[5] << 16) |
      (bytes[6] << 8)|
      (bytes[7] << 0);
    return (UInt64(hi)<<32) | lo;
  } 
  

  /**
   * Convert an integer from network byte order into host byte order
   * @param t value in network byte order
   * @return a value in host byte order
   */
  inline Int64 fromNetworkByteOrder (Int64 t) throws()
  { 
    return fromNetworkByteOrder(static_cast<UInt64>(t)); 
  }

   /**
   * Convert an integer from host byte order into network byte order
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline Int64 toNetworkByteOrder (Int64 t) throws()
  { 
    return toNetworkByteOrder(static_cast<UInt64>(t)); 
  }
  
  /**
   * Convert a floatig point number from host byte order into network byte order.
   * The number is treated like an integer (ULong64) and mapped to network byte order
   * like the corresponding integer.
   * @param t value in host byte order
   * @return a value in network byte order
   */
  inline double toNetworkByteOrder (double t) throws()
  { return reinterpret_type<double>(toNetworkByteOrder(reinterpret_type<UInt64>(t))); }

   /**
   * Convert a floatig point number from network byte order into host byte order.
   * The number is treated like an integer (UInt32) and mapped to host byte order
   * like the corresponding integer.
   * @param t value in network byte order
   * @return a value in host byte order
   */
 inline double fromNetworkByteOrder (double t) throws()
 { return reinterpret_type<double>(fromNetworkByteOrder(reinterpret_type<UInt64>(t))); }

   /**
   * Convert a floatig point number from host byte order into network byte order.
   * The number is treated like an integer (UInt32) and mapped to network byte order
   * like the corresponding integer.
   * @param t value in host byte order
   * @return a value in network byte order
   */
 inline float toNetworkByteOrder (float t) throws()
 { return reinterpret_type<float>(toNetworkByteOrder(reinterpret_type<UInt64>(t))); }

   /**
   * Convert a floatig point number from host byte order into network byte order.
   * The number is treated like an integer (UInt32) and mapped to network byte order
   * like the corresponding integer.
   * @param t value in network byte order
   * @return a value in host byte order
   */
 inline float fromNetworkByteOrder (float t) throws()
 { return reinterpret_type<float>(fromNetworkByteOrder(reinterpret_type<UInt32>(t))); }

}

#endif
