#ifndef _CANOPY_DEFINES_H
#define _CANOPY_DEFINES_H

#ifndef _CANOPY_COMPILER_H
#include <canopy/compiler.h>
#endif

/**
 * A simple function to stringify a defined value.
 * We use a helper function, so that macros
 * are recursively evaluated.
 * <p>
 * assume, that FOO is defined as BAR, then
 * __CANOPY_TO_STRING(FOO)=FOO
 * CANOPY_TO_STRING(FOO)=BAR
 */
#define __CANOPY_TO_STRING(x) #x
#define CANOPY_TO_STRING(x) __CANOPY_TO_STRING(x)

/**
 * With gcc we can use a macro to give the compiler a hint
 * about what the expected outcome of a comparison is.
 * The likely(cond) macro indicates that a condition is likely to be true,
 * whereas the unlikely(cond) indicates a condition is most likely false.
 * The macros are expected to be used in conditionals:
 * <code>
 *  if ( unlikely (x > 0) ) {
 *     ...
 *  }
 * </code>
 */
#if CANOPY_COMPILER == CANOPY_GCC
#define likely(cond) __builtin_expect(!!(cond), 1)
#define unlikely(cond) __builtin_expect(!!(cond), 0)
#else
#define likely(cond) (cond)
#define unlikely(cond) (cond)
#endif

/**
 * A macro that creates a string with the curent location.
 * @return a string literal with of the form &lt;file&gt;:&lt;line&gt;
 */
#define CANOPY_FILE_LOCATION CANOPY_FILE_LOCATION_HELPER(__FILE__,__LINE__)
#define CANOPY_FILE_LOCATION_HELPER(F,L) (F ":" CANOPY_TO_STRING(L))

/**
 * Declare that a method/function throws the specified exceptions.
 * This is using a GCC extension with variadic macro arguments
 */
#if CANOPY_EXCEPTION_SPECIFICATION == 1
#warning "C++ exception specs have been deprecated"
#define throws(...) throw( __VA_ARGS__ )
#else
#define throws(...) 
#endif

#endif
