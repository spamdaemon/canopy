#ifndef _CANOPY_FUNCTION_H
#define _CANOPY_FUNCTION_H

#ifndef _CANOPY_LAMBDA_H
#include <canopy/lambda.h>
#endif

#include <functional>

namespace canopy {
   namespace function {

      /**
       * Create a bound function.
       * @param obj an object
       * @param fn a function pointer
       * @return a function
       */
      template<typename T, class C, typename ... ARGS>
      static auto makeUnboundFunction(T (C::*)(ARGS...) &)
      {
         return ::std::function< T(ARGS...)>();
      }

      /**
       * Create a bound function.
       * @param obj an object
       * @param fn a function pointer
       * @return a function
       */
      template<typename T, class C, typename ... ARGS>
      static auto makeUnboundFunction(T (C::*)(ARGS...) const &)
      {
         return ::std::function< T(ARGS...)>();
      }

      /**
       * Wrap a function pointer as a std::function
       * @param fn a function pointer
       * @return a function
       */
      template<typename T, typename ... ARGS>
      static auto makeFunction(T (*f)(ARGS...))
      {
         return ::std::function< T(ARGS...)>(f);
      }

      /**
       * Wrap a lambda as a function.
       * @param lambda a lambda object
       * @return a function
       */
      template<class LAMBDA>
      static auto makeFunction(LAMBDA t)
      {
         return ::canopy::lambda::toFunction< LAMBDA>(t);
      }

      /**
       * Create a function that calls a given function and then changes
       * the return type using a conversion function.
       * @param fn the function to be invoked
       * @param conv a function that converts the return value of fn
       * @return a new function with the same parameters are fn, but conv's return type
       */
      template<typename T, typename ... ARGS, typename CONVERTER>
      inline static auto changeReturnType(::std::function< T(ARGS...)> fn, const CONVERTER& conv)
      {
         return [fn,conv] (ARGS... args) {return conv(fn(args...));};
      }

   }

}

#endif
