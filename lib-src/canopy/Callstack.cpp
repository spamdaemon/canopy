#include <canopy/Callstack.h>
#include <canopy/compiler.h>
#include <sstream>
#include <iostream>

#if CANOPY_HAVE_LIBUNWIND == 1
#include <libunwind.h>
#endif

using namespace ::std;

namespace canopy {
   namespace {
#if CANOPY_HAVE_LIBUNWIND == 1

      static ::std::string captureStack()
      {
         ostringstream traceBuf;

         unw_context_t ctx[1];
         unw_cursor_t cursor[1];
         int ok = unw_getcontext(ctx);
         if (ok != 0) {
            goto ERROR;
         }

         ok = unw_init_local(cursor, ctx);
         if (ok != 0) {
            goto ERROR;
         }

         char buf[4096];

         // step out twice
         ok = unw_step(cursor);
         if (ok <= 0) {
            goto ERROR;
         }

         while ((ok = unw_step(cursor)) > 0) {
            unw_word_t offp;
            ok = unw_get_proc_name(cursor, buf, sizeof(buf) - 1, &offp);
            if (ok != 0) {
               if (ok == UNW_ENOMEM) {
                  buf[sizeof(buf) - 1] = '\0';
               }
               else {
                  break;
               }
            }
            traceBuf << buf << endl;
         }
         if (ok != 0) {
            goto ERROR;
         }
         return traceBuf.str();
         ERROR: return "Error obtaining the callstack";
      }
#endif

      struct CallStackImpl : public Callstack
      {
            CallStackImpl(::std::string&& trace)
                  : _trace(::std::move(trace))
            {
            }

            ~CallStackImpl() throws()
            {
            }

            void print(::std::ostream& out) const throws()
            {
               out << _trace << endl;
            }

         private:
            ::std::string _trace;
      };

   }

   Callstack::Callstack() throws()
   {
   }

   Callstack::~Callstack() throws()
   {
   }

   ::std::unique_ptr< Callstack> Callstack::create() throws()
   {
      ::std::string trace;
#if CANOPY_HAVE_LIBUNWIND == 1
      trace = captureStack();
#endif
      ::std::unique_ptr< CallStackImpl> stk(new CallStackImpl(::std::move(trace)));
      return ::std::unique_ptr< Callstack>(::std::move(stk));
   }

   bool Callstack::isSupported() throws()
   {
#if CANOPY_HAVE_LIBUNWIND == 1
      return true;
#else
      return false;
#endif
   }

   CANOPY_BOILERPLATE_DEFINE_PRINT(Callstack,{})

}

