#ifndef _CANOPY_FS_TEMPFILE_H
#define _CANOPY_FS_TEMPFILE_H

#ifndef _CANOPY_FS_FILESYSTEM_H
#include <canopy/fs/FileSystem.h>
#endif

#include <string>
#include <iosfwd>

namespace canopy {
   namespace fs {

      /**
       * A temporary file is, as the name suggests a temporary file. Temporary files are regular
       * files, by they are automatically deleted once they go out of scope. To prevent a temporary
       * file to be deleted, it must either be renamed or fixed.
       */
      class TempFile
      {
            /** Copying is not allowed */
         private:
            TempFile(const TempFile&);
            TempFile&operator=(const TempFile&);

            /**
             * Create a temp file.
             * @throw std::exception if the file could not be created
             */
         public:
            TempFile();

            /**
             * Destroy this file lock. If the file also locked, then it is unlocked first.
             * The lockfile is also removed.
             */
         public:
            ~TempFile() throws();

            /**
             * Get the full path of this temp file. Do not change the name of the file.
             * @return the name of this temp file.
             */
         public:
            inline const ::std::string& path() const throws() { return _name; }

            /**
             * Open an input stream.
             * @param m the openmode
             * @return an input stream for the file or 0 if the file was already deleted
             */
         public:
            ::std::unique_ptr< ::std::istream> inputStream(
                  ::std::ios_base::openmode m = ::std::ios_base::in) throws();

            /**
             * Open an output stream.
             * @param m the openmode
             * @return an input stream for the file or 0 if the file was already deleted
             */
         public:
            ::std::unique_ptr< ::std::ostream> outputStream(
                  ::std::ios_base::openmode m = ::std::ios_base::out | ::std::ios_base::trunc) throws();

            /**
             * Fix this tempfile. Once fixed, it will no longer be deleted automatically.
             */
         public:
            inline void fix() throws()
            {
               _fixed = true;
            }

            /**
             * Rename this temp file to another name. If the file was succesfully renamed,
             * then it is also fixed.
             * @param name another file name
             * @return true if the file was renamed, false otherwise
             */
         public:
            bool rename(const ::std::string& name) throws();

            /**
             * Remove this temp file.
             * @param name another file name
             * @return true if the file was removed, false otherwise
             */
         public:
            bool remove() throws();

            /** The file */
         private:
            ::std::string _name;

            /** True if the file has been fixed or renamed */
         private:
            bool _fixed;

            /** The file system on which this file was created */
         private:
            FileSystem _fs;
      };

   }
}

#endif
