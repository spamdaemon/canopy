#include <canopy/fs/NativeFileSystem.h>
#include <canopy/SystemError.h>
#include <canopy/time/Time.h>

#include <cstring>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <cerrno>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <utime.h>

using namespace ::std;

namespace canopy {
   namespace fs {

      namespace {

         static NativeFileSystem::SearchFilter DEFAULT_FILTER = [] (const char*,const char*) {return true;};

         static void closeFD(int fd)
         {
            while (true) {
               int ok = ::close(fd);
               if (ok == 0) {
                  break;
               }
               if (errno != EINTR) {
                  assert(errno!=EBADF);
                  throw SystemError("Could not close newly created file descriptor");
               }
            }
         }

         static bool setCloseOnExec(int fd)
         {
            int flags = ::fcntl(fd, F_GETFD);
            if (flags == -1) {
               return false;
            }
            flags |= FD_CLOEXEC;
            return ::fcntl(fd, F_SETFD, flags) == 0;
         }

         class LocalNativeFileSystem : public NativeFileSystem
         {
               /**
                * Create a new local file system instance
                */
            public:
               LocalNativeFileSystem()
               {
               }

               /**
                * Destroy this file system
                */
            public:
               ~LocalNativeFileSystem() throws()
               {
               }

            public:
               void sync() const throws()
               {
                  ::sync();
               }

            public:
               bool createFile(const string& path)
               {
                  int fd = ::open(path.c_str(), O_CREAT | O_EXCL, S_IRWXU);
                  if (fd != -1) {
                     closeFD(fd);
                     return true;
                  }

                  if (errno != EEXIST) {
                     throw SystemError("Could not create the file " + path);
                  }
                  return false;
               }

               string createTempFile(const string& prefix, const string& suffix)
               {
                  string fileTemplate(prefix);
                  fileTemplate += "XXXXXX";
                  fileTemplate += suffix;

                  char buf[fileTemplate.length() + 1];
                  buf[fileTemplate.length()] = '\0';
                  fileTemplate.copy(buf, fileTemplate.length(), 0);
                  assert(buf[fileTemplate.length()] == '\0' && "BUG: stack corruption");

                  int fd = -1;
                  if (suffix.empty()) {
                     fd = ::mkstemp(buf);
                  }
                  else {
                     fd = ::mkstemps(buf, suffix.length());
                  }
                  if (fd != -1) {
                     closeFD(fd);
                     return string(buf, buf + fileTemplate.length());
                  }
                  throw SystemError("Could not create the temp file " + fileTemplate);
               }

            public:
               UInt64 timeStampAccuracy() const throws ()
               {
                  // most file systems on linux anyways don't support
                  // high-res timestamps; 1 second is the default
                  return 1000000000; // 1 second resolution by default
               }

            public:
               void setTime(const string& path, const ::canopy::time::Time& tm)
               {
                  struct timeval buf[2];
                  buf[1].tv_sec = buf[0].tv_sec = tm.seconds();
                  buf[1].tv_usec = buf[0].tv_usec = tm.nanoseconds() / 1000;
                  if (::utimes(path.c_str(), buf) != 0) {
                     throw SystemError("setTime failed " + path);
                  }
               }

            public:
               ::canopy::time::Time accessTime(const string& path) const throws(exception)
               {
                  struct stat64 buf[1];
                  int ok = ::stat64(path.c_str(), buf);
                  if (ok != 0) {
                     throw SystemError("accessTime failed " + path);
                  }
                  UInt64 aTime = buf[0].st_atime;
                  aTime *= 1000000000;
                  aTime += buf[0].st_atim.tv_nsec;
                  return ::canopy::time::Time(aTime);
               }

               ::canopy::time::Time modificationTime(const string& path) const throws(exception)
               {
                  struct stat64 buf[1];
                  int ok = ::stat64(path.c_str(), buf);
                  if (ok != 0) {
                     throw SystemError("modificationTime failed " + path);
                  }
                  UInt64 mTime = buf[0].st_mtime;
                  mTime *= 1000000000;
                  mTime += buf[0].st_mtim.tv_nsec;
                  return ::canopy::time::Time(mTime);
               }

            public:
               ::canopy::time::Time timeOfLastChange(const string& path) const throws(exception)
               {
                  struct stat64 buf[1];
                  int ok = ::stat64(path.c_str(), buf);
                  if (ok != 0) {
                     throw SystemError("modificationTime failed " + path);
                  }
                  UInt64 mTime = buf[0].st_mtime;
                  mTime *= 1000000000;
                  mTime += buf[0].st_mtim.tv_nsec;

                  UInt64 cTime = buf[0].st_ctime;
                  cTime *= 1000000000;
                  cTime += buf[0].st_ctim.tv_nsec;

                  return ::canopy::time::Time(max(mTime, cTime));
               }

            public:
               string directorySeparator() const throws()
               {
                  return "/";
               }

            public:
               bool isAbsolutePath(const string& path) const throws()
               {
                  return !path.empty() && path[0] == '/';
               }

            public:
               string getRelativePath(const string& base, const string& dest) const throws (exception)
               {
                  string absBase(normalizePath(getAbsolutePath(base)));
                  string absDest(normalizePath(getAbsolutePath(dest)));

                  if (absBase[absBase.length() - 1] != '/') {
                     absBase += '/';
                  }
                  string path;
                  while (absDest.find(absBase) != 0) {
                     absBase = directoryName(absBase);
                     if (absBase[absBase.length() - 1] != '/') {
                        absBase += '/';
                     }
                     if (!path.empty()) {
                        path += '/';
                     }
                     path += '.';
                     path += '.';
                  }
                  if (absDest == absBase) {
                     if (path.empty()) {
                        path += '.';
                     }
                  }
                  else {
                     if (!path.empty()) {
                        path += '/';
                     }
                     path += absDest.substr(absBase.length());
                  }
                  if (*dest.rbegin() == '/' && *path.rbegin() != '/') {
                     path += '/';
                  }
                  return path;
               }

            public:
               string getAbsolutePath(const string& path) const throws(::std::exception)
               {
                  if (path.empty()) {
                     return workingDirectory();
                  }
                  string np(path);
                  if (path[0] != '/') {
                     np = workingDirectory() + '/' + path;
                  }
                  return np;
               }

            public:
               string normalizePath(const string& p) const
               {
                  if (p == "/.." || p == "/.") {
                     return "/";
                  }
                  if (p == ".") {
                     return p;
                  }

                  string path(p);

                  // replace // with /
                  for (string::size_type pos = 0, npos = 0; (npos = path.find("//", pos)) != string::npos; pos = npos) {
                     path.erase(npos, 1);
                  }

                  // replace /./ with /
                  for (string::size_type pos = 0, npos = 0; (npos = path.find("/./", pos)) != string::npos; pos =
                        npos) {
                     path.erase(npos, 2);
                  }
                  // remove a leading ./
                  if (path.length() > 1 && path.find("./") == 0) {
                     path.erase(0, 2);
                  }

                  string prefix;
                  // if the string now starts with a sequence of [../]+ then remember that prefix
                  // and prepend it to the returned string
                  while (true) {
                     if (path.find("../") == 0) {
                        path.erase(0, 3);
                        prefix += "../";
                     }
                     else {
                        string::size_type pos = 0;
                        string::size_type npos = path.find("/../", pos);
                        if (npos == string::npos) {
                           break;
                        }

                        // find the preceding slash
                        if (npos == 0) {
                           path.erase(pos, 3);
                        }
                        else {
                           pos = path.find_last_of('/', npos - 1);
                           if (pos == string::npos) {
                              path.erase(0, npos + 4);
                           }
                           else {
                              ++pos;
                              path.erase(pos, npos + 4 - pos);
                           }
                        }
                     }
                  }

                  // replace y/.. with <empty> start at the end of the string

                  string::size_type len = path.length();
                  if (len > 2) {
                     string::size_type npos = path.rfind("/..", len - 1);
                     if (npos == len - 3) {
                        if (npos == 0) {
                           path = "/";
                        }
                        else {
                           // find the preceding slash
                           string::size_type pos = path.find_last_of('/', npos - 1);
                           if (pos == string::npos) {
                              path.clear();
                           }
                           else {
                              path.erase(pos + 1);
                           }
                        }
                        len = path.length();
                     }
                  }

                  path.insert(0, prefix);
                  len = path.length();

                  // remove trailing /
                  if (len > 1 && path[len - 1] == '/') {
                     path.erase(len - 1, 1);
                     len = path.length();
                  }

                  // remove a trailing /. , but if the string equals "/.", the leave /
                  if (len > 1) {
                     if (path.rfind("/.", len - 2) == len - 2) {
                        if (len == 2) {
                           path = "/";
                        }
                        else {
                           path.erase(len - 2, 2);
                        }
                        len = path.length();
                     }
                  }
                  if (path.empty()) {
                     path += '.';
                  }
                  return path;
               }

            public:
               void rename(const string& oldName, const string& newName) throws (exception)
               {
                  int ok = ::rename(oldName.c_str(), newName.c_str());
                  if (ok != 0) {
                     throw SystemError("rename failed");
                  }
               }

            public:
               bool createAlias(const string& oldFile, const string& newFile) throws (exception)
               {
                  int ok = ::link(oldFile.c_str(), newFile.c_str());
                  if (ok == EEXIST) {
                     return false;
                  }
                  if (ok == 0) {
                     // need to stat the file to ensure it really was created
                     struct stat buf[1];
                     ok = ::stat(newFile.c_str(), buf);
                     if (ok == 0) {
                        return true;
                     }
                  }
                  if (ok != 0) {
                     throw SystemError("alias failed");
                  }
                  return true;
               }

            public:
               bool createSymbolicLink(const string& oldName, const string& newName) throws (exception)
               {
                  int ok = ::symlink(oldName.c_str(), newName.c_str());
                  if (ok != 0) {
                     if (ok == EEXIST) {
                        return false;
                     }
                     throw SystemError("symbolic link failed");
                  }
                  return true;
               }

            public:
               string findLinkTarget(const string& linkFile) const throws (exception)
               {
                  // using lstat, we should be able to get at the length of the file

                  // give it a quick try with a local buffer
                  {
                     char buf[128];
                     int n = ::readlink(linkFile.c_str(), buf, sizeof(buf));
                     if (n < 0) {
                        throw SystemError("findLinkTarget failed " + linkFile);
                     }
                     if (n < (int) sizeof(buf)) {
                        buf[n] = '\0';
                        return string(buf);
                     }
                  }

                  // link target is at least 128 bytes long; need to loop to find the
                  // full path name
                  int bufSize = 256;
                  char* buf = new char[bufSize];
                  int n = 0;
                  while (true) {
                     n = ::readlink(linkFile.c_str(), buf, bufSize);
                     if (n < bufSize) {
                        break;
                     }
                     delete[] buf;
                     bufSize *= 2;
                     buf = new char[bufSize];
                  }
                  if (n < 0) {
                     delete[] buf;
                     throw SystemError("findLinkTarget failed " + linkFile);
                  }
                  buf[n] = '\0';
                  string res(buf);
                  delete[] buf;
                  return res;
               }

               string followLinks(const string& linkFile) const throws (exception)
               {
                  // probably there's a more efficient way to handle this, but for now....
                  string file(linkFile);
                  while (isSymbolicLink(file)) {
                     string tgt = findLinkTarget(file);
                     if (isAbsolutePath(tgt)) {
                        file.swap(tgt);
                     }
                     else {
                        file += "/../";
                        file += tgt;
                        file = normalizePath(file);
                     }
                  }
                  return file;
               }

            public:
               void mkdir(const string& dir) throws (exception)
               {
                  int status = ::mkdir(dir.c_str(), 0777);
                  if (status != 0) {
                     throw SystemError("mkdir failed");
                  }
               }

            public:
               void mkdirs(const string& dir) throws (exception)
               {
                  string p = normalizePath(dir);

                  string prefix;
                  while (p.find("../", 0) == 0) {
                     prefix += "../";
                     p.erase(0, 3);
                  }
                  if (p == "..") {
                     throw runtime_error("Cannot create directory ..");
                  }

                  for (string::size_type npos = 0; (npos = p.find("/", npos)) != string::npos; ++npos) {
                     string path(p, 0, npos);
                     path.insert(0, prefix);
                     if (!path.empty() && !exists(path, false)) {
                        mkdir(path);
                     }
                  }
                  if (!exists(dir, false)) {
                     mkdir(dir);
                  }
               }

               string joinPaths(const string& path1, const string& path2) const throws()
               {
                  string res;
                  res.reserve(1 + path1.length() + path2.length());
                  res += path1;
                  res += '/';
                  res += path2;
                  return res;
               }

               string directoryName(const string& path) const throws()
               {
                  string dir;
                  string::size_type posEnd = path.find_last_not_of('/');
                  if (posEnd == string::npos) {
                     dir = '/';
                  }
                  else {
                     string::size_type pos = path.rfind('/', posEnd);
                     if (pos != string::npos) {
                        string(path, 0, pos).swap(dir);
                        pos = dir.find_last_not_of('/');
                        dir.erase(pos + 1);
                     }
                  }
                  return dir;
               }

               string baseName(const string& path) const throws()
               {
                  string res;
                  string::size_type posEnd = path.find_last_not_of('/');
                  if (posEnd == string::npos) {
                     res = "/";
                  }
                  else {
                     string::size_type posBegin = path.rfind('/', posEnd);
                     if (posBegin == string::npos) {
                        posBegin = 0;
                     }
                     else {
                        ++posBegin;
                     }
                     path.substr(posBegin, 1 + posEnd - posBegin).swap(res);
                  }
                  return res;
               }

            public:
               UInt32 listEntries(const string& dir, vector< string>& entries,
                     SearchFilter& filter) const throws (exception)
               {
                  UInt32 n = 0;

                  ::DIR* stream = ::opendir(dir.c_str());
                  if (stream == 0) {
                     throw SystemError("Could not open directory " + dir);
                  }

                  char* buf = 0;

                  try {

                     // get the maximum length of the directory entry
                     long sz(-1);
                     {
#if CANOPY_HAVE_DIRFD == 1
                        // we need to compute the maximum size that path entry can be
                        // so that we can use readdir_r correctly. We need to use fpathconf
                        // rather than using pathconf, in order to avoid a possible
                        // security problem we'll be using fpathconf and thus need
                        // to obtain the stream's file descriptor.
                        // The security problem is related to a race-condition involving
                        // symbolic links across filesystems with different maximum
                        // file name lengths.
                        // For more details, see
                        // http://womble.decadentplace.org.uk/readdir_r-advisory.html
                        int dirFD = ::dirfd(stream);
                        if (dirFD == -1) {
                           throw SystemError("Could not get the directory file descriptor ");
                        }
                        if (!setCloseOnExec(dirFD)) {
                           throw SystemError("Could not set FD_CLOEXEC");
                        }

                        errno = 0; // reset
#if defined(_PC_NAME_MAX)
                        sz = ::fpathconf(dirFD, _PC_NAME_MAX);
#elif defined(NAME_MAX) 
                        sz = ::fpathconf(dirfD,NAME_MAX);
                        sz = max(sz,256);
#endif
#else
#warning "NativeFileSystem::listEntries implementation is potentially unsafe; #define CANOPY_HAVE_DIRFD 1 to make safe"
                        errno = 0;
#if defined(_PC_NAME_MAX)
                        sz = ::pathconf(dir.c_str(),_PC_NAME_MAX);
#elif defined(NAME_MAX) 
                        sz = ::pathconf(dir.c_str(),NAME_MAX);
                        sz = max(sz,256);
#endif

#endif
                        if (sz < 0) {
                           if (errno == 0) {
                              throw runtime_error("Could not obtain file name limit on filesystem for " + dir);
                           }
                           throw SystemError("Could not obtain _PC_NAME_MAX");
                        }
                     }

                     // it is possible to compute a smaller size for this value,
                     // but it probably doesn't matter
                     buf = new char[sz + sizeof(struct ::dirent)];

                     const string root = dir + directorySeparator();
                     struct ::dirent* entry = reinterpret_cast< struct ::dirent*>(buf);
                     struct ::dirent* result;
                     int status(0);
                     errno = 0; // reset errno to 0
                     while ((status = ::readdir_r(stream, entry, &result)) == 0 && result != 0) {
                        // skip the entries .. and . on Unix
                        if (entry->d_name[0] == '.') {
                           if (strcmp("..", entry->d_name) == 0 || strcmp(".", entry->d_name) == 0) {
                              continue;
                           }
                        }
                        if (filter(dir.c_str(), entry->d_name)) {
                           entries.push_back(root + entry->d_name);

                           // just in case, check for an empty file name
                           if (entries.back().empty()) {
                              entries.pop_back();
                           }
                        }

                        ++n;
                     }

                     if (status != 0) {
                        throw SystemError("Could not read directory entry from " + dir);
                     }

                  }
                  catch (const ::std::runtime_error&) {
                     // make sure the directory is closed
                     int ok = closedir(stream);
                     delete[] buf;

                     if (ok < 0) {
                        cerr << "System error : " << getSystemError("Could not close directory stream");
                     }
                     throw;
                  }

                  int ok = closedir(stream);
                  delete[] buf;
                  stream = 0;
                  if (ok < 0) {
                     throw SystemError("Could not close directory stream " + dir);
                  }
                  return n;
               }

            public:
               void rmdir(const string& file, bool recurse) throws (exception)
               {
                  if (!recurse) {
                     if (!isDirectory(file, false)) {
                        throw runtime_error("rmdir failed: not a directory");
                     }
                     remove(file);
                     return;
                  }

                  vector< string> files;
                  try {
                     files.push_back(file);
                     while (!files.empty()) {
                        string cur = files.back();
                        if (!isSymbolicLink(cur) && isDirectory(cur, false)) {
                           // if a directory, then get all files, but if the
                           // the directory is empty, then we remove it
                           if (listEntries(cur, files, DEFAULT_FILTER) == 0) {
                              remove(cur);
                              files.pop_back();
                           }
                        }
                        else {
                           // cur is not a directory, so try to remove it
                           remove(cur);
                           files.pop_back();
                        }
                     }
                  }
                  catch (const runtime_error& e) {
                     throw runtime_error(string("rmdir failed: ") + e.what());
                  }
                  catch (...) {
                     throw runtime_error("rmdir failed; file system state may be inconsistent");
                  }
               }

            public:
               bool remove(const string& file) throws (exception)
               {

                  int ok;
                  bool result = !isDirectory(file, false);
                  if (result) {
                     ok = ::unlink(file.c_str());
                  }
                  else {
                     ok = ::rmdir(file.c_str());
                  }

                  if (ok != 0) {
                     throw SystemError("remove failed :" + file);
                  }
                  return result;
               }

            public:
               bool exists(const string& file, bool followSymLink) const throws (exception)
               {
                  struct stat buf[1];
                  int ok;
                  if (followSymLink) {
                     ok = stat(file.c_str(), buf);
                  }
                  else {
                     ok = ::lstat(file.c_str(), buf);
                  }
                  return ok == 0;
               }

            public:
               bool isFile(const string& file, bool followSymLink) const throws (exception)
               {
                  struct stat buf[1];
                  int ok;
                  if (followSymLink) {
                     ok = stat(file.c_str(), buf);
                  }
                  else {
                     ok = ::lstat(file.c_str(), buf);
                  }

                  if (ok != 0) {
                     throw SystemError("isFile failed " + file);
                  }
                  return S_ISREG(buf[0].st_mode);
               }

            public:
               bool hasPermissions(const string& file, int flags) const throws (exception)
               {
                  int accessFlags = 0;
                  if (flags & NativeFileSystem::READ) {
                     accessFlags |= R_OK;
                  }
                  if (flags & NativeFileSystem::WRITE) {
                     accessFlags |= W_OK;
                  }
                  if (flags & NativeFileSystem::EXECUTE) {
                     accessFlags |= X_OK;
                  }
                  if (flags & ~(NativeFileSystem::EXECUTE | NativeFileSystem::WRITE | NativeFileSystem::READ)) {
                     throw ::std::invalid_argument("Invalid flags specified");
                  }
                  if (accessFlags == 0) {
                     return true;
                  }
                  int res = ::access(file.c_str(), accessFlags);
                  if (res < 0) {
                     if (errno == EACCES) {
                        return false;
                     }
                     throw SystemError("hasPermissions failed " + file);
                  }
                  return true;
               }

            public:
               bool isSymbolicLink(const string& path) const throws (exception)
               {
                  struct stat buf[1];
                  int ok = ::lstat(path.c_str(), buf);
                  if (ok != 0) {
                     throw SystemError("isSymbolicLink failed " + path);
                  }
                  return S_ISLNK(buf[0].st_mode);
               }

            public:
               bool isDirectory(const string& dir, bool followSymLink) const throws (exception)
               {
                  struct stat buf[1];
                  int ok;
                  if (followSymLink) {
                     ok = stat(dir.c_str(), buf);
                  }
                  else {
                     ok = ::lstat(dir.c_str(), buf);
                  }
                  if (ok != 0) {
                     throw SystemError("isDirectory failed " + dir);
                  }
                  return S_ISDIR(buf[0].st_mode);
               }

               string workingDirectory() const throws(exception)
               {
                  string res;
                  {
                     char buf[128];
                     char* dir = ::getcwd(buf, sizeof(buf));
                     if (dir != 0) {
                        res = dir;
                        return res;
                     }
                     if (errno != ERANGE) {
                        throw SystemError("Could not obtain current working directory");
                     }
                  }

                  // need a bigger buffer
                  {
                     int n = 256;
                     char* buf = new char[n];
                     while (true) {
                        char* dir = ::getcwd(buf, n);
                        if (dir != 0) {
                           break;
                        }
                        delete[] buf;
                        if (errno != ERANGE) {
                           throw SystemError("Could not obtain current working directory");
                        }
                        n *= 2;
                        buf = new char[n];
                     }
                     res = buf;
                     delete[] buf;
                     return res;
                  }
               }

               void setWorkingDirectory(const string& newDir) throws(::std::exception)
               {
                  int ok = ::chdir(newDir.c_str());
                  if (ok != 0) {
                     throw SystemError("Could not change working directory to " + newDir);
                  }
               }

         };
      }

      constexpr int NativeFileSystem::READ;
      constexpr int NativeFileSystem::WRITE;
      constexpr int NativeFileSystem::EXECUTE;

      NativeFileSystem::NativeFileSystem() throws()
      {
      }

      NativeFileSystem::~NativeFileSystem() throws()
      {
      }

      unique_ptr< NativeFileSystem> NativeFileSystem::open() throws(exception)
      {
         NativeFileSystem* fs = new LocalNativeFileSystem();

         return unique_ptr< NativeFileSystem>(fs);
      }

   }
}
