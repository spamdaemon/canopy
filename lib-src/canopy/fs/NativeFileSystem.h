#ifndef _CANOPY_FS_NATIVEFILESYSTEM_H
#define _CANOPY_FS_NATIVEFILESYSTEM_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <functional>
#include <memory>
#include <vector>
#include <string>
#include <stdexcept>

namespace canopy {
   namespace time {
      class Time;
   }
   namespace fs {

      /**
       * This class represents a file system.
       */
      class NativeFileSystem
      {
            /** A const permission flag */
         public:
            static constexpr int READ = 1;
            /** A const permission flag */
         public:
            static constexpr int WRITE = 2;

            /** A const permission flag */
         public:
            static constexpr int EXECUTE = 4;

            /** A filter filter function */
         public:
            typedef ::std::function< bool(const char* dir, const char* file)> SearchFilter;

            /**
             * Default constructor
             */
         protected:
            NativeFileSystem() throws ();

            /**
             * Destructor of this file system object. The actual
             * file system is not destroyed, of course.
             */
         public:
            virtual ~NativeFileSystem() throws () = 0;

            /**
             * Get an instance of a file system. This function may return
             * a new instance object every time it is invoked!
             * @return return a file system
             */
         public:
            static ::std::unique_ptr< NativeFileSystem> open() throws (::std::exception);

            /**
             * Schedule a file system synchronization with the storage system. Data buffered
             * by the OS will be scheduled to be written to disk or whatever storage medium
             * is represented by this FileSystem.
             * <p>
             * No guarantees are made that the buffer contents have indeed been sent from the
             * OS to the storage medium when this method returns. Furthermore, large disk caches
             * may also delay synchronization beyond what is controllable by the underlying OS.
             */
         public:
            virtual void sync() const = 0;

            /**
             * Create a file if it does not already exist. On unix, this is implementation
             * via <code>open(path.c_str(),O_CREAT|O_EXCL,S_IRWXU)</code>.
             * @param path a file name
             * @return true if the file was created, false if it already existed
             */
         public:
            virtual bool createFile(const ::std::string& path) = 0;

            /**
             * Create a temporary file on disk. If the file could not be created, then an exception is thrown
             * @param prefix the file name prefix (optional if empty)
             * @param suffix the file name suffix (optional if empty)
             * @return the name of the temporary file
             * @throws ::std::runtime_exception if the file cannot be created
             */
         public:
            virtual ::std::string createTempFile(const ::std::string& prefix, const ::std::string& suffix) = 0;

            /**
             * Join two file names by the directory separator. No normalization is done.
             * @param path1 a path
             * @param path2 a path
             * @return path1+directorySeparator()+path2
             */
         public:
            virtual ::std::string joinPaths(const ::std::string& path1, const ::std::string& path2) const throws() = 0;

            /**
             * Get the directory name of the specified path. If the path is <tt>a/b/c</tt>,
             * then this method will return <tt>a/b</tt>.
             * @param path a path.
             * @return the directory name
             */
         public:
            virtual ::std::string directoryName(const ::std::string& path) const throws() = 0;

            /**
             * Get the file name of the specified path. If the path is <tt>a/b/c</tt>,
             * then this method will return <tt>c</tt>.
             * @param path a path.
             * @return the file part
             */
         public:
            virtual ::std::string baseName(const ::std::string& path) const throws() = 0;

            /**
             * @name Time stamps
             * @{
             */

            /**
             * Get the accuracy of the file times in nanoseconds. This method should
             * never return a value smaller than the actual time, but may return a larger value.
             * <br>
             * Accuracy is usually 1 second on Linux at the present time (2007).
             * @return the accuracy of file times.
             * @note that this resolution is dependent upon the actual filesystem.
             */
         public:
            virtual UInt64 timeStampAccuracy() const throws () = 0;

            /**
             * Set the modification and access time for the specified file. Setting the time
             * may modify the timeOfLastChange() value and set it to the current time.
             *
             * @param path a path a file path
             * @param tm the new modification time.
             */
         public:
            virtual void setTime(const ::std::string& path, const ::canopy::time::Time& tm) = 0;

            /**
             * Get the time of the most recent access to the specifie path.
             * @param path a path
             * @return the last access time for the file
             * @throws ::std::exception if the file does not exist
             */
         public:
            virtual ::canopy::time::Time accessTime(const ::std::string& path) const = 0;

            /**
             * Get the most recent modification time of the specified path. This method
             * will return the time the content has changed as opposed to other changes
             * that might have occurred to the path.
             * @param path a path
             * @return the timestamp for the file
             * @throws ::std::exception if the file does not exist
             */
         public:
            virtual ::canopy::time::Time modificationTime(const ::std::string& path) const = 0;

            /**
             * Get the time of last change to the object pointed to by path. This time
             * includes changes to the file's contents, but also changes to the meta data about the
             * file.
             * @param path a path
             * @return the the time that something about the file has changed
             */
         public:
            virtual ::canopy::time::Time timeOfLastChange(const ::std::string& path) const = 0;

            /*@}*/

            /**
             * Get the directory separator.
             * @return the separator for directoris on this file system
             */
         public:
            virtual ::std::string directorySeparator() const throws () = 0;

            /**
             * Test if the specified path is an absolute path.
             * @param path a path
             * @return true if the specified path is an absolute path
             */
         public:
            virtual bool isAbsolutePath(const ::std::string& path) const throws (::std::exception) = 0;

            /**
             * Get the absolute path name for the specified path.
             * @param path a path
             * @return the absoluate path for path
             */
         public:
            virtual ::std::string getAbsolutePath(const ::std::string& path) const throws (::std::exception) = 0;

            /**
             * Find the relative path from a base path to a dest path. If the given
             * base path is prepended by the relative path, then it will point
             * to the target path.
             * @param base a base path
             * @param dest the target path
             * @return a path p such that base + p = target
             */
         public:
            virtual ::std::string getRelativePath(const ::std::string& base,
                  const ::std::string& dest) const throws (::std::exception) = 0;

            /**
             * Normalize a path.
             * On this Unix system this will perform the following operations:
             * <ol>
             *  <li>replace <tt> // </tt> with <tt> / </tt>
             *  <li>replace <tt> /./ </tt> with <tt> / </tt>
             *  <li>remove a leading <tt> ./ </tt>
             *  <li>replace <tt> /../rem </tt> with <tt> /rem </tt>. Leading sequences of ../
             *  found will be prepended to the resulting string.
             *  <li>remove <tt> /y/../ </tt> with <tt> / </tt>
             *  <li>remove trailing <tt> x/.. </tt>
             *  <li>remove trailing <tt> /. </tt>
             *  <li>remove tailing <tt> / </tt>
             * </ol>
             * On Windows systems, this function will perform the equivalent of the Unix operations,
             * but will use the <tt> \ </tt> instead of <tt> / </tt>.
             * <p>
             * Some examples:
             * <ol>
             * <li><tt> /../b </tt>  -->  <tt> /b </tt>
             * <li><tt> a/b/./c  </tt>  -->  <tt> a/b/c </tt>
             * <li><tt> a/b/../c </tt>  -->  <tt> a/c </tt>
             * <li><tt> ./  </tt>  -->  <tt> . </tt>
             * <li><tt> /. </tt>  -->  <tt> / </tt>
             * <li><tt> ../../a/../../ </tt>  -->  <tt> ../../.. </tt>
             * <li><tt> a/../../ </tt>  -->  <tt> .. </tt>
             * <li><tt> a/b/c/ </tt>  -->  <tt> a/b/c </tt>
             * <li><tt> a/b/c/. </tt>  -->  <tt> a/b/c </tt>
             * </ol>
             * <p>
             * The path is not required to be a valid path for the underlying operating system,
             * but rather is a kind of virtual path. For example, if the path <tt>/home/user/.tcshrc</tt>
             * refers to a file, then the normalization of <tt>/home/user/.tcshrc/..</tt> will result
             * in <tt>/home/user</tt>, which would most likely not be supported by the file system.
             *
             * @note Under normal circumstances, a normalized path will not contain the special sequence
             * <tt> ../ </tt> but it can happen when relative paths are being used. See examples.
             * @param path a path
             * @return a normalized path, but never an empty path.
             */
         public:
            virtual ::std::string normalizePath(const ::std::string& path) const = 0;

            /**
             * Get the current working directory. Different instances of this class
             * must return the same working directory.
             * @return the current working directory.
             */
         public:
            virtual ::std::string workingDirectory() const throws (::std::exception) = 0;

            /**
             * Change the current working directory.
             * @param newDir the new working directory
             */
         public:
            virtual void setWorkingDirectory(const ::std::string& newDir) throws (::std::exception) = 0;

            /**
             * Rename a file or directory
             * @param oldName the original name
             * @param newName the new name
             * @return true if the file was renamed, false otherwise
             */
         public:
            virtual void rename(const ::std::string& oldName,
                  const ::std::string& newName) throws (::std::exception) = 0;

            /**
             * Create a new file that aliases an existing file. The path oldFile must refer
             * to an actual file and nothing else. Also, if a file newFile already exists,
             * then this method fails. Open either oldFile or newFile will refer to the same object.
             * @param oldFile the existing file
             * @param newFile the file name
             * @return true if the file was aliased, false if there was already newFile.
             * @note it is possible that this file system does not support creation of file aliases
             */
         public:
            virtual bool createAlias(const ::std::string& oldFile,
                  const ::std::string& newFile) throws (::std::exception) = 0;

            /**
             * Create a file newName which when traversed by the OS will be find linkTarget.
             * If there is already an object newName, then false will be returned.
             * <p>
             * It is not an error if linkTarget does not actually exist!
             * @param linkTarget the target to which the new file will refer.
             * @param newName the new file name
             * @return true if the symbolic link was created, false otherwise
             * @note it is possible that this file system does not support creation of symbolic links
             */
         public:
            virtual bool createSymbolicLink(const ::std::string& linkTarget,
                  const ::std::string& newName) throws (::std::exception) = 0;

            /**
             * Find the target of a symlink. If an actual object is linked via a chain of symlinks
             * starting with the specified linkFile, then only the next link in that chain will
             * be returned.
             * @param linkFile a symbolic link
             * @return the path to which the symbolic link refers.
             */
         public:
            virtual ::std::string findLinkTarget(const ::std::string& linkFile) const throws (::std::exception) = 0;

            /**
             * Follow a link target. This will find the actual file or directory to which a chain
             * of symbolic links, starting with the specified linkFile, leads. If linkFile not
             * a symbolic link, then linkFile is returned.
             * @param linkFile a link file
             * @return the file or directory which can be reached via a chain of symbolic links
             */
         public:
            virtual ::std::string followLinks(const ::std::string& linkFile) const throws (::std::exception) = 0;

            /**
             * Create a directory. This operation is atomic. This operation fails if the
             * parent directory of the path does not exist. For example, if the path is
             * a/b/c, but a or a/b do not exist, then this function fails. In such a case, use mkdirs()
             * instead.
             * @param path a directory
             */
         public:
            virtual void mkdir(const ::std::string& path) throws (::std::exception) = 0;

            /**
             * Create directories on the specified path. Given the input string
             * a/b/c with will ensure that all missing directories are created. The
             * path will be normalized before creating the directories. The following
             * path <tt> x/y/../z </tt> would thus create these directories <tt> x/z </tt>. Trying to create
             * directories such as <tt> ../.. </tt> is not permitted and will throw an exception.
             * @param path a directory
             * @note this method is <em>NOT</em> atomic and if it fails may leave created directories behind.
             */
         public:
            virtual void mkdirs(const ::std::string& path) throws (::std::exception) = 0;

            /**
             * List all files in a directory. On Unix systems, the
             * two special entries <tt> .. </tt> and <tt> . </tt> will not
             * be inserted into the vector.
             * This operation is not atomic.
             * @param dir a directory
             * @param entries a vector to which entries will be appended.
             * @param filter a search filter
             * @return a list of files
             */
         public:
            virtual UInt32 listEntries(const ::std::string& dir, ::std::vector< ::std::string>& entries,
                  SearchFilter& filter)const  throws (::std::exception) = 0;

            /**
             * Remove a directory recursively. This operation is not atomic.
             * @param dir a directory
             * @param recurse if true, then delete all entries in the directory recursively
             */
         public:
            virtual void rmdir(const ::std::string& dir, bool recurse) throws (::std::exception) = 0;

            /**
             * Delete a file or directory. If the file is a directory, then the directory
             * must be empty. This operation is atomic.
             * @param file or directory
             * @return true if the deleted entry was a file or symlink, false if it was a directory
             */
         public:
            virtual bool remove(const ::std::string& file) throws (::std::exception) = 0;

            /**
             * Test if the specified filesystem entry exists.
             * @param entry a filesystem entry
             * @param followSymLink if true then follow symlinks
             * @return true if the entry exists
             */
         public:
            virtual bool exists(const ::std::string& entry, bool followSymLink = true) const  throws (::std::exception) = 0;

            /**
             * Determine if the specified file is accessible. The definition of accessible
             * is implementation defined.
             * @param file a path to a file
             * @param flags to be checked (or of EXECUTE,READ,WRITE)
             * @return true if all the permissions are granted, false otherwise
             * @note This operation is not atomic
             */
         public:
            virtual bool hasPermissions(const ::std::string& file, int flags) const throws (::std::exception) = 0;

            /**
             * Test if the specified path leads to a file.  If the file
             * is actually a symbolic link, then this method returns false.
             * @param file a path to a file
             * @param followSymLink if true then follow symlinks
             * @return true if the path leads to a file or a symbolic link, false if it is not a file
             * @note This operation is atomic only if symbolic links are not followed.
             */
         public:
            virtual bool isFile(const ::std::string& file, bool followSymLink = true) const  throws (::std::exception) = 0;

            /**
             * Test if the specified path leads to a directory. If the dir
             * is actually a symbolic link, then this method returns false.
             * @param dir a path to a directory
             * @param followSymLink if true then follow symlinks
             * @return true if the path leads to a directory, false if it is not a directory
             * @note This operation is atomic only if symbolic links are not followed.
             */
         public:
            virtual bool isDirectory(const ::std::string& dir, bool followSymLink = true) const  throws (::std::exception) = 0;

            /**
             * Test if the specified path object is a symbolic link. Filesystems that do not
             * support symbolic links must return false.
             * @param path a path
             * @return true if the path leads to a symbolic link, false otherwise
             */
         public:
            virtual bool isSymbolicLink(const ::std::string& path) const throws (::std::exception) = 0;
      };
   }
}

#endif
