#ifndef _CANOPY_FS_LOCKFILE_H
#define _CANOPY_FS_LOCKFILE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <string>
#include <stdexcept>

namespace canopy {
  namespace fs {
    
    /**
     * A file can be locked via this class. The functions are similar to those of a
     * mutex, but work slightly differently. 
     */
    class LockFile {
      /** Copying is not allowed */
    private:
      LockFile(const LockFile&);
      LockFile&operator=(const LockFile&);
      
      /**
       * Create a file lock object for a given file. If the lockfile does not already
       * exist, then it created such that it can be accessed by any user. 
       * @param file a filename that may be locked with this lock
       * @param lockIt lock the file immediately
       */
    public:
      LockFile (const ::std::string& file, bool lockIt=true) throws (::std::runtime_error);

      /**
       * Destroy this file lock. If the file also locked, then it is unlocked first.
       * The lockfile is also removed.
       */
    public:
      ~LockFile() throws();

      /**
       * Lock this file. If the file is already locked by another process, then 
       * this method will suspend the caller until the file can be locked. If this
       * method returns successfully, then this file has been locked.
       * @throws ::std::runtime_error if the file could not be locked
       */
    public:
      void lock() const volatile throws (::std::runtime_error);
	
      /**
       * Try to lock this file. If the file is already locked by another process, then 
       * false is returned. If this method returns true, then this file has been locked.
       * @throws ::std::runtime_error if the file could not be locked
       */
    public:
      bool tryLock() const volatile throws (::std::runtime_error);
	
      /**
       * Unlock this file. If the file is not locked by this process then
       * an exception is thrown.
       * @throws ::std::runtime_error if the file could not be locked
       */
    public:
      void unlock() const volatile throws (::std::runtime_error);
	
      /** The file */
    private:
      const ::std::string _file;

      /** The lock */
    private:
      int _fd;

      /** True if this class has the lock */
    private:
      mutable bool _haveLock;
    };
    
  }
}

#endif
