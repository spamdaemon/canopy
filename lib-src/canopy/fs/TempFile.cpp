#include <canopy/fs/TempFile.h>
#include <fstream>

namespace canopy {
   namespace fs {

      TempFile::TempFile()
            : _fixed(false)
      {
         _name = _fs.getAbsolutePath(_fs.createTempFile("tmp",""));
      }

      TempFile::~TempFile() throws()
      {
         if (!_fixed) {
            _fs.remove(_name);
         }
      }

      ::std::unique_ptr< ::std::istream> TempFile::inputStream(::std::ios_base::openmode m) throws()
      {
         ::std::unique_ptr< ::std::istream> res(new ::std::ifstream(_name.c_str(), m));
         return move(res);
      }

      ::std::unique_ptr< ::std::ostream> TempFile::outputStream(::std::ios_base::openmode m) throws()
      {
         ::std::unique_ptr< ::std::ostream> res(new ::std::ofstream(_name.c_str(), m));
         return move(res);
      }

      bool TempFile::rename(const ::std::string& newName) throws()
      {
         try {
            if (_name != newName) {
               _fs.rename(_name, newName);
               _name = "";
               _fixed = true;
               return true;
            }
         }
         catch (const ::std::exception& e) {
         }
         return false;
      }

      bool TempFile::remove() throws()
      {
         try {
            _fs.remove(_name);
            _fixed = true;
            return true;
         }
         catch (const ::std::exception& e) {
         }
         return false;
      }
   }
}

