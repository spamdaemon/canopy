#include <canopy/fs/LockFile.h>
#include <canopy/SystemError.h>

#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

namespace canopy {
  namespace fs {
    namespace {
      static bool setCloseOnExec(int fd) throws()
      {
	int flags = ::fcntl(fd,F_GETFD);
	if (flags == -1) {
	  return false;
	}
	flags |= FD_CLOEXEC;
	return ::fcntl(fd,F_SETFD,flags)==0;
      }
    }

    
    LockFile::LockFile (const ::std::string& file, bool lockIt) throws(::std::runtime_error)
      : _file(file),_haveLock(false)
    {
      _fd = ::open(_file.c_str(),O_CREAT|O_RDWR,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
      if (_fd<0) {
	throw ::canopy::SystemError("Could not open lock file");
      }
      if (!setCloseOnExec(_fd)) {
	::close(_fd);
	throw ::canopy::SystemError("Could not set FD_CLOEXEC");
      }

      if (lockIt) {
	lock();
      }
    }
    
    LockFile::~LockFile() throws()
    {
      if (::close(_fd) < 0) {
	::std::cerr << "Could not close lock file" << ::std::endl;
      }
      // remove the file; 
      if (::unlink(_file.c_str())<0) {
	//
	//::std::cerr << "Lock file " << _file << " not removed" << ::std::endl;
	//
      }

    }

    void LockFile::lock() const volatile throws(::std::runtime_error)
    {
      int ok = lockf(_fd,F_LOCK,0);
      if (ok!=0) {
	throw ::canopy::SystemError("Could not obtain lock for file "+const_cast< ::std::string&>(_file));
      }
      _haveLock = true;
    }
	
    bool LockFile::tryLock() const volatile throws(::std::runtime_error)
    {
      int ok = lockf(_fd,F_TLOCK,0);
      if (ok!=0) {
	if (errno==EAGAIN) {
	  return false;
	}

	throw ::canopy::SystemError("Could not obtain lock for file "+const_cast< ::std::string&>(_file));
      }
      _haveLock = true;
      return true;
    }
	
    void LockFile::unlock() const volatile throws(::std::runtime_error)
    {
      int ok = lockf(_fd,F_ULOCK,0);
      if (ok!=0) {
	throw ::canopy::SystemError("Could not unlock file "+const_cast< ::std::string&>(_file));
      }
      _haveLock = false;
    }
    
  }
}

