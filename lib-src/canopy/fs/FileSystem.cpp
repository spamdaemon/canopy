#include <canopy/fs/FileSystem.h>
#include <canopy/fs/NativeFileSystem.h>
#include <canopy/time/Time.h>

using namespace ::std;

namespace canopy {
   namespace fs {

      void FileSystem::sync() const
      {
         _fs->sync();
      }

      bool FileSystem::createFile(const string& path)
      {
         return _fs->createFile(path);
      }

      ::std::string FileSystem::createTempFile(const ::std::string& prefix, const ::std::string& suffix)
      {
         return _fs->createTempFile(prefix, suffix);
      }

      UInt64 FileSystem::timeStampAccuracy() const throws ()
      {
         return _fs->timeStampAccuracy();
      }

      void FileSystem::setTime(const string& path, const ::canopy::time::Time& tm)
      {
         _fs->setTime(path, tm);
      }

      ::canopy::time::Time FileSystem::accessTime(const string& path) const
      {
         return _fs->accessTime(path);
      }

      ::canopy::time::Time FileSystem::modificationTime(const string& path) const
      {
         return _fs->modificationTime(path);
      }

      ::canopy::time::Time FileSystem::timeOfLastChange(const string& path) const
      {
         return _fs->timeOfLastChange(path);
      }

      string FileSystem::directorySeparator() const throws()
      {
         return _fs->directorySeparator();
      }

      bool FileSystem::isAbsolutePath(const string& path) const throws (exception)
      {
         return _fs->isAbsolutePath(path);
      }

      string FileSystem::getRelativePath(const string& base, const string& dest) const throws (exception)
      {
         return _fs->getRelativePath(base, dest);
      }

      string FileSystem::getAbsolutePath(const string& path) const throws(exception)
      {
         return _fs->getAbsolutePath(path);
      }

      string FileSystem::normalizePath(const string& p) const
      {
         return _fs->normalizePath(p);
      }

      void FileSystem::rename(const string& oldName, const string& newName) throws (exception)
      {
         _fs->rename(oldName, newName);
      }

      bool FileSystem::createAlias(const string& oldFile, const string& newFile) throws (exception)
      {
         return _fs->createAlias(oldFile, newFile);
      }

      bool FileSystem::createSymbolicLink(const string& oldName, const string& newName) throws (exception)
      {
         return _fs->createSymbolicLink(oldName, newName);
      }

      string FileSystem::findLinkTarget(const string& linkFile)const  throws (exception)
      {
         return _fs->findLinkTarget(linkFile);
      }

      string FileSystem::followLinks(const string& linkFile) const throws (exception)
      {
         return _fs->followLinks(linkFile);
      }

      void FileSystem::mkdir(const string& dir) throws (exception)
      {
         _fs->mkdir(dir);
      }

      void FileSystem::mkdirs(const string& dir) throws (exception)
      {
         _fs->mkdirs(dir);
      }

      string FileSystem::joinPaths(const string& path1, const string& path2) const throws()
      {
         return _fs->joinPaths(path1, path2);
      }

      string FileSystem::directoryName(const string& path) const throws()
      {
         return _fs->directoryName(path);
      }

      string FileSystem::baseName(const string& path) const throws()
      {
         return _fs->baseName(path);
      }

      UInt32 FileSystem::listEntries(const string& dir, vector< string>& entries,
            SearchFilter& filter)const  throws (exception)
      {
         return _fs->listEntries(dir, entries, filter);
      }

      UInt32 FileSystem::listEntries(const string& dir, vector< string>& entries) const throws (exception)
      {
         SearchFilter filter = [] (const char*,const char*) {return true;};
         return listEntries(dir, entries, filter);
      }

      void FileSystem::rmdir(const string& file, bool recurse) throws (exception)
      {
         _fs->rmdir(file, recurse);
      }

      bool FileSystem::remove(const string& file) throws (exception)
      {
         return _fs->remove(file);
      }
      bool FileSystem::hasPermissions(const ::std::string& file, int flags) const throws (::std::exception)
      {
         return _fs->hasPermissions(file, flags);
      }

      bool FileSystem::exists(const string& file, bool followSymLink) const throws (exception)
      {
         return _fs->exists(file, followSymLink);
      }

      bool FileSystem::isFile(const string& file, bool followSymLink) const throws (exception)
      {
         return _fs->isFile(file, followSymLink);
      }

      bool FileSystem::isSymbolicLink(const string& path) const throws (exception)
      {
         return _fs->isSymbolicLink(path);
      }

      bool FileSystem::isDirectory(const string& dir, bool followSymLink) const throws (exception)
      {
         return _fs->isDirectory(dir, followSymLink);
      }

      string FileSystem::workingDirectory() const throws()
      {
         return _fs->workingDirectory();
      }

      void FileSystem::setWorkingDirectory(const string& newDir) throws()
      {
         _fs->setWorkingDirectory(newDir);
      }

      FileSystem::FileSystem() throws()
            : _fs(NativeFileSystem::open())
      {
      }

      FileSystem::~FileSystem() throws()
      {
      }

   }
}
