#ifndef _CANOPY_STATIC_H
#define _CANOPY_STATIC_H

#include <cassert>

namespace canopy {

  /**
   * This class is used to ensure that static objects are destroyed
   * properly and to allow used to know when they have been destroyed.
   * The primary use of this class is to guard static object allocation
   * and its use is envisoned like this:
   * @code
   * Foo* Foo::instance()
   * {
   *    static Foo* _staticFoo;
   *    static Static<Foo> _guard(new Foo(),_staticFoo);
   *    return _staticFoo;
   * }
   * @endcode
   * When application exits either via a return from main() or 
   * via the ::std::exit() function, the guard object is destroyed and
   * resets the static foo object to 0. Calling instance again will
   * result in a 0 pointer.
   */
  template <class T> class Static {
    
    /**
     * Create a new static. Assigns an object to the
     * the static pointer reference.
     * @param obj a pointer to an object
     * @param ptr a reference to an object pointer
     * @pre obj must not be 0
     */
  public:
    Static (T* obj, T* volatile& ptr)
      : _pointer(ptr) 
      {
	assert(obj!=0);
	_pointer = obj;
      }
      
      /**
       * Destroy this object. Deletes the object pointed
       * to and sets the pointer reference to 0.
       */
  public:
    ~Static ()
      {
	delete _pointer;
	_pointer = 0;
      }
    
  private:
    T* volatile& _pointer;
  };
}

#endif
