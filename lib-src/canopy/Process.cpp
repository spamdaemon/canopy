#include <canopy/Process.h>
#include <canopy/fs/FileSystem.h>
#include <canopy/io/FileDescriptorImpl.h>

#include <sstream>
#include <fstream>
#include <iostream>
#include <memory>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <atomic>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

using namespace ::std;

namespace canopy {

   namespace {
      static const int NULL_PID = -1;
      static const UInt32 DEFAULT_STREAM_BUFFER_SIZE = 16 * 1024;
      static volatile UInt32 streamBufferSize = DEFAULT_STREAM_BUFFER_SIZE;

      static void closeFD(int fd)
      {
         if (::close(fd) < 0) {
#ifdef DEBUG_VERBOSE
            const string errmsg = getSystemError("Could not close filedescriptor");
            ::std::cerr << errmsg << endl;
#endif
         }
      }

      static void installSIGPIPEHandler()
      {
         struct sigaction sa;
         sa.sa_handler = SIG_IGN;
         sa.sa_flags = 0;
         sigemptyset(&sa.sa_mask);
         if (sigaction(SIGPIPE, &sa, 0) != 0) {
            printDebugMessage(getSystemError("Could not install sigpipe handler"));
         }
      }

      struct SigPipeHandler
      {
            SigPipeHandler()
            {
               printDebugMessage("Installing a sig-pipe handler");
               installSIGPIPEHandler();
            }
      };
      static SigPipeHandler sigPipeHandler;

      struct UnixStreamBuf : public streambuf
      {
            typedef streambuf::traits_type traits_type;

            UnixStreamBuf(bool inputStream)
                  : _fd(-1), _bufSize(streamBufferSize), _inputStream(inputStream)
            {
               if (false && !inputStream) {
                  _bufSize = 1;
               }

               _buf = new char[_bufSize]; // must! use _bufSize
               if (inputStream) {
                  this->setg(_buf, _buf, _buf);
               }
               else {
                  this->setp(_buf, _buf + _bufSize);
               }
            }

            ~UnixStreamBuf() throws()
            {
               delete[] _buf;
               if (_fd >= 0) {
#ifdef DEBUG_VERBOSE
                  ::std::cerr << "Closing UnixStreamBuf " << _fd << endl;
#endif
                  closeFD(_fd);
                  _fd = -1;
               }
            }

         public:
            void close()
            {
               if (_fd >= 0) {
                  if (_inputStream) {
                     underflow();
                  }
                  else {
                     overflow(traits_type::eof());
                  }
                  closeFD(_fd);
                  _fd = -1;
               }
            }

            int sync()
            {
               return overflow(traits_type::eof()) == traits_type::eof() ? -1 : 0;
            }

            int_type underflow()
            {
               // if no buffer is available, then return EOF
               if (_fd < 0 || this->eback() == NULL) {
                  return traits_type::eof();
               }
               assert(this->eback() == _buf);

               // determine the total number of bytes in the buffer
               const ptrdiff_t usedCount = this->gptr() - this->eback();

               assert(static_cast< size_t>(usedCount) <= _bufSize);

               // if there are bytes to be backed up,
               if (static_cast< size_t>(usedCount) == _bufSize) {
                  // adjust the get area
                  this->setg(_buf, _buf, _buf);
               }

               assert(this->egptr() == this->gptr());

               // read at least one byte into the buffer
               const ptrdiff_t requestCount = _bufSize - (this->gptr() - this->eback());
               ssize_t readCount = ::read(_fd, this->gptr(), requestCount);
               if (readCount < 0) {
                  readCount = 0;
               }

               // adjust the get area
               this->setg(this->eback(), this->gptr(), this->gptr() + readCount);
               return readCount == 0 ? traits_type::eof() : traits_type::to_int_type(*(this->gptr()));
            }

            int_type overflow(int_type c)
            {
               if (_fd < 0 || this->pbase() == NULL) {
                  return traits_type::eof();
               }

               ptrdiff_t size = this->pptr() - this->pbase();
               if (size == 0 && traits_type::eq_int_type(c, traits_type::eof())) {
                  return traits_type::not_eof(c);
               }

               /* If c is not eof, then append it to the output buffer
                * so that it gets written to the output medium along
                * with the buffer contents */
               if (!traits_type::eq_int_type(c, traits_type::eof())) {
                  *(this->pptr()) = static_cast< char>(c);
                  ++size;
               }

               /* Write the buffer to the medium */
               const ssize_t writeCount = ::write(_fd, this->pbase(), size);

               if (writeCount <= 0) {
                  //FIXME
                  return traits_type::eof();
               }

               /* If the entire buffer was not written, then don't
                * consider it a failure and shift the contents that
                * were not written to the beginning of the buffer. */
               if (writeCount < size) {
                  memcpy(this->pbase(), this->pbase() + writeCount, (size - writeCount));
               }

               /* Set the buffer up */
               setp(_buf, _buf + _bufSize);
               if (writeCount < size) {
                  this->pbump(size - writeCount);
               }

               if (traits_type::eq_int_type(c, traits_type::eof())) {
                  return traits_type::not_eof(c);
               }
               else {
                  return c;
               }
            }

         public:
            void setDescriptor(int fd)
            {
               _fd = fd;
            }

         private:
            int _fd;
         private:
            size_t _bufSize;
         private:
            char* _buf;
         private:
            bool _inputStream;
      };

      struct UnixStream : public iostream
      {
            UnixStream(bool inputStream)
                  : iostream(0), // this compile on older versions
                  _streambuf(inputStream)
            {

               // install a sig pipe handler while writing
               if (!inputStream) {
                  installSIGPIPEHandler();
               }

               init(&_streambuf);
            }

            ~UnixStream() throws ()
            {
            }

         public:
            void close()
            {
               _streambuf.close();
            }

         public:
            void setDescriptor(int fd)
            {
               _streambuf.setDescriptor(fd);
            }

         private:
            UnixStreamBuf _streambuf;
      };

   }

   struct Process::Impl
   {

         Impl()
               : _pid(NULL_PID), _cerr(true), _cout(true), _cin(false), _exited(false), _exitStatus(0)
         {

         }

         ~Impl() throws()
         {
            // do nothing; it's only the handle that gets destroyed
            if (_pid > 0 && _pid != getpid() && !_exited) {
               try {
                  ::std::cerr << "waiting for child process " << _pid << ::std::endl;
                  waitFor();
               }
               catch (...) {
               }
            }
         }

         ostream& print(ostream& out) volatile const
         {
            if (_pid < 0) {
               out << '-';
            }
            else {
               out << _pid;
            }
            return out;
         }

         /**
          * Execute a process.
          * @param
          */
         static unique_ptr< Impl> execute(const Process::Command& command)
         {
            const auto& filename = command.path;
            const auto& argv0 = command.name.empty() ? command.path : command.name;
            const auto& argv = command.arguments;
            const auto& env = command.environment;
            // FIXME: use command.inheritEnvironment

            if (filename.length() == 0) {
               throw invalid_argument("No path");
            }
#if CANOPY_UNIX == 1

            size_t argc = argv.size() + 2;

            // allocate any memory before we fork, because the memory
            // allocation might fail and then it is better to do so
            // in the parent process than in the child process
            char** args = new char*[argc];

            // copy the argument stuff over
            args[0] = new char[argv0.length() + 1];
            argv0.copy(args[0], argv0.length());
            args[0][argv0.length()] = '\0';

            for (string::size_type i = 0; i < argv.size(); ++i) {
               args[i + 1] = new char[argv[i].length() + 1];
               argv[i].copy(args[i + 1], argv[i].length());
               args[i + 1][argv[i].length()] = '\0';
            }
            args[argc - 1] = 0;

            const char* file = filename.c_str();

            try {
               unique_ptr< Impl> newProc = fork();
               if (newProc.get() != 0) {
                  // we're the parent

                  // delete the memory used for the arguments
                  for (int i = 0; args[i] != 0; ++i) {
                     delete[] args[i];
                  }
                  delete[] args;

                  return newProc;
               }
            }
            catch (...) {
               for (int i = 0; args[i] != 0; ++i) {
                  delete[] args[i];
               }
               delete[] args;
               throw;
            }
            // we're the child

            int err = -1;
            // build the environment if necessary
            if (env) {
               // 1 extra to store the null pointer
               char** envp = new char*[1 + env->size()];
               int j = 0;
               for (map< string, string>::const_iterator i = env->begin(); i != env->end(); ++i, ++j) {
                  string var;
                  var.reserve(i->first.length() + 1 + i->second.length());
                  var = i->first;
                  var += '=';
                  var += i->second;
                  envp[j] = new char[var.length() + 1];
                  var.copy(envp[j], var.length(), 0);
                  envp[j][var.length()] = '\0';
               }
               envp[j] = 0;
               err = ::execve(file, args, envp);

               // for completeness, we delete the memory now, but
               // most likely, this is not needed
               for (char** e = envp; *e != 0; ++e) {
                  delete[] *e;
               }
               delete[] envp;
            }
            else {
               // execute with the current environment
               err = ::execv(file, args);
            }
            if (err < 0) {
               const string errmsg = getSystemError(("Could not exec process " + filename).c_str());
               // do indeed print an error message to stderr
               ::std::cerr << errmsg << endl;
               // exit the child process
               exit(1);
            }
            else {
               abort();
            }
#else
            throw UnsupportedException("Process::execute not supported on this platform");
#endif
         }
         /**
          * Create a duplicate of the current process and return a pointer to the process implementation
          */
         static unique_ptr< Impl> fork()
         {
            unique_ptr< Impl> newProc(new Impl());

            int cinPipe[2] = { -1, -1 };
            int coutPipe[2] = { -1, -1 };
            int cerrPipe[2] = { -1, -1 };

            // open the pipes
            {
               if (::pipe(cinPipe) != 0) {
                  const string errmsg = getSystemError("Could not setup stdin for new process");
#ifdef DEBUG_VERBOSE
                  ::std::cerr << errmsg << endl;
#endif
                  throw runtime_error(errmsg);
               }
#ifdef DEBUG_VERBOSE
               ::std::cerr << "Allocated file descriptors " << cinPipe[0] << " and " << cinPipe[1] << endl;
#endif
               if (::pipe(coutPipe) != 0) {
                  closeFD(cinPipe[0]);
                  closeFD(cinPipe[1]);
                  const string errmsg = getSystemError("Could not setup stdout for new process");
#ifdef DEBUG_VERBOSE
                  ::std::cerr << errmsg << endl;
#endif
                  throw runtime_error(errmsg);
               }
#ifdef DEBUG_VERBOSE
               ::std::cerr << "Allocated file descriptors " << coutPipe[0] << " and " << coutPipe[1] << endl;
#endif
               if (::pipe(cerrPipe) != 0) {
                  closeFD(cinPipe[0]);
                  closeFD(cinPipe[1]);
                  closeFD(coutPipe[0]);
                  closeFD(coutPipe[1]);
                  const string errmsg = getSystemError("Could not setup stderr for new process");
#ifdef DEBUG_VERBOSE
                  ::std::cerr << errmsg << endl;
#endif
                  throw runtime_error(errmsg);
               }
#ifdef DEBUG_VERBOSE
               ::std::cerr << "Allocated file descriptors " << cerrPipe[0] << " and " << cerrPipe[1] << endl;
#endif
            }

            // fork the process
            pid_t child = ::fork();

            if (child < 0) {

               // get the error message
               const string errmsg = getSystemError("Could not fork process");
#ifdef DEBUG_VERBOSE
               ::std::cerr << errmsg << endl;
#endif

               // close the pipes
               closeFD(cinPipe[0]);
               closeFD(cinPipe[1]);
               closeFD(cerrPipe[0]);
               closeFD(cerrPipe[1]);
               closeFD(coutPipe[0]);
               closeFD(coutPipe[1]);

               throw runtime_error(errmsg);
            }

            if (child == 0) {
               // we're child process
               newProc.reset(0);

               // reset stdin,stdout, and stderr
               closeFD(0);
               closeFD(1);
               closeFD(2);
               dup2(cinPipe[0], 0);
               dup2(coutPipe[1], 1);
               dup2(cerrPipe[1], 2);

               // can we close the pipes no?
               closeFD(cinPipe[0]);
               closeFD(cinPipe[1]);
               closeFD(cerrPipe[0]);
               closeFD(cerrPipe[1]);
               closeFD(coutPipe[0]);
               closeFD(coutPipe[1]);
            }
            else {
               // we're the parent process; so reset the id of newProc
               newProc->_pid = child;

               ::std::unique_ptr< ::canopy::io::FileDescriptorImpl> fd;
               fd.reset(new ::canopy::io::FileDescriptorImpl(cinPipe[1], true));
               newProc->_cinDescriptor = ::canopy::io::FileDescriptor(::std::move(fd));
               fd.reset(new ::canopy::io::FileDescriptorImpl(coutPipe[0], true));
               newProc->_coutDescriptor = ::canopy::io::FileDescriptor(::std::move(fd));
               fd.reset(new ::canopy::io::FileDescriptorImpl(cerrPipe[0], true));
               newProc->_cerrDescriptor = ::canopy::io::FileDescriptor(::std::move(fd));

               // set the filedescriptor for the stream
               newProc->_cin.setDescriptor(cinPipe[1]);
               newProc->_cout.setDescriptor(coutPipe[0]);
               newProc->_cerr.setDescriptor(cerrPipe[0]);

               // the other side of the pipe is no longer needed by this process
               closeFD(cinPipe[0]);
               closeFD(cerrPipe[1]);
               closeFD(coutPipe[1]);

            }
            return newProc;
         }

         int exitCode() throws(exception)
         {
            switch (state()) {
               case Process::RUNNING:
                  if (_exited) {
                     return WEXITSTATUS(_exitStatus);
                  }
                  throw logic_error("Cannot get exit status from process that has not terminated");
               case Process::EXITED:
                  return WEXITSTATUS(_exitStatus);
               default:
                  throw logic_error("Process did not terminate normally");
            };
         }

         bool waitPID(bool suspend)
         {
            if (!_exited) {
               ::pid_t pid = ::waitpid(_pid, &_exitStatus, suspend ? 0 : WNOHANG);

               if (pid < 0) {
                  const string errmsg = getSystemError("Could not wait on process ");
#ifdef DEBUG_VERBOSE
                  print(::std::cerr << errmsg) << endl;
#endif
                  throw runtime_error(errmsg);

               }
               if (pid == 0) {
                  // child has not yet exited
                  return false;
               }
               // ok, child has indeed exited
               _exited = true;
               if (WIFSIGNALED(_exitStatus)) {
                  ::std::cerr << "Process ";
                  this->print(::std::cerr) << " exited with signal " << WTERMSIG(_exitStatus) << (
                  WCOREDUMP(_exitStatus) ? " (core dumped)" : "") << endl;
               }
            }
            // child has exited
            return true;
         }

         Process::State state() throws(exception)
         {
            if (!waitPID(false)) {
               return Process::RUNNING;
            }

            if (WIFEXITED(_exitStatus)) {
               return Process::EXITED;
            }
            if (WIFSIGNALED(_exitStatus)) {
               return Process::TERMINATED;
            }
            if (_pid == NULL_PID) {
               return Process::UNBOUND;
            }
            return Process::UNKNOWN;
         }

         void waitFor() throws(exception)
         {
            waitPID(true);
         }

         /** The process id */
         ::pid_t _pid;

         /** The streams */
         UnixStream _cerr;
         UnixStream _cout;
         UnixStream _cin;

         /** The file descriptors */
         ::canopy::io::FileDescriptor _cerrDescriptor;
         ::canopy::io::FileDescriptor _coutDescriptor;
         ::canopy::io::FileDescriptor _cinDescriptor;

         /** True if wait has been called */
      private:
         volatile bool _exited;

         /** The exit status */
      private:
         int _exitStatus;
   };

   Process::Process(::std::shared_ptr< Impl> impl)
throws         ()
         : _process(::std::move(impl))
         {
         }

   Process::Process()
throws         ()
         : _process(::std::make_shared<Impl>())
         {
         }

   Process::Process(const Process& p)
throws         ()
         : _process(p._process)
         {
         }

   Process::~Process() throws()
   {
   }

   Process& Process::operator=(const Process& p) throws()
   {
      _process = p._process;
      return *this;
   }

   bool Process::operator==(const Process& p) const throws()
   {
      return _process->_pid == p._process->_pid;
   }

   bool Process::operator<(const Process& p) const throws()
   {
      return _process->_pid < p._process->_pid;
   }

   CANOPY_BOILERPLATE_DEFINE_PRINT(::canopy::Process, _process->print(out))

   string Process::findExecutable() throws (UnsupportedException, exception)
   {
#if CANOPY_OS == CANOPY_LINUX
      fs::FileSystem f;
      // on linux we can access the executable of the current
      // process by using /proc/self
      return f.findLinkTarget("/proc/self/exe");
#else
      throw UnsupportedException("Process::findExecutable not supported on this platform");
#endif
   }

   vector< string> Process::argv() throws (UnsupportedException, exception)
   {
#if CANOPY_OS == CANOPY_LINUX
      ifstream cmdline("/proc/self/cmdline");
      vector< string> args;
      string cur;
      while (cmdline.good()) {
         char c = cmdline.get();
         if (cmdline.good()) {
            if (c == '\0') {
               if (!cur.empty()) {
                  args.push_back(cur);
               }
               cur.clear();
            }
            else {
               cur += c;
            }
         }
      }
      if (!cur.empty()) {
         args.push_back(cur);
      }
      return args;
#else
      throw UnsupportedException("Process::argv not supported on this platform");
#endif
   }

   string Process::name() throws (UnsupportedException, exception)
   {
      vector< string> args = argv();
      if (args.empty()) {
         throw ::std::runtime_error("Cannot determine program name; argv.length() == 0");
      }
      return args[0];
   }

   ::std::string Process::getEnv(const char* envName, const char* defaultValue) throws (::std::exception)
   {
      bool isSet;
      return getEnv(envName, defaultValue, isSet);
   }

   ::std::string Process::getEnv(const char* envName, const char* defaultValue, bool& isSet) throws (::std::exception)
   {
      if (envName == nullptr) {
         throw ::std::invalid_argument("No environment variable name specified");
      }
      const char* value = ::getenv(envName);
      isSet = (value != nullptr);
      if (!isSet) {
         value = defaultValue;
      }
      if (value == nullptr) {
         return string();
      }
      else {
         return string(value);
      }
   }

   void Process::unsetEnv(const char* envName) throws (::std::exception)
   {
      setEnv(envName, nullptr);
   }

   void Process::setEnv(const char* envName, const char* value) throws(::std::exception)
   {
      if (envName == nullptr) {
         return;
      }
#ifdef CANOPY_HAVE_ENV
      if (value == nullptr) {
         ::unsetenv(envName);
      }
      else {
         ::setenv(envName, value, true); // always overwrite
      }
#else
      throw UnsupportedException("Process::setEnv not supported on this platform");
#endif
   }

   Process Process::execute(const Command& command) throws(::std::exception, ::std::invalid_argument, UnsupportedException)
   {
      return Process(Impl::execute(command));
   }

   void Process::waitFor() throws(exception)
   {
      _process->waitFor();
   }

   Process::State Process::state() const throws(exception)
   {
      return _process->state();
   }

   int Process::exitCode() throws(exception)
   {
      return _process->exitCode();
   }

   Process::ProcessID Process::currentProcessID() throws()
   {
      return getpid();
   }

   Process::ProcessID Process::processID() const throws()
   {
      return _process->_pid;
   }

   ::canopy::io::FileDescriptor Process::cinDescriptor() throws()
   {
      return _process->_cinDescriptor;
   }
   ::canopy::io::FileDescriptor Process::coutDescriptor() throws()
   {
      return _process->_coutDescriptor;
   }
   ::canopy::io::FileDescriptor Process::cerrDescriptor() throws()
   {
      return _process->_cerrDescriptor;
   }

   ostream& Process::cin() throws()
   {
      return _process->_cin;
   }

   istream& Process::cout() throws()
   {
      return _process->_cout;
   }

   istream& Process::cerr() throws()
   {
      return _process->_cerr;
   }

   void Process::closeCin() throws()
   {
      _process->_cinDescriptor.close();
   }

   void Process::closeCout() throws()
   {
      _process->_coutDescriptor.close();
   }

   void Process::closeCerr() throws()
   {
      _process->_cerrDescriptor.close();
   }

   void Process::terminate() throws (exception)
   {
      if (::kill(_process->_pid, SIGTERM) < 0) {
         const string errmsg = getSystemError("Could not terminate process");
         throw runtime_error(errmsg);
      }
   }

   void Process::interrupt() throws (exception)
   {
      if (::kill(_process->_pid, SIGINT) < 0) {
         const string errmsg = getSystemError("Could not interrupt process");
         throw runtime_error(errmsg);
      }
   }

   void Process::setStreamBufferSize(UInt32 bufSize) throws()
   {
      if (bufSize == 0) {
         bufSize = DEFAULT_STREAM_BUFFER_SIZE;
      }
      streamBufferSize = bufSize;
   }

   Process::Command::Command()
   {
   }

   Process::Command::Command(::std::string xpath)
         : path(::std::move(xpath))
   {
   }

   Process::Command::Command(::std::string xpath, ::std::vector< ::std::string> args)
         : path(::std::move(xpath)), arguments(::std::move(args))
   {
   }

   Process::Command::Command(::std::string xpath, ::std::map< ::std::string, ::std::string> env,
         ::std::vector< ::std::string> args)
         : path(::std::move(xpath)), arguments(::std::move(args)), environment(
               ::std::make_shared< ::std::map< ::std::string, ::std::string>>(::std::move(env)))
   {
   }

}
