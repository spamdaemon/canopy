#ifndef _CANOPY_COMPILER_H
#define _CANOPY_COMPILER_H

// Pointer sizes (default is 4)
#if __GNUC__ != 0
#if __SIZEOF_POINTER__ != 0
#define CANOPY_POINTER_SIZE __SIZEOF_POINTER__
#endif
#endif

#ifndef CANOPY_POINTER_SIZE
#define CANOPY_POINTER_SIZE 4
#endif

#define CANOPY_UNKNOWN 0

// canopy architectures
#define CANOPY_INTEL 1
#define CANOPY_PPC 2
#define CANOPY_SPARC 3
#define CANOPY_MIPS 4

// intel cpus
#define CANOPY_INTEL_386 1
#define CANOPY_INTEL_486 2
#define CANOPY_INTEL_586 3
#define CANOPY_INTEL_686 4
#define CANOPY_INTEL_X86_64 5

// canopy compiler
#define CANOPY_GCC 1
#define CANOPY_ICC 2

// the os family
#define CANOPY_LINUX 1
#define CANOPY_SUNOS 2
#define CANOPY_BSD 3
#define CANOPY_IRIX 4
#define CANOPY_AIX 5

// the os versions (for solaris)
#define CANOPY_SUNOS_8 8
#define CANOPY_SUNOS_9 9
#define CANOPY_SUNOS_10 10

// if the os is Unix like
#define CANOPY_UNIX 1
// posix-support
#define CANOPY_POSIX 1

// ELF is the linking and executable format
#ifdef __GNUC__
#ifdef __ELF__
#define CANOPY_ELF 1
#endif
#endif

#ifndef CANOPY_ELF
#define CANOPY_ELF 0
#endif

#ifdef __GNUC__
#define CANOPY_COMPILER CANOPY_GCC
#endif

#define CANOPY_ARCH CANOPY_INTEL
#define CANOPY_CPU CANOPY_686

#define CANOPY_OS CANOPY_LINUX
#define CANOPY_LIB_C CANOPY_GLIBC
#define CANOPY_EXECUTABLE_FORMAT CANOPY_ELF

// elf-support via GELF
#if CANOPY_EXECUTABLE_FORMAT == CANOPY_ELF

#ifndef CANOPY_HAVE_LIBELF
#define CANOPY_HAVE_LIBELF 1
#endif

#ifndef CANOPY_HAVE_GELF
#define CANOPY_HAVE_GELF 1
#endif

#endif

// define posix thread support
#define CANOPY_THREAD_SUPPORT CANOPY_POSIX

// compile with exception specs
#define CANOPY_EXCEPTION_SPECIFICATION 0

// define this if dirfd() exists (linux and bsd)
#if CANOPY_OS==CANOPY_LINUX || CANOPY_OS==CANOPY_BSD
#define CANOPY_HAVE_DIRFD 1
#endif

#ifndef CANOPY_HAVE_LIBUNWIND
#define CANOPY_HAVE_LIBUNWIND 1
#endif

#ifndef CANOPY_ASSERT_ON_CHECKED_CAST
#define CANOPY_ASSERT_ON_CHECKED_CAST 1
#endif

#endif
