#ifndef _CANOPY_INJECTOR_H
#define _CANOPY_INJECTOR_H

#ifndef _CANOPY_FUNCTION_H
#include <canopy/function.h>
#endif

#include <functional>
#include <tuple>

namespace canopy {
   /**
    * This is the type used by the injector.
    */
   template<typename TYPE> class InjectedType
   {
         typedef typename ::std::remove_const< typename ::std::remove_reference< TYPE>::type>::type type;

         // fixme: ensure the TYPE is the same as type
   };

   template<typename INJECTOR> class Injector
   {
         /**
          * Default constructor.
          */
      public:
         inline Injector()
         {
         }

         /**
          * Destructor.
          */
      public:
         inline ~Injector()
         {
         }

         /**
          * Inject the specified function with the given injector.
          * @param fn a function
          * @return a new function that takes an instance of the specified injector
          */
      public:
         template<typename T>
         ::std::function< T(INJECTOR&)> injectFunction(::std::function< T()> fn) const
         {
            return [fn] (INJECTOR&) -> T {return fn();};
         }

         /**
          * Inject the specified function with the given injector.
          * @param fn a function
          * @return a new function that takes an instance of the specified injector
          */
      public:
         template<typename T, typename ... ARGS>
         ::std::function< T(INJECTOR&)> injectFunction(::std::function< T(ARGS...)> fn) const
         {
            return [fn] (INJECTOR& injector) {
               typedef ::std::tuple< ARGS...> Arguments;
               auto args = IBinder<Arguments,::std::tuple_size<Arguments>::value>::assign(injector);
               return ::std::apply(fn,args);
            };
         }

         /**
          * Create a new tuple.
          * @param inject the injector
          */
      private:
         template<typename TUPLE, size_t INDEX>
         static auto bindTuple(INJECTOR& injector)
         {
            using TYPE = typename ::std::tuple_element<INDEX-1,TUPLE>::type;
            using NO_REF = typename ::std::remove_reference< TYPE>::type;
            using NO_REF_CONST = typename ::std::remove_const<NO_REF>::type;
            InjectedType< NO_REF_CONST> dummy;
            using RETURN_TYPE=decltype(injector.get(dummy));
            return ::std::tuple< RETURN_TYPE>(injector.get(dummy));
         }

         /**
          * Assign a value to the specified typle index.
          */
      private:
         template<typename TUPLE, size_t INDEX>
         struct IBinder
         {
               static auto assign(INJECTOR& injector)
               {
                  return std::tuple_cat(IBinder< TUPLE, INDEX - 1>::assign(injector),
                        Injector::bindTuple< TUPLE, INDEX>(injector));
               }
         };

         /**
          * Assign a value to the specified typle index.
          * This is the basis case for the injection. A tuple of 1 value creates a new tuple of the same
          * type, but without references types.
          * For examples, the tuple<int&> gets changed into tuple<int>
          */
      private:
         template<typename TUPLE>
         struct IBinder< TUPLE, 1>
         {
               static auto assign(INJECTOR& injector)
               {
                  return Injector::bindTuple< TUPLE, 1>(injector);
               }
         };
   };
}

#endif
