#ifndef _CANOPY_ARRAYCOPY_H
#define _CANOPY_ARRAYCOPY_H

#include <cstring>
#include <cstddef>
#include <cassert>

#ifndef _CANOPY_DEFINES_H
#include <canopy/defines.h>
#endif

namespace canopy {
  
  /**
   * Copy arrays. This function is specialized for various 
   * basic types.
   * @param source the source array
   * @param dest the destination array
   * @param len the number of items to copy
   * @todo arraycopy: implement using CopyTraits
   */
  template <class T>
    inline void arraycopy (const T* source, T* dest, ::std::size_t len)
    {
      assert (source+len <= dest || source >= dest);
      const T *const sourceEnd = source+len;

      while(source<sourceEnd) {
	*(dest++) = *(source++);
      }
    }
  
  
  /**
   * Copy an array to a new location. The source and destination arrays may overlap.
   * regions may overlap.
   * @param source the source array
   * @param dest the destination array
   * @param len the number of items to copy
   * @todo arraymove: implement using CopyTraits
   */
  template <class T>
    inline void arraymove (const T* source, T* dest, ::std::size_t len)
    {
      if (source < dest) {
	const T *const sourceStart = source;
	source = source+len;
	dest   = dest+len;
	while(source-- > sourceStart) {
	  *(--dest) = *source;
	}
      }
      else {
	arraycopy(source,dest,len);
      }
    }
  
  
  /**
   * Definitions of the function arraycopy and arraymove for builtin types 
   * @{
   */

  template <class T*>
    inline void arraycopy (const T** source, T** dest, ::std::size_t len)throws()
    { ::std::memcpy(dest,source,len*sizeof(T*)); } 
  
  template <class T*>
    inline void arraymove (const T** source, T** dest, ::std::size_t len)
    { ::std::memmove(dest,source,len*sizeof(T*)); } 

  inline void arraycopy(const double* source, double* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(double));   }
  inline void arraymove(const double* source, double* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(double));   }
  
  inline void arraycopy(const float* source, float* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(float));   }
  inline void arraymove(const float* source, float* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(float));   }
  
  inline void arraycopy(const bool* source, bool* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(bool));   }
  inline void arraymove(const bool* source, bool* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(bool));   }
  
  inline void arraycopy(const long long* source, long long* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(long long));   }
  inline void arraymove(const long long* source, long long* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(long long));   }
  
  inline void arraycopy(const long* source, long* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(long));   }
  inline void arraymove(const long* source, long* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(long));   }
  
  inline void arraycopy(const int* source, int* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(int));   }
  inline void arraymove(const int* source, int* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(int));   }
  
  inline void arraycopy(const short* source, short* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(short));   }
  inline void arraymove(const short* source, short* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(short));   }
  
  inline void arraycopy(const char* source, char* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(char));   }
  inline void arraymove(const char* source, char* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(char));   }
  
  inline void arraycopy(const wchar_t* source, wchar_t* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(wchar_t));   }
  inline void arraymove(const wchar_t* source, wchar_t* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(wchar_t));   }
  
  inline void arraycopy(const unsigned long long* source, unsigned long long* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(unsigned long long));   }
  inline void arraymove(const unsigned long long* source, unsigned long long* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(unsigned long long));   }
  
  inline void arraycopy(const unsigned long* source, unsigned long* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(unsigned long));   }
  inline void arraymove(const unsigned long* source, unsigned long* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(unsigned long));   }
  
  inline void arraycopy(const unsigned int* source, unsigned int* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(unsigned int));   }
  inline void arraymove(const unsigned int* source, unsigned int* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(unsigned int));   }
  
  inline void arraycopy(const unsigned short* source, unsigned short* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(unsigned short));   }
  inline void arraymove(const unsigned short* source, unsigned short* dest, ::std::size_t len)
    {  ::std::memmove(dest,source,len*sizeof(unsigned short));   }
  
  inline void arraycopy(const unsigned char* source, unsigned char* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(unsigned char));   }
  inline void arraymove(const unsigned char* source, unsigned char* dest, ::std::size_t len) 
    {  ::std::memmove(dest,source,len*sizeof(unsigned char));   }
  
  inline void arraycopy(const signed char* source, signed char* dest, ::std::size_t len)
    {  ::std::memcpy(dest,source,len*sizeof(signed char));   }
  inline void arraymove(const signed char* source, signed char* dest, ::std::size_t len) 
    {  ::std::memmove(dest,source,len*sizeof(signed char));   }
  
  /*@}*/
}
#endif
