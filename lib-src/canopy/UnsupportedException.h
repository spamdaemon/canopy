#ifndef _CANOPY_UNSUPPORTEDEXCEPTION_H
#define _CANOPY_UNSUPPORTEDEXCEPTION_H

#include <canopy/canopy.h>
#include <stdexcept>

namespace canopy {

  /**
   * The unsupported exception is thrown by function and methods
   * that are not supported by the version of the operating
   * system.
   */
  class UnsupportedException : public ::std::runtime_error {

    /**
     * Create a new unsupported exception
     */
  public:
    UnsupportedException() throws ();

    /**
     * Create a new unsupported exception
     * @param msg a message
     */
  public:
      UnsupportedException(::std::string msg) throws ();

    /** Destructor */
  public:
    ~UnsupportedException() throw() {}
  };
   
}

#endif
