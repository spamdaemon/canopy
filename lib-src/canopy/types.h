#ifndef _CANOPY_TYPES_H
#define _CANOPY_TYPES_H

#include <cstdint>

namespace canopy {

  /** A signed byte (8-bit) */
  typedef ::std::int8_t		Int8;
    
  /** A signed short (16 bit) */
  typedef ::std::int16_t	Int16;
    
  /** A signed int (32 bit) */
  typedef ::std::int32_t Int32;

  /** A signed long (64 bit) */
  typedef ::std::int64_t Int64;
    
  /** An unsigned byte (8-bit) */
  typedef ::std::uint8_t  UInt8;
    
  /** An unsigned short (16 bit) */
  typedef ::std::uint16_t UInt16;
    
  /** An unsigned int (32 bit) */
  typedef ::std::uint32_t    UInt32;
    
  /** An unsigned long (64 bit) */
  typedef ::std::uint64_t UInt64;
    
  /** A character */
  typedef char Char;
    
  /** The wide character type  */
  typedef wchar_t WChar;
    
  /** A single precision floating point number */
  typedef float Float;
    
  /** A double precision floating point number */
  typedef double  Double;
}

#endif
