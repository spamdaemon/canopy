#ifndef _CANOPY_SHAREDPTER_H
#define _CANOPY_SHAREDPTR_H

#include <memory>
#include <stdexcept>

namespace canopy {

   /**
    * The shared ref is used to track a shared_ptr.
    */
   template<class T> class SharedPtr
   {
         template<class U> friend class SharedPtr;

         /**
          * Create a shared ref from a shared pointer.
          * @param ptr a shared pointer
          * @throws ::std::invalid_argument if ptr==nullptr
          */
      public:
         inline SharedPtr(::std::shared_ptr< T> ptr)
               : _ptr(::std::move(ptr))
         {
         }

         /**
          * Create a shared ref from a shared pointer.
          * @param ptr a shared pointer
          * @throws ::std::invalid_argument if ptr==nullptr
          */
      public:
         template<class U>
         inline SharedPtr(::std::shared_ptr< U> ptr)
         {
            if(ptr!=nullptr) {
               _ptr = ::std::dynamic_pointer_cast<T>(::std::move(ptr));
               if (_ptr==nullptr) {
                  throw ::std::invalid_argument("Failed to cast");
               }
            }
         }

         /**
          * Create a shared ref from a shared pointer.
          * @param ptr a shared pointer
          * @throws ::std::invalid_argument if ptr==nullptr
          */
      public:
         template<class U>
         inline SharedPtr(::std::unique_ptr< U> ptr)
               : SharedPtr(::std::shared_ptr< U>(::std::move(ptr)))
         {
         }

         /**
          * Copy constructor.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedPtr(const SharedPtr& ref)
               : _ptr(ref._ptr)
         {
         }

         /**
          * Move constructor.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedPtr(SharedPtr&& ref)
               : _ptr(::std::move(ref._ptr))
         {
         }

         /**
          * Copy constructor.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedPtr(const SharedPtr< U>& ref)
               : SharedPtr(ref._ptr)
         {
         }

         /**
          * Move constructor.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedPtr(SharedPtr< U>&& ref)
               : SharedPtr(::std::move(ref._ptr))
         {
         }


         /**
          * A conversion operator
          */
      public:
         template<class U>
         inline operator ::std::shared_ptr<U>() const
         {
            return ::std::dynamic_pointer_cast< U>(_ptr);
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedPtr& operator=(const SharedPtr& ref)
         {
            _ptr = ref._ptr;
            return *this;
         }

         /**
          * Move operator.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedPtr& operator=(SharedPtr&& ref)
         {
            _ptr = ::std::move(ref._ptr);
            return *this;
         }

         /**
            * Copy operator.
            * @param ref a ref
            * @return this
            */
        public:
           template<class U>
           inline SharedPtr& operator=(const SharedPtr< U>& ref)
           {
              return operator=(ref._ptr);
           }

           /**
              * Move operator.
              * @param ref a ref
              * @return this
              */
          public:
             template<class U>
             inline SharedPtr& operator=(  SharedPtr< U>&& ref)
             {
                return operator=(::std::move(ref._ptr));
             }

        /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedPtr& operator=(::std::shared_ptr< T> ptr)
         {
            _ptr = ::std::move(ptr);
            return *this;
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedPtr& operator=(::std::shared_ptr< U> ptr)
         {
            SharedPtr tmp(::std::move(ptr));
            _ptr = ::std::move(tmp._ptr);
            return *this;
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedPtr& operator=(::std::unique_ptr< U> ptr)
         {
            return operator=(::std::shared_ptr< U>(::std::move(ptr)));
         }

          /**
          * Deref the pointer.
          * @return a pointer
          */
      public:
         inline T& operator*() const
         {
            return *_ptr;
         }

         /**
          * Deref the pointer.
          * @return a pointer
          */
      public:
         inline T* operator->() const
         {
            return _ptr.get();
         }

         /**
          * Get the shared pointer.
          * @return the shared pointer object
          */
      public:
         inline ::std::shared_ptr< T> get() const
         {
            return _ptr;
         }

         /**
          * Compare two references.
          * @return true if get() < p.get()
          */
      public:
         template<class U>
         inline bool operator<(const SharedPtr< U>& ref) const
         {
            return _ptr < ref._ptr;
         }

         /**
          * Compare two references for equality.
          * @param p a ref
          * @return true the two references are the same
          */
      public:
         template<class U>
         bool operator==(const SharedPtr< U>& ref) const
         {
            return _ptr == ref._ptr;
         }

         /**
          * Compare two references for inequality.
          * @param p a ref
          * @return true the two references are not the same
          */
      public:
         template<class U>
         inline bool operator!=(const SharedPtr< U>& p) const
         {
            return _ptr != p._ptr;
         }

         /**
          * Swap two refs.
          * @param a a ref
          * @param b a ref
          */
      public:
         static inline void swap(::canopy::SharedPtr< T>& a, ::canopy::SharedPtr< T>& b)
         {
            ::std::swap(a._ptr, b._ptr);
         }

         /** The shared pointer */
      private:
         ::std::shared_ptr< T> _ptr;
   }
   ;
}

namespace std {
   template<class T>
   void swap(::canopy::SharedPtr< T>& a, ::canopy::SharedPtr< T>& b)
   {
      ::canopy::SharedPtr< T>::swap(a, b);
   }
}

#endif
