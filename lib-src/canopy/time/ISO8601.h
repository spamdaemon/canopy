#ifndef _CANOPY_TIME_ISO8601_H
#define _CANOPY_TIME_ISO8601_H

#ifndef _CANOPY_CANOPY_H
#include <canopy/canopy.h>
#endif

namespace canopy {
  namespace time {
    class Time;
    /** 
     * This class provides static functions to format and parse strings in ISO 8601 format. 
     */
    class ISO8601 {
      ISO8601 () throws ();
      ISO8601(const ISO8601&) throws ();
      ISO8601&operator=(const ISO8601&) throws ();
      ~ISO8601() throws();

      /** The output buffer formatting */
    public:
      static const size_t DATETIME_BUFFER_SIZE = 31;
    
      /** The output buffer for formatting time only */
    public:
      static const size_t TIME_BUFFER_SIZE = 20;
    
      /** The output buffer for formatting date only */
    public:
      static const size_t DATE_BUFFER_SIZE = 11;

      /**
       * Parse a time value given in the ISO 8601 format. ISO8601 values
       * before the Epoch are not supported.
       * @param str a string in ISO 8601 format
       * @return a time
       * @throws ::std::exception if the string could not be parsed
       */
    public:
      static Time parseTime (const char* str) throws (::std::exception);

      /**
       * Parse a time value given in the ISO 8601 format. ISO8601 values
       * before the Epoch are not supported.
       * @param str a string in ISO 8601 format
       * @return a time
       * @throws ::std::exception if the string could not be parsed
       */
    public:
      static Time parseTime (const ::std::string& str) throws (::std::exception);

      /**
       * Parse a date value given in the ISO 8601 format. ISO8601 values
       * before the Epoch are not supported.
       * @param str a string in ISO 8601 format
       * @return a time
       * @throws ::std::exception if the string could not be parsed
       */
    public:
      static Time parseDate (const char* str) throws (::std::exception);

      /**
       * Parse a date value given in the ISO 8601 format. ISO8601 values
       * before the Epoch are not supported.
       * @param str a string in ISO 8601 format
       * @return a time
       * @throws ::std::exception if the string could not be parsed
       */
    public:
      static Time parseDate (const ::std::string& str) throws (::std::exception);
	  
      /**
       * Parse a time value given in the ISO 8601 format. ISO8601 values
       * before the Epoch are not supported.
       * @param str a string in ISO 8601 format
       * @return a time
       * @throws ::std::exception if the string could not be parsed
       */
    public:
      static Time parseDateTime (const char* str) throws (::std::exception);

      /**
       * Parse a time value given in the ISO 8601 format. ISO8601 values
       * before the Epoch are not supported.
       * @param str a string in ISO 8601 format
       * @return a time
       * @throws ::std::exception if the string could not be parsed
       */
    public:
      static Time parseDateTime (const ::std::string& str) throws (::std::exception);

      /**
       * Format this time into the specified buffer. The
       * format will be that of ISO 8601:
       * <code>YYYY-MM-DDThh:mm:ss.xZ</code>. The fields are
       * are as follows:
       * <dl>
       * <dt>YYYY</dt><dd>The 4-digit year</dd>
       * <dt>MM</dt><dd>The 2-digit month in the range 1-12</dd>
       * <dt>DD</dt><dd>The 2-digit day of the month in the range 1-31</dd>
       * <dt>hh</dt><dd>The 2-digit hour of the day in the range 0-23</dd>
       * <dt>mm</dt><dd>The 2-digit minute of the hour in the range 0-59</dd>
       * <dt>ss</dt><dd>The 2-digit second of the minute in the range 0-59</dd>
       * <dt>x</dt><dd>The subsecond time accuracy with leading zeros</dd>
       * </dl>
       * The size of the buffer must be exactly BUFFER_SIZE bytes, including a trailing <em>nul</em> character.
       * 
       * @param tm the time to be formatted as a string
       * @param buf a buffer of size
       * @param nDigits the number of digits for subsecond time
       * @return the number of characters actually written to the buffer
       */
    public:
      static size_t formatDateTime (const Time& tm, char buf[DATETIME_BUFFER_SIZE], size_t nDigits) throws ();

      /**
       * Format this time into the specified buffer. The
       * format will be that of ISO 8601 (time only):
       * <code>hh:mm:ss.xxxxxxxxxZ</code>. The fields are
       * are as follows:
       * <dl>
       * <dt>hh</dt><dd>The 2-digit hour of the day in the range 0-23</dd>
       * <dt>mm</dt><dd>The 2-digit minute of the hour in the range 0-59</dd>
       * <dt>ss</dt><dd>The 2-digit second of the minute in the range 0-59</dd>
       * <dt>x</dt><dd>The subsecond time accuracy with leading zeros</dd>
       * </dl>
       * The size of the buffer must be exactly TIME_BUFFER_SIZE bytes, including a trailing <em>nul</em> character.
       * 
       * @param tm the time to be formatted as a string
       * @param buf a buffer of size
       * @param nDigits the number of digits for subsecond time
       * @return the number of characters actually written to the buffer
       */
    public:
      static size_t formatTime (const Time& tm, char buf[TIME_BUFFER_SIZE], size_t nDigits) throws ();

      /**
       * Format this time into the specified buffer. The
       * format will be that of ISO 8601 (date only):
       * <code>YYYY-MM-DD</code>. The fields are
       * are as follows:
       * <dl>
       * <dt>YYYY</dt><dd>The 4-digit year</dd>
       * <dt>MM</dt><dd>The 2-digit month in the range 1-12</dd>
       * <dt>DD</dt><dd>The 2-digit day of the month in the range 1-31</dd>
       * </dl>
       * The size of the buffer must be exactly DATE_BUFFER_SIZE bytes, including a trailing <em>nul</em> character.
       * 
       * @param tm the time to be formatted as a string
       * @param buf a buffer of size
       * @return the number of characters actually written to the buffer
       */
    public:
      static size_t formatDate (const Time& tm, char buf[DATE_BUFFER_SIZE]) throws ();
    };
  }
}
#endif
