#ifndef _CANOPY_TIME_TIME_H
#define _CANOPY_TIME_TIME_H

#ifndef _CANOPY_CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_TIME_ISO8601_H
#include <canopy/time/ISO8601.h>
#endif

#include <string>
#include <chrono>

namespace canopy {
   namespace time {
      /**
       * This class measures the time that has elapsed since the Epoch (00:00:00 UTC, January 1, 1970). The
       * precision, but not necessarily the accuracy, is given in nanoseconds.
       * This class provides some convenience function to format a time value according to ISO 8601.
       */
      class Time
      {
            /** The duration type used */
         public:
            typedef ::std::chrono::nanoseconds Duration;

            /**
              * A duration of 1 second
              */
          public:
             static constexpr Duration ONE_HOUR = Duration(3600*1000000000LL);

             /**
               * A duration of 1 second
               */
           public:
              static constexpr Duration ONE_MINUTE = Duration(60*1000000000LL);

             /**
               * A duration of 1 second
               */
           public:
              static constexpr Duration ONE_SECOND = Duration(1000000000LL);

            /**
             * A duration of 1 millisecond
             */
         public:
            static constexpr Duration ONE_MILLISECOND = Duration(1000000ULL);

            /**
             * A duration of 1 microsecond
             */
         public:
            static constexpr Duration ONE_MICROSECOND = Duration(1000ULL);

             /** The timepoint */
         public:
            typedef ::std::chrono::time_point< ::std::chrono::system_clock, Duration> TimePoint;

            /** The output buffer formatting */
         public:
            static const size_t BUFFER_SIZE = ISO8601::DATETIME_BUFFER_SIZE;

            /** The output buffer for formatting time only */
         public:
            static const size_t TIME_BUFFER_SIZE = ISO8601::TIME_BUFFER_SIZE;

            /** The output buffer for formatting date only */
         public:
            static const size_t DATE_BUFFER_SIZE = ISO8601::DATE_BUFFER_SIZE;

            /**
             * Get the current time as a time. The precision
             * of the underlying system may not necessarily
             * give time to nanosecond precision.
             * @see accuracy()
             */
         private:
            inline Time() throws ()
                  : _time(TimePoint::clock::now())
            {
            }

            /**
             * Create a time value.
             * @param t nanoseconds that have elapsed since the epoch.
             */
         public:
            inline Time(const UInt64& t) throws ()
                  : _time(Duration(t))
            {
            }

            /**
             * Create a time value.
             * @param t the duration that has elapsed since the epoch
             */
         public:
            inline Time(const Duration& t) throws ()
                  : _time(t)
            {
            }

            /**
             * Create a time value.
             * @param t a timepoint
             */
         public:
            inline Time(const TimePoint& t) throws ()
                  : _time(t)
            {
            }

            /**
             * Get the time for the epoch.
             * @return the time object for the epoch.
             */
         public:
            inline static Time epoch() throws()
            {
               return Time(TimePoint());
            }

            /**
             * Get the current time.
             * @return the current time.
             */
         public:
            inline static Time now() throws()
            {
               return Time(TimePoint::clock::now());
            }

            /**
             * Create a time object with a time from now.
             * @param duration the time into the future
             * @return a time
             */
         public:
            static Time fromNow(const Duration& t) throws()
            {
               return Time(TimePoint::clock::now() + t);
            }

            /**
             * Get the accuracy of the system in nanoseconds. For example,
             * systems with millisecond precision would return 1000000 and those
             * with microsecond precision 1000. If the system
             * support an accuracy of less than 1 nanosecond (highly unlikely),
             * then 0 must be returned.
             * @return the accuracy in nanoseconds.
             */
         public:
            static UInt32 accuracy() throws ();

            /**
             * Get the number of nanoseconds that have elapsed since the epoch.
             * @return the time in nanosecond precision
             */
         public:
            inline const TimePoint& time() const throws ()
            {
               return _time;
            }

            /**
             * Get time that has elapsed since the epoch.
             * @return the time that elapsed since the epoch
             */
         public:
            inline Duration timeElapsed() const throws ()
            {
               return _time.time_since_epoch();
            }

            /**
             * Get the full seconds that have elapsed since the epoch.
             * @return the number of full seconds that have elapsed since the epoch
             */
         public:
            inline UInt32 seconds() const throws ()
            {
               return std::chrono::duration_cast< std::chrono::seconds>(timeElapsed()).count();
            }

            /**
             * Get the number of nanoseconds that have elapsed since the last full
             * second. The value returned here is equivalent to
             * @code time() - second()*1000000000 @endcode.
             * @return the nanoseconds that have elapsed since the last full second.
             */
         public:
            inline UInt32 nanoseconds() const throws ()
            {
               return static_cast< UInt32>(timeElapsed().count() % 1000000000);
            }

            /**
             * Parse a time value given in the ISO 8601 format. Time values
             * before the Epoch are not supported.
             * @param str a string in IS) 8601 format
             * @return a time
             * @throws ::std::exception if the string could not be parsed
             */
         public:
            static Time parse(const char* str) throws (::std::exception);

            /**
             * Parse a time value given in the ISO 8601 format. Time values
             * before the Epoch are not supported.
             * @param str a string in IS) 8601 format
             * @return a time
             * @throws ::std::exception if the string could not be parsed
             */
         public:
            static Time parse(const ::std::string& str) throws (::std::exception);

            /**
             * Format this time into the specified buffer as a date and time.
             * @param nDigits the number of digits for subsecond time (0 to 9)
             * @return a string
             */
         public:
            ::std::string format(size_t nDigits = 0) const throws();

            /**
             * Format this time into the specified buffer as a date and time.
             * @param nDigits the number of digits for subsecond time (0 to 9)
             * @return a string
             */
         public:
            ::std::string formatTime(size_t nDigits = 0) const throws();

            /**
             * Format this time into the specified buffer as a date and time.
             * @param nDigits the number of digits for subsecond time
             * @return a string
             */
         public:
            ::std::string formatDate() const throws();

            /**
             * Format this time into the specified buffer. The
             * format will be that of ISO 8601:
             * <code>YYYY-MM-DDThh:mm:ss.x</code>. The fields are
             * are as follows:
             * <dl>
             * <dt>YYYY</dt><dd>The 4-digit year</dd>
             * <dt>MM</dt><dd>The 2-digit month in the range 1-12</dd>
             * <dt>DD</dt><dd>The 2-digit day of the month in the range 1-31</dd>
             * <dt>hh</dt><dd>The 2-digit hour of the day in the range 0-23</dd>
             * <dt>mm</dt><dd>The 2-digit minute of the hour in the range 0-59</dd>
             * <dt>ss</dt><dd>The 2-digit second of the minute in the range 0-59</dd>
             * <dt>x</dt><dd>The subsecond time accuracy with leading zeros</dd>
             * </dl>
             * The size of the buffer must be exactly BUFFER_SIZE bytes, including a trailing <em>nul</em> character.
             *
             * @param buf a buffer of size
             * @param nDigits the number of digits for subsecond time (0 to 9)
             * @return the number of characters actually written to the buffer
             */
         public:
            size_t format(char buf[BUFFER_SIZE], size_t nDigits = 0) const throws ();

            /**
             * Format this time into the specified buffer. The
             * format will be that of ISO 8601 (time only):
             * <code>hh:mm:ss.xxxxxxxxx</code>. The fields are
             * are as follows:
             * <dl>
             * <dt>hh</dt><dd>The 2-digit hour of the day in the range 0-23</dd>
             * <dt>mm</dt><dd>The 2-digit minute of the hour in the range 0-59</dd>
             * <dt>ss</dt><dd>The 2-digit second of the minute in the range 0-59</dd>
             * <dt>x</dt><dd>The subsecond time accuracy with leading zeros</dd>
             * </dl>
             * The size of the buffer must be exactly TIME_BUFFER_SIZE bytes, including a trailing <em>nul</em> character.
             *
             * @param buf a buffer of size
             * @param nDigits the number of digits for subsecond time (0 to 9)
             * @return the number of characters actually written to the buffer
             */
         public:
            size_t formatTime(char buf[TIME_BUFFER_SIZE], size_t nDigits = 0) const throws ();

            /**
             * Format this time into the specified buffer. The
             * format will be that of ISO 8601 (date only):
             * <code>YYYY-MM-DD</code>. The fields are
             * are as follows:
             * <dl>
             * <dt>YYYY</dt><dd>The 4-digit year</dd>
             * <dt>MM</dt><dd>The 2-digit month in the range 1-12</dd>
             * <dt>DD</dt><dd>The 2-digit day of the month in the range 1-31</dd>
             * </dl>
             * The size of the buffer must be exactly DATE_BUFFER_SIZE bytes, including a trailing <em>nul</em> character.
             *
             * @param buf a buffer of size
             * @return the number of characters actually written to the buffer
             */
         public:
            size_t formatDate(char buf[DATE_BUFFER_SIZE]) const throws ();

            /**
             * Compare two times.
             * @param t a time
             * @return true if this time is before the specified time
             */
         public:
            inline bool operator<(const Time& t) const throws ()
            {
               return _time < t._time;
            }

            /**
             * Compare two times.
             * @param t a time
             * @return true if this time is the same as the specified time
             */
         public:
            inline bool operator==(const Time& t) const throws ()
            {
               return _time == t._time;
            }

            /** The time */
         private:
            TimePoint _time;
      };
   }
}
#endif
