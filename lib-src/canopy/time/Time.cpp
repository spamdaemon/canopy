#include <canopy/time/Time.h>

namespace canopy {
   namespace time {
      const size_t Time::BUFFER_SIZE;
      const size_t Time::TIME_BUFFER_SIZE;
      const size_t Time::DATE_BUFFER_SIZE;
      constexpr Time::Duration Time::ONE_HOUR;
      constexpr Time::Duration Time::ONE_MINUTE;
      constexpr Time::Duration Time::ONE_SECOND;
      constexpr Time::Duration Time::ONE_MILLISECOND;
      constexpr Time::Duration Time::ONE_MICROSECOND;

      UInt32 Time::accuracy() throws()
      {
         return 1000;
      }

      size_t Time::format(char buf[BUFFER_SIZE], size_t nDigits) const throws()
      {
         return ISO8601::formatDateTime(*this, buf, nDigits);
      }

      size_t Time::formatTime(char buf[TIME_BUFFER_SIZE], size_t nDigits) const throws()
      {
         return ISO8601::formatTime(*this, buf, nDigits);
      }

      size_t Time::formatDate(char buf[DATE_BUFFER_SIZE]) const throws()
      {
         return ISO8601::formatDate(*this, buf);
      }

      Time Time::parse(const char* str) throws (::std::exception)
      {
         return ISO8601::parseDateTime(str);
      }

      Time Time::parse(const ::std::string& str) throws (::std::exception)
      {
         return ISO8601::parseDateTime(str);
      }

      ::std::string Time::format(size_t nDigits) const throws()
      {
         char buf[BUFFER_SIZE + 1];
         size_t n = format(buf, nDigits);
         buf[n] = '\0';
         return ::std::string(buf);
      }

      ::std::string Time::formatTime(size_t nDigits) const throws()
      {
         char buf[TIME_BUFFER_SIZE + 1];
         size_t n = formatTime(buf, nDigits);
         buf[n] = '\0';
         return ::std::string(buf);
      }

      ::std::string Time::formatDate() const throws()
      {
         char buf[DATE_BUFFER_SIZE + 1];
         size_t n = formatDate(buf);
         buf[n] = '\0';
         return ::std::string(buf);
      }

   }
}
