#include <canopy/time/ISO8601.h>
#include <canopy/time/Time.h>

#include <time.h>
#include <sys/time.h>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cassert>
#include <cctype>

namespace canopy {
   namespace time {
      namespace {
         static const Int32 l_leapYearDaysPerMonth[12] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
         static const Int32 l_daysPerMonth[12] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
         static const Int32 l_leapYearDays[12] = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 };
         static const Int32 l_yearDays[12] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };

         static char parseChar(const char** p, const char* end)
         {
            if (*p == end) {
               throw ::std::runtime_error("Unexpected end of string");
            }

            char c = **p;
            if (c == '\0') {
               throw ::std::runtime_error("Unexpected end of string");
            }
            ++(*p);
            return c;
         }

         static bool matchChar(const char**p, const char* end, char c)
         {
            if (*p == end) {
               return false;
            }

            if (**p == c) {
               ++*p;
               return true;
            }
            return false;
         }

         static UInt64 parseDigit(const char** p, const char* end)
         {
            char c = parseChar(p, end);
            if (!isdigit(c)) {
               throw ::std::runtime_error("Expected a digit");
            }
            return c - '0';
         }

         static UInt64 parse2Digits(const char ** p, const char* end)
         {
            size_t value = parseDigit(p, end) * 10;
            value += parseDigit(p, end);
            return value;
         }

         // parse a 4-digit year
         static UInt64 parseYear(const char** p, const char* end)
         {
            UInt64 CC = parse2Digits(p, end) * 100;
            UInt64 YY = parse2Digits(p, end);
            return CC + YY;
         }

         // parse month (months from 0 to 11)
         static UInt64 parseMonth(const char** p, const char* end)
         {
            UInt64 m = parse2Digits(p, end);
            if (m == 0 || m > 12) {
               throw ::std::runtime_error("Invalid month");
            }
            return m;
         }

         // parse month (months from 0 to 30)
         static UInt64 parseDay(const char** p, const char* end)
         {
            UInt64 d = parse2Digits(p, end);
            if (d == 0 || d > 31) {
               throw ::std::runtime_error("Invalid day");
            }
            return d;
         }

         // parse an hour
         static UInt64 parseHour(const char** p, const char* end)
         {
            UInt64 h = parse2Digits(p, end);
            if (h > 23) {
               throw ::std::runtime_error("Invalid hour");
            }
            return h;
         }

         // parse an hour
         static UInt64 parseMinute(const char** p, const char* end)
         {
            UInt64 m = parse2Digits(p, end);
            if (m > 59) {
               throw ::std::runtime_error("Invalid minute");
            }
            return m;
         }

         // parse an hour
         static UInt64 parseSecond(const char** p, const char* end)
         {
            UInt64 s = parse2Digits(p, end);
            if (s > 59) {
               throw ::std::runtime_error("Invalid second");
            }
            return s;
         }

         static UInt64 parseNanoSecond(const char** p, const char* end)
         {
            static const size_t MULTIPLIERS[] = { 1000000000, // 0
                  100000000, // 1
                  10000000, // 2
                  1000000, // 3
                  100000, // 4
                  10000, // 5
                  1000, // 6
                  100, // 7
                  10, // 8
                  1 // 9
                  };

            size_t n = 0;
            UInt64 value = 0;
            // read up to 9 digits
            for (char c; *p != end && isdigit(c = **p) && n < 9; ++n) {
               value = value * 10 + (c - '0');
               ++*p;
            }
            // multiply as needed to get nanoseconds
            assert(n < 10);
            value *= MULTIPLIERS[n];

            // skip any remaining digits
            while (isdigit(**p) && *p != end) {
               ++*p;
            }
            return value;
         }

         static bool isLeapYear(Int32 y) throws()
         {
            return (y % 4 == 0 && y % 100 != 0) || y % 400 == 0;
         }

         static Int64 getDaysNS(Int32 d, Int32 m, Int32 y) throws (::std::invalid_argument)
         {
            if (m < 1 || m > 12) {
               throw ::std::invalid_argument("Invalid month");
            }
            if (y == 0) {
               throw ::std::invalid_argument("Invalid year");
            }

            // need to adjust the year, because year 0 does not
            // exist
            if (y < 0) {
               ++y;
            }

            const Int32 adjustBase = 1970 * 365 + 1970 / 4 - 1970 / 100 + 1970 / 400;
            Int32 adjust = 0;
            adjust = y * 365 + y / 4 - y / 100 + y / 400;
            Int64 days = adjust - adjustBase;

            if (isLeapYear(y)) {
               if (d < 1 || d > l_leapYearDaysPerMonth[m - 1]) {
                  throw ::std::invalid_argument("Invalid day");
               }
               d += l_leapYearDays[m - 1];
               d -= 1; // this was added in the initial calculation of "days"
            }
            else {
               if (d < 1 || d > l_daysPerMonth[m - 1]) {
                  throw ::std::invalid_argument("Invalid day");
               }
               d += l_yearDays[m - 1];
            }

            days += d - 1;
            return days * ((24 * 3600) * INT64_C(1000000000));
         }

         static const char* parse8601Time(const char* s, size_t len, UInt64& tm) throws (::std::runtime_error)
         {
            const char* ptr = s;

            tm = parseHour(&ptr, s + len) * (3600 * INT64_C(1000000000));
            if (matchChar(&ptr, s + len, ':')) {
               tm += parseMinute(&ptr, s + len) * (60 * INT64_C(1000000000));
               if (matchChar(&ptr, s + len, ':')) {
                  tm += parseSecond(&ptr, s + len) * 1000000000;
                  if (matchChar(&ptr, s + len, '.')) {
                     tm += parseNanoSecond(&ptr, s + len);
                  }
               }
            }
            // check if we need to match a time zone
            if (matchChar(&ptr, s + len, 'Z')) {
               // zulu time; the default
            }
            else if (matchChar(&ptr, s + len, '+')) {
               // corresponds to eastern longitudes
               UInt64 tz = parseHour(&ptr, s + len) * (3600 * INT64_C(1000000000));
               if (matchChar(&ptr, s + len, ':')) {
                  tz += parseMinute(&ptr, s + len) * (60 * INT64_C(1000000000));
               }
               tm -= tz;
            }
            else if (matchChar(&ptr, s + len, '-')) {
               // corresponds to western longitudes
               UInt64 tz = parseHour(&ptr, s + len) * (3600 * INT64_C(1000000000));
               if (matchChar(&ptr, s + len, ':')) {
                  tz += parseMinute(&ptr, s + len) * (60 * INT64_C(1000000000));
               }
               tm += tz;
            }
            else {
               // no timezone specified; assuming ZULU
            }
            return ptr;
         }

         static const char* parse8601Date(const char* s, size_t len, UInt64& tm) throws (::std::exception)
         {
            const char* ptr = s;
            UInt16 YYYY = parseYear(&ptr, s + len);
            UInt16 MM = 1;
            UInt16 DD = 1;

            if (matchChar(&ptr, s + len, '-')) {
               MM = parseMonth(&ptr, s + len);
               if (matchChar(&ptr, s + len, '-')) {
                  DD = parseDay(&ptr, s + len);
               }
            }
            tm = getDaysNS(DD, MM, YYYY);
            return ptr;
         }

         static const char* parse8601DateTime(const char* s, size_t len, char timeSeparator,
               UInt64& tm) throws (::std::exception)
         {
            UInt64 xdate(0);
            UInt64 xtime(0);

            const char* ptr = parse8601Date(s, len, xdate);
            if (matchChar(&ptr, s + len, timeSeparator)) {
               ptr = parse8601Time(ptr, len, xtime);
            }
            tm = xdate + xtime;
            return ptr;
         }
      }

      const size_t ISO8601::DATETIME_BUFFER_SIZE;
      const size_t ISO8601::TIME_BUFFER_SIZE;
      const size_t ISO8601::DATE_BUFFER_SIZE;

      size_t ISO8601::formatDateTime(const Time& xtm, char buf[DATETIME_BUFFER_SIZE], size_t nDigits) throws()
      {
         assert(nDigits < 10 && "Too many digits for subsecond time");
         tm t;
         time_t d;
         const char* formatString = "%Y-%m-%dT%H:%M:%S";
         d = static_cast< time_t>(xtm.seconds());

         gmtime_r(&d, &t);
         size_t len = ::std::strftime(buf, 24, formatString, &t);
         assert(len == 19);
         if (nDigits > 0) {
            // print all nine digits, but then truncate
            ::std::sprintf(buf + len, ".%09u", xtm.nanoseconds());
            len += 1 + nDigits;
         }
         // always ZULU time
         buf[len++] = 'Z';
         buf[len] = '\0';
         return len;
      }

      size_t ISO8601::formatTime(const Time& xtm, char buf[TIME_BUFFER_SIZE], size_t nDigits) throws()
      {
         assert(nDigits < 10 && "Too many digits for subsecond time");
         tm t;
         time_t d;
         const char* formatString = "%H:%M:%S";
         d = static_cast< time_t>(xtm.seconds());

         gmtime_r(&d, &t);
         size_t len = ::std::strftime(buf, TIME_BUFFER_SIZE, formatString, &t);
         assert(len == 8);
         if (nDigits > 0) {
            // print all nine digits, but then truncate
            ::std::sprintf(buf + len, ".%09u", xtm.nanoseconds());
            len += 1 + nDigits;
         }
         // always ZULU time
         buf[len++] = 'Z';
         buf[len] = '\0';
         return len;
      }

      size_t ISO8601::formatDate(const Time& xtm, char buf[DATE_BUFFER_SIZE]) throws()
      {
         tm t;
         time_t d;
         const char* formatString = "%Y-%m-%d";
         d = static_cast< time_t>(xtm.seconds());

         gmtime_r(&d, &t);
         size_t len = ::std::strftime(buf, DATE_BUFFER_SIZE, formatString, &t);
         assert(len == DATE_BUFFER_SIZE - 1);
         return len;
      }

      Time ISO8601::parseTime(const char* str) throws (::std::exception)
      {
         if (str == 0) {
            throw ::std::invalid_argument("Null pointer");
         }
         UInt64 tm;
         size_t len = ::std::strlen(str);
         const char* end = parse8601Time(str, len, tm);
         if (end != (str + len)) {
            throw ::std::runtime_error("Invalid time string");
         }
         return Time(tm);
      }

      Time ISO8601::parseDate(const char* str) throws (::std::exception)
      {
         if (str == 0) {
            throw ::std::invalid_argument("Null pointer");
         }
         UInt64 tm;
         size_t len = ::std::strlen(str);
         const char* end = parse8601Date(str, len, tm);
         if (end != (str + len)) {
            throw ::std::runtime_error("Invalid date string");
         }
         return Time(tm);
      }

      Time ISO8601::parseDateTime(const char* str) throws (::std::exception)
      {
         if (str == 0) {
            throw ::std::invalid_argument("Null pointer");
         }
         UInt64 tm;
         size_t len = ::std::strlen(str);
         const char* end = parse8601DateTime(str, len, 'T', tm);
         if (end != (str + len)) {
            throw ::std::runtime_error("Invalid date-time string");
         }
         return Time(tm);
      }

      Time ISO8601::parseTime(const ::std::string& str) throws (::std::exception)
      {
         return parseTime(str.c_str());
      }

      Time ISO8601::parseDate(const ::std::string& str) throws (::std::exception)
      {
         return parseDate(str.c_str());
      }

      Time ISO8601::parseDateTime(const ::std::string& str) throws (::std::exception)
      {
         return parseDateTime(str.c_str());
      }

   }
}
