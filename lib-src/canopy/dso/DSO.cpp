#include <canopy/dso/DSO.h>
#include <canopy/fs/FileSystem.h>
#include <canopy/compiler.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cassert>
#include <string>
#include <cstring>

#include <dlfcn.h>

namespace canopy {
   namespace dso {
      namespace {
         struct SysDSO : public DSO
         {
               SysDSO(const char* dsoName, bool deleteWhenDone) throws(::std::exception, UnsupportedException)
                     : _dso(0), _deleteWhenDone(deleteWhenDone)
               {
                  if (dsoName == 0) {
                     throw ::std::invalid_argument("Missing dso name");
                  }

#if CANOPY_UNIX == 1
                  /* just in case, reset the dlerror */
                  dlerror();

                  _dso = dlopen(dsoName, RTLD_LOCAL | RTLD_NOW);
                  if (_dso == 0) {
                     ::std::string s("could not load dso: ");
                     throw ::std::runtime_error(s + dlerror());
                  }
#else
                  throw UnsupportedException("DSO not supported");
#endif
               }

               ~SysDSO() throws()
               {
#if CANOPY_UNIX == 1
                  if (_deleteWhenDone) {
                     try {
                        SysDSO::close(); // close is virtual!
                     }
                     catch (const ::std::exception& e) {
                        ::std::cerr << "Exception in DSO destructor " << e.what() << ::std::endl;
                     }
                  }
#endif
               }

               void* symbol(const char* s) throws (::std::exception)
               {
                  if (s == 0) {
                     throw ::std::invalid_argument("Missing symbol name");
                  }

                  if (_dso == 0) {
                     throw ::std::logic_error("DSO has been closed");
                  }

#if CANOPY_UNIX == 1
                  dlerror();
                  void* sym = dlsym(_dso, s);

                  /*
                   * We don't check the return value of sym, but need
                   * to check dlerror to see if the symbol was found;
                   * the symbol might actually be at 0
                   */
                  const char *error = dlerror();
                  if (error == 0) {
                     return sym;
                  }
#endif

                  throw ::std::runtime_error("Symbol not found");
               }

               void close() throws (::std::exception)
               {
                  if (_dso != 0) {
                     if (!_deleteWhenDone) {
                        throw ::std::logic_error("DSO is not supposed to be closed");
                     }
                     dlerror();
                     if (dlclose(_dso) != 0) {
                        ::std::string s("could not close dso: ");
                        throw ::std::runtime_error(s + dlerror());
                     }
                     _dso = 0;
                  }
               }

            private:
               void* _dso;

            private:
               bool _deleteWhenDone;
         };
      }

      DSO::DSO() throws()
      {
      }
      DSO::~DSO() throws()
      {
      }

      DSO::Spec::Spec(const char* dsoName, const char* dsoVersion) throws()
            : name(dsoName ? dsoName : ""), version(dsoVersion ? dsoVersion : "")
      {
      }

      DSO::Spec::Spec(const ::std::string& dsoName, const ::std::string& dsoVersion) throws()
            : name(dsoName), version(dsoVersion)
      {
      }

      DSO::Spec::Spec(const ::std::string& dsoName, unsigned int dsoVersion) throws()
            : name(dsoName)
      {
         ::std::ostringstream out;
         out << dsoVersion;
         version = ::std::move(out.str());
      }

      ::std::unique_ptr< DSO> DSO::load(const char* dsoPath,
            bool deleteWhenDone) throws (UnsupportedException,::std::exception)
      {
         return ::std::unique_ptr < DSO > (new SysDSO(dsoPath, deleteWhenDone));
      }

      ::std::unique_ptr< DSO> DSO::load(const ::std::vector< ::std::string>& spath, const Spec& dsoSpec,
            bool deleteWhenDone, ::std::string* path) throws (UnsupportedException,::std::exception)
      {
         ::std::unique_ptr< DSO> res;
         if (spath.empty()) {
            res = load(".", dsoSpec, deleteWhenDone, path);
         }
         else {
            for (const ::std::string& p : spath) {
               try {
                  res = load(p.c_str(), dsoSpec, deleteWhenDone, path);
                  break;
               }
               catch (...) {
                  // continue
               }
            }
         }
         if (res) {
            return res;
         }
         throw ::std::runtime_error("DSO not found or failure loading DSO");
      }

      ::std::unique_ptr< DSO> DSO::load(const char* path, const Spec& dsoSpec, bool deleteWhenDone,
            ::std::string* dsopath) throws (UnsupportedException,::std::exception)
      {
         const ::std::string searchPath(path ? path : ".");
         const ::std::string dsoFile = makeDSOPath(nullptr, dsoSpec);
         ::std::string p;

         ::std::unique_ptr< DSO> res;
         ::canopy::fs::FileSystem fs;
         p = fs.joinPaths(searchPath, dsoFile);
         if (fs.exists(p, true)) {
            res = load(p.c_str(), deleteWhenDone);
         }
         else if (!dsoSpec.validVersion()) {
            // is there a unambiguous dso at the path specified with a version? If so, load that
            ::std::vector< ::std::string> paths;
            const ::std::string prefix = dsoFile + '.';

            ::canopy::fs::FileSystem::SearchFilter filter = [&prefix] (const char*, const char* file) {
               return ::std::strncmp(prefix.c_str(),file,prefix.size()) == 0;
            };
            fs.listEntries(searchPath, paths, filter);
            if (paths.size() == 1) {
               p = ::std::move(paths[0]);
               res = load(p.c_str(), deleteWhenDone);
            }
            else if (paths.size() > 1) {
               throw ::std::runtime_error("Failed to locate an unambiguous version of "+dsoFile);
            }
         }

         if (res && dsopath) {
            *dsopath = p;
         }
         if (!res) {
            throw ::std::runtime_error("DSO not found or failure loading DSO");
         }
         return res;
      }

      ::std::string DSO::makeDSOPath(const char* path, const Spec& dsoSpec) throws (::std::invalid_argument)
      {
         if (dsoSpec.name.empty()) {
            throw ::std::invalid_argument("Not a valid dsoName");
         }

         // for now, we're only on unix
         ::canopy::fs::FileSystem fs;
         ::std::ostringstream out;
         out << "lib" << dsoSpec.name << ".so";
         if (dsoSpec.validVersion()) {
            out << '.' << dsoSpec.version;
         }
         ::std::string sysDSO = out.str();

         if (path != nullptr && ::std::strlen(path) != 0) {
            sysDSO = fs.joinPaths(path, sysDSO);
         }
         return sysDSO;
      }

   }
}
