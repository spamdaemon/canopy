#ifndef _CANOPY_DSO_SYMBOLTABLE_H
#define _CANOPY_DSO_SYMBOLTABLE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_UNSUPPORTEDEXCEPTION_H
#include <canopy/UnsupportedException.h>
#endif

#include <memory>
#include <stdexcept>
#include <string>
#include <set>

namespace canopy {
  namespace dso {

    /**
     * The SymbolTable enables access to the set of external symbols defined in
     * an executable or a shared library. This class is highly dependent on OS
     * support and the functionality it provides may not be available on certain
     * platforms. The set of external symbols that can be located are currently only
     * function and data symbols. The OTHER_SYMBOL category is to provide a way to
     * represent other types of symbols which may be local or global, and neither 
     * function nor data.
     * @note
     * The symbol table assumes that a symbol is uniquely identified by its name,
     * but it is possible that some systems, for example ELF based ones, have
     * objects with the same name at different addresses. How to deal with this?
     */
    class SymbolTable {
      /** The various symbol types */
    public:
      enum SymbolType {
	OTHER_SYMBOL,
	FUNCTION_SYMBOL,
	DATA_SYMBOL
      };

    private:
      CANOPY_BOILERPLATE_PREVENT_COPYING(SymbolTable);
      
      /**
       * Create a new SymbolTable
       */
    protected:
      inline SymbolTable() throws () {}

      /**
       * Destroy this symbol table.
       */
    public:
      virtual ~SymbolTable() throws () {}

      /**
       * Load the symbols available in the specified file. It is necessary to specifiy
       * the full path name to the library as the lookup rules applied to DSO objects are
       * not used.
       * @param filename the name of an executable or a (shared) library
       * @return a table of the symbols in the file
       * @throws ::std::runtime_error if the file is not an executable or a 
       * @throws UnsupportedException if this SymbolTable is not supported on this platform.
       */
    public:
      static ::std::unique_ptr<SymbolTable> load(const ::std::string& filename) throws (::std::exception, UnsupportedException);

      /**
       * Get the file name from which this symbol table was extracted.
       * @return the name of the file from which this table was loaded
       */
    public:
      virtual ::std::string source() const throws () = 0;

      /**
       * Test if the specified symbol is defined.
       * @param s a symbol name
       * @return true if s is a symbol
       */
    public:
      virtual bool isSymbolName(const ::std::string& s) const throws () = 0;

      /**
       * Get the symbol type.
       * @param s a symbol name
       * @return a symbol type
       * @throws ::std::invalid_argument if !isSymbolName(s)
       */
    public:
      virtual SymbolType symbolType(const ::std::string& s) const throws (::std::invalid_argument) = 0;

      /**
       * Get the names of all symbols
       * @return the names of symbols
       */
    public:
      virtual ::std::set< ::std::string> names() const throws () = 0;
    };
  }
}


#endif
