#ifndef _CANOPY_DSO_H
#define _CANOPY_DSO_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_UNSUPPORTEDEXCEPTION_H
#include <canopy/UnsupportedException.h>
#endif

#include <memory>
#include <string>
#include <vector>

namespace canopy {
   namespace dso {
      /**
       * The DSO is used to provide a handle to system shared libray which can
       * be explicitly loaded by the program. The purpose of this class is to
       * support configurable plugins.
       * When using a DSO users must decide if they wish to remove the shared
       * library when the DSO object destroyed, or if they wish to keep it around.
       * <p>
       * The following example illustrates use of this class by loading the math
       * library <em>libm.so</em> and calling
       * the function sqrt(4.0):
       * @code
       *  try {
       *   // the function in the library
       *   typedef double (*SQRT)(double);
       *
       *   // load the DSO
       *   ::std::unique_ptr<DSO> dso = DSO::load("libm.so",true);
       *
       *   // find the sqrt symbol and cast to appropriate type
       *   SQRT dynSQRT  = reinterpret_cast<SQRT>(dso->symbol("sqrt"));
       *
       *   // call the symbol
       *   ::std::cerr << "square root of 4 = " << dynSQRT(4.0) << ::std::endl;
       *
       *   // close the dso  before it gets deleted
       *   dso->close();
       *  }
       *  catch (const ::std::exception& e) {
       *    ::std::cerr << "Could not load shared library" << ::std::endl;
       *  }
       *  //
       * @endcode
       * If loading the same DSO twice or more, then the same object must be returned. That means, that the
       * addresses of all symbols must be the same.
       * @note Not all systems may support this class in which case loading a DSO will fail.
       */
      class DSO
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(DSO);

            /**
             * A DSO spec is just a name and a version number.
             */
         public:
            struct Spec
            {
                  /**
                   * Create a spec from a simple name.
                   * @param dsoName the dso name
                   * @param dsoVersion an optional version string
                   */
                  Spec(const char* dsoName = nullptr, const char* dsoVersion = nullptr) throws();

                  /**
                   * Create a spec from a simple name.
                   * @param dsoName the dso name
                   * @param dsoVersion an optional version string
                   */
                  Spec(const ::std::string& dsoName, const ::std::string& dsoVersion = ::std::string()) throws();

                  /**
                   * Create a spec from a simple name.
                   * @param dsoName the dso name
                   * @param dsoVersion a simple version number
                   */
                  Spec(const ::std::string& dsoName, unsigned int dsoVersion) throws();

                  /**
                   * Test if the version is valid.
                   * @return true if a version was specified, false otherwise
                   */
                  bool validVersion() const throws()
                  {
                     return !version.empty();
                  }

                  /** The simple library DSO name.
                   */
                  ::std::string name;

                  /** The version of the library or <0 if unspecified */
                  ::std::string version;
            };

            /**
             * A default constructor for a null-dso
             */
         protected:
            DSO() throws ();

            /**
             * Destroy a dso. If the DSO has not been closed prior to this call
             * then the DSO will be closed. If closing of the DSO does not succeed,
             * no exception will be thrown, but an error message will be written to
             * std::cerr.
             */
         public:
            virtual ~DSO() throws ();

            /**
             * Load a new dso. The deleteWhenDone should be set to false if a function or pointer
             * escaped from the loaded object and will remain active long after this DSO object
             * has been destroyed. The following example illustrate when it is appropriate to set
             * deleteWhenDone=true:
             * @code
             *  Plugin* openPlugin (const char* pluginLibrary)
             *  {
             *        typedef Plugin* (*PluginFunction)()
             *        ::std::unique_ptr<DSO> dso = DSO::load(pluginLibrary,false);
             *        PluginFunction pf = (PluginFunction)dso->symbol("createPlugin");
             *        // instantiate a new plugin
             *        Plugin* res = pf();
             *        // return the plugin
             *        return res;
             *  }
             * @endcode
             * If deleteWhenDone is set to true, then the resulting Plugin* would contain data
             * structures and function located in the deleted DSO which would lead to a segmentation
             * fault. When in doublt, set deleteWhenDone=false.
             *
             * @param dsoPath the full path to the DSO (must be findable from the current working directory)
             * @param deleteWhenDone delete the dso and any resource in the destructor if true.
             * @return a pointer to a dso (never 0)
             * @throws ::std::invalid_argument if dsoName==0
             * @throws ::std::exception if the dso could not be loaded
             * @throws UnsupportedException if DSO are not supported
             */
         public:
            static ::std::unique_ptr< DSO> load(const char* dsoPath,
                  bool deleteWhenDone) throws (UnsupportedException,::std::exception);

            /**
             * Find and load a new dso specified by a OS independent name and a search path. This function utilizes
             * makeDSOPath() to generate an OS specific path. If the search path is empty, then this method proceeds as if
             * "." was in the search path.
             *
             * @param spath the vector of search paths.
             * @param dsoSpec the spec for the dso
             * @param deleteWhenDone delete the dso and any resource in the destructor if true.
             * @param path if not null this parameter is set to the path at which the DSO was found
             * @return a pointer to a dso (never 0)
             * @throws ::std::invalid_argument if dsoName==0
             * @throws ::std::exception if the dso could not be loaded
             * @throws UnsupportedException if DSO are not supported
             */
         public:
            static ::std::unique_ptr< DSO> load(const ::std::vector< ::std::string>& spath, const Spec& dsoSpec,
                  bool deleteWhenDone, ::std::string* path = nullptr) throws (UnsupportedException,::std::exception);

            /**
             * Load a new dso specified by a OS independent name and a separate path. This function utilizes
             * makeDSOPath() to generate an OS specific path.
             * If the path is 0, then finding the DSO is system dependent, which means, it may use a system specific
             * search path, such as LD_LIBRARY_PATH.
             *
             * @param path the path to the dso
             * @param dsoSpec the spec for the dso
             * @param deleteWhenDone delete the dso and any resource in the destructor if true.
             * @param dsopath if not null this parameter is set to the path at which the DSO was found
             * @return a pointer to a dso (never 0)
             * @throws ::std::invalid_argument if dsoName==0
             * @throws ::std::exception if the dso could not be loaded
             * @throws UnsupportedException if DSO are not supported
             */
         public:
            static ::std::unique_ptr< DSO> load(const char* path, const Spec& dsoSpec, bool deleteWhenDone,
                  ::std::string* dsopath = nullptr) throws (UnsupportedException,::std::exception);

            /**
             * Create the dso path from a search path and a simple DSO name. The simple DSO name must not have
             * any prefix or suffix added to it. This function will add an appropriate prefix and suffix to the
             * simple name according to the OS naming scheme for DSOs.
             * <p>
             * For example,
             * the call @code makeDSOPath("/modules","X") @endcode results in @code /modules/libX.so @endcode on on Unix-like systems,
             * and @code modules\X.dll  @endcode on Windows systems.
             * @param path the path to the dso (can be null or empty)
             * @param dsoSpec the simple name of the dso
             * @return the name of dso
             * @throws ::std::invalid_argument if dsoName==0
             */
         public:
            static ::std::string makeDSOPath(const char* path, const Spec& dsoSpec) throws (::std::invalid_argument);

            /**
             * Close this DSO. Once closed, a symbol cannot be retrieved.
             * If closing this DSO is successful, then the ~DSO() will <em>not</em>
             * throw an exeption. If this method is called more then once, then all but
             * the first invocation are ignored.
             * @throws ::std::logic_error if this DSO may not be deleted
             * @throws an exception if the DSO could not be closed
             */
         public:
            virtual void close() throws (::std::exception) = 0;

            /**
             * Get the dso symbol with the specified name.
             * @param s the symbol name.
             * @return a pointer to the symbol
             * @throws ::std::logic_error if this DSO has already been closed
             * @throws ::std::invalid_argument if s==0
             * @throws ::std::exception if the symbol cannot be found
             */
         public:
            virtual void* symbol(const char* s) throws (::std::exception) = 0;

      };
   }
}

#endif
