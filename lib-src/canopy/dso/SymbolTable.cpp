#include <canopy/dso/SymbolTable.h>
#include <canopy/compiler.h>

#include <map>
#include <iostream>
#include <cassert>
#include <memory>


#if CANOPY_EXECUTABLE_FORMAT == CANOPY_ELF
#if CANOPY_HAVE_LIBELF==1
// these are needed for elf support
#include <elf.h>
#else
#warning "Executable format is elf, but libelf not available"
#endif

#if CANOPY_HAVE_GELF == 1
#include <gelf.h>
#endif

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#else
#warning "CANOPY_EXECUTABLE_FORMAT not supported"
#endif

using namespace ::std;

namespace canopy {
  namespace dso {

    static bool setCloseOnExec(int fd) throws()
    {
      int flags = ::fcntl(fd,F_GETFD);
      if (flags == -1) {
	return false;
      }
      flags |= FD_CLOEXEC;
      return ::fcntl(fd,F_SETFD,flags) == 0;
    }

#if CANOPY_HAVE_LIBELF==1
    namespace elf {

      /** The symbols */
      struct Symbol {
	/** The symbol's address */
	void* address;

	/** The name of the symbol */
	string name;

	/** The ascii name (aka demangled name) */
	string asciiName;

	/** The file of this symbol (0 if not available) */
	string file;

	/** The line number of the symbol (0 if not available) */
	int line;

	/** The type (0 function, 1 data ) */
	SymbolTable::SymbolType type; 
      };

      /**
       * Callback that is invoked when a symbol is processed.
       */
      typedef void (*SymbolCB)(const Symbol&,void*);
#if CANOPY_HAVE_GELF == 1
      static SymbolTable::SymbolType filterSymbolType(GElf_Sym* sym)
      {
	// if a local symbol 
	if (GELF_ST_BIND(sym->st_info)==STB_LOCAL) {
	  return SymbolTable::OTHER_SYMBOL;
	}
	
	switch (GELF_ST_TYPE(sym->st_info)) {
	case STT_FUNC: 
	  return SymbolTable::FUNCTION_SYMBOL;
	case STT_OBJECT: 
	  return SymbolTable::DATA_SYMBOL;
	case STT_COMMON: 
	  return SymbolTable::DATA_SYMBOL;
	default:
	  return SymbolTable::OTHER_SYMBOL;
	};
      }

      static int loadSymbols_GELF(Elf* elf, SymbolCB cb,void* udata)
      {
	GElf_Ehdr ehdr;
	GElf_Shdr shdr;
	GElf_Sym sym;

	if (gelf_getehdr(elf,&ehdr)==0) {
	  goto ELF_ERROR;
	}
	
	for (Elf_Scn* scn =0;(scn = elf_nextscn(elf,scn))!=0;) {
	  if (gelf_getshdr(scn,&shdr)==0) {
	    goto ELF_ERROR;
	  }
	  // skip anything not in the regular symbol table or the dynamic
	  // symbol table
	  if (shdr.sh_type != SHT_SYMTAB && shdr.sh_type!=SHT_DYNSYM) {
	    continue;
	  }
	  // there will only be 1 data buffer, since we're reading the 
	  // object from a file (we'll only have multiple buffers if
	  // we're manipulating the section).
	  Elf_Data* data = elf_getdata(scn,0);

	  // this how we can compute the number of entries; let's 
	  // just hope that the fields offsets are the same 
	  // across different elf implementations. Normally,
	  // we should be able to do this with the data,
	  // but the size field is at different locations in 
	  // RedHat's elfutils and libelf.
	  const int nSym = static_cast<int>(shdr.sh_size / shdr.sh_entsize);
	  assert(nSym * shdr.sh_entsize == shdr.sh_size);
 
	  for (int i=0;i<nSym;++i) {
	    if (gelf_getsym(data,i,&sym)==0) {
	      goto ELF_ERROR;
	    }
	    const char* name = elf_strptr(elf,shdr.sh_link,sym.st_name);
	    if (name!=0 && name[0]!='\0') {
	      Symbol symbol;
	      symbol.type = filterSymbolType(&sym);
	      symbol.address = reinterpret_cast<void*>(sym.st_value);
	      if (symbol.address!=0) {
		symbol.name = name;
		(*cb)(symbol,udata);
	      }
	    }
	  }
	}

	return 0;

      ELF_ERROR:
	cerr << "Internal ELF Error : " << elf_errmsg(elf_errno()) << endl;
	return 1;
      }
#endif

      /**
       * Load the function symbols from the specified executable file
       * @param file an executable file in ELF object format
       * @param cb callback that is invoked foreach symbol that is found
       * @param udata user data that is passed to the callback
       * @return 0 on success, 1 on failure
       */
      static int loadSymbols (const char* file,SymbolCB cb,void* udata)
      {
	if (elf_version(EV_CURRENT)==EV_NONE) {
	  cerr << "Incompatible ELF version" << endl;
	  return 1;
	}

	int fp = open(file,O_RDONLY);
	if (fp==-1) {
	  cerr << "Could not open file '" << file << "'" << endl;
	  return 1;
	}
	if (!setCloseOnExec(fp)) {
	  cerr << "Could not set FD_CLOEXEC" << endl;
	  ::close(fp);
	  return 1;
	}
	
	Elf *elf = elf_begin(fp,ELF_C_READ,0);
	if (elf==0) {
	  cerr << "Could not open elf archive '" << file << "'" << endl;
	  close(fp);
	  return 1;
	}

	int res(1);
	try {
#if CANOPY_HAVE_GELF == 1
	  res = loadSymbols_GELF(elf,cb,udata);
#else
#warning  "Cannot load symbols"
#endif
	}
	catch (...) {
	}
	
        elf_end(elf);
        close(fp);
	return res;
      }

    }
#endif
 
    namespace {
      struct SysSymbolTable : public SymbolTable {
	SysSymbolTable(const string& filename)  throws(exception)
	  : _source(filename)
	{
#if CANOPY_HAVE_LIBELF==1
	  if (filename.empty()){ 
	    throw invalid_argument("Missing filename name");
	  }
	  if (::canopy::dso::elf::loadSymbols(filename.c_str(),insertSymbol, this)!=0) {
	    throw runtime_error("Could not load symbols");
	  }
#endif
	}

	~SysSymbolTable() throws()
	{
	}

	string source() const throws()
	{ return _source; }

	
	bool isSymbolName(const string& s) const throws()
	{ 
#if CANOPY_HAVE_LIBELF==1
           return _symbols.count(s)==1; 
#else
	   return false;
#endif
        }

	SymbolType symbolType(const string& s) const throws(invalid_argument)
	{
#if CANOPY_HAVE_LIBELF==1
	  map< string, ::canopy::dso::elf::Symbol>::const_iterator i = _symbols.find(s);
	  if (i !=_symbols.end()) {
	    return i->second.type;
	  }
#endif
	  throw invalid_argument("Not a global symbol "+s);
	}

	
	set< string> names() const throws()
	{
	  set< string> res;
#if CANOPY_HAVE_LIBELF==1
	  for (map< string, ::canopy::dso::elf::Symbol>::const_iterator i=_symbols.begin();i!=_symbols.end();++i) {
	    res.insert(i->first);
	  }
#endif
	  return res;
	}

	
#if CANOPY_HAVE_LIBELF==1
      private:
	static void insertSymbol(const ::canopy::dso::elf::Symbol& s,void* data)
	{
	  SysSymbolTable* v = reinterpret_cast<SysSymbolTable*>(data);
	  v->_symbols[s.name] = s;
	}
#endif	
      private:
	const string _source;

#if CANOPY_HAVE_LIBELF==1
      private:
	map< string, ::canopy::dso::elf::Symbol> _symbols;
#endif	
      };
    }


    unique_ptr<SymbolTable> SymbolTable::load(const string& filename) throws (exception,UnsupportedException)
    {
#if CANOPY_HAVE_LIBELF==1 && CANOPY_HAVE_GELF==1
      return unique_ptr<SymbolTable>(new SysSymbolTable(filename)); 
#else
      throw UnsupportedException("SymbolTable is not supported");
#endif
    }
  }
}

