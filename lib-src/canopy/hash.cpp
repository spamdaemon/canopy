#include <canopy/hash.h>
#include <cctype>

namespace canopy {
  
  size_t hashString (const char* str,size_t   length) throws()
  {
    // this hashfunction was found on the web
    // and seems to be based on g_str_hash()
    size_t h = 0;
    const char* a = str;
    const char* e = a + length;
    while (a<e) {
      char c = *a++;
      if (c >= 0x60) {
	c -= 40;
      }
      h = (h<<3) + (h>>28) + c;
    }
    return h;
  }

  size_t hashString (const char* str) throws()
  {
    // this hashfunction was found on the web
    // and seems to be based on g_str_hash()
    size_t h = 0;
    const char* a = str;
    while (*a!='\0') {
      char c = *a++;
      if (c >= 0x60) {
	c -= 40;
      }
      h = (h<<3) + (h>>28) + c;
    }
    return h;
  }

  size_t hashStringIgnoreCase (const char* str, size_t length) throws()
  {
    // this hashfunction was found on the web
    // and seems to be based on g_str_hash()
    size_t h = 0;
    const char* a = str;
    const char* e = a + length;
    while (a<e) {
      char c = ::std::tolower(*a++);
      if (c >= 0x60) {
	c -= 40;
      }
      h = (h<<3) + (h>>28) + c;
    }
    return h;
  }

  size_t hashStringIgnoreCase (const char* str) throws()
  {
    // this hashfunction was found on the web
    // and seems to be based on g_str_hash()
    size_t h = 0;
    const char* a = str;
    while (*a!='\0') {
      char c = ::std::tolower(*a++);
      if (c >= 0x60) {
	c -= 40;
      }
      h = (h<<3) + (h>>28) + c;
    }
    return h;
  }

 
}
