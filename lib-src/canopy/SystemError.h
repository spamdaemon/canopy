#ifndef _CANOPY_SYSTEMERROR_H
#define _CANOPY_SYSTEMERROR_H

#include <canopy/canopy.h>
#include <stdexcept>

namespace canopy {

  /**
   * The system error is thrown functions or methods that may fail
   * for reasons that are not related to programmer error, but may
   * occur due to other circumstances.
   * For example, trying the delete a non-existent file.
   */
  class SystemError : public ::std::runtime_error {

    /**
     * Create a new system error
     */
  public:
    SystemError() throws ();

    /**
     * Create a new system error.
     * @param msg a message
     */
  public:
    SystemError(  ::std::string msg) throws ();

    /**
     * Destructor
     */
  public:
    ~SystemError() throw();

  };
   
}

#endif
