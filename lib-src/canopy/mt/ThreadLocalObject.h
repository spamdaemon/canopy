#ifndef _CANOPY_MT_THREADLOCALOBJECT_H
#define _CANOPY_MT_THREADLOCALOBJECT_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_MT_THREADLOCAL_H
#include <canopy/mt/ThreadLocal.h>
#endif

#include <stdexcept>

namespace canopy {
  namespace mt {
    /**
     * This class allows users to associate data with individual
     * threads. It is essentially possible to create static data
     * in a multithreded system to hold temporary data. Objects
     * are created during the first access to an instance of a 
     * ThreadLocal. <br>
     * A typical use of ThreadLocal is this
     * @code
     *   void foo ()
     *   {
     *       static ThreadLocalObject<int> bar;
     *       (*bar)++;
     *   }
     * @endcode
     * The operating system may limit the total number of ThreadLocalObject instances.
     */
    template <class T> class ThreadLocalObject {
      /** No copying allowed */
    private:
      ThreadLocalObject(const ThreadLocalObject&);
      ThreadLocalObject& operator=(const ThreadLocalObject&);
      
      /**
       * Create a new thread local. This method
       * requires that <code>T::T()</code> exists.
       * @throws a ::std::runtime_error if there were not sufficient resources
       */
    public:
      inline ThreadLocalObject () throws (::std::runtime_error)
	try : _local(destructor) 
	{}
      catch (...) {
	throw ::std::runtime_error("Could not create thread local");
      }
    
      /**
       * Create a new thread local with the specified default value.
       * @param t a default value
       * @throws a ::std::runtime_error if there were not sufficient resources
       */
    public:
      inline ThreadLocalObject (const T& t) throws (::std::runtime_error)
	try : _defaultValue(t),_local(destructor) {
      }
      catch (...) {
	throw ::std::runtime_error("Could not create thread local");
      }
    
      /**
       * Destroy this thread local.
       */
    public:
      inline ~ThreadLocalObject () throws ()
	{ }
    
      /**
       * Access the value of this thread local.
       * @return a reference to the value
       */
    public:
      inline T& operator*() throws ()
	{ return *value(); }

      /**
       * Access the value of this thread local.
       * @return a pointer to the thread local
       */
    public:
      inline T* operator->() throws ()
	{ return &value(); }

      /**
       * Access the value of this thread local.
       * @return the associated value as a reference
       */
    public:
      inline T* value () throws ()
	{
	  void* data = _local.value();
	  return data==0 ? createValue() : static_cast<T*>(data);
	}
    
      /**
       * Test if a value has been set.
       * @return true if the local has a non 0 value
       */
    public:
      inline bool hasValue () const throws ()
	{ return _local.hasValue(); }

      /**
       * Reset the value to the default value.
       */
    public:
      inline void reset () throws ()
	{ value() = _defaultValue; }

      /**
       * Initialize the value.
       * @return a pointer to the new value
       */
    private:
      T* createValue() throws ()
	{
	  T* data = new T(_defaultValue);
	  _local.setValue(data);
	  return data;
	}

      /**
       * The destruction function for the value.
       * Initialize the value.
       * @return a pointer to the new value
       */
    private:
      static void destructor (void* data) throws ()
	{	delete static_cast<T*>(data); }
    
      /** The default value */
    private:
      const T _defaultValue;
    
      /** The key associated with this local */
    private:
      ::canopy::mt::ThreadLocal _local;
    };

    template <> inline ThreadLocalObject<Int32>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<UInt32>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<Int16>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<UInt16>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<Int8>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<UInt8>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<Float>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<Double>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(0),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
    template <> inline ThreadLocalObject<bool>::ThreadLocalObject() throws (::std::runtime_error)
      try : _defaultValue(false),_local(destructor) {}
    catch (...) {
      throw ::std::runtime_error("Could not create thread local");
    }
  
  
  }
}
#endif
