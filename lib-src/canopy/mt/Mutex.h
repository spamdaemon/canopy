#ifndef _CANOPY_MT_MUTEX_H
#define _CANOPY_MT_MUTEX_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <mutex>

namespace canopy {
   namespace mt {

      /**
       * This is an implementation of a simple mutex
       * which can be used to protect a section
       * of code from concurrent execution.
       */
      class Mutex
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Mutex)
            ;

            /**
             * Creates a new Mutex instance.
             */
         public:
            inline Mutex() throws ()
            {
            }

            /**
             * Destroys this Mutex object.
             * @pre this mutex is not locked
             */
         public:
            inline ~Mutex() throws ()
            {
            }

            /**
             * @name Mutex operations
             * @{
             */

            /**
             * Lock this mutex.
             */
         public:
            inline void lock() const throws ()
            {
               _mutex.lock();
            }

            /**
             * Try to lock this mutex.
             * @return true if the mutex was locked, false otherwise
             */
         public:
            inline bool tryLock() const throws ()
            {
               return _mutex.try_lock();
            }

            /**
             * Unlock this mutex.
             */
         public:
            inline void unlock() const throws ()
            {
               _mutex.unlock();
            }

            /*@}*/

            /** The mutex */
         private:
            mutable ::std::mutex _mutex;
      };
   }
}
#endif
