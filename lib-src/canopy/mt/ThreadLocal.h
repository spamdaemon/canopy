#ifndef _CANOPY_MT_THREADLOCAL_H
#define _CANOPY_MT_THREADLOCAL_H

#ifndef _CANOPY_DEFINES_H
#include <canopy/defines.h>
#endif

#ifndef _CANOPY_BOILERPLATE_H
#include <canopy/boilerplate.h>
#endif

#ifndef _CANOPY_TYPES_H
#include <canopy/types.h>
#endif

#include <iosfwd>

namespace canopy {
  namespace mt {

    /**
     * This class allows the creation of a thread private or
     * local data. Thread local data is accessed through one
     * of these keys. 
     * @note This class is primarily for internal use only.
     * @see ThreadLocalObject for thread-local objects.
     */
    class ThreadLocal {
          CANOPY_BOILERPLATE_PREVENT_COPYING(ThreadLocal);

      /**
       * Create a new thread local key. The destructor for this key
       * is automatically called when the current thread exits. If the
       * destructor is 0, nothing is done.
       * @param destructor the destructor for this key
       * @throws throws an exception if the key cannot be created
       */
    public:
      ThreadLocal (void (*destructor)(void*) );
    
      /**
       * Destroy this thread local key.
       */
    public:
      ~ThreadLocal () throws();
    
      /**
       * Set the value associated with this key.
       * @param v a value to be associated
       * @return true if the value was associated, false otherwise
       */
    public:
      void setValue (void* v) throws();
      
      /**
       * Access the value that is associated with this key.
       * @return the associated value.
       */
    public:
      void* value () const throws();
      
      /**
       * Test if a value is currently associated with this key.
       * @return true if a value is currently associated with this key.
       */
    public:
      inline bool hasValue () const throws()
	{ return value()!=0; }

      /** The key */
    private:
      const UInt32 _key;

      /** The destructor */
    private:
      void (*_destructor)(void* );
    };
  }
}
#endif
