#ifndef _CANOPY_MT_MUTEXGUARD_H
#define _CANOPY_MT_MUTEXGUARD_H

#ifndef _CANOPY_BOILERPLATE_H
#include <canopy/boilerplate.h>
#endif

namespace canopy {
   namespace mt {
      /** Forward declaration */
      class Mutex;

      /**
       * This class is used to make unlocking
       * and locking of mutexes safer.
       */
      template<class MUTEX = Mutex> class MutexGuard
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(MutexGuard);

            /**
             * Creates a new Guard that locks the
             * specified mutex.
             * @param m a mutex
             */
         public:
            inline MutexGuard(const volatile MUTEX& m)
                  : _mutex(const_cast<MUTEX&>(m))
            {
               _mutex.lock();
            }

            /**
             * Creates a new Guard that locks the
             * specified mutex.
             * @param m a mutex
             */
         public:
            inline MutexGuard(const MUTEX& m)
                  : _mutex(m)
            {
               _mutex.lock();
            }

            /**
             * Destroys this Guard object. The
             * guarded mutex is unlocked.
             */
         public:
            inline ~MutexGuard()
            {
               _mutex.unlock();
            }

            /** The mutex to be guarded */
         private:
            const MUTEX& _mutex;
      };
   }
}
#endif
