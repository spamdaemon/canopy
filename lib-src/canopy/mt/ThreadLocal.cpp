#include <canopy/mt/ThreadLocal.h>
#include <canopy/Static.h>
#include <canopy/compiler.h>

#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
#include <pthread.h>
#endif

#include <canopy/canopy.h>
#include <vector>
#include <cstdlib>
#include <cassert>

namespace canopy {
  namespace mt {
    namespace {
      struct Tuple {
	inline Tuple (void (*destructor) (void*), void* data) 
	  : _destructor(destructor),_data(data) {}

	void (*_destructor) (void*);
	void* _data;
      };

      /** The typle vector */
      typedef ::std::vector<Tuple> TupleVector;

#if CANOPY_THREAD_SUPPORT != CANOPY_POSIX
      static TupleVector* POOL(0);
#endif

#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
      /**
       * The key which each thread uses
       * to store the Thread.
       */
      static ::pthread_key_t threadKey;
    
      /**
       * This is required by the pthread_once call.
       */
      static ::pthread_once_t onceControl = PTHREAD_ONCE_INIT;
#endif

      /**
       * Holds returned keys so that we can reuse them
       */
      static volatile ::std::vector<UInt32>* returnedKeys;

      /** The guard for the retured keys */
      static Static<volatile ::std::vector<UInt32> > returnedKeysGuard(new ::std::vector<UInt32>(),returnedKeys);
      
      /**
       * The maximum number of keys allocated so far.
       */
      static volatile UInt32 nextKey = 0;

#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
      /**
       * The mutex that protects access to some shared data
       */
      static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
      static void deletePool (void* xpool) throws()
#else
	static void deletePool () throws()
#endif
      {
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
	TupleVector* pool = static_cast< TupleVector* >(xpool);
#else
	TupleVector* pool = POOL;
#endif

	if (pool!=0) {
	  // make sure to clean out all the tuples in the pool
	  TupleVector::iterator i = pool->begin();
	  TupleVector::iterator end = pool->end();
	  while (i!=end) {
	    if (i->_data!=0 && i->_destructor!=0) {
	      (i->_destructor)(i->_data);
	    }
	    ++i;
	  }
	  delete pool;
	}
      }

      /**
       * Initialize the thread key
       */
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
      static void setupThreadKey () throws()
      {
	// create the thread key (which we won't destroy)
	int err = ::pthread_key_create(&threadKey,deletePool);
	abortOnError(err,__FILE__,__LINE__);
	assert (::pthread_getspecific(threadKey)==0);
      }
#endif
    
      /**
       * Ensure that a thread key is created.
       */
      static void init () throws()
      { 
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
	::pthread_once(&onceControl,setupThreadKey); 
#else
	if (POOL==0) {
	  POOL = new TupleVector();
	  int err = ::std::atexit(deletePool);
	  abortOnError(err,__FILE__,__LINE__);
	}	      
#endif
      }

      static void returnKey (UInt32 oldKey) throws()
      {
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
	::pthread_mutex_lock(&mutex); 
#endif
	if (returnedKeys!=0) {
	  const_cast< ::std::vector<UInt32>&>(*returnedKeys).push_back(oldKey);
	}

#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
	::pthread_mutex_unlock(&mutex);
#endif
      }

      static UInt32 createKey() throws()
      {
	init();

	UInt32 newKey = 0;

#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
	::pthread_mutex_lock(&mutex); 
#endif

	{
	  if (returnedKeys!=0 && const_cast< ::std::vector<UInt32>&>(*returnedKeys).size()>0) {
	    newKey = const_cast< ::std::vector<UInt32>&>(*returnedKeys).back();
	    const_cast< ::std::vector<UInt32>&>(*returnedKeys).pop_back();
	  }
	  else {
	    newKey = nextKey++;
	  }
	}
	
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
	::pthread_mutex_unlock(&mutex);
#endif

	return newKey;
      }
    
    }
    ThreadLocal::ThreadLocal(void (*destructor)(void*)) 
      : _key(createKey()),_destructor(destructor)
    {
    }

    ThreadLocal::~ThreadLocal() throws()
    {
      // there might still be some live objects!
      returnKey(_key);
    }

    void ThreadLocal::setValue (void* v) throws()
    {
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
      TupleVector* pool = static_cast< TupleVector* >(::pthread_getspecific(threadKey));
      if (pool==0) {
	pool = new TupleVector();
	::pthread_setspecific(threadKey,pool);
      }
#else
      TupleVector* pool = POOL;
#endif
      if (pool->size() <= _key) {
	pool->resize(_key+10,Tuple(0,0));
      }
      pool->at(_key) = Tuple(_destructor,v);
    }

    void* ThreadLocal::value () const throws()
    {
#if CANOPY_THREAD_SUPPORT == CANOPY_POSIX
      TupleVector* pool = static_cast< TupleVector* >(::pthread_getspecific(threadKey));
#else
      TupleVector* pool = POOL;
#endif
      if (pool==0 || pool->size() <= _key) {
	return 0;
      }
      return pool->at(_key)._data;
    }

  }
}
