#ifndef _CANOPY_MT_CONDITION_H
#define _CANOPY_MT_CONDITION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_TIME_TIME_H
#include <canopy/time/Time.h>
#endif

#include <mutex>
#include <condition_variable>

namespace canopy {
   namespace mt {
      /**
       * The condition class is used to synchronize two or more threads
       * by exchanging a single signal. A simple case is the producer-consumer
       * pattern illustrated in the following example where a produce creates
       * a character and passes it on to a consumer.
       * @code
       * // this code will work well on a single processor system with a single
       * // producer and a consumer.
       * static Condition condition;
       * static volatile int iteration=-1;
       *
       * void producer() {
       *   condition.lock();
       *   while (true) {
       *      // the value to be exchanged
       *      ++iteration;
       *      // notify the consumer
       *      condition.notify();
       *      // give up the mutex until the consumer has picked up the value
       *      condition.wait();
       *   }
       *   condition.unlock();
       * }
       *
       * void consumer () {
       *   condition.lock();
       *   // if consumer gets here first
       *   // then he has to wait for the producer
       *   if (iteration==-1) {
       *     condition.wait();
       *   }
       *   while(true) {
       *     // pick up the value
       *     ::std::cerr << "Iteration value " << iteration << ::std::endl;
       *     // and notify the producer that we've got the value
       *     condition.notify();
       *     // wait until the producer does a new iteration.
       *     condition.wait();
       *   }
       *   condition.unlock();
       * }
       *
       * @endcode
       *
       */
      class Condition
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Condition);

            /** The result of wait */
         public:
            enum ConditionResult
            {
               SIGNALLED = 0, TIMEOUT = 1, INTERRUPTED = 2
            };

            /**
             * Creates a new Condition instance.
             */
         public:
            inline Condition() throws ()
            {
            }

            /**
             * Destroys this Condition object.
             */
         public:
            inline ~Condition() throws ()
            {
            }

            /**
             * @name Mutex operations
             * @{
             */
            /**
             * Lock this mutex.
             */
         public:
            inline void lock() const throws ()
            {
               _mutex.lock();
            }

            /**
             * Try to lock this mutex.
             * @return true if this mutex was locked, false otherwise.
             */
         public:
            inline bool tryLock() const throws ()
            {
               return _mutex.try_lock();
            }

            /**
             * Unlock this mutex
             */
         public:
            inline void unlock() const throws ()
            {
               _mutex.unlock();
            }

            /*@}*/

            /**
             * @name Condition Handling
             * @{
             */

            /**
             * Suspend the calling thread until another thread calls notify() or notifyAll(). While
             * waiting for notification, the mutex is unlocked. Once this call completes, the mutex
             * will be locked again.
             * @return true if a notification was sent, false if it was interrupted.
             */
         public:
            inline bool wait() const throws ()
            {
               _condition.wait(_mutex);
               return true;
            }

            /**
             * Suspend the calling thread until another thread calls notify() or notifyAll() or the specified
             * time has elapsed. While
             * waiting for notification, the mutex is unlocked. Once this call completes, the mutex
             * will be locked again.
             * @param t a time at which the current thread should wakeup with a timeout.
             * @return ConditionResult indicating what type of event occurred
             */
         public:
            inline ConditionResult waitUntil(const ::canopy::time::Time& t) const throws ()
            {
               ::std::cv_status status = _condition.wait_until(_mutex, t.time());
               if (status == ::std::cv_status::no_timeout) {
                  return SIGNALLED;
               }
               else {
                  return TIMEOUT;
               }
            }

            /**
             * Suspend the calling thread until another thread calls notify() or notifyAll() or the specified
             * time has elapsed. While
             * waiting for notification, the mutex is unlocked. Once this call completes, the mutex
             * will be locked again.
             * @param t a time duration
             * @return ConditionResult indicating what type of event occurred
             */
         public:
            ConditionResult waitFor(const ::canopy::time::Time::Duration& t) const throws ()
            {
               ::std::cv_status status = _condition.wait_for(_mutex, t);
               if (status == ::std::cv_status::no_timeout) {
                  return SIGNALLED;
               }
               else {
                  return TIMEOUT;
               }
            }

            /**
             * Notify one of the threads waiting to be notified.
             */
         public:
            inline void notify() const throws ()
            {
               _condition.notify_one();
            }

            /**
             * Notify all threads currently waiting for notification.
             */
         public:
            inline void notifyAll() const throws ()
            {
               _condition.notify_all();
            }

            /*@}*/

            /** The mutex */
         private:
            mutable ::std::mutex _mutex;

            /**
             * The condition which is part of this mutex
             */
         private:
            mutable ::std::condition_variable_any _condition;

      };
   }
}
#endif
