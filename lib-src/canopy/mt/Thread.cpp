#include <canopy/mt/Thread.h>
#include <canopy/canopy.h>
#include <canopy/mt/ThreadLocal.h>
#include <canopy/mt/Mutex.h>
#include <canopy/mt/Condition.h>
#include <canopy/time/Time.h>

#include <cstdlib>
#include <cerrno>
#include <cassert>
#include <iostream>
#include <ctime>
#include <thread>
#include <atomic>
#include <system_error>

namespace canopy {
   namespace mt {
      namespace {

         /** Set an uncaught exception handler */
         static volatile Thread::UncaughtExceptionHandler uncaughtExceptionHandler = 0;

         /**
          * A global mutex for threads
          */
         static ::std::mutex threadList;

         /** The global thread list (pointer to first item) */
         static Thread::ThreadData* threadListHead = 0;

         /**
          * This class guards against cancelling. In posix threads it is possible to cancel
          * the current thread, but in C++ it is not possible.
          */
         struct CancelState
         {
#if 0
               CancelState() throws()
               {
                  int err = ::pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &_oldState);
                  abortOnError(err, __FILE__, __LINE__);
                  if (err) {
                     ::std::cerr << "Internal error in file " << __FILE__ << ", line  " << __LINE__ << "; aborting"
                     << ::std::flush;
                     ::std::abort();
                  }
               }
               ~CancelState() throws()
               {
                  int err = ::pthread_setcancelstate(_oldState, 0);
                  abortOnError(err, __FILE__, __LINE__);
               }
               private:
               int _oldState;
#endif
         };

         static Thread::LogLevel _logLevel(Thread::NONE);

         static void log(Thread::LogLevel level, const char* msg)
         {
            if (level <= _logLevel) {
               ::canopy::time::Time tm = ::canopy::time::Time::now();
               char buf[::canopy::time::Time::BUFFER_SIZE];
               tm.format(buf, 6);
               ::std::cerr << buf << " : Thread [" << level << "] : " << msg << ::std::endl;
            }
         }

         static void log(Thread::LogLevel level, const ::std::string& msg)
         {
            if (level <= _logLevel) {
               log(level, msg.c_str());
            }
         }

         static void notifyUncaughtException(Thread t, const char* msg) throws()
         {
            Thread::UncaughtExceptionHandler h = uncaughtExceptionHandler;
            if (h != 0) {
               try {
                  (*h)(t, msg);
                  return;
               }
               catch (...) {
                  try {
                     log(Thread::ALL, "UncaughtExceptionHandler throw an exception; aborting program");
                  }
                  catch (...) {
                     ::std::cerr << "UncaughtExceptionHandler throw an exception; aborting program";
                  }
                  ::std::abort();
               }
            }
         }

      }

      /**
       * The thread structure
       */
      struct Thread::ThreadData
      {
            // this constructor is invoked by the current thread only
            ThreadData() throws(::std::exception)
                  : _refCount(0)
            {
               init();
               // the current thread must be link so it shows up in the active threads list
               link(this);
               log(Thread::INFO, "Created thread data for current thread");
            }

            ThreadData(const ::std::string& id, Thread::ThreadFunction func, void* userdata) throws(::std::exception)
                  : _name(id), _refCount(0)
            {
               init();
               _threadFunction = func;
               _userData = userdata;
               log(Thread::INFO, ::std::string("Created thread data for new  thread '") + id + "'");
            }

            void init() throws(::std::exception)
            {
               _refCount.store(0);
               _threadFunction = 0;
               _userData = 0;
               _cancelled = false;
               _started = false;
               _running = false;
               _next = _prev = 0;
            }

            ~ThreadData() throws()
            {
               join();
               _thread.reset(nullptr);

               // unlink this data object, just in case
               unlink(this);

               log(Thread::INFO, ::std::string("Destroyed thread data for thread '") + _name + "'");
            }

            static void link(ThreadData* td) throws()
            {
               log(Thread::INFO, "Adding thread '" + const_cast< ThreadData*>(td)->_name + "' to active thread list");
               // now, lock the global mutex and link the thread
               {
                  ::std::lock_guard < ::std::mutex > lock(threadList);

                  td->_next = threadListHead;
                  td->_prev = 0;
                  threadListHead = td;
                  if (td->_next != 0) {
                     td->_next->_prev = td;
                  }
               }

               log(Thread::INFO, "Added thread '" + const_cast< ThreadData*>(td)->_name + "' to active thread list");
            }

            static void unlink(ThreadData* td) throws()
            {
               log(Thread::INFO,
                     "Removing thread '" + const_cast< ThreadData*>(td)->_name + "' from active thread list");
               // now, lock the global mutex and link the thread
               {
                  ::std::lock_guard < ::std::mutex > lock(threadList);
                  if (td->_prev != 0) {
                     td->_prev->_next = td->_next;
                  }
                  else if (td == threadListHead) {
                     threadListHead = td->_next;
                  }
                  else {
                     log(Thread::INFO,
                           "Thread '" + const_cast< ThreadData*>(td)->_name + "' no longer on active thread list");
                  }

                  if (td->_next != 0) {
                     td->_next->_prev = td->_prev;
                  }
                  td->_prev = td->_next = 0;
                  td->_running = false;
               }
               log(Thread::INFO,
                     "Removed thread '" + const_cast< ThreadData*>(td)->_name + "' from active thread list");
            }

            inline void reference() throws()
            {
               ++_refCount;
            }

            static void destroy(ThreadData* data)
            {
               if (unlikely(--data->_refCount == 0)) {
                  // safe to delete
                  try {
                     delete data;
                  }
                  catch (...) {
                     abortOnError(1, __FILE__, __LINE__);
                  }
               }
            }

            bool join() throws()
            {
               bool result = false;
               try {
                  ::std::lock_guard < ::std::mutex > lock(_joinMutex);
                  _thread->join();
                  result = true;
               }
               catch (const ::std::system_error& err) {
                  if (err.code() == ::std::errc::invalid_argument) {
                     result = true;
                  }
                  else if (err.code() == ::std::errc::no_such_process) {
                     result = false;
                  }
                  else if (err.code() == ::std::errc::resource_deadlock_would_occur) {
                     result = false;
                  }
                  else {
                     abortOnError(_running, __FILE__, __LINE__);
                  }
               }
               return result;
            }

            static void* threadEntryFunc(ThreadData* data)
            {
               // completely disable cancellation (is there anyone who actually cancel it, other
               // than the creating thread?)
               const CancelState disableCancel;

               // immediately, increment the reference count
               data->reference();

               const ::std::string& threadName = const_cast< ThreadData*>(data)->_name;

               _thisThreadData = data;

               // notify the creating thread that this thread has started executing
               data->_condition.lock();

               data->_started = true;
               data->_running = true;

               // now link the data; even if there is a cancellation at this time, the
               // unlink function will perform the correct operation; we can't really
               // link any sooner, because we need to be in the running state
               // WARNING: potential deadlock here
               link(data);

               data->_condition.notifyAll();
               data->_condition.unlock();

               // at this point, the native thread state has been restored

               // execute the actual function
               try {
                  log(Thread::INFO, ::std::string("starting thread '") + threadName + "'");
                  data->_threadFunction(data->_userData);
                  log(Thread::INFO, "thread '" + threadName + "' completed");
                  unlink(data);
               }
               catch (const Thread::Cancelled&) {
                  unlink(data);
                  log(Thread::INFO, "thread '" + threadName + "' was cancelled");
               }
               catch (const ::std::exception&e) {
                  unlink(data);
                  log(Thread::EXCEPTION, "thread '" + threadName + "' finished with exception " + e.what());
                  notifyUncaughtException(Thread(data), e.what());
               }
               catch (...) {
                  unlink(data);
                  log(Thread::EXCEPTION, "thread '" + threadName + "' finished with unknown exception ");
                  notifyUncaughtException(Thread(data), 0);
               }

               return 0;
            }

            /** The thread itself */
            ::std::unique_ptr< ::std::thread> _thread;

            /** The reference count */
            volatile ::std::atomic< Int32> _refCount;

            /** The thread name */
            ::std::string _name;

            Thread::ThreadFunction _threadFunction;

            void* _userData;

            /** The thread id */
         public:
            ::std::thread::id _id;

            /** True if this thread was cancelled */
            volatile bool _cancelled;

            /** The flag that indicates that this thread is currently running */
            volatile bool _started;

            /** True if this thread is running */
            volatile bool _running;

            /** The condition */
            Condition _condition;

            /** A mutex that protects access to certain attributes */
            ::std::mutex _mutex;

            /** The join mutex */
            ::std::mutex _joinMutex;

            /** The next thread in the list */
            ThreadData* _next;

            /** The previous thread in the list */
            ThreadData* _prev;

            /** The the thread data in this thread */
            static thread_local ThreadData* _thisThreadData;
      };

      /** The the thread data in this thread */
      thread_local Thread::ThreadData* Thread::ThreadData::_thisThreadData(nullptr);

      Thread::~Thread() throws()
      {
         ThreadData::destroy(_data);
      }

      Thread::Thread(const Thread& t) throws()
            : _data(t._data)
      {
         // disable cancellation
         const CancelState disableCancel;
         _data->reference();
      }

      Thread::Thread(ThreadData* dat) throws()
            : _data(dat)
      {
         const CancelState disableCancel;
         _data->reference();
      }

      Thread::Thread(const ::std::string& nm, ThreadFunction func,
            void* userdata) throws(::std::exception, ::std::invalid_argument)
      {
         // disable cancellation while in this method
         const CancelState disableCancel;

         if (func == 0) {
            throw ::std::invalid_argument("Missing thread-function, was null");
         }

         // allocate the structure that holds our thread information
         try {
            _data = new ThreadData(nm, func, userdata);
            _data->reference();
         }
         catch (const ::std::exception&) {
            throw;
         }
         catch (...) {
            throw ::std::runtime_error("Could not allocate resources for thread");
         }

         // before starting the thread, lock the data's mutex; this will cause
         // the created thread to wait
         _data->_condition.lock();

         try {
            _data->_thread.reset(new ::std::thread(ThreadData::threadEntryFunc, _data));
            _data->_id = _data->_thread->get_id();
            _data->_condition.wait();
         }
         catch (const ::std::system_error& err) {
         }

         // cleanup, and throw an exception
         _data->_condition.unlock();

         // finally, throw the exception
         if (!_data->_thread) {
            ThreadData::destroy(_data);
            throw ::std::runtime_error("Could not allocate native thread resources");
         }
      }

      Thread& Thread::operator=(const Thread& t) throws()
      {
         // disable cancellation
         const CancelState disableCancel;

         if (_data != t._data) {
            ThreadData::destroy(_data);
            _data = t._data;
            _data->reference();
         }
         return *this;
      }

      Thread::Thread() throws(std::exception)
      {
         // disable cancellation
         const CancelState disableCancel;

         // this method is by definition threadsafe

         // get the thread data
         _data = ThreadData::_thisThreadData;

         // we need to allocate a thread data structure for this thread
         if (_data == nullptr) {
            try {
               _data = new ThreadData();
               _data->reference();
               ThreadData::_thisThreadData = _data;
            }
            catch (const ::std::exception&) {
               throw;
            }
            catch (...) {
               throw ::std::runtime_error("Could not allocate resources");
            }
         }

         // make sure to reference it from here as well
         _data->reference();
      }

      size_t Thread::hashValue() const throws()
      {
         return reinterpret_cast< size_t>(_data);
      }

      bool Thread::equals(const Thread& t) const throws()
      {
         return _data == t._data;
      }

      void Thread::yield() throws()
      {
         ::std::this_thread::yield();
      }

      ::std::string Thread::name() const throws(::std::exception)
      {
         ::std::string res;
         const CancelState disableCancel;

         // notify the creating thread that this thread has started executing
         {
            ::std::lock_guard < ::std::mutex > lock(_data->_mutex);
            res = const_cast< ThreadData*>(_data)->_name.c_str();
         }
         return res;
      }

      void Thread::setName(const ::std::string& nm) throws(::std::exception)
      {
         const CancelState disableCancel;

         // notify the creating thread that this thread has started executing

         ::std::lock_guard < ::std::mutex > lock(_data->_mutex);
         const_cast< ThreadData*>(_data)->_name = nm.c_str();
      }

      bool Thread::isRunning() const throws()
      {
         return _data->_running;
      }

      bool Thread::join(Thread thread) throws(::std::exception, ::std::logic_error)
      {
         log(Thread::INFO, "begin join '" + thread.name() + "'");

         const CancelState disableCancel;
         Thread currentThread;

         if (currentThread._data == thread._data) {
            log(Thread::EXCEPTION, "join failed with thread '" + thread.name() + "'");
            throw ::std::logic_error("Thread cannot join itself");
         }

         bool result = thread._data->join();
         log(Thread::INFO, "joined '" + thread.name() + "'");
         return result;
      }

      void Thread::setLogLevel(LogLevel level) throws()
      {
         _logLevel = level;
      }

      ::std::vector< Thread> Thread::activeThreads() throws(::std::exception)
      {
         ::std::vector< Thread> result;
         result.reserve(10);

         CancelState cancelState;

         // make sure that the current thread exists
         Thread currentThread;

         // now, lock the global mutex and link the thread
         {
            ::std::lock_guard < ::std::mutex > lock(threadList);

            try {
               for (ThreadData* td = threadListHead; td != 0; td = td->_next) {
                  result.push_back(Thread(td));
               }
               abortOnError(result.empty(), __FILE__, __LINE__);
            }
            catch (const ::std::exception& e) {
               log(Thread::EXCEPTION, ::std::string("Could not insert into vector : ") + e.what());
               result.clear();
            }
         }

         if (result.empty()) {
            throw ::std::runtime_error("Could not get active threads");
         }

         return result;
      }

      ::std::chrono::nanoseconds Thread::suspend(const ::std::chrono::nanoseconds& nsec) throws ()
      {
         auto until = ::canopy::time::Time::fromNow(nsec);
         ::std::this_thread::sleep_until(until.time());
         auto now = ::canopy::time::Time::now();

         if (now < until) {
            return until.time() - now.time();
         }
         else {
            return ::std::chrono::nanoseconds::zero();
         }
      }

      UInt64 Thread::suspend(UInt64 nsec) throws()
      {
         return suspend(::std::chrono::nanoseconds(nsec)).count();
      }

      void Thread::cancel(Thread thread) throws(::std::exception)
      {
         log(Thread::INFO, "request cancel thread '" + thread.name() + "'");

         const CancelState disableCancel;
         Thread currentThread;

         ::std::lock_guard < ::std::mutex > lock(thread._data->_mutex);
         thread._data->_cancelled = true;
      }

      void Thread::testCancelled() throws(Cancelled,::std::exception)
      {
         const CancelState disableCancel;
         Thread curThread;

         bool cancelled;
         {
            ::std::lock_guard < ::std::mutex > lock(curThread._data->_mutex);
            cancelled = curThread._data->_cancelled;
            curThread._data->_cancelled = false;
         }

         if (cancelled) {
            throw Cancelled();
         }
      }

      Thread::UncaughtExceptionHandler Thread::setUncaughtExceptionHandler(UncaughtExceptionHandler handler) throws()
      {
         UncaughtExceptionHandler old = uncaughtExceptionHandler;
         uncaughtExceptionHandler = handler;
         return old;
      }

   }
}
