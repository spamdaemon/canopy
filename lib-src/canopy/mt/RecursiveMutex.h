#ifndef _CANOPY_MT_RECURSIVEMUTEX_H
#define _CANOPY_MT_RECURSIVEMUTEX_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <mutex>

namespace canopy {
   namespace mt {
      /** A condition */
      class Condition;

      /**
       * This is an implementation of a simple mutex
       * which can be used to protect a section
       * of code from concurrent execution.
       */
      class RecursiveMutex
      {
            /** The condition is a friend so that it may access the mutex directly */
            friend class Condition;

            CANOPY_BOILERPLATE_PREVENT_COPYING(RecursiveMutex);

            /**
             * Creates a new Mutex instance.
             */
         public:
            inline RecursiveMutex() throws()
            {
            }

            /**
             * Destroys this Mutex object.
             * @pre this mutex is not locked
             */
         public:
            inline ~RecursiveMutex() throws ()
            {
            }

            /**
             * @name Mutex operations
             * @{
             */
            /**
             * Lock this mutex.
             */
         public:
            inline void lock() const throws ()
            {
               _mutex.lock();
            }

            /**
             * Try to lock this mutex.
             * @return true if the mutex was locked, false otherwise
             */
         public:
            inline bool tryLock() const throws ()
            {
               return _mutex.try_lock();
            }

            /**
             * Unlock this mutex.
             */
         public:
            inline void unlock() const throws ()
            {
               _mutex.unlock();
            }

            /*@}*/

            /** The mutex */
         private:
            mutable ::std::recursive_mutex _mutex;
      };
   }
}
#endif
