#ifndef _CANOPY_MT_THREAD_H
#define _CANOPY_MT_THREAD_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <string>
#include <vector>
#include <stdexcept>
#include <chrono>

namespace canopy {
   namespace mt {
      /**
       * The native thread provides the minimum functionality
       * to start a system thread.
       */
      class Thread
      {
            /** The thread's own data */
         public:
            class ThreadData;

            /** An exception handler */
         public:
            typedef void (*UncaughtExceptionHandler)(Thread t, const char*);

            /**
             * The object thrown when the a thread
             * has been cancelled and testCancelled()
             * has been called
             */
         public:
            class Cancelled
            {
            };

            /** The log levels available */
         public:
            enum LogLevel
            {
               NONE, EXCEPTION, INFO, ALL
            };

            /**
             * The function to execute
             */
         public:
            typedef void (*ThreadFunction)(void*);

            /**
             * Create a new thread by assigning thread data
             * @param data the thread data
             */
         private:
            Thread(ThreadData* data) throws ();

            /**
             * Create a native thread object that represents the current thread
             * @throws ::std::exception if some sort of error occurred
             */
         public:
            Thread() throws (::std::exception);

            /**
             * Copy constructor.
             * @param nt
             */
         public:
            Thread(const Thread& nt) throws ();

            /**
             * Create a native thread. This method will immediately start the native thread and
             * execute the function.
             * @param name the thread name  (maybe an empty string)
             * @param func the function to be executed
             * @param data data to be sent to the thread
             * @return a thread
             * @throws ::std::exception if the thread cannot be created
             * @throws ::std::invalid_argument if func==0
             */
         public:
            Thread(const ::std::string& name, ThreadFunction func,
                  void* data) throws (::std::exception, ::std::invalid_argument);

            /**
             * Destroys this Thread object.
             * @pre assertFalse(isRunning())
             */
         public:
            ~Thread() throws ();

            /**
             * Copy operator
             * @param nt
             * @return this thread
             */
         public:
            Thread& operator=(const Thread& nt) throws ();

            /**
             * @name Accessors
             * @{
             */

            /**
             * Get the currently executing thread. The
             * reference count of the returned thread
             * is increased by one to prevent the
             * pointer from becoming invalid.
             * @return a thread pointer or null if
             * the current thread was not started by this class.
             */
         public:
            inline static Thread currentThread() throws (::std::exception)
            {
               Thread t;
               return t;
            }

            /**
             * Test if this thread is currently running
             * @return true if this thread is running
             */
         public:
            bool isRunning() const throws ();

            /**
             * Get this thread's name.
             * @return the name of this thread or an empty string if the name cannot be determined.
             */
         public:
            ::std::string name() const throws (::std::exception);

            /**
             * Set a new name for this thread. The name is only used for identification
             * purposes, it will not be used to equality testing and such.
             * @param nm the new name
             * @throws ::std::exception if the name could not be changed
             */
         public:
            void setName(const ::std::string& nm) throws (::std::exception);

            /**
             * Compare this thread with the specified thread.
             * @param thread another thread
             * @return true if this thread and thread are the
             * same thread
             */
         public:
            bool equals(const Thread& thread) const throws ();

            /**
             * Test if this thread is the same as the given thread.
             * @param thread a thread
             * @return @code this->equals(t) @endcode
             */
         public:
            bool operator==(const Thread& thread) const throws ()
            {
               return equals(thread);
            }

            /**
             * Test if this thread is not the same as the given thread.
             * @param thread a thread
             * @return @code !this->equals(t) @endcode
             */
         public:
            bool operator!=(const Thread& thread) const throws ()
            {
               return !equals(thread);
            }

            /**
             * Get the hash value for this thread.
             * @return a hashvalue for this thread.
             */
         public:
            size_t hashValue() const throws ();

            /*@}*/

            /**
             * @name Thread control functions
             * @{
             */

            /**
             * Request that the specified thread be cancelled. It is not an error
             * if the calling thread is also the thread to request cancellation.
             * @param thread a thread
             * @throws ::std::exception if some other kind of error occurred
             */
         public:
            static void cancel(Thread thread) throws (::std::exception);

            /**
             * Determine if another thread called cancel() on this thread. If this thread
             * was cancelled, an unspecified exception is thrown and the cancel bit
             * is reset.
             * @throws Cancelled if the thread was cancelled
             * @throws ::std::exception if some kind of error occurred
             */
         public:
            static void testCancelled() throws (Cancelled, ::std::exception);

            /**
             * Yield the currently executing thread.
             */
         public:
            static void yield() throws ();

            /**
             * Suspend the current thread for the specified number of
             * nanoseconds. Upon exit from this method will return
             * with the number of nanoseconds left to sleep in case
             * the sleep was interrupted.
             * @param nsec the number of nanoseconds
             * @return the number of nanoseconds left to sleep or 0.
             */
         public:
            static UInt64 suspend(UInt64 nsec) throws ();

            /**
             * Suspend the current thread for the specified number of
             * nanoseconds. Upon exit from this method will return
             * with the number of nanoseconds left to sleep in case
             * the sleep was interrupted.
             * @param nsec the number of nanoseconds
             * @return the number of nanoseconds left to sleep or 0.
             */
         public:
            static ::std::chrono::nanoseconds suspend(const ::std::chrono::nanoseconds& nsec) throws ();

            /**
             * Suspend this thread until the specified thread has
             * terminated. This method may be invoked simulatenously
             * for the same thread in multiple threads
             * @param thread the thread to wait for
             * @return true if the join was not successful
             * @throws ::std::logic_error if the current thread is the same as the given thread
             * @throws ::std::exception if some other kind of error occurred
             */
         public:
            static bool join(Thread thread) throws (::std::exception, ::std::logic_error);

            /*@}*/

            /**
             * Get the set of threads currently executing. Threads not started
             * through this class may not be found! Not all threads returned may
             * actually be running once this method returns.
             * @return a list of threads, including the current thread
             */
         public:
            static ::std::vector< Thread> activeThreads() throws (::std::exception);

            /**
             * Set the verbosity level when generating log messages.
             * The default is to log nothing.
             * @param level the log level
             */
         public:
            static void setLogLevel(LogLevel level) throws ();

            /**
             * Set the uncaught-exception handler. The exception handler will be
             * passed the thread that terminated and possibly the exception's error message.
             * This handler is <em>not</em> invoked if the thread terminates with a Cancelled
             * exception.
             * @param handler an exception handler or 0
             * @return the previous exception handler
             */
         public:
            static UncaughtExceptionHandler setUncaughtExceptionHandler(UncaughtExceptionHandler handler) throws ();

            /** The thread data */
         private:
            ThreadData* _data;
      };
   }
}
#endif

