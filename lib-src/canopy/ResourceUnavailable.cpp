#include <canopy/ResourceUnavailable.h>
#include <memory>

namespace canopy {
  
  ResourceUnavailable::ResourceUnavailable(bool again) throws()
  : ::std::runtime_error("Resource Unavailable"),_tryAgain(again) {}

  ResourceUnavailable::ResourceUnavailable(  ::std::string msg, bool again) throws ()
  : ::std::runtime_error(::std::move(msg)),_tryAgain(again)
  {}
 
  
}

