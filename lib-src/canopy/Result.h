#ifndef _CANOPY_RESULT_H
#define _CANOPY_RESULT_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <memory>

namespace canopy {

   /**
    * The function that is executed when a check fails.
    */
   void resultNotChecked(const char* message) throw();

   /**
    * Set the function to executed when a check fails.
    * @param f a function to be executed when a check fails
    */
   void setResultNotChecked(void(*f)(const char*)) throw();

   /**
    * This class can be used as a way to return status values from a function. If the
    * status value is never read, then the program is aborted. Any functions that return
    * status values rather exceptions or the like should return a Result object.
    */
   template<class T = int>
   class Result
   {
         /** Default constructors and copy operator are disabled */
      private:
         Result() = delete;
         CANOPY_BOILERPLATE_PREVENT_COPYING(Result);

         /**
          * Create a result with the specified value.
          * @param v the result value
          */
      public:
         inline Result(const T& v)
               : _value(v)
#ifndef NDEBUG
                     , _checked(false)
#endif
         {
         }

         /**
          * Create a result.
          * @param v an object
          */
      public:
         inline Result(const T&& v) : _value(v)
#ifndef NDEBUG
               ,_checked(false)
#endif
               {

               }

               /**
                * Create a result.
                * @param v an object
                */
               public:
               Result (Result&& v)
               : _value(::std::move(v._value))
#ifndef NDEBUG
               ,_checked(v._checked)
#endif
               {
#ifndef NDEBUG
               // since we're transferring the result to this new object, we can safely
               // mark the original object as checked
               v._checked = true;
#endif
            }

            /**
             * Destroy this result. If the result has not been checked, then
             * abort is called.
             */
            public:
            inline ~Result() throw()
            {
               ensureCheckedResult();
            }

            /**
             * Assign a result to this result. If this result has not yet been checked, then
             * the program is aborted.
             * @param v another result value
             * @return this result
             */
            public:
            Result& operator=(const T& v) throw()
            {
               ensureCheckedResult();
#ifndef NDEBUG
               _checked = false;
#endif
               _value = v;
               return *this;
            }

            /**
             * Assign a result to this result. If this result has not yet been checked, then
             * the program is aborted.
             * @param v another result value
             * @return this result
             */
            public:
            Result& operator=(Result&& v) throw()
            {
               ensureCheckedResult();
#ifndef NDEBUG
               _checked = v._checked;
               v._checked = true;
#endif
               _value = ::std::move(v._value);
               return *this;
            }

            /**
             * Mark this result as checked.
             * @return the result
             */
            public:
            inline const T& check() const throw()
            {
#ifndef NDEBUG
               _checked=true;
#endif
               return _value;
            }

            /**
             * Mark this result as checked.
             * @return the result
             */
            public:
            inline T& check() throw()
            {
#ifndef NDEBUG
               _checked=true;
#endif
               return _value;
            }

            /**
             * Ensure that this result is checked.
             */
            private:
            inline void ensureCheckedResult() const throw()
            {
#ifndef NDEBUG
               if (!_checked) {
                  resultNotChecked(0);
               }
#endif
            }

            /** The object value */
            private:
            T _value;

#ifndef NDEBUG
               /** True if the value has been checked */
               private:
               mutable bool _checked;
#endif
            };

         }

#endif
