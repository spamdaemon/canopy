#ifndef _CANOPY_PROCESS_H
#define _CANOPY_PROCESS_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_UNSUPPORTEDEXCEPTION_H
#include <canopy/UnsupportedException.h>
#endif

#ifndef _CANOPY_IO_FILEDESCRIPTOR_H
#include <canopy/io/FileDescriptor.h>
#endif

#include <stdexcept>
#include <iosfwd>
#include <sstream>
#include <string>
#include <memory>
#include <vector>
#include <map>

namespace canopy {

   /**
    * The Process provides an OS independent means to execute external programs
    * as subprocesses of the current process.
    * The execute() function returns a Process object to the parent process, representing
    * that child. Interaction with the child is made possible primarily through the
    * three standard streams:
    * <ol>
    * <li> The cin() function returns an output stream which is connected to the standard input
    * stream of the child process. The standard input stream of the child maybe closed via the
    * closeCin() method.
    * <li> The cout() function returns an input stream which is connected to the standard output
    * stream of the child process. The standard out stream of the child maybe closed via the
    * closeCout() method.
    * <li> The cerr() function returns an input stream which is connected to the standard error
    * stream of the child process. The standard error stream of the child maybe closed via the
    * closeCerr() method.
    * </ol>
    * It is necessary for the parent process to read the cout() and cerr() streams to ensure that
    * the child is not blocked on that output. Likewise, writing to cin() may cause the parent
    * process to block if the child does not consume its input stream.
    * <p>
    * The Process objects are merely reference counted handles to an internal
    * represenation. Once that reference count reaches zero, the function
    * waitFor() will be invoked to ensure that the child process is properly collected.
    * <p>
    * The following simple  example illustrates how to create child process and communicate via
    * its input and output streams. In this example, the tr process is used to convert upper-case
    * characters in a string to their lower-case equivalents. This example is not foolproof and
    * may deadlock under certain circumstances.
    * @code
    * ::std::string toLowerCase (const ::std::string& string)
    * {
    *    // an empty argument vector
    *    ::std::vector< ::std::string> args;
    *    args.push_back("ABCDEFGHIJKLMNOPQURSTUVWXYZ");
    *    args.push_back("abcdefghijklmnopqurstuvwxyz");
    *    // the process to be executed is the tr process
    *    ::std::string tr("/usr/bin/tr");
    *
    *    // create the child process
    *    Process child = Process::execute(tr,tr,args);
    *
    *    // send the string that we want echoed to the child
    *    child.cin() << string << ::std::flush ;
    *    // close the input stream for the child (this will also
    *    // flush the output buffer)
    *    child.closeCin();
    *
    *    // at this point, wait until the child has exited
    *    child.waitFor();
    *
    *    // consume any data that the child had written
    *    ::std::string echoedString;
    *    ::std::getline(child.cout(),echoedString);

    *    return echoedString;
    * }
    * @endcode
    * <p>
    * A conscious decision has been made not to support the Unix fork function call due to its problems in
    * multithreaded applications. The problem with fork is that locks (mutexes) that are held in threads
    * other than the forking thread will persist in the child process, but cannot be unlocked since the
    * threads holding the locks are not cloned.
    * <p>
    * @note On Unix a SIGPIPE handler is installed to ensure that a sudden death
    * does not lead to the death of the parent due to a SIGPIPE on cin.
    */
   class Process
   {
         /** The process id */
      public:
         typedef uint32_t ProcessID;

         /**
          * A command that captures the parameters for process execution.
          * @note All strings are passed to the OS verbatim with stripping leading or trailing whitespace
          * or splitting strings at whitespace.
          */
      public:
         struct Command
         {
               /** Create an empty command */
               Command();

               /**
                * Create a new command that executes the program at the specified path and
                * inherits the current environment
                * @param xpath a path
                */
               Command(::std::string xpath);

               /**
                * Create a new command that executes the program at the specified path.
                * The new process inherits the environment of the calling process.
                * @param xpath a path
                * @param args arguments
                */
               Command(::std::string xpath, ::std::vector< ::std::string> args);

               /**
                * Create a new command that executes the program at the specified path.
                * The new process will use the specified environment.
                * @param xpath a path
                * @param args arguments
                * @param env the environment to use
                */
               Command(::std::string xpath, ::std::map< ::std::string, ::std::string> env,
                     ::std::vector< ::std::string> args);

               /** The path to the command */
               ::std::string path;
               /** The name to give to the process as the first argument. If not empty, the path will be used as the first argument */
               ::std::string name;
               /** The arguments */
               ::std::vector< ::std::string> arguments;
               /** The environment (if nullptr, then calling process's environment is inherited) */
               ::std::shared_ptr< ::std::map< ::std::string, ::std::string>> environment;
         };

         /**
          * The process state.
          */
      public:
         enum State
         {
            UNBOUND, ///< the process object is unbound
            RUNNING, ///< process has not terminated
            EXITED, ///< process exited normally
            TERMINATED, ///< process terminated abnormally
            UNKNOWN ///< the current process status is unknown
         };

         /** The process implementation */
      public:
         struct Impl;

         /**
          * Create a new process using the specified implementation
          * @param impl
          */
      private:
         Process(::std::shared_ptr< Impl> impl) throws();

         /**
          * Default constructor to create a null process.
          */
      public:
         Process() throws();

         /**
          * Copy constructor. This method <em>does not</em> perform
          * a clone or fork of this process, but rather creates
          * a copy of the handle.
          * @param p a process
          */
      public:
         Process(const Process& p) throws();

         /**
          * Assign another process handle to this process
          * @param p a process
          * @return this process object
          */
      public:
         Process& operator=(const Process& p) throws();

         /**
          * Destroy this process handle.
          */
      public:
         ~Process() throws();

         /**
          * Get the path to the executable of the currently executing process.
          * This is an extremely OS dependent operation and may not always be
          * supported on all platforms.
          * @return the path to the currently executed program
          * @throws UnsupportedException if this operation is not supported
          * @throws ::std::exception if some kind of error has occurred
          * @note the Linux implementation of this function was inspired by <a href="http://www.flipcode.com/cgi-bin/fcarticles.cgi?show=64160">Path To Executable On Linux</a>.
          */
      public:
         static ::std::string findExecutable() throws(UnsupportedException, ::std::exception);

         /**
          * Get the name of the currently executing process. This name may not necessarily correspond to the name of the
          * program, but corresponds to argv[0] in the call to main().
          * @return the name of the currently executing process.
          * @throws UnsupportedException if this operation is not supported
          * @throws ::std::exception if some kind of error has occurred
          */
      public:
         static ::std::string name() throws(UnsupportedException, ::std::exception);

         /**
          * Get the command line arguments for this process. The vector contains the same parameters
          * as are passed to main().
          * @return the arguments to the current process.
          * @throws UnsupportedException if this operation is not supported
          * @throws ::std::exception if some kind of error has occurred
          */
      public:
         static ::std::vector< ::std::string> argv() throws(UnsupportedException, ::std::exception);

         /**
          * @defgroup Environment Environment variable managment.
          * @note Environment variables cannot be manipulated in a threadsafe way
          * on linux or any posix compliant system. Calling getEnv() maybe invoked concurrently,
          * but setEnv() and unsetEnv() are not thread-safe. Invoking getEnv() and setEnv() concurrently
          * is undefined.
          * @note Whether or not environment variable names are case-sensitive is implementation defined.
          * @{
          */

         /**
          * Determine the value of an environment variable. This method cannot be
          * used to determine if the environment variable has been set or not.
          * @param envName the name of the variable
          * @param defaultValue if not-null then this default value is returned if the variable is not set
          * @return the value of the variable or an empty string if not set.
          * @throws ::std::invalid_argument if envName==0
          * @throws UnsupportedException if this operation is not supported
          */
      public:
         static ::std::string getEnv(const char* envName, const char* defaultValue = nullptr) throws(::std::exception);

         /**
          * Get an environment variable with a default value. If the environment variable is not set,
          * or cannot be parsed into the type through operator<<, then the default value is returned.
          * @param envName
          * @param defaultValue
          * @return a value
          */
      public:
         template<class T>
         static T getEnv(const char* envName, const T& defaultValue) throws(::std::exception)
         {
            bool isSet;
            ::std::string str = getEnv(envName, nullptr, isSet);
            if (isSet) {
               T result;
               ::std::istringstream in(str);
               in >> result;
               if (!in.fail()) {
                  return result;
               }
            }
            return defaultValue;
         }

         /**
          * Get the environment variable.
          * @param envName the name of the environment variable
          * @param defaultValue if not-null, returns this value if the variable is not set
          * @param isSet an output parameter that is set to true if specified, false otherwise
          * @return the value of the environment variable or the default value or an empty string.
          * @throws ::std::invalid_argument if envName==0
          * @throws UnsupportedException if this operation is not supported
          */
      public:
         static ::std::string getEnv(const char* envName, const char* defaultValue,
               bool& isSet) throws(::std::exception);

         /**
          * Unset an environment variable. If
          * the envName is null, then this method does nothing.
          * @param envName the name of the variable to unset
          * @throws UnsupportedException if this operation is not supported
          */
      public:
         static void unsetEnv(const char* envName) throws(::std::exception);

         /**
          * Set an environment variable. If the value is 0, then this function behaves just as unsetEnv. If
          * the envName is null, then this method does nothing. The value is copied.
          * @param envName the name of the variable
          * @param value the value
          * @throws UnsupportedException if this operation is not supported
          */
      public:
         static void setEnv(const char* envName, const char* value) throws(::std::exception);

         /*@}*/

         /**
          * Execute the specified command.
          * @param command a command object
          * @return a process
          * @throws UnsupportedException if this method is not supported.
          */
      public:
         static Process execute(
               const Command& command) throws(::std::exception, ::std::invalid_argument, UnsupportedException);

         /**
          * @name Communication streams
          * @{
          */

         /**
          * Get the id of this process
          */
      public:
         ProcessID processID() const throws();

         /**
          * Get the file descriptor stdin for the process.
          * @return the file descriptor that can be used to write to stdin for this process.
          */
      public:
         ::canopy::io::FileDescriptor cinDescriptor() throws();

         /**
          * Get the file descriptor stdout for the process.
          * @return the file descriptor that can be used to read from the processes stdout.
          */
      public:
         ::canopy::io::FileDescriptor coutDescriptor() throws();

         /**
          * Get the file descriptor stderr for the process.
          * @return the file descriptor that can be used to read from the processes stderr.
          */
      public:
         ::canopy::io::FileDescriptor cerrDescriptor() throws();

         /**
          * Set the buffer size for input and output streams. This function
          * does not affect already created processes. A buffer size of 0
          * indicates that a default size should be used.
          * @param bufSize the new buffer size
          */
      public:
         static void setStreamBufferSize(UInt32 bufSize) throws();

         /**
          * Close the cin stream.
          */
      public:
         void closeCin() throws();

         /**
          * Close the cout stream
          */
      public:
         void closeCout() throws();

         /**
          * Close the cerr stream
          */
      public:
         void closeCerr() throws();

         /**
          * Access the input for this process.
          * @return an output stream which connects the current process to this child process
          */
      public:
         ::std::ostream& cin() throws();

         /**
          * Access the standard output stream for this process.
          * @return an input stream which is connection to the standard
          * output stream of this child process.
          */
      public:
         ::std::istream& cout() throws();

         /**
          * Access the standard error stream for this process.
          * @return an input stream which is connection to the standard
          * error stream of this child process.
          */
      public:
         ::std::istream& cerr() throws();

         /*@}*/

         /**
          * Wait for this process to end. If this process has already
          * ended, then this method returns immediately, otherwise it will
          * block the caller until this has exited.
          * @throws ::std::exception if an error occurred while waiting
          */
      public:
         void waitFor() throws(::std::exception);

         /**
          * Get the current state of this process.
          * @return the status of this process.
          */
      public:
         State state() const throws(::std::exception);

         /**
          * Get the exit code. This function should only be
          * called if the @code state()==State::EXITED @endcode.
          * @return the exit code of this process
          * @throws ::std::exception if state()!=EXITED
          */
      public:
         Int32 exitCode() throws(::std::exception);

         /**
          * Notify this process that it should terminate. It is still necessary
          * to call waitFor() to know when this process has terminated.
          * @throws ::std::exception if this process could not be notified
          */
      public:
         void terminate() throws(::std::exception);

         /**
          * Interrupt this process.
          * @throws ::std::exception if this process could not be notified
          */
      public:
         void interrupt() throws(::std::exception);

         /**
          * Determine if this process is the same the specified process.
          * @param p a process
          * @return true if this process and p are the same
          */
      public:
         bool operator==(const Process& p) const throws();

         /**
          * Determine if this process is the same the specified process.
          * @param p a process
          * @return true if this process and p are the same
          */
      public:
         bool operator==(const nullptr_t&) const throws()
         {
            return _process == nullptr;
         }

         /**
          * Determine if this process is the same the specified process.
          * @param p a process
          * @return true if this process and p are the same
          */
      public:
         inline bool operator!=(const Process& p) const throws()
         {
            return !(*this == p);
         }

         /**
          * Determine if this process is the same the specified process.
          * @param p a process
          * @return true if this process and p are the same
          */
      public:
         inline bool operator!=(const nullptr_t&) const throws()
         {
            return _process != nullptr;
         }

         /**
          * Allow for sorting of processes.
          * @param p a process
          * @return true if this process comes before the given process
          */
      public:
         bool operator<(const Process& p) const throws();

         /**
          * Write some representation of this process out
          * @param out an output stream
          */
      public:
         CANOPY_BOILERPLATE_DECLARE_PRINT
         ;

         /** A hash function */
      public:
         CANOPY_BOILERPLATE_DECLARE_HASH;

         /**DEC
          * Get a string describing the current process
          * @return a string for the current process
          */
      public:
         static ProcessID currentProcessID() throws();

         /**
          * The process implmentation
          */
      private:
         mutable ::std::shared_ptr< Impl> _process;
   }
   ;
}
inline CANOPY_BOILERPLATE_DEFINE_HASH(::canopy::Process, ::std::hash<::std::shared_ptr<Impl>>()(_process) )CANOPY_BOILERPLATE_HASH(::canopy::Process)
CANOPY_BOILERPLATE_PRINT(::canopy::Process)

#endif
