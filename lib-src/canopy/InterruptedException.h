#ifndef _CANOPY_INTERRUPTEDEXCEPTION_H
#define _CANOPY_INTERRUPTEDEXCEPTION_H

#include <canopy/canopy.h>
#include <stdexcept>

namespace canopy {

   /**
    * The interrupted exception is thrown by methods that
    * would normally block in the kernel, but which have
    * received a signal or interrupt
    */
   class InterruptedException : public ::std::runtime_error
   {

         /**
          * Create a new interrupted exception
          */
      public:
         InterruptedException() throws ();

         /**
          * Create a new interrupted exception
          * @param msg a message
          */
      public:
         InterruptedException(::std::string msg) throws ();

         /** Destructor */
      public:
         ~InterruptedException() throw()
         {
         }
   };

}

#endif
