#ifndef _CANOPY_CALLSTACK_H
#define _CANOPY_CALLSTACK_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <iosfwd>
#include <memory>

namespace canopy {

  /**
   * The callstack is used to provide some information about
   * the location of the executation of the program. This information
   * is particualarly useful in debugging a program.
   */
  class Callstack {
    
    /** The default constructor */
  protected:
    Callstack() throws();

    /** The destructor */
  public:
    virtual ~Callstack() throws();
    
    /**
     * Determine if this installation supports callstacks
     * @return true if valid callstacks can be created.
     */
  public:
    static bool isSupported() throws();

    /**
     * Create a new callstack object.
     * @return a callstack
     */
  public:
    static ::std::unique_ptr<Callstack> create() throws();


    /**
     * Print a callstack representation. This will print either
     * a callstack or some error message if the callstack could
     * not be obtained.
     * @param out an output stream
     */
  public:
    virtual CANOPY_BOILERPLATE_DECLARE_PRINT;
  };
}

CANOPY_BOILERPLATE_PRINT(::canopy::Callstack)

#endif
