#include <canopy/signals/Signal.h>

namespace canopy {
   namespace signals {

      Signal::Signal(SignalID id, ProcessID pid) throws()
            : _signal(id),_process(pid)
      {
      }

      Signal::Signal(SignalID id) throws()
            : _signal(id),_process(0)
      {
      }

      Signal::~Signal() throws()
      {
      }
   }
}
