#ifndef _CANOPY_SIGNALS_SIGNALDESCRIPTOR_H
#define _CANOPY_SIGNALS_SIGNALDESCRIPTOR_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_SIGNALS_NATIVESIGNALDESCRIPTOR_H
#include <canopy/signals/NativeSignalDescriptor.h>
#endif

#ifndef _CANOPY_SIGNALS_SIGNAL_H
#include <canopy/signals/Signal.h>
#endif

#ifndef _CANOPY_INTERRUPTEDEXCEPTION_H
#include <canopy/InterruptedException.h>
#endif

#ifndef _CANOPY_IO_IODESCRIPTOR_H
#include <canopy/io/IODescriptor.h>
#endif

#include <memory>
#include <atomic>

namespace canopy {
   namespace signals {

      /**
       * This is a wrapper for NativeSignalDescriptor objects.
       */
      class SignalDescriptor
      {
            /**
             * Create a new socket with the specified socket descriptor.
             * @param d a descriptor
             */
         public:
            explicit SignalDescriptor(::std::shared_ptr< NativeSignalDescriptor> d) throws(::canopy::io::IOException);

            /** The default constructor */
         public:
            SignalDescriptor() throws ();

            /**
             * Create a copy of the socket.
             * @param s a socket
             */
         public:
            inline SignalDescriptor(const SignalDescriptor& s)
throws               ()
               : _sd(s._sd)
               {
               }

               /**
                * Create a socket from descriptor. If the NativeDescriptor associated
                * with the IODescriptor cannot be cast to a socket provider, then
                * an invalid socket is created.
                * @param d a descriptor (possibly invalid)
                */
               public:
               SignalDescriptor(const ::canopy::io::IODescriptor& d) throws ();

               /**
                * Destroy this socket reference. If this is the last
                * reference to the socket, then it is destroyed.
                * If the socket cannot be destroyed properly, then an error
                * message is printed to cerr
                */
               public:
               ~SignalDescriptor() throws ();

               /**
                * Create a descriptor that can be used to deliver the specified signal
                * @param id a signal id
                */
               public:
               static SignalDescriptor create(const Signal::SignalID& id) throws();

               /**
                * Create a descriptor that can be used to deliver the specified signal
                * @param signals various signals
                * @return a new signal descriptor
                */
               public:
               static SignalDescriptor create(const ::std::vector< Signal::SignalID>& signals) throws();

               /**
                * Assign the specified socket. An exception will be thrown
                * if this socket is the last reference and cannot be closed.
                * @param s a socket
                * @return *this
                */
               public:
               inline SignalDescriptor& operator=(const SignalDescriptor& s) throws (::canopy::io::IOException)
               {
                  _sd = s._sd;
                  return *this;
               }

               /**
                * Get the descriptor
                * @return the descriptor
                */
               public:
               inline ::canopy::io::IODescriptor descriptor() const throws ()
               {
                  return _sd->ioDescriptor();
               }

               /**
                * Test if this is an invalid socket.
                * @return true if this socket is <em>invalid</em>
                */
               public:
               inline bool operator!() const throws ()
               {
                  return _sd->invalid();
               }

               /**
                * Close this socket.
                * @note this method will change the socket's id
                * @throws Exception if the socket could not be closed
                */
               public:
               void close() throws (::canopy::io::IOException);

               /**
                * Test if this socket has been closed. If this
                * method returns false, then this does not mean
                * that the next method invocation for this socket
                * will succeed.
                * @return true if this socket can no longer be used, false
                * if the socket currently not closed
                */
               public:
               bool isClosed() const throws ();

               /**
                * Peek at the next signal.
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
               public:
               ::std::unique_ptr< Signal> peek(
                     const ::canopy::io::Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) const throws (::canopy::io::IOException,InterruptedException);

               /**
                * Read the next signal.
                * @param timeout a timeout
                * @return the next signal
                * @exception IOException if an error occurred during reading or the descriptor was closed
                * @exception InterruptedException if the call was interrupted
                */
               public:
               ::std::unique_ptr< Signal> read(
                     const ::canopy::io::Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) throws (::canopy::io::IOException,InterruptedException);

               /**
                * Set the socket to block on I/O operations.
                * @param enabled if true, then I/O operations may block
                * @exception SocketException if an error occurred
                */
               public:
               void setBlockingEnabled(bool enabled) throws (::canopy::io::IOException);

               /**
                * Test if blocking I/O is enabled.
                * @return true if I/O calls may be block, false otherwise
                * @exception SocketException if an error occurred
                */
               public:
               bool isBlockingEnabled() const throws (::canopy::io::IOException);

               /**
                * Test for equality. The test is NOT based on the socket's id
                * as it is not constant for the life-time of the socket.
                */
               public:
               inline bool operator==(const SignalDescriptor& s) const throws ()
               {
                  return _sd == s._sd;
               }

               /**
                * Test for inequality. The test is NOT based on the socket's id
                * as it is not constant for the life-time of the socket.
                */
               public:
               inline bool operator!=(const SignalDescriptor& s) const throws ()
               {
                  return _sd != s._sd;
               }

               /**
                * Get a partial ordering of two sockets. The test is NOT based on the socket's id
                * as it is not constant for the life-time of the socket.
                * @return true if this socket is before the specified socket.
                */
               public:
               inline bool operator<(const SignalDescriptor& s) const throws ()
               {
                  return _sd < s._sd;
               }

               /**
                * Swap two sockets.
                * @param s a socket
                * @note this method is not threadsafe!!!
                */
               public:
               inline void swap(SignalDescriptor& s) throws()
               {
                  ::std::swap(_sd, s._sd);
               }

               /** The socket descriptor for this socket */
               private:
               ::std::shared_ptr<NativeSignalDescriptor> _sd;
            };
         }
      }
#endif
