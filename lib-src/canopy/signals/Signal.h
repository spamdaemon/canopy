#ifndef _CANOPY_SIGNALS_SIGNAL_H
#define _CANOPY_SIGNALS_SIGNAL_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <signal.h>

namespace canopy {
   namespace signals {

      /**
       * This class represents a received signal.
       */
      class Signal
      {
            /** The signal ID */
         public:
            typedef uint32_t SignalID;

            /** The process id */
         public:
            typedef uint32_t ProcessID;

            /**
              * Create a new signal.
              * @param id the signal
              */
          public:
             Signal(SignalID id) throws();

             /**
               * Create a new signal.
               * @param id the signal
               */
           public:
              Signal(SignalID id, ProcessID) throws();

            /**
             * Destructor
             */
         public:
            virtual ~Signal() throws();

            /**
              * Get the signal
              * @return the signal number
              */
          public:
             inline SignalID id() const throws()
             {
                return _signal;
             }

             /**
               * Get the process from which this signal was sent.
               * @return the process that sent this signal
               */
           public:
              inline ProcessID processID() const throws()
              {
                 return _process;
              }

            /** The signal */
         private:
            SignalID _signal;

            /** The process that sent the signal */
         private:
            ProcessID _process;
      };
   }
}
#endif
