#include <canopy/signals/NativeSignalDescriptor.h>
#include <canopy/io/FileDescriptorImpl.h>

#include <iostream>
#include <cassert>

#include <sys/signalfd.h>
#include <signal.h>

using namespace ::std;

namespace canopy {
   namespace signals {
      using namespace ::canopy::io;

      namespace {
         ::std::unique_ptr< Signal> createSignal(const signalfd_siginfo& info)
         {
            ::std::unique_ptr< Signal> res;
            res.reset(new Signal(info.ssi_signo, info.ssi_pid));
            return ::std::move(res);
         }
      }

      NativeSignalDescriptor::NativeSignalDescriptor()
throws            ()
            : _fd(new FileDescriptorImpl())
            {
            }

      NativeSignalDescriptor::NativeSignalDescriptor(int fd)
throws            ()
            : _fd(new FileDescriptorImpl(fd,true))
            {
            }

      NativeSignalDescriptor::~NativeSignalDescriptor() throws()
      {
      }

      ::std::unique_ptr< NativeSignalDescriptor> NativeSignalDescriptor::create(
            const ::std::vector< Signal::SignalID>& signals) throws (IOException)
      {
         sigset_t mask;
         sigemptyset(&mask);
         for (auto id : signals) {
            sigaddset(&mask, id);
         }

         // block the signal
         sigprocmask(SIG_BLOCK, &mask, nullptr);

         auto fd = signalfd(-1, &mask, SFD_CLOEXEC);
         if (fd < 0) {
            throw IOException(getSystemError("SignalDescriptor::create"));
         }
         ::std::unique_ptr< NativeSignalDescriptor> sd(new NativeSignalDescriptor(fd));
         return ::std::move(sd);
      }

      ::std::unique_ptr< NativeSignalDescriptor> NativeSignalDescriptor::create() throws (IOException)
      {
         ::std::vector< Signal::SignalID> sigs;
         return ::std::move(create(sigs));
      }

      NativeSignalDescriptor::Descriptor NativeSignalDescriptor::descriptor() const throws ()
      {
         return _fd->descriptor();
      }

      bool NativeSignalDescriptor::invalid() const throws ()
      {
         return descriptor() == -1;
      }

      void NativeSignalDescriptor::close() throws(IOException)
      {
         _fd->close();
      }

      bool NativeSignalDescriptor::isClosed() const throws()
      {
         return _fd->isClosed();
      }

      IOEvents NativeSignalDescriptor::block(const IOEvents& events, const Timeout& timeout) const throws (IOException, InterruptedException)
      {
         return _fd->block(events, timeout);
      }

      ssize_t NativeSignalDescriptor::peek(char*, size_t, const Timeout&) const throws(IOException)
      {
         throw IOException("Unsupported method");
      }

      ssize_t NativeSignalDescriptor::read(char*, size_t, const Timeout&) throws(IOException)
      {
         throw IOException("Unsupported method");
      }

      size_t NativeSignalDescriptor::write(const char*, size_t, const Timeout&) throws(IOException)
      {
         throw IOException("Unsupported method");
      }

      void NativeSignalDescriptor::setBlockingEnabled(bool enabled) throws(IOException)
      {
         _fd->setBlockingEnabled(enabled);
      }

      bool NativeSignalDescriptor::isBlockingEnabled() const throws (IOException)
      {
         return _fd->isBlockingEnabled();
      }

      ::std::unique_ptr< Signal> NativeSignalDescriptor::peek(const Timeout& timeout) const throws(IOException,InterruptedException)
      {
         signalfd_siginfo info;
         auto nRead = _fd->peek(reinterpret_cast< char*>(&info), sizeof(signalfd_siginfo), timeout);
         if (nRead == sizeof(signalfd_siginfo)) {
            return createSignal(info);
         }
         else if (nRead < 0) {
            // looks like the descriptor has been closed
            throw IOException("Signal Descriptor closed");
         }
         return nullptr;
      }

      ::std::unique_ptr< Signal> NativeSignalDescriptor::read(const Timeout& timeout) throws (IOException,InterruptedException)
      {
         signalfd_siginfo info;
         auto nRead = _fd->read(reinterpret_cast< char*>(&info), sizeof(signalfd_siginfo), timeout);
         if (nRead == sizeof(signalfd_siginfo)) {
            return createSignal(info);
         }
         else if (nRead < 0) {
            // looks like the descriptor has been closed
            throw IOException("Signal Descriptor closed");
         }
         return nullptr;
      }

   }
}
