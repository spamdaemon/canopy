#include <canopy/signals/SignalDescriptor.h>

namespace canopy {
   namespace signals {

      using namespace ::canopy::io;

      SignalDescriptor::SignalDescriptor(::std::shared_ptr< NativeSignalDescriptor> fd)
throws            (IOException)
            : _sd(::std::move(fd))
            {
               if (!_sd) {
                  _sd = ::std::make_shared<NativeSignalDescriptor>();
               }
            }

      SignalDescriptor::SignalDescriptor(const IODescriptor& d)
throws            ()
            : SignalDescriptor(d.cast< NativeSignalDescriptor>())
            {
            }

      SignalDescriptor SignalDescriptor::create(const ::std::vector< Signal::SignalID>& signals) throws()
      {
         return SignalDescriptor(NativeSignalDescriptor::create(signals));
      }

      SignalDescriptor SignalDescriptor::create(const Signal::SignalID& id) throws()
      {
         ::std::vector< Signal::SignalID> signals;
         signals.push_back(id);
         return SignalDescriptor(NativeSignalDescriptor::create(signals));
      }

      SignalDescriptor::~SignalDescriptor() throws()
      {
      }

      SignalDescriptor::SignalDescriptor()
throws            ()
            : SignalDescriptor(nullptr)
            {
            }

      void SignalDescriptor::close() throws (IOException)
      {
         _sd->close();
      }

      bool SignalDescriptor::isClosed() const throws ()
      {
         return _sd->isClosed();
      }

      ::std::unique_ptr< Signal> SignalDescriptor::peek(const Timeout& timeout) const throws (IOException,InterruptedException)
      {
         return _sd->peek(timeout);
      }

      ::std::unique_ptr< Signal> SignalDescriptor::read(const ::canopy::io::Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
      {
         return _sd->read(timeout);
      }

      void SignalDescriptor::setBlockingEnabled(bool enabled) throws (IOException)
      {
         _sd->setBlockingEnabled(enabled);
      }
      bool SignalDescriptor::isBlockingEnabled() const throws ( IOException)
      {
         return _sd->isBlockingEnabled();
      }
   }
}
