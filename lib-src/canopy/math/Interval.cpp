#include <canopy/math/Interval.h>
#include <cstdio>
#include <limits>

namespace canopy {
  namespace math {
    namespace {
      static constexpr double NOT_A_NUMBER = ::std::numeric_limits<double>::quiet_NaN();
    }

    constexpr double Interval::INF;
    constexpr double Interval::MAX_FINITE;
    constexpr double Interval::MIN_FINITE;


    Interval Interval::abs() const throws()
    {
      if (_lo>=0) {
	return *this;
      }
      else if (_hi<=0) {
	return neg();
      }
      assert(_lo<0.0 && _hi>0.0);
      double xlo = -_lo;
      double xhi = _hi;
      return Interval(0,::std::max(xlo,xhi));
    }
    
    
    Interval Interval::divDouble(double x, double y) throws()
    {
      Interval i;
      if (unlikely(y==0)) {
	i._lo = i._hi = NOT_A_NUMBER;
      }
      else {
	volatile double vx(x);
	volatile double vy(y);
	RoundingMode roundingMode(RoundingMode::DOWN);
	i._lo = vx/vy;
	roundingMode.setRoundingMode(RoundingMode::UP);
	i._hi = vx/vy;
      }
      return i;
    }

    Interval Interval::invMult (double s) const throws()
    {
      double hi,lo;
      if (unlikely(isUndefined() || (_hi==0 && _lo==0))) {
	hi = lo = NOT_A_NUMBER;
      }
      else if (likely(_hi<=0 || _lo>=0)) {
	RoundingMode roundingMode(RoundingMode::DOWN);
	lo=s/_hi;
	roundingMode.setRoundingMode(RoundingMode::UP);
	hi=s/_lo;
      }
      else {
	lo = -INF;
	hi = INF;
      }
      return Interval(lo,hi);
    }


    Interval Interval::sqrt () const throws()
    {
      if (unlikely(isUndefined() || _hi < 0.0)) {
	double lo = NOT_A_NUMBER;
	return Interval(lo,lo);
      }
      RoundingMode roundingMode(RoundingMode::UP);
      double hi = ::sqrt(_hi);
      double lo = 0;
      if (_lo>=0) {
	roundingMode.setRoundingMode(RoundingMode::DOWN);
	lo = ::sqrt(_lo);
      }
      return Interval(lo,hi);
    }

    Interval Interval::log () const throws()
    {
      // cannot compute if undefined
      if (unlikely(isUndefined() || _hi <= 0.0)) {
	double lo=NOT_A_NUMBER;
	return Interval(lo,lo);
      }
      RoundingMode roundingMode(RoundingMode::UP);
      double hi = ::log(_hi);
      double lo = -INF;
      if (_lo>0) {
	roundingMode.setRoundingMode(RoundingMode::DOWN);
	lo = ::log(_lo);
      }
      return Interval(lo,hi);
    }

    Interval Interval::exp () const throws()
    {
      // cannot compute if undefined
      if (unlikely(isUndefined())) {
	return *this;
      }
      RoundingMode roundingMode(RoundingMode::DOWN);
      double lo = ::exp(_lo);
      roundingMode.setRoundingMode(RoundingMode::UP);
      double hi = ::exp(_hi);
      return Interval(lo,hi);
    }
    
    void Interval::divInterval(double s) throws()
    {
      // cannot compute if undefined
      if (unlikely(isUndefined() || s==0)) {
	_lo = _hi = NOT_A_NUMBER;
	return;
      }

      double lo = _lo;
      double hi = _hi;

      if (s<0) {
	lo = -_hi;
	hi = -_lo;
	s = -s;
      }

      RoundingMode roundingMode(RoundingMode::DOWN);
      _lo = lo/s;
      roundingMode.setRoundingMode(RoundingMode::UP);
      _hi = hi/s;
    }

    void Interval::divInterval (double a, double b) throws()
    {
      // cannot compute if undefined
      if (unlikely(isUndefined() || (a==0 && b==0))) {
	_hi = _lo = NOT_A_NUMBER;
	return;
      }
      if (unlikely(_lo==0 && _hi==0)) {
	return;
      }

      // mark the variables as volatile to
      // disable compiler optimization
      volatile double lo = a;
      volatile double hi = b;

      if (a>=0.0) {
	if (_lo>=0.0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo / hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi / lo;
	}
	else if (_hi<=0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo / lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi / hi;
	}
	else {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo / lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi / lo;
	}
      }
      else if (b<=0.0) {
	if (_lo>=0.0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi / hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo / lo;
	}
	else if (_hi<=0.0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi / lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo / hi;
	}
	else {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi / hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo / hi;
	}
      }
      else {
	b = INF;
	a = -b;
      }
      _lo = a;
      _hi = b;
    }

    void Interval::multInterval(double s) throws()
    {
      // cannot compute if undefined
      if (unlikely(isUndefined())) {
	return;
      }

      if (s==0) {
	_lo=_hi=0;
	return;
      }

      double lo = _lo;
      double hi = _hi;
      if (s<0) {
	lo = -_hi;
	hi = -_lo;
	s  = -s;
      }

      RoundingMode roundingMode(RoundingMode::DOWN);
      _lo = lo*s;
      roundingMode.setRoundingMode(RoundingMode::UP);
      _hi = hi*s;
    }

    void Interval::multInterval(double a, double b) throws()
    {
      // cannot compute if undefined, or this is zero,
      // then just return
      if (unlikely(isUndefined() || (_lo==0 && _hi==0))) {
	return;
      }
      if (a==0 && b==0) {
	_lo=_hi=0;
	return;
      }

      // mark the variables as volatile to
      // disable compiler optimization
      volatile double lo = a;
      volatile double hi = b;

      if (_lo>=0.0) {
	if (a>=0.0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo * lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi * hi;
	}
	else if (b<=0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi*lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo * hi;
	}
	else {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi*lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi*hi;
	}
      }
      else if (_hi<=0.0) {
	if (a>=0.0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo*hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi*lo;
	}
	else if (b<=0.0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi*hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo*lo;
	}
	else {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo*hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo*lo;
	}
      }
      else {
	if (a >= 0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _lo*hi;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _hi*hi;
	}
	else if (b <= 0) {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = _hi*lo;
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = _lo*lo;
	}
	else {
	  RoundingMode roundingMode(RoundingMode::DOWN);
	  a = ::std::min(_lo*hi,_hi*lo);
	  roundingMode.setRoundingMode(RoundingMode::UP);
	  b = ::std::max(_lo*lo,_hi*hi);
	}
      }
      _lo = a;
      _hi = b;
    }

    ::std::string Interval::toString () const throws()
    {
      char data[100];
      ::std::sprintf(data,"[ %.19G ; %.19G ]",_lo,_hi);
      return ::std::string(data);
    }
  }

}

