#ifndef _CANOPY_MATH_REALREP_H
#define _CANOPY_MATH_REALREP_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <memory>
#include <gmp.h>

namespace canopy {
   namespace math {
      /**
       * This class provides an implementation of
       * an multiprecision floating point number.
       * @author Raimund Merkert
       * @date 13 Jan 2002
       * @todo Rounding of numbers is not done correctly
       */
      class RealRep
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(RealRep)
            ;

            /**
             * Create a new real object
             */
         private:
            RealRep() throws();

            /**
             * Destroys this Real object.
             */
         public:
            ~RealRep() throws();

            /**
             * Create a new instance.
             * @return an instance
             */
         private:
            static ::std::shared_ptr<RealRep> create();


            /** Create the zero object */
         public:
            static ::std::shared_ptr< RealRep> zero() throws();

            /**
             * Creates a new Real initialized with the
             * specified double value
             * @param value a value
             * @throw ::std::exception if the value is not a good value
             */
         public:
            static ::std::shared_ptr< RealRep> create(double value) throws(::std::exception);

            /**
             * Creates a real number from a string.
             * @param value a string
             * @throw ::std::exception if the string is not a number
             */
         public:
            static ::std::shared_ptr< RealRep> create(const char* value) throws(::std::exception);

            /**
             * Compare this real with the specified real.
             * @param x a real
             * @return -1 if this < x, 0 if this==x, and 1 if this > x
             */
         public:
            Int32 compare(const ::std::shared_ptr< RealRep>& x) const throws();

            /**
             * @name Rounding operations
             * @{
             */

            /**
             * Round this real to the nearest double.
             * @return the double nearest this real
             */
         public:
            double roundToNearest() const throws();

            /**
             * Round to the nearest double that is no smaller
             * than the actual real value
             * @return a double
             */
         public:
            double roundUp() const throws();

            /**
             * Round to the nearest double that is no larger
             * than this real value
             */
         public:
            double roundDown() const throws();

            /*@}*/

            /**
             * @name Unary operations
             * @{
             */

            /**
             * Get the sign of this value.
             * @return -1 if this <0, 0 if this==0, and 1 if this>0
             */
         public:
            Int32 sign() const throws();

            /**
             * Return the negative of this number
             * @return the negative of this number
             */
         public:
            ::std::shared_ptr< RealRep> neg() const throws();

            /*@}*/

            /**
             * @name Binary arithmetic operations
             * @{
             */

            /**
             * Add two reals.
             * @param x a real
             * @return the sum of this and x
             */
         public:
            ::std::shared_ptr< RealRep> add(const ::std::shared_ptr< RealRep>& x) const throws();

            /**
             * Subtract two reals.
             * @param x a real
             * @return the difference of this with x
             */
         public:
            ::std::shared_ptr< RealRep> sub(const::std::shared_ptr< RealRep>& x) const throws();

            /**
             * Multiply two reals
             * @param x a real
             * @return the product of this and x
             */
         public:
            ::std::shared_ptr< RealRep> mult(const ::std::shared_ptr< RealRep>& x) const throws();

            /*@}*/

            /**
             * @name Optimized operations
             * @{
             */

            /**
             * Multiply and add three numbers. This operation
             * may be implemented more efficiently.
             * @param x a real
             * @param y a real
             * @param z a real
             * @return x*y+z
             */
         public:
            static ::std::shared_ptr< RealRep> multiplyAdd(const ::std::shared_ptr< RealRep>& x,
                  const ::std::shared_ptr< RealRep>& y, const ::std::shared_ptr< RealRep>& z) throws();

            /**
             * Multiply and subtract three numbers. This operation
             * may be implemented more efficiently.
             * @param x a real
             * @param y a real
             * @param z a real
             * @return x*y-z
             */
         public:
            static ::std::shared_ptr< RealRep> multiplySub(const ::std::shared_ptr< RealRep>& x,
                  const ::std::shared_ptr< RealRep>& y, const ::std::shared_ptr< RealRep>& z) throws();

            /**
             * Subtract a multiple. This operation
             * may be implemented more efficiently.
             * @param x a real
             * @param y a real
             * @param z a real
             * @return x-y*z
             */
         public:
            static ::std::shared_ptr< RealRep> subMultiply(const ::std::shared_ptr< RealRep>& x,
                  const ::std::shared_ptr< RealRep>& y, const ::std::shared_ptr< RealRep>& z) throws();

            /**
             * Multiply two sets of numbers and add them.
             * @param w a real
             * @param x a real
             * @param y a real
             * @param z a real
             * @return w*x+y*z
             */
         public:
            static ::std::shared_ptr< RealRep> multiplyAddPairs(const ::std::shared_ptr< RealRep>& w,
                  const ::std::shared_ptr< RealRep>& x, const ::std::shared_ptr< RealRep>& y,
                  const ::std::shared_ptr< RealRep>& z) throws();

            /**
             * Multiply two sets of numbers and subtract them.
             * @param w a real
             * @param x a real
             * @param y a real
             * @param z a real
             * @return w*x-y*z
             */
         public:
            static ::std::shared_ptr< RealRep> multiplySubPairs(const ::std::shared_ptr< RealRep>& w,
                  const ::std::shared_ptr< RealRep>& x, const ::std::shared_ptr< RealRep>& y,
                  const ::std::shared_ptr< RealRep>& z) throws();

            /*@}*/

            /** The floating point value */
         private:
            ::mpf_t _value;
      };
   }
}
#endif
