#ifndef _CANOPY_MATH_PRECISION_H
#define _CANOPY_MATH_PRECISION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace canopy {
  namespace math {
    /**
     * This class sets the precision for the floating point unit.
     */
    class Precision {
      Precision(const Precision&);
      Precision&operator=(const Precision&);

      /** The type of the FPU's control word */
    private:
      typedef UInt32 ControlWord;
    
      /**
       * The FPU precision
       */
    public:
      enum Mode { 
	SINGLE =   0x000,
	DOUBLE =   0x200,
	EXTENDED = 0x300 
      };
    
      /** 
       * Create a new FPU object and change the
       * precision.
       * @param prec the new precision
       */
    public:
      inline Precision (Mode prec) throws() 
      {
	getFPU(_initial); 
	ControlWord current = (_initial & ~(SINGLE|DOUBLE|EXTENDED)) | prec;
	setFPU(current);
      }

      /**
       * Destroy this FPU object, restoring
       * the setting that were in effect before
       * construction of this object.
       */
    public:
      inline ~Precision () throws() {
	setFPU(_initial);
      }

      /**
       * Change the current modes relative to the inital settings.
       * @param prec the new precision
       */
    public:
      inline static void setFPU (Mode prec) throws() 
      {
	ControlWord ctrl;
	getFPU(ctrl);
	ctrl = ctrl & ~(SINGLE|DOUBLE|EXTENDED);
	ctrl = ctrl | prec;
	setFPU(ctrl);
      }

      /**
       * Test the current precision
       * @param prec the new precision
       * @return true if the precision are as specified
       */
    public:
      inline static bool testFPU (Mode prec) throws() 
      {
	ControlWord ctrl;
	getFPU(ctrl);
	ControlWord value = prec;
	return (ctrl & (SINGLE|DOUBLE|EXTENDED)) == value;
      }

      /** Set the FPU's control word */
    private:
      inline static void setFPU (ControlWord ctrl) 
      { asm volatile ("fldcw %0" : : "m" (*&ctrl)); }

      /**
       * Get the fpu's control word.
       * @param ctrl a location where the FPU will place it's status
       */
    private:
      inline static void getFPU (ControlWord volatile&  ctrl)
      {	asm ("fnstcw %0" : "=m" (ctrl)); }
      
      /** The initial settings */
    private:
      volatile ControlWord _initial;
    };
  }
}
#endif
