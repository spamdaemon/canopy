#include <canopy/math/ComplexNumber.h>
#include <iostream>

namespace canopy {
  namespace math {

    ComplexNumber& ComplexNumber::operator*= (const ComplexNumber& c) throws()
    {
      double xr = _r;
      double xi = _i;
      
      _r = xr*c._r - xi*c._i;
      _i = xr*c._i + xi*c._r;
      
      return *this;
    }

    ComplexNumber& ComplexNumber::operator/= (const ComplexNumber& c) throws()
    {
      double div = c._r*c._r + c._i*c._i;
      
      double xr = _r;
      double xi = _i;
      
      _r = (xr*c._r + xi*c._i)/div;
      _i = (xr*c._i - xi*c._r)/div;
      
      return *this;
    }
    ComplexNumber ComplexNumber::operator/ (const ComplexNumber& c) const throws()
    {
      const double div = c._r*c._r + c._i*c._i;
      return ComplexNumber((_r*c._r + _i*c._i)/div,(_r*c._i - _i*c._r)/div);
    }

    ::std::ostream& operator<< (::std::ostream& out, const ComplexNumber& c)
    {
      return out << '(' << c.r() << ';' << c.i() << ')';
    }

  }
}
