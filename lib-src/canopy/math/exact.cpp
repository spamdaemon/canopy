#include <canopy/math/exact.h>
#include <cmath>

namespace canopy {
  namespace math {

    void twoProduct (double a, double b, double& x, double& y) throws()
    { 
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::DOUBLE));
      double ah,al;
      split(a,ah,al);
      double bh,bl;
	
      split(b,bh,bl);
      volatile double ahbh = ah*bh;
      volatile double ahbl = ah*bl;
      volatile double albh = al*bh;
      volatile double albl = al*bl;
      x = a*b;
      double err1 = x - ahbh;
      double err2 = err1 - albh;
      double err3 = err2 - ahbl;
      y = albl - err3;
    }

    void twoProduct (float a, float b, float& x, float& y) throws()
    { 
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::SINGLE));
      float ah,al;
      split(a,ah,al);
      float bh,bl;
	
      split(b,bh,bl);
      volatile float ahbh = ah*bh;
      volatile float ahbl = ah*bl;
      volatile float albh = al*bh;
      volatile float albl = al*bl;
      x = a*b;
      float err1 = x - ahbh;
      float err2 = err1 - albh;
      float err3 = err2 - ahbl;
      y = albl - err3;
    }
    
    void threeProduct(double a, double b, double c, double* res) throws()
    {
      double tmp1,tmp2;
      twoProduct(a,b,tmp1,tmp2);
      twoProduct(tmp1,c,res[0],res[1]);
      twoProduct(tmp2,c,res[2],res[3]);
    }
    
    void threeProduct(float a, float b, float c, float* res) throws()
    {
      float tmp1,tmp2;
      twoProduct(a,b,tmp1,tmp2);
      twoProduct(tmp1,c,res[0],res[1]);
      twoProduct(tmp2,c,res[2],res[3]);
    }
    void arraySum(size_t n, double* p, double& sum, double& errBounds) throws()
    {
      assert(n>1);
      const double* end = p+n;
      double res = *p;
      double sumP=0;
      while (likely(++p<end)) {
	sumP += ::std::abs(p[-1]); // needed to compute the error
	twoSum(*p,res,res,p[-1]);
      }
      p[-1] = res;
      
      const double epsRes = eps<double>(::std::abs(res));
      // update the error bound
      const double x = 2.0*n*eps<double>(1.0);
      const double beta = sumP*(x/(1.0-x));
      
      sum = res;
      errBounds = epsRes +(beta+(2.0*epsRes*epsRes));
    }

    
    void arraySum(size_t n, float* p, float& sum, float& errBounds) throws()
    {
      assert(n>1);
      const float* end = p+n;
      float res = *p;
      float sumP=0;
      while (likely(++p<end)) {
	sumP += ::std::abs(p[-1]); // needed to compute the error
	twoSum(*p,res,res,p[-1]);
      }
      p[-1] = res;
      
      const float epsRes = eps<float>(::std::abs(res));
      // update the error bound
      const float x = 2.0f*n*eps<float>(1.0);
      const float beta = sumP*(x/(1.0f-x));
      
      sum = res;
      errBounds = epsRes +(beta+(2.0f*epsRes*epsRes));
    }


    Int32 signOfSum (size_t n, double* p,Int32& sgn) throws()
    {
      double sum,errorBound;
      arraySum(n,p,sum,errorBound);
      Int32 ok = testErrors();
      while (ok==0 && ::std::abs(sum) < errorBound) {
	double tmpSum,tmpErr;
	arraySum(n,p,tmpSum,tmpErr);
	ok = testErrors();
	if (unlikely(tmpSum==sum && tmpErr==errorBound)) {
	  return 1; // failure
	}
	sum = tmpSum;
	errorBound = tmpErr;
      }
	
      sgn = (sum > 0 ? 1 : (sum < 0 ? -1 : 0));
      return ok;
    }

    Int32 signOfSum (size_t n, float* p,Int32& sgn) throws()
    {
      float sum,errorBound;
      arraySum(n,p,sum,errorBound);
      Int32 ok = testErrors();
      while (ok==0 && ::std::abs(sum) < errorBound) {
	float tmpSum,tmpErr;
	arraySum(n,p,tmpSum,tmpErr);
	ok = testErrors();
	if (unlikely(tmpSum==sum && tmpErr==errorBound)) {
	  return 1; // failure
	}
	sum = tmpSum;
	errorBound = tmpErr;
      }
	
      sgn = (sum > 0 ? 1 : (sum < 0 ? -1 : 0));
      return ok;
    }

    Int32 sum (size_t n, double* p, double& sum, double& error) throws()
    {
      double errorBound;
      arraySum(n,p,sum,error);
      arraySum(n,p,sum,errorBound);
      Int32 ok = testErrors();
      while (ok==0 && errorBound < error) {
	error = errorBound;
	arraySum(n,p,sum,errorBound);
	ok = testErrors();
      }
      return ok;
    }

    Int32 sum (size_t n, float* p, float& sum, float& error) throws()
    {
      float errorBound;
      arraySum(n,p,sum,error);
      arraySum(n,p,sum,errorBound);
      Int32 ok = testErrors();
      while (ok==0 && errorBound < error) {
	error = errorBound;
	arraySum(n,p,sum,errorBound);
	ok = testErrors();
      }
      return ok;
    }

    Int32 sum (double relError, size_t n, double* p, double& sum, double& errorBound) throws()
      {
	arraySum(n,p,sum,errorBound);
	Int32 ok = testErrors();
	double err = ::std::abs(sum)*relError;

	while (ok==0 && err < errorBound) {
	  arraySum(n,p,sum,errorBound);
	  ok = testErrors();
	  double tmp = ::std::abs(sum)*relError;
	  if (unlikely(ok!=0 || tmp >= err)) {
	    break;
	  }
	  tmp = err;
	}

	return ok;
      }

    Int32 sum (float relError, size_t n, float* p, float& sum, float& errorBound) throws()
      {
	arraySum(n,p,sum,errorBound);
	Int32 ok = testErrors();
	float err = ::std::abs(sum)*relError;

	while (ok==0 && err < errorBound) {
	  arraySum(n,p,sum,errorBound);
	  ok = testErrors();
	  float tmp = ::std::abs(sum)*relError;
	  if ( unlikely( ok!=0 || tmp >= err) ) {
	    break;
	  }
	  tmp = err;
	}

	return ok;
      }

    Int32 determinantSign3x3 (const double* A, Int32& sgn) throws()
    {
      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::DOUBLE);
      double tmp[4*6];

      threeProduct(A[0],A[4],A[8],tmp);
      threeProduct(A[1],A[5],A[6],tmp+4);
      threeProduct(A[2],A[3],A[7],tmp+8);

      threeProduct(-A[6],A[4],A[2],tmp+12);
      threeProduct(-A[7],A[5],A[0],tmp+16);
      threeProduct(-A[8],A[3],A[1],tmp+20);
	
      return signOfSum(24,tmp,sgn);
    }

    Int32 determinantSign3x3 (const float* A, Int32& sgn) throws()
    {
      // instead of doing the operation in floating point precision, we
      // use double precision and avoid the underflow problem.

      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::DOUBLE);
      double tmp[4*6];

      threeProduct((double)A[0],(double)A[4],(double)A[8],tmp);
      threeProduct((double)A[1],(double)A[5],(double)A[6],tmp+4);
      threeProduct((double)A[2],(double)A[3],(double)A[7],tmp+8);

      threeProduct(-(double)A[6],(double)A[4],(double)A[2],tmp+12);
      threeProduct(-(double)A[7],(double)A[5],(double)A[0],tmp+16);
      threeProduct(-(double)A[8],(double)A[3],(double)A[1],tmp+20);
	
      return signOfSum(24,tmp,sgn);
    }
    
    Int32 determinant3x3 (const double* A, double& value, double& error) throws()
    {
      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::DOUBLE);
      double tmp[4*6];

      threeProduct(A[0],A[4],A[8],tmp);
      threeProduct(A[1],A[5],A[6],tmp+4);
      threeProduct(A[2],A[3],A[7],tmp+8);

      threeProduct(-A[6],A[4],A[2],tmp+12);
      threeProduct(-A[7],A[5],A[0],tmp+16);
      threeProduct(-A[8],A[3],A[1],tmp+20);
	
      return sum(24,tmp,value,error);
    }

    Int32 determinant3x3 (const float* A, float& value, float& error) throws()
    {
      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::SINGLE);
      float tmp[4*6];

      threeProduct(A[0],A[4],A[8],tmp);
      threeProduct(A[1],A[5],A[6],tmp+4);
      threeProduct(A[2],A[3],A[7],tmp+8);

      threeProduct(-A[6],A[4],A[2],tmp+12);
      threeProduct(-A[7],A[5],A[0],tmp+16);
      threeProduct(-A[8],A[3],A[1],tmp+20);
	
      return sum(24,tmp,value,error);
    }

    void scaleSum (double s, double x, double y, double* res)
    {
      twoProduct(s,x,res[1],res[0]);
      twoProduct(s,y,res[3],res[2]);
    }
    
    void scaleSum (float s, float x, float y, float* res)
    {
      twoProduct(s,x,res[1],res[0]);
      twoProduct(s,y,res[3],res[2]);
    }

    Int32 determinantSign2x2 (double a00, double a01, double a10, double a11, Int32& sgn) throws()
    {
      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::DOUBLE);

      double tmp[4];
      twoProduct(a00,a11,tmp[0],tmp[1]);
      twoProduct(-a10,a01,tmp[2],tmp[3]);
      return signOfSum(4,tmp,sgn);
    }

    Int32 determinantSign2x2 (float a00, float a01, float a10, float a11, Int32& sgn) throws()
    {
      // instead of doing the operation in floating point precision, we
      // use double precision and avoid the underflow problem.

      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::DOUBLE);

      double tmp[4];
      twoProduct((double)a00,(double)a11,tmp[0],tmp[1]);
      twoProduct((double)-a10,(double)a01,tmp[2],tmp[3]);
      return signOfSum(4,tmp,sgn);
    }
    Int32 determinant2x2 (double a00, double a01, double a10, double a11, double& value, double& error) throws()
    {
      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::DOUBLE);

      double tmp[4];
      twoProduct(a00,a11,tmp[0],tmp[1]);
      twoProduct(-a10,a01,tmp[2],tmp[3]);
      return sum(4,tmp,value,error);
    }

    Int32 determinant2x2 (float a00, float a01, float a10, float a11, float& value, float& error) throws()
    {
      RoundingMode rmode(RoundingMode::NEAREST);
      Precision pmode(Precision::SINGLE);
      float tmp[4];
      twoProduct(a00,a11,tmp[0],tmp[1]);
      twoProduct(-a10,a01,tmp[2],tmp[3]);
      return sum(4,tmp,value,error);
    }

  }
}
