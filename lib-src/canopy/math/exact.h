#ifndef _CANOPY_MATH_EXACT_H
#define _CANOPY_MATH_EXACT_H

#ifndef _CANOPY_MATH_IEEE_H
#include <canopy/math/ieee.h>
#endif

#include <cmath>
#include <cassert>

namespace canopy {
  namespace math {

    /**
     * Compute the sum of two floating point numbers return their
     * exact sum as two floating point numbers.
     * @note For this function to work correctly, it is necessary that the
     *       all operations are performed using a ROUND to nearest rounding mode
     *       for the specified type.
     *
     * @param a a floating point number
     * @param b a floating point number
     * @param value the floating point value
     * @param error the error in the sum
     * @pre assert(isRoundToNearest())
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    inline void twoSum(double a,double b, double& value, double& error) throws()
    {
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::DOUBLE));

      value = a+b;
      double bVirtual = value-a;
      double aVirtual = value-bVirtual;
      double bRoundOff = b-bVirtual;
      double aRoundOff = a-aVirtual;
      error = bRoundOff + aRoundOff;
    }
    
    /**
     * Compute the sum of two floating point numbers return their
     * exact sum as two floating point numbers.
     * @note For this function to work correctly, it is necessary that the
     *       all operations are performed using a ROUND to nearest rounding mode
     *       for the specified type.
     *
     * @param a a floating point number
     * @param b a floating point number
     * @param value the floating point value
     * @param error the error in the sum
     * @pre assert(isRoundToNearest())
     * @pre Precision::testFPU(Precision::SINGLE)
     */
    inline void twoSum(float a,float b, float& value, float& error) throws()
    {
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::SINGLE));
      
      value = a+b;
      float bVirtual = value-a;
      float aVirtual = value-bVirtual;
      float bRoundOff = b-bVirtual;
      float aRoundOff = a-aVirtual;
      error = bRoundOff + aRoundOff;
    }
    
    /**
     * Compute the sum of two floating point numbers and return their
     * exact sum as two floating point numbers.
     * This method is a faster version of twoSum(), provided the 
     * input values are ordered by magnitude.
     *
     * @param a a floating point number
     * @param b a floating point number
     * @param value the floating point value
     * @param error the error in the sum
     * @pre isRoundToNearest()
     * @pre ::std::abs(a) > ::std::abs(b)
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    inline void fastTwoSum(double a,double b, double& value, double& error) throws()
    {
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::DOUBLE));
      assert(::std::abs(a) > ::std::abs(b));
      
      value = a+b;
      double bVirtual = value-a;
      error = b-bVirtual;
    }
    
    /**
     * Compute the sum of two floating point numbers and return their
     * exact sum as two floating point numbers.
     * This method is a faster version of twoSum(), provided the 
     * input values are ordered by magnitude.
     *
     * @param a a floating point number
     * @param b a floating point number
     * @param value the floating point value
     * @param error the error in the sum
     * @pre isRoundToNearest()
     * @pre ::std::abs(a) > ::std::abs(b)
     * @pre Precision::testFPU(Precision::SINGLE)
     */
    inline void fastTwoSum(float a,float b, float& value, float& error) throws()
    {
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::SINGLE));
      assert(::std::abs(a) > ::std::abs(b));
      
      value = a+b;
      float bVirtual = value-a;
      error = b-bVirtual;
    }
   
    /**
     * Split a number into a sum  of two numbers.
     * @param a a floating point number
     * @param hi the larger of the resulting two numbers
     * @param lo the smaller of  the resulting two numbers
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     * @post fastTwoSum(x,y,gg,hh),gg==a && hh==0
     */
    inline void split (double a, double& hi, double& lo) throws()
    { 
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::DOUBLE));
	
      const double splitValue = 1+ (1<<(precision<double>()/2));
	
      double c = a * splitValue;
      double t = c-a;
      hi = c-t;
      lo = a-hi;
    }
    
    /**
     * Split a number into a sum  of two numbers.
     * @param a a floating point number
     * @param hi the larger of the resulting two numbers
     * @param lo the smaller of  the resulting two numbers
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::SINGLE)
     * @post fastTwoSum(x,y,gg,hh),gg==a && hh==0
     */
    inline void split (float a, float& hi, float& lo) throws()
    { 
      assert(isRoundToNearest());
      assert(Precision::testFPU(Precision::SINGLE));
	
      const float splitValue = 1+ (1<<(precision<float>()/2));
	
      float c = a * splitValue;
      float t = c-a;
      hi = c-t;
      lo = a-hi;
    }
    
    
    /**
     * Compute the product of two numbers. The
     * result of the product are two floating
     * point numbers that if added together will
     * represent the product, such that a*b = x+y
     * @param a a value
     * @param b a value
     * @param x part of the result
     * @param y part of the result
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    void twoProduct (double a, double b, double& x, double& y) throws();

    /**
     * Compute the product of two numbers. The
     * result of the product are two floating
     * point numbers that if added together will
     * represent the product, such that a*b = x+y
     * @param a a value
     * @param b a value
     * @param x part of the result
     * @param y part of the result
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::SINGLE)
     */
    void twoProduct (float a, float b, float& x, float& y) throws();
    
    
    /**
     * Compute the product of a scalar and the sum of two floating point
     * numbers.The result of this operation is thus the product : s*(x+y)
     * @param s a double
     * @param x a double
     * @param y a double
     * @param res the 4 floating point numbers that contain the product
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    void scaleSum (double s, double x, double y, double* res);
      
    /**
     * Compute the product of a scalar and the sum of two floating point
     * numbers.The result of this operation is thus the product : s*(x+y)
     * @param s a double
     * @param x a double
     * @param y a double
     * @param res the 4 floating point numbers that contain the product
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    void scaleSum (float s, float x, float y, float* res);
    
    /**
     * Compute the product of three numbers. The result of this operation
     * are 4 floating point numbers whose sum represents the product, i.e.
     * a*b*c = res[0]+res[1]+res[2]+res[3].
     * @param a a number
     * @param b a number
     * @param c a number
     * @param res an array that holds at least 4 floating point numbers
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
      */
    void threeProduct(double a, double b, double c, double* res) throws();

    /**
     * Compute the product of three numbers. The result of this operation
     * are 4 floating point numbers whose sum represents the product, i.e.
     * a*b*c = res[0]+res[1]+res[2]+res[3].
     * @param a a number
     * @param b a number
     * @param c a number
     * @param res an array that holds at least 4 floating point numbers
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
      */
    void threeProduct(float a, float b, float c, float* res) throws();

    /**
     * Compute the sum of the numbers in the specified array. The
     * contents of the array <em>are</em> modified. Repeated execution of
     * this method will yield incrementally better results. This 
     * method may return false in cases where overflow or underflow
     * has occurred. 
     * @param n the number of values in the array
     * @pre REQUIRE_GREATER(n,1)
     * @param p the values in the array
     * @param sum the result of the summation
     * @param errBounds the error bounds (an output parameter)
     * @return 0 if the result can be trusted, any integer otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    void arraySum(size_t n, double* p, double& sum, double& errBounds) throws();
    /**
     * Compute the sum of the numbers in the specified array. The
     * contents of the array <em>are</em> modified. Repeated execution of
     * this method will yield incrementally better results. This 
     * method may return false in cases where overflow or underflow
     * has occurred. 
     * @param n the number of values in the array
     * @pre REQUIRE_GREATER(n,1)
     * @param p the values in the array
     * @param sum the result of the summation
     * @param errBounds the error bounds (an output parameter)
     * @return 0 if the result can be trusted, any integer otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::SINGLE)
     */
    void arraySum(size_t n, float* p, float& sum, float& errBounds) throws();
    
    /**
     * Compute the sign of a sum of floating point numbers. The
     * contents in the array <em>will</em> be modified in a way
     * that preserves the value of the sum.
     * @param n the number of floating point numbers
     * @param p the floating point numbers
     * @param sgn the sign of the 
     *        -1 if the sign of the sum is negative,
     *         0 if the sum is zero
     *         1 if the sign of the sum is positive
     * @return 0 if the sign can be trusted, <> 0 otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    Int32 signOfSum (size_t n, double* p,Int32& sgn) throws();

    /**
     * Compute the sign of a sum of floating point numbers. The
     * contents in the array <em>will</em> be modified in a way
     * that preserves the value of the sum.
     * @param n the number of floating point numbers
     * @param p the floating point numbers
     * @param sgn the sign of the 
     *        -1 if the sign of the sum is negative,
     *         0 if the sum is zero
     *         1 if the sign of the sum is positive
     * @return 0 if the sign can be trusted, <> 0 otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    Int32 signOfSum (size_t n, float* p,Int32& sgn) throws();
    
    /**
     * Compute the sum of n floating point numbers as accurately as possible.
     * @param n the number of floating point numbers
     * @param p the floating point numbers
     * @param sum the computed sum
     * @param errorBound the computed error bound
     * @return 0 if the result can be trusted, any integer otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    Int32 sum (size_t n, double* p, double& sum, double& errorBound) throws();
    /**
     * Compute the sum of n floating point numbers as accurately as possible.
     * @param n the number of floating point numbers
     * @param p the floating point numbers
     * @param sum the computed sum
     * @param errorBound the computed error bound
     * @return 0 if the result can be trusted, any integer otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::SINGLE)
     */
    Int32 sum (size_t n, float* p, float& sum, float& errorBound) throws();

    
    /**
     * Compute the sum of n floating point numbers to
     * an accuracy given by a relative error relError. The content
     * of the array p will be changed in such a way that the
     * total sum will remain the same.
     * @param relError the relative error to be achieved
     * @param n the number of floating point numbers
     * @param p the floating point numbers
     * @param sum the result sum
     * @param errorBound the error bound
     * @return true if the result can be trusted, false otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::DOUBLE)
     */
    Int32 sum (double relError, size_t n, double* p, double& sum, double& errorBound) throws();
     /**
     * Compute the sum of n floating point numbers to
     * an accuracy given by a relative error relError. The content
     * of the array p will be changed in such a way that the
     * total sum will remain the same.
     * @param relError the relative error to be achieved
     * @param n the number of floating point numbers
     * @param p the floating point numbers
     * @param sum the result sum
     * @param errorBound the error bound
     * @return true if the result can be trusted, false otherwise
     * @pre isRoundToNearest()
     * @pre Precision::testFPU(Precision::SINGLE)
     */
    Int32 sum (float relError, size_t n, float* p, float& sum, float& errorBound) throws();

    /**
     * Compute the sign of a 2x2 determinant a00*a11-a01*a10
     * @param a00 
     * @param a01
     * @param a10
     * @param a11
     * @param sgn the sign of the determinant
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinantSign2x2 (double a00, double a01, double a10, double a11, Int32& sgn) throws();

    /**
     * Compute the sign of a 2x2 determinant a00*a11-a01*a10. This method
     * may be able to produce a valid result, whereas testing the sign of 
     * of determinant2x2() may not work due to underflow or overlow.
     * 
     * @param a00 
     * @param a01
     * @param a10
     * @param a11
     * @param sgn the sign of the determinant
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinantSign2x2 (float a00, float a01, float a10, float a11, Int32& sgn) throws();

    /**
     * Compute the 2x2 determinant a00*a11-a01*a10 as accurately as possible.
     * @param a00 
     * @param a01
     * @param a10
     * @param a11
     * @param value the value of the determinant
     * @param error an error bound for the value
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinant2x2 (double a00, double a01, double a10, double a11, double& value, double& error) throws();

    /**
     * Compute the 2x2 determinant a00*a11-a01*a10 as accurately as possible.
     * @param a00 
     * @param a01
     * @param a10
     * @param a11
     * @param value the value of the determinant
     * @param error an error bound for the value
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinant2x2 (float a00, float a01, float a10, float a11, float& value, float& error) throws();

    /**
     * Compute the sign of the determinant of a 3x3 matrix. The matrix is encoded
     * inside an array of length 9. The first row is stored in A[0] to A[2],
     * the second row is stored in A[3] to A[5], and the third row is stored in
     * A[6] to A[8]
     * @param A a matrix
     * @param sgn the sign of the determinant of the matrix
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinantSign3x3 (const double* A, Int32& sgn) throws();

    /**
     * Compute the sign of the determinant of a 3x3 matrix. The matrix is encoded
     * inside an array of length 9. The first row is stored in A[0] to A[2],
     * the second row is stored in A[3] to A[5], and the third row is stored in
     * A[6] to A[8].
     * <p>This method
     * may be able to produce a valid result, whereas testing the sign of 
     * of determinant3x3() may not work due to underflow or overlow.
     * 
     * @param A a matrix
     * @param sgn the sign of the determinant of the matrix
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinantSign3x3 (const float* A, Int32& sgn) throws();

    /**
     * Compute the determinant of a 3x3 matrix as exactly as possible. The matrix is encoded
     * inside an array of length 9. The first row is stored in A[0] to A[2],
     * the second row is stored in A[3] to A[5], and the third row is stored in
     * A[6] to A[8]
     * @param A a matrix
     * @param value the value of the determinant
     * @param error an error bound for the value
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinant3x3 (const double* A, double& value, double& error) throws();

    /**
     * Compute the determinant of a 3x3 matrix as exactly as possible. The matrix is encoded
     * inside an array of length 9. The first row is stored in A[0] to A[2],
     * the second row is stored in A[3] to A[5], and the third row is stored in
     * A[6] to A[8]
     * @param A a matrix
     * @param value the value of the determinant
     * @param error an error bound for the value
     * @return 0 if the result can be trusted, any integer otherwise
     * @note This function will set the appropriate precision and rounding mode
     */
    Int32 determinant3x3 (const float* A, float& value, float& error) throws();
  }
}

#endif
