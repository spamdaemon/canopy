#include <canopy/math/RealRep.h>

namespace canopy {
   namespace math {

      /** The maximum number of precision bits */
      const UInt32 PRECISION = 0x1000;

      RealRep::RealRep() throws()
      {
         ::mpf_init2(_value, PRECISION);
      }

      ::std::shared_ptr< RealRep> RealRep::create() throws(::std::exception)
      {
         return ::std::shared_ptr< RealRep>(new RealRep());
      }

      ::std::shared_ptr< RealRep> RealRep::zero() throws()
      {
         return create(0.0);
      }

      ::std::shared_ptr< RealRep> RealRep::create(double value) throws(::std::exception)
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_set_d(r->_value, value);
         assert(value == ::mpf_get_d(r->_value));
         return r;
      }

      ::std::shared_ptr< RealRep> RealRep::create(const Char* value) throws(::std::exception)
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_set_str(r->_value, value, 10);
         return r;
      }

      RealRep::~RealRep() throws()
      {
         ::mpf_clear(_value);
      }

      Int32 RealRep::sign() const throws()
      {
         Int32 s = mpf_sgn(_value);
         return s < 0 ? -1 : (s > 0 ? 1 : 0);
      }

      ::std::shared_ptr< RealRep> RealRep::neg() const throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_neg(r->_value, _value);
         return r;
      }

      double RealRep::roundToNearest() const throws()
      {
         return ::mpf_get_d(_value);
      }

      double RealRep::roundUp() const throws()
      {
         return ::mpf_get_d(_value);
      }

      double RealRep::roundDown() const throws()
      {
         return ::mpf_get_d(_value);
      }

      ::std::shared_ptr< RealRep> RealRep::add(const ::std::shared_ptr< RealRep>& x) const throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_add(r->_value, _value, x->_value);
         return r;
      }

      ::std::shared_ptr< RealRep>  RealRep::sub(const ::std::shared_ptr< RealRep>& x) const throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_sub(r->_value, _value, x->_value);
         return r;
      }

      ::std::shared_ptr< RealRep>  RealRep::mult(const ::std::shared_ptr< RealRep>& x) const throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_mul(r->_value, _value, x->_value);
         return r;
      }

      Int32 RealRep::compare(const ::std::shared_ptr< RealRep>& x) const throws()
      {
         return ::mpf_cmp(_value, x->_value);
      }

      ::std::shared_ptr< RealRep>  RealRep::multiplyAdd(const ::std::shared_ptr< RealRep>& x,
            const ::std::shared_ptr< RealRep>& y, const ::std::shared_ptr< RealRep>& z) throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_mul(r->_value, x->_value, y->_value);
         ::mpf_add(r->_value, r->_value, z->_value);
         return r;
      }

      ::std::shared_ptr< RealRep> RealRep::multiplySub(const ::std::shared_ptr< RealRep>& x,
            const ::std::shared_ptr< RealRep>& y, const ::std::shared_ptr< RealRep>& z) throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_mul(r->_value, x->_value, y->_value);
         ::mpf_sub(r->_value, r->_value, z->_value);
         return r;
      }

      ::std::shared_ptr< RealRep> RealRep::subMultiply(const ::std::shared_ptr< RealRep>& x,
            const ::std::shared_ptr< RealRep>& y, const ::std::shared_ptr< RealRep>& z) throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_mul(r->_value, y->_value, z->_value);
         ::mpf_sub(r->_value, x->_value, r->_value);
         return r;
      }

      ::std::shared_ptr< RealRep> RealRep::multiplyAddPairs(const ::std::shared_ptr< RealRep>& w,
            const ::std::shared_ptr< RealRep>& x, const ::std::shared_ptr< RealRep>& y,
            const ::std::shared_ptr< RealRep>& z) throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_t tmp;
         ::mpf_init2(tmp, PRECISION);
         ::mpf_mul(r->_value, w->_value, x->_value);
         ::mpf_mul(tmp, y->_value, z->_value);
         ::mpf_add(r->_value, r->_value, tmp);
         ::mpf_clear(tmp);
         return r;
      }

      ::std::shared_ptr< RealRep> RealRep::multiplySubPairs(const ::std::shared_ptr< RealRep>& w,
            const ::std::shared_ptr< RealRep>& x, const ::std::shared_ptr< RealRep>& y,
            const ::std::shared_ptr< RealRep>& z) throws()
      {
         ::std::shared_ptr< RealRep> r(create());
         ::mpf_t tmp;
         ::mpf_init2(tmp, PRECISION);
         ::mpf_mul(r->_value, w->_value, x->_value);
         ::mpf_mul(tmp, y->_value, z->_value);
         ::mpf_sub(r->_value, r->_value, tmp);
         ::mpf_clear(tmp);
         return r;
      }

   }
}
