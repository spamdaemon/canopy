#ifndef _CANOPY_MATH_INTERVAL_H
#define _CANOPY_MATH_INTERVAL_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_MATH_ROUNDINGMODE_H
#include <canopy/math/RoundingMode.h>
#endif

#include <cmath>
#include <cassert>
#include <limits>

namespace canopy {
   namespace math {

      /**
       * This class represents floating points numbers as
       * an interval. For any operation, the infinitely
       * precise result of will be contained within the interval.
       * Note that division by 0 or the interval [0;0] will result
       * in an undefined number.
       * @todo need trigonometric methods
       * @todo define interval constants for E, PI, etc.
       * @todo MAX_FINITE and MIN_FINITE have not been declared correctly
       * @bug compiler optimizes too much
       */
      class Interval
      {

            /** The infinity value */
         public:
            static constexpr double INF = ::std::numeric_limits< double>::infinity();

            /** The maximum finite value for the bounds of an interval */
         public:
            static constexpr double MAX_FINITE = ::std::numeric_limits< double>::max();

            /** The minimum value for the bounds that is not 0 */
         public:
            static constexpr double MIN_FINITE = ::std::numeric_limits< double>::min();

            /**
             * Creates a new Interval instance initialized with 0.
             */
         public:
            inline constexpr Interval() throws()
                  : _lo(0), _hi(0)
            {
            }

            /**
             * Creates a new Interval instance initialized with the
             * specified double value
             * @param value a value
             * @throw Exception if the value is not a good value
             */
         public:
            inline constexpr Interval(double value) throws()
                  : _lo(value), _hi(value)
            {
            }

            /**
             * Creates a new Interval instance initialized with the
             * specified double value
             * @param lo the lower end of the interval
             * @param hi the higher end of the interval
             * @pre REQUIRE_LESS_OR_EQUAL(lo,hi)
             */
         public:
            inline constexpr Interval(double lo, double hi) throws()
                  : _lo(lo), _hi(hi)
            {
               //	assert(!(hi<lo)); // this also will work if either lo or hi is NaN
            }

            /**
             * Assign an new interval.
             * @param ival an interval
             * @return this
             */
         public:
            inline Interval& operator=(const Interval& ival) throws()
            {
               _lo = ival._lo;
               _hi = ival._hi;
               return *this;
            }

            /**
             * Assign an new interval representing the number.
             * @param value the value to be representsed.
             * @return this
             */
         public:
            inline Interval& operator=(double value) throws()
            {
               _lo = _hi = value;
               return *this;
            }

            /**
             * @name Interval methods.
             * @{
             */

            /**
             * Test if this is an undefined interval. Both, upper
             * and lower bounds are undefined, or the interval is
             * [ -INF;-INF] or [ INF;INF]
             * @return true if this is not a valid interval.
             */
         public:
            inline bool isUndefined() const throws()
            {
               return ::std::isnan(_lo) || ::std::isnan(_hi) || _lo == INF || _hi == -INF;
            }

            /**
             * Get the lower bound of the this interval
             * @return the lower bound of this interval
             */
         public:
            inline double lowerBound() const throws()
            {
               return _lo;
            }

            /**
             * Get the upper bound of the this interval
             * @return the upper bound of this interval
             */
         public:
            inline double upperBound() const throws()
            {
               return _hi;
            }

            /**
             * Return the midpoint of this interval.
             * @return the value (lowerBound()+upperBound())/2.0
             */
         public:
            inline double midPoint() const throws()
            {
               return 0.5 * (_lo + _hi);
            }

            /**
             * Get an estimate of the maximum error of the midpoint
             * with respect to the infintely precise result.
             * @return the maximum absolute error for the midpoint of this interva.
             */
         public:
            inline double absError() const throws()
            {
               return 0.5 * (_hi - _lo);
            }

            /*@}*/

            /**
             * Returns zero.
             * @return zero
             */
         public:
            inline static Interval zero() throws()
            {
               return Interval(0);
            }

            /**
             * @name Unary arithmetic functions
             * @{
             */

            /**
             * Get the absolute value of this interval.
             * @return the absolute value of this interval
             */
         public:
            Interval abs() const throws();

            /**
             * Return the negative of this number
             * @return the negative of this number
             */
         public:
            inline Interval neg() const throws()
            {
               return Interval(-_hi, -_lo);
            }

            /**
             * Invert this interval.
             * @return the 1.0/ *this
             */
         public:
            inline Interval inv() const throws()
            {
               return invMult(1.0);
            }

            /**
             * Compute s / *this.
             * @param s a scalar
             * @return the interval s/ *this
             */
         public:
            Interval invMult(double s) const throws();

            /**
             * Compute the square root
             * @return the square root of this value
             */
         public:
            Interval sqrt() const throws();

            /**
             * Compute the log this interval.
             * @return the log this value
             */
         public:
            Interval log() const throws();

            /**
             * Compute the exponential function e^(*this)
             * @return the interval of e^(*this)
             */
         public:
            Interval exp() const throws();

            /*@}*/

            /**
             * @name Binary arithmetic functions
             * @{
             */

            /**
             * Create an interval by adding two double numbers
             * @param x a double
             * @param y a double
             * @return the interval for x+y
             */
         public:
            inline static Interval addDouble(double x, double y) throws()
            {
               volatile double vx(x);
               volatile double vy(y);

               Interval i;
               RoundingMode roundingMode(RoundingMode::DOWN);
               i._lo = vx + vy;
               roundingMode.setRoundingMode(RoundingMode::UP);
               i._hi = vx + vy;
               return i;
            }

            /**
             * Create an interval by subtracting two double numbers
             * @param x a double
             * @param y a double
             * @return the interval for x-y
             */
         public:
            inline static Interval subDouble(double x, double y) throws()
            {
               return addDouble(x, -y);
            }

            /**
             * Create an interval by multiplying two double numbers
             * @param x a double
             * @param y a double
             * @return the interval for x*y
             */
         public:
            inline static Interval mulDouble(double x, double y) throws()
            {
               volatile double vx(x);
               volatile double vy(y);
               Interval i;
               RoundingMode roundingMode(RoundingMode::DOWN);
               i._lo = vx * vy;
               roundingMode.setRoundingMode(RoundingMode::UP);
               i._hi = vx * vy;
               return i;
            }

            /**
             * Create an interval by dividing two double numbers
             * @param x a double
             * @param y a double
             * @return the interval for x/y
             */
         public:
            static Interval divDouble(double x, double y) throws();

            /**
             * Add the specified interval to this interval.
             * @param x an interval
             * @return this
             */
         public:
            inline Interval& add(const Interval& x) throws()
            {
               addInterval(x._lo, x._hi);
               return *this;
            }

            /**
             * Add a scalar to this interval.
             * @param d a scalar value
             * @return this interval
             */
         public:
            inline Interval& add(double d) throws()
            {
               addInterval(d, d);
               return *this;
            }

            /**
             * Subtract the specified interval from this interval.
             * @param x an interval
             * @return this
             */
         public:
            inline Interval& sub(const Interval& x) throws()
            {
               addInterval(-x._hi, -x._lo);
               return *this;
            }

            /**
             * Subtract a scalar from this interval.
             * @param d a scalar value
             * @return this interval
             */
         public:
            inline Interval& sub(double d) throws()
            {
               addInterval(-d, -d);
               return *this;
            }

            /**
             * Multiply this interval with the specified interval
             * @param x an interval
             * @return this
             */
         public:
            inline Interval& mult(const Interval& x) throws()
            {
               multInterval(x._lo, x._hi);
               return *this;
            }

            /**
             * Multiply this interval by a scalar.
             * @param d a double value
             * @return this value
             */
         public:
            inline Interval& mult(double d) throws()
            {
               multInterval(d);
               return *this;
            }

            /**
             * Divide this interval with the specified interval
             * @param x an interval
             * @return this
             */
         public:
            inline Interval& div(const Interval& x) throws()
            {
               divInterval(x._lo, x._hi);
               return *this;
            }

            /**
             * Divide this interval by  a scalar
             * @param d a scalar
             * @return this
             */
         public:
            inline Interval& div(double d) throws()
            {
               divInterval(d);
               return *this;
            }

            /**
             * Add the specified interval to this interval.
             * @param lo the lower bound
             * @param hi the upper bound
             */
         private:
            inline void addInterval(double lo, double hi) throws()
            {
               // only compute if not undefined
               if (!isUndefined()) {
                  RoundingMode roundingMode(RoundingMode::DOWN);
                  _lo += lo;
                  roundingMode.setRoundingMode(RoundingMode::UP);
                  _hi += hi;
               }
            }

            /**
             * Multiply this interval by the specified
             * lo and hi values.
             * @param lo a lower bound
             * @param hi an upper bound
             */
         private:
            void multInterval(double lo, double hi) throws();

            /**
             * Multiply this interval by the specified
             * scalar.
             * @param s a scalar value
             */
         private:
            void multInterval(double s) throws();

            /**
             * Divide this interval by the specified interval.
             * @param lo a lo value
             * @param hi a hi value
             */
         private:
            void divInterval(double lo, double hi) throws();
            /**
             * Divide this interval by the specified scalar.
             * @param s a scalar
             */
         private:
            void divInterval(double s) throws();

            /*@}*/

            /**
             * @name Comparison of interval numbers
             * @{
             */

            /**
             * Compare this interval with the specified interval. The
             * return value is undefined if this interval is undefined.
             * @param x a interval
             * @return <0 if this < x, 0 if this==x, and >0 if this > x
             */
         public:
            inline Int32 compare(const Interval& x) const throws()
            {
               if (_hi < x._lo) {
                  return -1;
               }
               if (_lo > x._hi) {
                  return 1;
               }
               return 0;
            }

            /**
             * Compare the specified point to this interval. The
             * return value is undefined if this interval is undefined.
             * @param x a value
             * @return -1 if x > upperBound(), 1 if x<lowerBound(),
             * 0 otherwise
             */
         public:
            inline Int32 compare(double x) const throws()
            {
               return x > _hi ? -1 : (x < _lo ? 1 : 0);
            }

            /**
             * Test if this interval contains the specified interval completely.
             * @param i an internval
             * @return true if i completely contained with this interval
             */
         public:
            inline bool contains(const Interval& i) const throws()
            {
               return _lo <= i._lo && i._hi <= _hi;
            }

            /**
             * Test if this interval contains the specified value.
             * @param x a value
             * @return true if x is within this interval.
             */
         public:
            inline bool contains(double x) const throws()
            {
               return _lo <= x && x <= _hi;
            }

            /**
             * Get the sign of this value. The
             * return value is undefined if this interval is undefined.
             * @return -1 if the upper bound of the interval is less than 0
             *          0 if the interval contains 0
             *          1 if the lower bound of the interval is greater than 0
             */
         public:
            inline Int32 sign() const throws()
            {
               return _hi < 0.0 ? -1 : (_lo > 0.0 ? 1 : 0);
            }

            /*@}*/

            /**
             * Swap two interval numbers
             * @param interval another interval
             */
         public:
            inline void swap(Interval& interval) throws()
            {
               ::std::swap(_lo, interval._lo);
               ::std::swap(_hi, interval._hi);
            }

            /**
             * Convert this interval to a string.
             * @return a string
             */
         public:
            ::std::string toString() const throws();

            /** The lower bound of the interval */
         private:
            volatile double _lo;
            /** The upper bound of the interval */

         private:
            volatile double _hi;
      };

   }

   /* Functions overloaded from math.h */
   /**
    * Computes the sign of a interval number
    * @param x a interval number
    * @return the sign of x
    */
   inline Int32 sign(const ::canopy::math::Interval& x) throws()
   {
      return x.sign();
   }

   /**
    * Swap two intervals
    * @param x a interval number
    * @param y a interval number
    */
   inline void swap(::canopy::math::Interval& x, ::canopy::math::Interval& y) throws()
   {
      x.swap(y);
   }

   /**
    * Get the absolute value of a interval number
    * @param x a interval number
    * @return the absolute value of x
    */
   inline ::canopy::math::Interval abs(const ::canopy::math::Interval& x) throws()
   {
      return x.abs();
   }
}

/**
 * Add two intervals
 * @param x a interval
 * @param y a interval
 * @return x+y
 */
inline ::canopy::math::Interval operator+(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   ::canopy::math::Interval r(x);
   return r.add(y);
}

/**
 * Add a scalar to an interval.
 * @param x a interval
 * @param y a scalar
 * @return x+y
 */
inline ::canopy::math::Interval operator+(const ::canopy::math::Interval& x, double y) throws()
{
   ::canopy::math::Interval r(x);
   return r.add(y);
}

/**
 * Add an interval to a scalar.
 * @param y a scalar
 * @param x a interval
 * @return x+y as an interval
 */
inline ::canopy::math::Interval operator+(double y, const ::canopy::math::Interval& x) throws()
{
   ::canopy::math::Interval r(y);
   return r.add(x);
}

/**
 * Add two intervals.
 * @param x a interval
 * @param y a interval
 * @return *this + x
 */
inline ::canopy::math::Interval& operator+=(::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.add(y);
}

/**
 * Subtract two intervals
 * @param x a interval
 * @param y a interval
 * @return x+y
 */
inline ::canopy::math::Interval operator-(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   ::canopy::math::Interval r(x);
   return r.sub(y);
}

/**
 * Subtract a scalar from an interval.
 * @param x a interval
 * @param y a interval
 * @return x+y
 */
inline ::canopy::math::Interval operator-(const ::canopy::math::Interval& x, double y) throws()
{
   ::canopy::math::Interval r(x);
   return r.sub(y);
}

/**
 * Subtract an interval from a scalar.
 * @param y a interval
 * @param x a interval
 * @return x+y as an interval
 */
inline ::canopy::math::Interval operator-(double y, const ::canopy::math::Interval& x) throws()
{
   ::canopy::math::Interval r(y);
   return r.sub(x);
}

/**
 * Subtract a value from this value.
 * @param x a interval
 * @param y a interval
 * @return x+y
 */
inline ::canopy::math::Interval& operator-=(::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.sub(y);
}

/**
 * Multiply two intervals
 * @param x a interval
 * @param y a interval
 * @return x*y
 */
inline ::canopy::math::Interval operator*(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   ::canopy::math::Interval r(x);
   return r.mult(y);
}

/**
 * Multiply two intervals
 * @param x a interval
 * @param y a interval
 * @return x*y
 */
inline ::canopy::math::Interval& operator*=(::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.mult(y);
}

/**
 * Multiply an interval by a scalar.
 * @param x a interval
 * @param y a scalar
 * @return x*y
 */
inline ::canopy::math::Interval operator*(const ::canopy::math::Interval& x, double y) throws()
{
   ::canopy::math::Interval r(x);
   return r.mult(y);
}

/**
 * Multiply an interval by a scalar.
 * @param x a interval
 * @param y a scalar
 * @return x*y
 */
inline ::canopy::math::Interval& operator*=(::canopy::math::Interval& x, double y) throws()
{
   return x.mult(y);
}

/**
 * Multiply an interval by a scalar.
 * @param x a interval
 * @param y a scalar
 * @return x*y
 */
inline ::canopy::math::Interval operator*(double y, const ::canopy::math::Interval& x) throws()
{
   ::canopy::math::Interval r(x);
   return r.mult(y);
}

/**
 * Divide two intervals
 * @param x a interval
 * @param y a interval
 * @return x*y
 */
inline ::canopy::math::Interval operator/(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   ::canopy::math::Interval r(x);
   return r.div(y);
}

/**
 * Divide two intervals
 * @param x a interval
 * @param y a interval
 * @return x*y
 */
inline ::canopy::math::Interval& operator/=(::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.div(y);
}

/**
 * Divide an interval by a scalar.
 * @param x a interval
 * @param y a scalar
 * @return x*y
 */
inline ::canopy::math::Interval operator/(const ::canopy::math::Interval& x, double y) throws()
{
   ::canopy::math::Interval r(x);
   return r.div(y);
}

/**
 * Divide an interval by a scalar.
 * @param x a interval
 * @param y a scalar
 * @return x*y
 */
inline ::canopy::math::Interval& operator/=(::canopy::math::Interval& x, double y) throws()
{
   return x.div(y);
}

/**
 * Divide a scalar by an interval.
 * @param x a interval
 * @param y a scalar
 * @return x*y
 */
inline ::canopy::math::Interval operator/(double y, const ::canopy::math::Interval& x) throws()
{
   return x.invMult(y);
}

/**
 * The unary minus.
 * @param x a interval
 * @return -x
 */
inline ::canopy::math::Interval operator-(const ::canopy::math::Interval& x) throws()
{
   return x.neg();
}

/**
 * Compare two intervals for equality.
 * @param x a interval
 * @param y a interval
 * @return true if x==this
 */
inline bool operator==(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.compare(y) == 0;
}

/**
 * Compare two intervals for equality.
 * @param x a interval
 * @param y a interval
 * @return true if x==this
 */
inline bool operator!=(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.compare(y) != 0;
}

/**
 * Test if this interval is less than the specified interval
 * @param x a interval
 * @param y a interval
 * @return true if x is less than the specified interval
 */
inline bool operator<(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.compare(y) < 0;
}

/**
 * Test if this interval is less than or equal to the specified interval
 * @param x a interval
 * @param y a interval
 * @return true if x is less  than or equal to the specified interval
 */
inline bool operator<=(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.compare(y) <= 0;
}

/**
 * Test if this interval is greater than the specified interval
 * @param x a interval
 * @param y a interval
 * @return true if x is greater than the specified interval
 */
inline bool operator>(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.compare(y) > 0;
}

/**
 * Test if this interval is greater than or equal to the specified interval
 * @param x a interval
 * @param y a interval
 * @return true if x is greater  than or equal to the specified interval
 */
inline bool operator>=(const ::canopy::math::Interval& x, const ::canopy::math::Interval& y) throws()
{
   return x.compare(y) >= 0;
}

/**
 * Print a representation of this interval
 * @param out an output stream
 * @param ival an interval
 * @return out
 */
inline ::std::ostream& operator<<(::std::ostream& out, const ::canopy::math::Interval& ival) throws()
{
   return out << ival.toString();
}

#endif
