#ifndef _CANOPY_MATH_IEEE_H
#define _CANOPY_MATH_IEEE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_MATH_PRECISION_H
#include <canopy/math/Precision.h>
#endif

#ifndef _CANOPY_MATH_ROUNDINGMODE_H
#include <canopy/math/RoundingMode.h>
#endif

#include <cmath>
#include <fenv.h>
#include <iostream>

namespace canopy {
  namespace math {
    
    /** This types defines an IEEE 754 double */
    typedef double Double754;

    /** This types defines an IEEE 754 float */
    typedef float Float754;
    
    /**
     * Determine the number of precision bits.
     * @return the number of bits of precision
     */
    template <class T> 
      inline size_t precision() throws()
      { return 0; }
    
    /**
     * Determine the number of precision bits.
     * @return the number of bits of precision
     */
    template <> 
      inline size_t precision<Double754>() throws()
      { return 53; }
    
    /**
     * Determine the number of precision bits.
     * @return the number of bits of precision
     */
    template <> 
      inline size_t precision<Float754>() throws()
      { return 24; }
    
    /**
     * Determine the value of the least significant bit.
     * @param v the eps value for a given type
     * @return the least significant bit of v
     */
    template <class T> inline T eps(const T& v) throws()
      { return v/(INT64_C(1)<<precision<T>()); }
    
    /**
     * Set the rounding mode to nearest and the precision to float
     * for the next operation.
     */
    inline void setRoundToNearest() throws()
    { RoundingMode::setFPU(RoundingMode::NEAREST);   }
    
    /**
     * Test if the rounding mode is round to nearsest float
     * @return true if the rounding mode is to the nearest float
     */
    inline bool isRoundToNearest() throws()
    { return RoundingMode::testFPU(RoundingMode::NEAREST); }
    
    /**
     * Test for a floating point errors.
     * @return a non-zero number indicating an error
     */
    inline Int32 testErrors() throws()
    { 
      Int32 ok = fetestexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW|FE_UNDERFLOW);
#ifdef DEBUG
      if (ok!=0) {
	if (ok & FE_DIVBYZERO) {
	  ::std::cerr << "FE_DIVBYZERO ";
	}
	if (ok & FE_INVALID) {
	  ::std::cerr << "FE_INVALID ";
	}
	if (ok & FE_OVERFLOW) {
	  ::std::cerr << "FE_OVERFLOW ";
	}
	if (ok & FE_UNDERFLOW) {
	  ::std::cerr << "FE_UNDERFLOW ";
	}
	::std::cerr << '\n';
      }
#endif
      return ok;
    }
    
    /**
     * Test for a floating point errors and clear them.
     * @return a non-zero number indicating an error
     */
    inline Int32 testAndClearErrors() throws()
    { 
      Int32 ok = testErrors();
      feclearexcept(FE_ALL_EXCEPT);
      return ok;
    }
  }
}

#endif
