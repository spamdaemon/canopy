#ifndef _CANOPY_MATH_REAL_H
#define _CANOPY_MATH_REAL_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <memory>

namespace canopy {
   namespace math {
      class RealRep;

      /**
       * Real provides arbitrary precision
       * arithmetic with floating point numbers.
       * @note Each Real object is really a handle to some
       * implementation, which means using Reals
       * as arguments by value rather than reference is
       * still efficient. <br>
       * @note
       * This class is not thread-safe, which means objects
       * cannot be passed between threads safely.
       * @author Raimund Merkert
       * @date 13 Jan 2002
       */
      class Real
      {
            /**
             * The different rounding modes when converting a
             * Real to a Double
             */
         public:
            enum RoundingMode
            {
               /** Round to the nearest double */
               ROUND_TO_NEAREST,
               /** Round to the nearest double that is no greater than the real */
               ROUND_DOWN,
               /** Round to the nearest double that is no smaller that the real */
               ROUND_UP
            };

            /**
             * Creates a new Real instance with the specifeid representation.
             * @param r a representation
             */
         private:
            Real(::std::shared_ptr<RealRep> r) throws();

            /**
             * Creates a new Real instance initialized with 0.
             */
         public:
            Real() throws();

            /**
             * Creates a new Real instance initialized with the
             * specified double value
             * @param value a value
             */
         public:
            Real(Int32 value) throws();

            /**
             * Creates a new Real instance initialized with the
             * specified double value
             * @param value a value
             * @throw ::std::exception if the value is not a good value
             */
         public:
            Real(double value) throws(::std::exception);

            /**
             * Creates a real number from a string.
             * @param value a string
             * @throw ::std::exception if the string is not a number
             */
         public:
            Real(const char* value) throws(::std::exception);

            /**
             * Creates a real number from a string.
             * @param value a string
             * @throw ::std::exception if the string is not a number
             */
         public:
            Real(const ::std::string& value) throws(::std::exception);

            /**
             * Copy-constructs a new Real object
             * @param source the source Real object
             */
         public:
            Real(const Real& source) throws();

            /**
             * Copies the specified Real object.
             * @param source the Real object to be copied
             * @return a reference to this object
             */
         public:
            Real& operator=(const Real& source) throws();

            /**
             * Destroys this Real object.
             */
         public:
            ~Real() throws();

            /**
             * Swap two real numbers
             * @param real another real
             */
         public:
            inline void swap(Real& real) throws()
            {
               ::std::swap(_rep, real._rep);
            }

            /**
             * @name Conversion methods
             * @{
             */

            /**
             * Convert this real to a double using the
             * specified rounding mode.
             * @param mode a rounding mode
             */
         public:
            Double toDouble(RoundingMode mode) const throws();

            /**
             * Convert this real to a double using the
             * ROUND_TO_NEAREST rounding mode.
             * @return the double corresponding to this real
             */
         public:
            inline Double toDouble() const throws()
            {
               return toDouble(ROUND_TO_NEAREST);
            }

            /*@}*/

            /**
             * @name Unary functions
             * @{
             */

            /**
             * Get the absolute value of this real.
             * @return the absolute value of this real
             */
         public:
            inline Real abs() const throws()
            {
               return sign() < 0 ? neg() : (*this);
            }

            /**
             * Return the negative of this number
             * @return the negative of this number
             */
         public:
            Real neg() const throws();

            /*@}*/

            /**
             * Get the sign of this value.
             * @return -1 if this <0, 0 if this==0, and 1 if this>0
             */
         public:
            Int32 sign() const throws();

            /**
             * @name Binary operations
             * @{
             */

            /**
             * Add x to this value.
             * @param x a real
             * @return this
             */
         public:
            Real& add(const Real& x) throws();

            /**
             * Subtract x from this value.
             * @param x a real
             * @return this
             */
         public:
            Real& sub(const Real& x) throws();

            /**
             * Multiply this value by x.
             * @param x a real
             * @return this
             */
         public:
            Real& mult(const Real& x) throws();

            /*@}*/

            /**
             * @name Optimized operations
             * @{
             */

            /**
             * Multiply and add three numbers. This operation
             * may be implemented more efficiently.
             * @param x a real
             * @param y a real
             * @param z a real
             * @return x*y+z
             */
         public:
            static Real multiplyAdd(const Real& x, const Real& y, const Real& z) throws();

            /**
             * Multiply and subtract three numbers. This operation
             * may be implemented more efficiently.
             * @param x a real
             * @param y a real
             * @param z a real
             * @return x*y-z
             */
         public:
            static Real multiplySub(const Real& x, const Real& y, const Real& z) throws();

            /**
             * Subtract a multiple. This operation
             * may be implemented more efficiently.
             * @param x a real
             * @param y a real
             * @param z a real
             * @return x-y*z
             */
         public:
            static Real subMultiply(const Real& x, const Real& y, const Real& z) throws();

            /**
             * Multiply two sets of numbers and add them.
             * @param w a real
             * @param x a real
             * @param y a real
             * @param z a real
             * @return w*z+y*z
             */
         public:
            static Real multiplyAddPairs(const Real& w, const Real& x, const Real& y, const Real& z) throws();

            /**
             * Multiply two sets of numbers and subtract them.
             * @param w a real
             * @param x a real
             * @param y a real
             * @param z a real
             * @return w*z-y*z
             */
         public:
            static Real multiplySubPairs(const Real& w, const Real& x, const Real& y, const Real& z) throws();

            /*@}*/

            /**
             * @name Comparison of real numbers
             * @{
             */

            /**
             * Compare this real with the specified real.
             * @param x a real
             * @return <0 if this < x, 0 if this==x, and >0 if this > x
             */
         public:
            inline Int32 compare(const Real& x) const throws()
            {
               return x._rep == _rep ? 0 : compareWith(x);
            }

            /**
             * Compare this real with the specified real.
             * @param x a real
             * @return <0 if this < x, 0 if this==x, and >0 if this > x
             */
         private:
            Int32 compareWith(const Real& x) const throws();

            /*@}*/

            /** The real implementation which is never 0 */
         private:
            mutable ::std::shared_ptr< RealRep> _rep;
      };

      /**
       * Multiply and add three numbers. This operation
       * may be implemented more efficiently.
       * @param x a real
       * @param y a real
       * @param z a real
       * @return x*y+z
       */
      inline Real multiplyAdd(const Real& x, const Real& y, const Real& z) throws()
      {
         return Real::multiplyAdd(x, y, z);
      }

      /**
       * Multiply and subtract three numbers. This operation
       * may be implemented more efficiently.
       * @param x a real
       * @param y a real
       * @param z a real
       * @return x*y-z
       */
      inline Real multiplySub(const Real& x, const Real& y, const Real& z) throws()
      {
         return Real::multiplySub(x, y, z);
      }

      /**
       * Subtract a multiple of some number. This operation
       * may be implemented more efficiently.
       * @param x a real
       * @param y a real
       * @param z a real
       * @return x-y*z
       */
      inline Real subMultiply(const Real& x, const Real& y, const Real& z) throws()
      {
         return Real::subMultiply(x, y, z);
      }

      /**
       * Multiply two sets of numbers and add them.
       * @param w a real
       * @param x a real
       * @param y a real
       * @param z a real
       * @return p0*p1+q0*q1
       */
      inline Real multiplyAddPairs(const Real& w, const Real& x, const Real& y, const Real& z) throws()
      {
         return Real::multiplyAddPairs(w, x, y, z);
      }

      /**
       * Multiply two sets of numbers and subtract them.
       * @param w a real
       * @param x a real
       * @param y a real
       * @param z a real
       * @return p0*p1-q0*q1
       */
      inline Real multiplySubPairs(const Real& w, const Real& x, const Real& y, const Real& z) throws()
      {
         return Real::multiplySubPairs(w, x, y, z);
      }

   }

   /* Functions overloaded from math.h */
   /**
    * Computes the sign of a real number
    * @param x a real number
    * @return the sign of x
    */
   inline Int32 sign(const ::canopy::math::Real& x) throws()
   {
      return x.sign();
   }

   /**
    * Swap two reals
    * @param x a real number
    * @param y a real number
    */
   inline void swap(::canopy::math::Real& x, ::canopy::math::Real& y) throws()
   {
      x.swap(y);
   }

   /**
    * Get the absolute value of a real number
    * @param x a real number
    * @return the absolute value of x
    */
   inline ::canopy::math::Real abs(const ::canopy::math::Real& x) throws()
   {
      return x.abs();
   }
}

/**
 * Add two reals
 * @param x a real
 * @param y a real
 * @return x+y
 */
inline ::canopy::math::Real operator+(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   ::canopy::math::Real r(x);
   return r.add(y);
}

/**
 * Add two reals.
 * @param x a real
 * @param y a real
 * @return *this + x
 */
inline ::canopy::math::Real& operator+=(::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.add(y);
}

/**
 * Subtract two reals
 * @param x a real
 * @param y a real
 * @return x+y
 */
inline ::canopy::math::Real operator-(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   ::canopy::math::Real r(x);
   return r.sub(y);
}

/**
 * Subtract a value from this value.
 * @param x a real
 * @param y a real
 * @return x+y
 */
inline ::canopy::math::Real& operator-=(::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.sub(y);
}

/**
 * Multiply two reals
 * @param x a real
 * @param y a real
 * @return x+y
 */
inline ::canopy::math::Real operator*(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   ::canopy::math::Real r(x);
   return r.mult(y);
}

/**
 * Multiply two reals
 * @param x a real
 * @param y a real
 * @return x+y
 */
inline ::canopy::math::Real& operator*=(::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.mult(y);
}

/**
 * The unary minus.
 * @param x a real
 * @return -x
 */
inline ::canopy::math::Real operator-(const ::canopy::math::Real& x) throws()
{
   return x.neg();
}

/**
 * Compare two reals for equality.
 * @param x a real
 * @param y a real
 * @return true if x==this
 */
inline bool operator==(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.compare(y) == 0;
}

/**
 * Compare two reals for equality.
 * @param x a real
 * @param y a real
 * @return true if x==this
 */
inline bool operator!=(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.compare(y) != 0;
}

/**
 * Test if this real is less than the specified real
 * @param x a real
 * @param y a real
 * @return true if x is less than the specified real
 */
inline bool operator<(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.compare(y) < 0;
}

/**
 * Test if this real is less than or equal to the specified real
 * @param x a real
 * @param y a real
 * @return true if x is less  than or equal to the specified real
 */
inline bool operator<=(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.compare(y) <= 0;
}

/**
 * Test if this real is greater than the specified real
 * @param x a real
 * @param y a real
 * @return true if x is greater than the specified real
 */
inline bool operator>(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.compare(y) > 0;
}

/**
 * Test if this real is greater than or equal to the specified real
 * @param x a real
 * @param y a real
 * @return true if x is greater  than or equal to the specified real
 */
inline bool operator>=(const ::canopy::math::Real& x, const ::canopy::math::Real& y) throws()
{
   return x.compare(y) >= 0;
}

#endif
