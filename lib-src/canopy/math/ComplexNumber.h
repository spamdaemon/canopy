#ifndef _CANOPY_MATH_COMPLEXNUMBER_H
#define _CANOPY_MATH_COMPLEXNUMBER_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <iosfwd>
#include <cmath>

namespace canopy {
  namespace math {

    /**
     * A simple complex number class. The complex number is represented as a pair of a real and 
     * imaginary number. All arithmetic operations are reasonably fast to implement using this representation.
     */
    class ComplexNumber {
      /**
       * Create the complex number representing 0.
       */
    public:
      inline ComplexNumber () throws()
	: _r(0),_i(0) {}
      
      /**
       * Create the complex number for the given real number.
       * @param xr a real number
       */
    public:
      inline ComplexNumber (double xr)
	: _r(xr),_i(0) {}
      
      /**
       * Create the complex number for the given real number.
       * @param xr a real number
       * @param xi the imaginary part of the number
       */
    public:
      inline ComplexNumber (double xr, double xi)
	: _r(xr),_i(xi) {}
      

      /**
       * Create a complex number from its polar form. This is usually
       * referred to as r * exp(theta)
       * @param xr the radius
       * @param theta the angle.
       */
    public:
      static inline ComplexNumber fromPolar (double xr, double theta) throws()
      {
	return ComplexNumber(xr* ::std::cos(theta),xr* ::std::sin(theta)); 
      }


      /**
       * Get the polar coordinates for this complex number.
       * @param xr the radius (output)
       * @param theta the angle (output in radians)
       */
    public:
      inline void toPolar (double& xr, double& theta) const throws()
      {
	xr = ::std::sqrt(_r*_r + _i*_i);
	theta = ::std::atan2(_i,_r);
      }


      /**
       * Get the real part
       * @return the real part of this number
       */
    public:
      double r() const throws() { return _r; }
      double& r() throws() { return _r; }

      /**
       * Get the imaginary part
       * @return the imaginary part of this number
       */
    public:
      double i() const throws() { return _i; }
      double& i() throws() { return _i; }


      /**
       * @name Arithmetic operations
       * @{
       */
      
      /* Addition  and subtraction */
    public:
      inline ComplexNumber& operator+= (const ComplexNumber& c) throws()
      { _r+= c._r; _i += c._i; return *this; }
      
      inline ComplexNumber operator+ (const ComplexNumber& c) const throws()
      { return ComplexNumber(_r+ c._r, _i + c._i); }

    public:
      inline ComplexNumber& operator-= (const ComplexNumber& c) throws()
      { _r-= c._r; _i -= c._i; return *this; }
      inline ComplexNumber operator- (const ComplexNumber& c) const throws()
      { return ComplexNumber(_r- c._r, _i - c._i); }
      
      /* Multiplication and division */
    public:
      ComplexNumber& operator*= (const ComplexNumber& c) throws();

      inline ComplexNumber operator* (const ComplexNumber& c) const throws()
	{ return ComplexNumber(_r*c._r - _i*c._i,_r*c._i + _i*c._r); }

      ComplexNumber& operator/= (const ComplexNumber& c) throws();
      ComplexNumber operator/ (const ComplexNumber& c) const throws();


      /*@}*/

      /** The complex number components */
    private:
      double _r,_i; 
    };

    /** Output operator */
    ::std::ostream& operator<< (::std::ostream& out, const ComplexNumber& c);

  }
}

#endif
