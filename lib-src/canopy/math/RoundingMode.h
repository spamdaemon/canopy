#ifndef _CANOPY_MATH_ROUNDINGMODE_H
#define _CANOPY_MATH_ROUNDINGMODE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <cassert>

namespace canopy {
  namespace math {
    /**
     * This class controls the rounding mode of the
     * floating point unit.
     */
    class RoundingMode {
      RoundingMode(const RoundingMode&);
      RoundingMode&operator=(const RoundingMode&);

      /** The type of the FPU's control word */
    private:
      typedef UInt32 ControlWord;
    
      /**
       * The rounding modes
       */
    public:
      enum Mode { 
	NEAREST = 0x000, 
	DOWN =    0x400, 
	UP =      0x800, 
	ZERO =    0xc00
      };
    
      /** 
       * Create a new FPU object.
       */
    public:
      inline RoundingMode () throws() 
      { getFPU(_initial); }
    
      /** 
       * Create a new FPU object and change the
       * rounding mode.
       * @param rounding the new rounding mode
       */
    public:
      inline RoundingMode (Mode rounding) throws() 
      {
	getFPU(_initial); 
	setRoundingMode(rounding); 
      }
    
      /**
       * Destroy this FPU object, restoring
       * the setting that were in effect before
       * construction of this object.
       */
    public:
      inline ~RoundingMode () throws() {
	setFPU(_initial);
      }

      /**
       * Change the current modes relative to the inital settings.
       * @param rounding the new rounding mode
       */
    public:
      inline static void setFPU (Mode rounding) throws() 
      {
	ControlWord ctrl;
	getFPU(ctrl);
	ctrl = ctrl & ~(NEAREST|DOWN|UP|ZERO);
	ctrl = ctrl | rounding;
	setFPU(ctrl);
      }

      /**
       * Test the current rounding mode 
       * @param rounding the new rounding mode
       * @return true if the rounding mode is as specified
       */
    public:
      inline static bool testFPU (Mode rounding) throws() 
      {
	ControlWord ctrl;
	getFPU(ctrl);
	ControlWord value = rounding;
	return (ctrl & (NEAREST|DOWN|UP|ZERO)) == value;
      }

      /**
       * Set a new rounding mode. If the FPU control word
       * has changed since creation of this class, then an assertion
       * raised in debug mode. Use setFPU() if unobserved changes
       * to the FOU control word have occurred.
       *
       * @param rounding a new rounding mode
       */
    public:
      inline void setRoundingMode (Mode rounding) throws() {
        ControlWord ctrl;
        getFPU(ctrl);
	ctrl = (ctrl & ~(NEAREST|DOWN|UP|ZERO)) | rounding;
	setFPU(ctrl);
      }
    
      /**
       * Reset the settings to those that were in
       * effect prior to construction of this object.
       */
    public:
      inline void reset () throws() 
      { setFPU(_initial); }
    
      /** Set the FPU's control word */
    private:
      inline static void setFPU (ControlWord ctrl) 
      { asm volatile ("fldcw %0" : : "m" (*&ctrl)); }

      /**
       * Get the fpu's control word.
       * @param ctrl a location where the FPU will place it's status
       */
    private:
      inline static void getFPU (ControlWord volatile&  ctrl)
      {	asm ("fnstcw %0" : "=m" (ctrl)); }
      
      /** The initial settings */
    private:
      volatile ControlWord _initial;
    };
  }
}
#endif
