#include <canopy/math/Real.h>
#include <canopy/math/RealRep.h>

namespace canopy {
   namespace math {

      Real::~Real() throws()
      {
      }

      Real::Real(::std::shared_ptr< RealRep> r)
throws            ()
            : _rep(::std::move(r))
            {
               assert(_rep);
            }

      Real::Real()
throws            ()
            : _rep(RealRep::zero())
            {}

      Real::Real(Int32 value)
throws            ()
            : _rep(RealRep::create(value))
            {}

      Real::Real(double value)
throws            (::std::exception)
            : _rep(RealRep::create(value))
            {}

      Real::Real(const char* value)
throws            (::std::exception)
            : _rep(RealRep::create(value))
            {}

      Real::Real(const ::std::string& value)
throws            (::std::exception)
            : _rep(RealRep::create(value.c_str()))
            {}

      Real::Real(const Real& initializer)
throws            ()
            : _rep(initializer._rep)
            {}

      Real& Real::operator=(const Real& source) throws()
      {
         _rep = source._rep;
         return *this;
      }

      double Real::toDouble(RoundingMode mode) const throws()
      {
         switch (mode) {
            case ROUND_UP:
               return _rep->roundUp();
            case ROUND_DOWN:
               return _rep->roundDown();
            default:
               return _rep->roundToNearest();
         };
      }

      Real& Real::add(const Real& x) throws()
      {
         if (sign() == 0) {
            *this = x;
         }
         else if (x.sign() != 0) {
            _rep = _rep->add(x._rep);
         }
         return *this;
      }

      Real& Real::sub(const Real& x) throws()
      {
         if (x.sign() != 0) {
            _rep =  _rep->sub(x._rep);
         }
         return *this;
      }

      Real& Real::mult(const Real& x) throws()
      {
         _rep = _rep->mult(x._rep);
         return *this;
      }

      Int32 Real::compareWith(const Real& x) const throws()
      {
         return _rep->compare(x._rep);
      }

      Int32 Real::sign() const throws()
      {
         return _rep->sign();
      }

      Real Real::neg() const throws()
      {
         if (sign() == 0) {
            return Real();
         }
         return _rep->neg();
      }

      Real Real::multiplyAdd(const Real& x, const Real& y, const Real& z) throws()
      {
         return RealRep::multiplyAdd(x._rep, y._rep, z._rep);
      }

      Real Real::multiplySub(const Real& x, const Real& y, const Real& z) throws()
      {
         return RealRep::multiplySub(x._rep, y._rep, z._rep);
      }

      Real Real::subMultiply(const Real& x, const Real& y, const Real& z) throws()
      {
         return RealRep::subMultiply(x._rep, y._rep, z._rep);
      }

      Real Real::multiplyAddPairs(const Real& w, const Real& x, const Real& y, const Real& z) throws()
      {
         return RealRep::multiplyAddPairs(w._rep, x._rep, y._rep, z._rep);
      }

      Real Real::multiplySubPairs(const Real& w, const Real& x, const Real& y, const Real& z) throws()
      {
         return RealRep::multiplySubPairs(w._rep, x._rep, y._rep, z._rep);
      }
   }
}
