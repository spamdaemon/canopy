#ifndef _CANOPY_SHAREDREF_H
#define _CANOPY_SHAREDREF_H

#include <memory>
#include <stdexcept>

namespace canopy {

   /**
    * The shared ref is used to track a non-null shared_ptr.
    */
   template<class T> class SharedRef
   {
         template<class U> friend class SharedRef;

         /**
          * Create a shared ref from a shared pointer.
          * @param ptr a shared pointer
          * @throws ::std::invalid_argument if ptr==nullptr
          */
      public:
         inline SharedRef(::std::shared_ptr< T> ptr)
               : _ref(ptr)
         {
            if (!_ref) {
               throw ::std::invalid_argument("Null pointer");
            }
         }

         /**
          * Create a shared ref from a shared pointer.
          * @param ptr a shared pointer
          * @throws ::std::invalid_argument if ptr==nullptr
          */
      public:
         template<class U>
         inline SharedRef(::std::shared_ptr< U> ptr)
               : SharedRef(::std::dynamic_pointer_cast< T>(::std::move(ptr)))
         {
         }

         /**
          * Create a shared ref from a shared pointer.
          * @param ptr a shared pointer
          * @throws ::std::invalid_argument if ptr==nullptr
          */
      public:
         template<class U>
         inline SharedRef(::std::unique_ptr< U> ptr)
               : SharedRef(::std::shared_ptr< U>(::std::move(ptr)))
         {
         }

         /**
          * Copy constructor.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedRef(const SharedRef& ref)
               : _ref(ref._ref)
         {
         }

         /**
          * Copy constructor.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedRef(const SharedRef< U>& ref)
               : SharedRef(ref._ref)
         {
         }

         /**
          * A conversion operator
          */
      public:
         template<class U>
         inline operator ::std::shared_ptr<U>() const
         {
            return ::std::dynamic_pointer_cast< U>(_ref);
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedRef& operator=(const SharedRef& ref)
         {
            _ref = ref._ref;
            return *this;
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         inline SharedRef& operator=(::std::shared_ptr< T> ptr)
         {
            if (!ptr) {
               throw ::std::invalid_argument("Null pointer");
            }
            _ref = ::std::move(ptr);
            return *this;
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedRef& operator=(::std::shared_ptr< U> ptr)
         {
            return operator=(::std::dynamic_pointer_cast< T>(::std::move(ptr)));
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedRef& operator=(::std::unique_ptr< U> ptr)
         {
            return operator=(::std::shared_ptr< U>(::std::move(ptr)));
         }

         /**
          * Copy operator.
          * @param ref a ref
          * @return this
          */
      public:
         template<class U>
         inline SharedRef& operator=(const SharedRef< U>& ref)
         {
            return operator=(ref._ref);
         }

         /**
          * Deref the pointer.
          * @return a pointer
          */
      public:
         inline T& operator*() const
         {
            return *_ref;
         }

         /**
          * Deref the pointer.
          * @return a pointer
          */
      public:
         inline T* operator->() const
         {
            return _ref.get();
         }

         /**
          * Get the shared pointer.
          * @return the shared pointer object
          */
      public:
         inline ::std::shared_ptr< T> get() const
         {
            return _ref;
         }

         /**
          * Compare two references.
          * @return true if get() < p.get()
          */
      public:
         template<class U>
         inline bool operator<(const SharedRef< U>& ref) const
         {
            return _ref < ref._ref;
         }

         /**
          * Compare two references for equality.
          * @param p a ref
          * @return true the two references are the same
          */
      public:
         template<class U>
         bool operator==(const SharedRef< U>& ref) const
         {
            return _ref == ref._ref;
         }

         /**
          * Compare two references for inequality.
          * @param p a ref
          * @return true the two references are not the same
          */
      public:
         template<class U>
         inline bool operator!=(const SharedRef< U>& p) const
         {
            return _ref != p._ref;
         }

         /**
          * Swap two refs.
          * @param a a ref
          * @param b a ref
          */
      public:
         static inline void swap(::canopy::SharedRef< T>& a, ::canopy::SharedRef< T>& b)
         {
            ::std::swap(a._ref, b._ref);
         }

         /** The shared pointer */
      private:
         ::std::shared_ptr< T> _ref;
   }
   ;
}

namespace std {
   template<class T>
   void swap(::canopy::SharedRef< T>& a, ::canopy::SharedRef< T>& b)
   {
      ::canopy::SharedRef< T>::swap(a, b);
   }
}

#endif
