#include <canopy/Result.h>
#include <cstdlib>
#include <iostream>

namespace canopy {

   namespace {

      static void abort_if_result_not_checked(const char* m)
      {
         ::std::cerr << "Result not checked: ";
         if (m) {
            ::std::cerr << m;
         }
         ::std::cerr << ::std::endl << "Aborting" << ::std::endl;
         ::std::abort();
      }

      static void (*result_not_checked)(const char*) = abort_if_result_not_checked;

   }

   void resultNotChecked(const char* message) throw()
   {
      result_not_checked(message);
   }

   /**
    * Set the function to executed when a check fails.
    * @param f a function to be executed when a check fails
    */
   void setResultNotChecked(void(*f)(const char*)) throw()
   {
      if (f == 0) {
         f = abort_if_result_not_checked;
      }
      result_not_checked = f;
   }
}
