#ifndef _CANOPY_LAMBDA_H
#define _CANOPY_LAMBDA_H

#include <functional>

namespace canopy {
   /**
    * This namespace contains some utility functions that
    * deal with lambda functions.
    * <p>
    * Most of this code is I found <a href="http://meh.schizofreni.co/programming/magic/2013/01/23/function-pointer-from-lambda.html">here</a>.
    */
   namespace lambda {

      /** A helper class to derive the types for a lambda */
      template<typename LAMBDA>
      struct LambdaTraits : public LambdaTraits< decltype(&LAMBDA::operator())>
      {
      };

      /** A helper type */
      template<typename ClassType, typename ReturnType, typename ... Args>
      struct LambdaTraits< ReturnType (ClassType::*)(Args...) const>
      {
            typedef ReturnType (*pointer)(Args...);
            typedef std::function< ReturnType(Args...)> function;
      };

      /**
       * Determine the function pointer type of the specified lambda.
       * @param lambda
       * @return a function pointer to specified lambda
       */
      template<typename LAMBDA>
      typename LambdaTraits< LAMBDA>::pointer toFunctionPointer(LAMBDA lambda)
      {
         return static_cast< typename LambdaTraits< LAMBDA>::pointer>(lambda);
      }


      /**
       * Determine the the exact ::std::function type for a lambda. The resulting
       * value cannot be used, but it's decltype can.
       * @param lambda
       * @return a function object that wraps the lambda
       */
      template<typename LAMBDA>
      typename LambdaTraits< LAMBDA>::function toFunction(LAMBDA lambda)
      {
         return static_cast< typename LambdaTraits< LAMBDA>::function>(lambda);
      }

   }
}

#endif
