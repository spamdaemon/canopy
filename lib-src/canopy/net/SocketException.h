#ifndef _CANOPY_NET_SOCKETEXCEPTION_H
#define _CANOPY_NET_SOCKETEXCEPTION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_NET_NETEXCEPTION_H
#include <canopy/net/NetException.h>
#endif

namespace canopy {
   namespace net {

      /**
       * This exception may thrown by operations that work
       * no connected sockets.
       */
      class SocketException : public NetException
      {
         private:
            SocketException();

            /**
             * The various exception codes.
             */
         public:
            enum ExceptionCode
            {
               CONNECTION_BROKEN, /// the socket has been broken
               INVALID_ADDRESS, /// an invalid address was specified
               DENIED, /// the user does not have permission to bind the address
               ALREADY_BOUND, /// the socket is already bound to an address
               ALREADY_USED, /// the address has already been bound by another socket
               CONNECTION_ABORTED, /// connection was closed during accept
               NO_RESOURCES, /// not enough resources to accept a connection
               UNSUPPORTED_OPERATION, /// the socket type cannot accept connections
               ALREADY_CONNECTED, /// the socket is already connected
               CONNECTION_REFUSED, /// no one is listening on the remote address
               CONNECTION_TIMED_OUT, /// the server may be too busy
               NETWORK_UNREACHABLE, /// the network is unreachable
               CONNECTION_IN_PROGRESS, /// non-blocking socket is being connected
               CONNECTION_RESET, /// connection reset by peer
               OTHER /// other error codes
            };

            /**
             * Create an exception with the specified code.
             * @param code an exception code
             */
         public:
            SocketException(ExceptionCode code) throws ();

            /**
             * Create an exception with the specified error string.
             * The exception code is set to OTHER.
             * @param msg an error message
             */
         public:
            SocketException(::std::string msg) throws ();

            /** Destructor */
         public:
            ~SocketException() throws();

            /**
             * Get the exception code.
             * @return exception code.
             */
         public:
            inline ExceptionCode getCode() const throws ()
            {
               return _code;
            }

            /** The error code. */
         private:
            ExceptionCode _code;
      };
   }
}

#endif
