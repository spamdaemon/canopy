#include <canopy/net/NetException.h>
#include <memory>

namespace canopy {
   namespace net {

      NetException::NetException(::std::string s)
throws            ()
            : ::canopy::io::IOException(::std::move(s))
            {
            }

      NetException::~NetException() throws()
      {
      }
   }
}
