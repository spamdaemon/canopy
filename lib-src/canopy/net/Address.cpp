#include <canopy/net/Address.h>
#include <canopy/SystemError.h>

#include <cstring>
#include <cassert>
#include <iostream>

#include <netdb.h>
#include <netinet/in.h>

using namespace ::std;

namespace canopy {
   namespace net {

      Address::Address()
throws            ()
            : _addrlen(0)
            {
               memset(&_address, 0, sizeof(_address));
            }

      Address::Address(SocketType t, Protocol p, size_t addrlen, const ::sockaddr_storage& addr)
throws            (::std::exception)
            : _type(t),_domain(addr.ss_family), _protocol(p), _addrlen(static_cast< ::socklen_t>(addrlen))
            {
               if (addrlen > sizeof(::sockaddr_storage)) {
                  throw ::std::invalid_argument("Invalid address length: got " +::std::to_string(addrlen)+", expected "+::std::to_string(sizeof(::sockaddr_storage)));
               }
               assert(addrlen < UINT64_C(0x10000));
               memcpy(&_address, &addr, _addrlen);
            }


      Address::~Address() throws()
      {
      }

      string Address::nodeID() const throws()
      {
         char node[NI_MAXHOST];
         int niFlags = NI_NUMERICHOST;

         int ok = getnameinfo(sockaddr(), _addrlen, node, sizeof(node), 0, 0, niFlags);

         if (ok != 0) {
            ::std::string details;
            switch (ok) {
               case EAI_SYSTEM:
                  throw SystemError("getnameinfo failed");

               case EAI_AGAIN:
                  details = "AGAIN";
                  break;
               case EAI_BADFLAGS:
                  details = "BADFLAGS";
                  break;
               case EAI_FAIL:
                  details = "FAIL";
                  break;
               case EAI_FAMILY:
//                  details = "FAMILY";
                  break;
               case EAI_MEMORY:
                  details = "MEMORY";
                  break;
               case EAI_NONAME:
                  details = "NONAME";
                  break;
#ifdef EAI_OVERFLOW
               case EAI_OVERFLOW:
                  details = "OVERFLOW";
                  break;
#endif
            }
            if (!details.empty()) {
               ::canopy::printErrorMessage("Could not convert address to numeric name: " + details);
            }
            return string();
         }

         return string(&node[0]);
      }

      string Address::nodeName() const throws()
      {
         char node[NI_MAXHOST];
         int niFlags = 0;

         int ok = getnameinfo(sockaddr(), _addrlen, node, sizeof(node), 0, 0, niFlags);

         if (ok != 0) {
            ::std::string details;
            switch (ok) {
               case EAI_SYSTEM:
                  throw SystemError("getnameinfo failed");

               case EAI_AGAIN:
                  details = "AGAIN";
                  break;
               case EAI_BADFLAGS:
                  details = "BADFLAGS";
                  break;
               case EAI_FAIL:
                  details = "FAIL";
                  break;
               case EAI_FAMILY:
                  //                  details = "FAMILY";
                  break;
               case EAI_MEMORY:
                  details = "MEMORY";
                  break;
               case EAI_NONAME:
                  details = "NONAME";
                  break;
#ifdef EAI_OVERFLOW
               case EAI_OVERFLOW:
                  details = "OVERFLOW";
                  break;
#endif
            }
            if (!details.empty()) {
               ::canopy::printErrorMessage("Could not convert address to name: " + details);
            }
            return string();
         }

         return string(&node[0]);
      }

      string Address::serviceName() const throws()
      {
         char service[NI_MAXSERV];

         int niFlags = 0;

         int ok = getnameinfo(sockaddr(), _addrlen, 0, 0, service, sizeof(service), niFlags);

         if (ok != 0) {
            ::std::string details;
            switch (ok) {
               case EAI_SYSTEM:
                  throw SystemError("getnameinfo failed");

               case EAI_AGAIN:
                  details = "AGAIN";
                  break;
               case EAI_BADFLAGS:
                  details = "BADFLAGS";
                  break;
               case EAI_FAIL:
                  details = "FAIL";
                  break;
               case EAI_FAMILY:
                  //                  details = "FAMILY";
                  break;
               case EAI_MEMORY:
                  details = "MEMORY";
                  break;
               case EAI_NONAME:
                  details = "NONAME";
                  break;
#ifdef EAI_OVERFLOW
               case EAI_OVERFLOW:
                  details = "OVERFLOW";
                  break;
#endif
            }
            if (!details.empty()) {
               ::canopy::printErrorMessage("Could not convert service to name: " + details);
            }
            return string();
         }

         return string(&service[0]);
      }

      string Address::serviceID() const throws()
      {
         char service[NI_MAXSERV];

         int niFlags = NI_NUMERICSERV;

         int ok = getnameinfo(sockaddr(), _addrlen, 0, 0, service, sizeof(service), niFlags);

         if (ok != 0) {
            ::std::string details;
            switch (ok) {
               case EAI_SYSTEM:
                  throw SystemError("getnameinfo failed");

               case EAI_AGAIN:
                  details = "AGAIN";
                  break;
               case EAI_BADFLAGS:
                  details = "BADFLAGS";
                  break;
               case EAI_FAIL:
                  details = "FAIL";
                  break;
               case EAI_FAMILY:
                  //                  details = "FAMILY";
                  break;
               case EAI_MEMORY:
                  details = "MEMORY";
                  break;
               case EAI_NONAME:
                  details = "NONAME";
                  break;
#ifdef EAI_OVERFLOW
               case EAI_OVERFLOW:
                  details = "OVERFLOW";
                  break;
#endif
            }
            if (!details.empty()) {
               ::canopy::printErrorMessage("Could not convert service to a numeric name: " + details);
            }
            return string();
         }

         return string(&service[0]);
      }

      NodeService Address::getNodeServiceID() const throws()
      {
         return NodeService { nodeID(), serviceID() };
      }

      NodeService Address::getNodeServiceName() const throws()
      {
         return NodeService { nodeName(), serviceName() };
      }

      bool Address::isMulticastAddress() const throws()
      {
         if (_domain == INET_4) {
            const sockaddr_in* saddr = reinterpret_cast< const sockaddr_in*>(sockaddr());
            // address are stored in host-byte order
            auto addr = htonl(saddr->sin_addr.s_addr);
            // address must be in the range 224.0.0.0 and 239.255.255.255
            // reserved: 224.0.0.0 - 224.0.0.255
            // global: 224.0.1.0 - 238.255.255.255
            // local: 239.0.0.0 - 239.255.255.255
            // return 0xE0000000 <= addr && addr <= 0xEFFFFFFF;
            return IN_MULTICAST(addr);
         }
         else if (_domain == INET_6) {
            const sockaddr_in6* saddr = reinterpret_cast< const sockaddr_in6*>(sockaddr());
            return IN6_IS_ADDR_MULTICAST(saddr->sin6_addr.s6_addr);
         }
         return false;
      }
   }
}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::Address& addr)
{
   ::std::string n = addr.nodeID();
   ::std::string s = addr.serviceID();

   if (n.empty()) {
      return out << "<unknown domain " << addr.domain().id() << ">";
   }
   out << n;
   if (!s.empty()) {
      out << ":" << s;
   }
   return out;
}
