#include <canopy/net/socket/PosixSocket.h>
#include <canopy/net/net.h>
#include <canopy/io/FileDescriptorImpl.h>
#include <canopy/io/io.h>
#include <iostream>
#include <cassert>

#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

using namespace ::std;

namespace canopy {
   namespace net {
      namespace socket {
         namespace {
            /**
             * Ensure that a file descriptor is closed on exec.
             * @param fd a file descriptor
             * @return true if the operation was sucessful, false otherwise
             */
            static bool setCloseOnExec(int fd) throws()
            {
               int flags = ::fcntl(fd, F_GETFD);
               if (flags == -1) {
                  return false;
               }
               flags |= FD_CLOEXEC;
               return ::fcntl(fd, F_SETFD, flags) == 0;
            }

         }

         PosixSocket::PosixSocket() throws ()
               : _sd(-1), _domain(UNIX), _type(STREAM)
         {
         }

         PosixSocket::PosixSocket(const SocketType& t, const Domain& d, const Protocol& p,
               SocketDescriptor sd) throws(SocketException)
               : _sd(sd), _domain(d), _type(t), _protocol(p)
         {
            if (!setCloseOnExec(sd)) {
               ::close(sd);
               throw SocketException(getSystemError("Could not set socket flag FD_CLOEXEC"));
            }
         }

         PosixSocket::PosixSocket(const SocketType& t, const Domain& d, const Protocol& p) throws(SocketException)
               : _domain(d), _type(t), _protocol(p)
         {
            int sd = ::socket(d.id(), t.id(), p.id());
            if (sd < 0) {
               ::std::ostringstream msg;
               msg << "Could not create socket: PosixSocket(" << t.id() << ", " << d.id() << ", " << p.id() << ")";
               printDebugMessage(msg.str());
               throw SocketException(getSystemError("Could not create socket"));
            }
            else {
#if 0
               ::std::ostringstream msg;
               msg << "Created PosixSocket(" << t.id() << ", " << d.id() << ", " << p.id() << ")";
               printDebugMessage(msg.str());
#endif
            }
            if (!setCloseOnExec(sd)) {
               ::close(sd);
               throw SocketException(getSystemError("Could not set socket flag FD_CLOEXEC"));
            }

            _sd = sd;

         }

         PosixSocket::~PosixSocket() throws()
         {
            try {
               close();
            }
            catch (const exception& e) {
               cerr << "Exception in Socket destructor " << e.what() << endl;
            }
         }

         ::canopy::io::IOEvents PosixSocket::block(const ::canopy::io::IOEvents& events,
               const Timeout& timeout) const throws (::canopy::io::IOException, InterruptedException)
         {
            ::canopy::io::FileDescriptorImpl fd(descriptor(), false);
            return fd.block(events, timeout);
         }

         void PosixSocket::close() throws(SocketException)
         {
            SocketDescriptor sd = descriptor();
            SocketDescriptor invalidSd(-1);
            if (sd != -1 && _sd.compare_exchange_strong(sd, invalidSd)) {
               // close the file descriptor
               if (::close(sd) == -1) {
                  // TODO: is this really what we need to do, to undo the change
                  if (_sd.compare_exchange_strong(invalidSd, sd)) {
                     throw SocketException(getSystemError("Could not close socket"));
                  }
               }
               // note: we could try to free the structure, but that would mean
               // that we no longer can do equality testing on the socket
            }
         }

         bool PosixSocket::isClosed() const throws()
         {
            if (descriptor() == -1) {
               return true;
            }
            // select on the socket
            ::pollfd fd = { descriptor(), POLLIN | POLLOUT | POLLPRI, 0 };
            Int32 res = ::poll(&fd, 1, 0);
            if (res == 1) {
               return (fd.revents & (POLLERR | POLLHUP | POLLNVAL)) != 0;
            }
            return res < 0;
         }

         void PosixSocket::shutdownWrite() throws(SocketException)
         {
            if (::shutdown(descriptor(), SHUT_WR) == -1) {
               throw SocketException(getSystemError("Could not shutdown for write"));
            }
         }

         void PosixSocket::shutdownRead() throws(SocketException)
         {
            if (::shutdown(descriptor(), SHUT_RD) == -1) {
               throw SocketException(getSystemError("Could not shutdown for read"));
            }
         }

         size_t PosixSocket::writeTo(const char* buffer, size_t bufSize, const Address& to,
               const Timeout& timeout) throws(SocketException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::WRITE, timeout);
               if (!events.test(::canopy::io::IOEvents::WRITE)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            ssize_t count = ::sendto(descriptor(), buffer, bufSize, flags, to.sockaddr(), to._addrlen);

            if (count == -1) {
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0;
                  case EINTR:
                     throw InterruptedException("PosixSocket::writeTo");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  case ECONNREFUSED:
                     // this one may be thrown if the socket is a udp socket
                     throw ConnectionRefused();
                  default:
                     throw SocketException(getSystemError("writeTo"));
               }
            }
            return count;
         }

         ssize_t PosixSocket::readFrom(char* buffer, size_t bufSize, Address& from,
               const Timeout& timeout) throws(SocketException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            // we cannot more bytes than we can indicate that we've read
            bufSize = min(static_cast< size_t>(0x7fffffff), bufSize);

            ::socklen_t len = sizeof(from._address);
            ssize_t count = ::recvfrom(descriptor(), buffer, bufSize, flags, from.sockaddr(), &len);

            if (count <= 0) {
               if (count == 0) {
                  return -1; // socket closed
               }
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0; // no data right now (non-blocking)
                  case EINTR:
                     throw InterruptedException("PosixSocket::readFrom");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  default:
                     throw SocketException(getSystemError("readFrom"));
               }
            }
            from._addrlen = len;
            from._domain = _domain;
            from._type = _type;
            from._protocol = _protocol;
            return count;
         }

         ssize_t PosixSocket::peek(char* buffer, size_t bufSize,
               const Timeout& timeout) const throws (::canopy::io::IOException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            flags |= MSG_PEEK; // we peek (this is posix)

            // we cannot more bytes than we can indicate that we've read
            bufSize = min(static_cast< size_t>(0x7fffffffU), bufSize);
            // instead of the read system call, we use recv, because
            // we can suppress signals
            ssize_t count = ::recv(descriptor(), buffer, bufSize, flags);
            if (count <= 0) {
               if (count == 0) {
                  return -1; // socket closed
               }
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0; // currently no data available
                  case EINTR:
                     throw InterruptedException("PosixSocket::read");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  default:
                     throw SocketException(SocketException::OTHER);
               }
            }
            return count;
         }

         ssize_t PosixSocket::read(char* buffer, size_t bufSize,
               const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            // we cannot more bytes than we can indicate that we've read
            bufSize = min(static_cast< size_t>(0x7fffffffU), bufSize);
            // instead of the read system call, we use recv, because
            // we can suppress signals
            ssize_t count = ::recv(descriptor(), buffer, bufSize, flags);
            if (count <= 0) {
               if (count == 0) {
                  return -1; // socket closed
               }
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0; // currently no data available
                  case EINTR:
                     throw InterruptedException("PosixSocket::read");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  default:
                     throw SocketException(SocketException::OTHER);
               }
            }
            return count;
         }

         size_t PosixSocket::write(const char* buffer, size_t bufSize,
               const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::WRITE, timeout);
               if (!events.test(::canopy::io::IOEvents::WRITE)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            // instead of the write system call, we use send, because
            // we can suppress signals
            ssize_t count = ::send(descriptor(), buffer, bufSize, flags);
            if (count == -1) {
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0;
                  case EINTR:
                     throw InterruptedException("PosixSocket::write");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  case ECONNREFUSED:
                     // this one may be thrown if the socket is a udp socket
                     throw ConnectionRefused();
                  default:
                     throw SocketException(SocketException::OTHER);
               }
            }
            return count;
         }

         SocketType PosixSocket::type() const throws ()
         {
            return _type;
         }

         Domain PosixSocket::domain() const throws ()
         {
            return _domain;
         }

         Protocol PosixSocket::protocol() const throws ()
         {
            return _protocol;
         }

         void PosixSocket::setBlockingEnabled(bool enabled) throws (SocketException)
         {
            Int32 flags = ::fcntl(descriptor(), F_GETFL);
            if (flags != -1) {
               if (enabled && (flags & O_NONBLOCK) != 0) {
                  if (::fcntl(descriptor(), F_SETFL, flags & (~O_NONBLOCK)) == 0) {
                     return;
                  }
               }
               else if (!enabled && (flags & O_NONBLOCK) == 0) {
                  if (::fcntl(descriptor(), F_SETFL, flags | O_NONBLOCK) == 0) {
                     return;
                  }
               }
               else {
                  return;
               }
            }
            throw SocketException(getSystemError("Socket option error: setBlockingEnabled"));
         }

         bool PosixSocket::isBlockingEnabled() const throws (SocketException)
         {
            Int32 flags = ::fcntl(descriptor(), F_GETFL);
            if (flags != -1) {
               return (flags & O_NONBLOCK) == 0;
            }
            throw SocketException(getSystemError("Socket option error: isBlockingEnabled"));
         }

         void PosixSocket::setSendBufferSize(UInt32 size) throws (SocketException)
         {
            Int32 res = ::setsockopt(descriptor(), SOL_SOCKET, SO_SNDBUF, &size, sizeof(size));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: setSendBufferSize"));
            }
         }

         UInt32 PosixSocket::getSendBufferSize() const throws (SocketException)
         {
            UInt32 size;
            ::socklen_t len = sizeof(size);
            Int32 res = ::getsockopt(descriptor(), SOL_SOCKET, SO_SNDBUF, &size, &len);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: getSendBufferSize"));
            }
            return size;
         }

         void PosixSocket::setReceiveBufferSize(UInt32 size) throws (SocketException)
         {
            Int32 res = ::setsockopt(descriptor(), SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: setReceiveBufferSize"));
            }
         }

         UInt32 PosixSocket::getReceiveBufferSize() const throws (SocketException)
         {
            UInt32 size;
            ::socklen_t len = sizeof(size);
            len = sizeof(size);
            Int32 res = ::getsockopt(descriptor(), SOL_SOCKET, SO_RCVBUF, &size, &len);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: getReceiveBufferSize"));
            }
            return size;
         }

         void PosixSocket::clearErrors() throws (SocketException)
         {
            Int32 error;
            ::socklen_t sz = sizeof(error);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_ERROR, &error, &sz);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: clearErrors"));
            }
         }

         bool PosixSocket::connect(const Address& addr) throws(SocketException)
         {

            int res = ::connect(descriptor(), addr.sockaddr(), addr._addrlen);
            if (res != 0) {
               switch (errno) {
                  case EISCONN:
                     throw SocketException(SocketException::ALREADY_CONNECTED);
                  case ECONNREFUSED:
                     throw ConnectionRefused();
                  case ECONNRESET:
                     throw SocketException(SocketException::CONNECTION_RESET);
                  case ETIMEDOUT:
                     throw SocketException(SocketException::CONNECTION_TIMED_OUT);
                  case ENETUNREACH:
                     throw SocketException(SocketException::NETWORK_UNREACHABLE);
                  case EINPROGRESS:
                     return false;
                  case EALREADY:
                     throw SocketException(SocketException::ALREADY_CONNECTED);
                  case EAFNOSUPPORT:
                     throw SocketException(SocketException::INVALID_ADDRESS);
                  case EACCES: // fall through
                  case EPERM:
                     throw SocketException(SocketException::DENIED);
                  default:
                     throw SocketException(getSystemError("Could not connect"));
               }
            }
            return true;
         }

         void PosixSocket::bind(const Address& addr) throws(SocketException)
         {
            int res = ::bind(descriptor(), addr.sockaddr(), addr._addrlen);

            // in an error occurred, then throw an exception
            if (res != 0) {
               switch (errno) {
                  case EINVAL:
                     throw SocketException(SocketException::ALREADY_BOUND);
                  case EACCES:
                     throw SocketException(SocketException::DENIED);
                  case EADDRINUSE:
                     throw SocketException(SocketException::ALREADY_USED);
                  default:
                     throw SocketException(getSystemError("Could not bind"));
               }
            }
         }

         bool PosixSocket::isServerSocket() const throws(SocketException)
         {
            // set the reuse address option
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_ACCEPTCONN, &enabled, &len);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_ACCEPTCONN"));
            }
            return enabled != 0;
         }

         void PosixSocket::setReuseAddressEnabled(bool enabled) throws(SocketException)
         {
            // set the reuse address option
            int enable = (int) enabled;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_REUSEADDR"));
            }
         }

         bool PosixSocket::isReuseAddressEnabled() const throws(SocketException)
         {
            // set the reuse address option
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_REUSEADDR, &enabled, &len);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_REUSEADDR"));
            }
            return enabled != 0;
         }

         void PosixSocket::setBroadcastEnabled(bool enabled) throws(SocketException)
         {
            // set the reuse address option
            int enable = (int) enabled;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_BROADCAST, &enable, sizeof(enable));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_BROADCAST"));
            }
         }

         bool PosixSocket::isBroadcastEnabled() const throws(SocketException)
         {
            // set the reuse address option
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_BROADCAST, &enabled, &len);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_BROADCAST"));
            }
            return enabled != 0;
         }

         void PosixSocket::setKeepAliveEnabled(bool enabled) throws(SocketException)
         {
            int enable = (int) enabled;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(enable));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_KEEPALIVE"));
            }
         }

         bool PosixSocket::isKeepAliveEnabled() const throws(SocketException)
         {
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_KEEPALIVE, &enabled, &len);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_KEEPALIVE"));
            }
            return enabled != 0;
         }

         void PosixSocket::enableLinger(UInt32 duration) throws(SocketException)
         {
            ::linger data;
            data.l_onoff = 1;
            data.l_linger = duration;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, sizeof(data));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_LINGER"));
            }
         }

         void PosixSocket::disableLinger() throws(SocketException)
         {
            ::linger data;
            data.l_onoff = 0;
            data.l_linger = 0;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, sizeof(data));
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_LINGER"));
            }
         }

         UInt32 PosixSocket::getLingerTime() const throws(SocketException)
         {
            ::socklen_t sz;
            ::linger data;
            data.l_onoff = 0;
            data.l_linger = 0;
            sz = sizeof(data);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, &sz);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_LINGER"));
            }
            return data.l_linger;
         }

         bool PosixSocket::isLingerEnabled() const throws(SocketException)
         {
            ::socklen_t sz;
            ::linger data;
            data.l_onoff = 0;
            data.l_linger = 0;
            sz = sizeof(data);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, &sz);
            if (res != 0) {
               throw SocketException(getSystemError("Socket option error: SO_LINGER"));
            }
            return data.l_onoff != 0;
         }

         Address PosixSocket::getBoundAddress() const throws(SocketException)
         {
            // got the peer name
            Address addr;
            ::socklen_t len = sizeof(addr._address);
            int res = ::getsockname(descriptor(), addr.sockaddr(), &len);
            // if an error occured, throw an IO exception
            if (res != 0) {
               throw SocketException(getSystemError("getBoundAddress failed"));
            }
            assert(len <= sizeof(addr._address));
            addr._domain = _domain;
            addr._type = _type;
            addr._protocol = _protocol;
            addr._addrlen = len;
            return addr;
         }

         Address PosixSocket::getPeerAddress() const throws(SocketException)
         {
            Address addr;
            ::socklen_t len = sizeof(addr._address);

            // got the peer name
            int res = ::getpeername(descriptor(), addr.sockaddr(), &len);
            // if an error occured, throw an IO exception
            if (res != 0) {
               throw SocketException(getSystemError("getPeerAddress failed"));
            }
            assert(len <= sizeof(addr._address));
            addr._domain = _domain;
            addr._type = _type;
            addr._protocol = _protocol;
            addr._addrlen = len;
            return addr;
         }

         void PosixSocket::listen(UInt32 backlog) throws (SocketException)
         {
            if (::listen(descriptor(), backlog) != 0) {
               throw SocketException(getSystemError("Cannot listen on socket"));
            }
         }

         ::std::unique_ptr< Socket> PosixSocket::accept(Address& addr) throws (SocketException)
         {
            // accept a connection
            socklen_t len = sizeof(sockaddr_storage);
            int sd = ::accept(descriptor(), addr.sockaddr(), &len);

            // if there is no error, then return
            // the socket
            if (sd != -1) {
               addr._domain = _domain;
               addr._type = _type;
               addr._protocol = _protocol;
               addr._addrlen = len;
               // the address has already been set
               return ::std::unique_ptr< Socket>(new PosixSocket(_type, _domain, _protocol, sd));
            }
            // this may create warning, if EAGAIN and EWOULDBLOCK are the same
            if ((errno == EAGAIN)
#if EAGAIN != EWOULDBLOCK
            || (errno == EWOULDBLOCK)
#endif
            ) {
               //FIXME: what to do; do'nt like this one at all
               //but throwing ResourceUnvailable is not too good either
               return ::std::unique_ptr< Socket>();
            }

            switch (errno) {
               case ECONNABORTED:
                  throw SocketException(SocketException::CONNECTION_ABORTED);
               case EOPNOTSUPP:
                  throw SocketException(SocketException::UNSUPPORTED_OPERATION);
               case EPERM:
                  throw SocketException(SocketException::DENIED);
               case ENOBUFS: // fall through
               case ENOMEM:
                  throw SocketException(SocketException::NO_RESOURCES);
               default:
                  throw SocketException(getSystemError("Could not accept connection"));
            }
         }

      }
   }
}
