#ifndef _CANOPY_NET_SOCKET_PROXY_H
#define _CANOPY_NET_SOCKET_PROXY_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_NET_SOCKET_SOCKET_H
#include <canopy/net/socket/Socket.h>
#endif

#include <memory>

namespace canopy {
   namespace net {
      namespace socket {

         /**
          * This is an implementation of a socket using the standard Ssl / BSD api.
          * This implementation supports UDP and TCP sockets and any sockets that
          * can be created via this class.
          */
         class Proxy : public ::canopy::net::socket::Socket
         {
               Proxy() = delete;
               CANOPY_BOILERPLATE_PREVENT_COPYING(Proxy);

               /**
                * Create a proxy for another socket.
                * @param socket a non-null socket
                * @throws ::std::invalid_argument if the socket is null
                */
            protected:
               Proxy(::std::unique_ptr< ::canopy::net::socket::Socket>  socket) throws (::std::invalid_argument);

               /**
                * Destroy this socket reference. If this is the last
                * reference to the socket, then it is destroyed.
                * If the socket cannot be destroyed properly, then an error
                * message is printed to cerr
                */
            public:
               ~Proxy() throws ();

               /**
                * Get the id for this socket.
                * @note the id is not constant for the lifetime of this socket.
                * Calling close() on the socket will change the id.
                * @return the unique id.
                */
            public:
               inline Int32 id() const throws ()
               {
                  // use the socket descirptor as an id
                  return descriptor();
               }

               /**
                * Test if this is an invalid socket.
                * @return true if this socket is <em>invalid</em>
                */
            public:
               bool invalid() const throws ()
               {
                  return descriptor() == -1;
               }

               /**
                * Get the socket type.
                * @return the type of this socket
                * @throw SocketException if the type could not be established
                */
            public:
               SocketType type() const throws ();

               /**
                * Get the socket domain.
                * @return the domain of this socket
                * @throw SocketException if the domain could not be established
                */
            public:
               Domain domain() const throws ();

               /**
                * Get the socket protcol.
                * @return the protocol used by this socket.
                * @throw SocketException if the domain could not be established
                */
            public:
               Protocol protocol() const throws ();

               /**
                * Close this socket.
                * @note this method will change the socket's id
                * @throws Exception if the socket could not be closed
                */
            public:
               void close() throws (::canopy::io::IOException);

               /**
                * Test if this socket has been closed. If this
                * method returns false, then this does not mean
                * that the next method invocation for this socket
                * will succeed.
                * @return true if this socket can no longer be used, false
                * if the socket currently not closed
                */
            public:
               bool isClosed() const throws ();

               /**
                * Shut this socket down for any writes.
                */
            public:
               void shutdownWrite() throws (SocketException);

               /**
                * Shut this socket down for any more reads.
                */
            public:
               void shutdownRead() throws (SocketException);

               /**
                * Block on the descriptor it is possible to use the descriptor for reading or writing.
                * @param events the event set
                * @param timeout a timeout or -1 for infinite wait
                * @return the events that are available for this   descriptor
                * @throws IOException on error
                * @throws InterruptedException if the call was interrupted
                */
            public:
               ::canopy::io::IOEvents block(const ::canopy::io::IOEvents& events,
                     const Timeout& timeout) const throws (::canopy::io::IOException, InterruptedException);

               /**
                * Peek at the buffer in the socket. This method works just like read, but does not
                * consume the data on the socket. This method is ideal for checking if a socket has been
                * closed in an orderly fashion, as this method will return 0 in that case.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t peek(char* buffer, size_t bufSize,
                     const Timeout& timeout) const throws (::canopy::io::IOException,InterruptedException);

               /**
                * Read a number of bytes from this socket. This method blocks
                * unless the socket is a non-blocking socket or there is already
                * data to be read. This method must return 0 to indicate that the socket
                * buffer is full.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t read(char* buffer, size_t bufSize,
                     const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException);

               /**
                * Write a number of bytes to this socket. This method blocks
                * until all bytes have been written. If this socket is in non-blocking mode
                * and no bytes can currently be written (because the buffer is full), then 0 is returned.
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
                * @exception SocketException if an error occurred during writing
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               size_t write(const char* buffer, size_t bufSize,
                     const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException);

               /**
                * @name UDP socket operations
                * @{
                */

               /**
                * Receive data on this socket. This method must be called
                * on an unconnected UDP socket. The address of the sender is
                * returned in the address.<p>
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param from the source address of the data
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t readFrom(char* buffer, size_t bufSize, Address& from,
                     const Timeout& timeout) throws (SocketException,InterruptedException);

               /**
                * Write a number of bytes via this socket to the specified destination.
                * This method must be called on an unconnected UDP socket.
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param to the destination address
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written or 0 if nothing could be written.
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               size_t writeTo(const char* buffer, size_t bufSize, const Address& to,
                     const Timeout& timeout) throws (SocketException,InterruptedException);

               /*@}*/

               /**
                * Determine if this socket is a server socket. A server socket is one
                * for which listen() has been invoked.
                * @return true if listen has been invoked
                * @exception SocketException if an error occurred
                */
            public:
               bool isServerSocket() const throws (SocketException);

               /**
                * Set the socket to block on I/O operations.
                * @param enabled if true, then I/O operations may block
                * @exception SocketException if an error occurred
                */
            public:
               void setBlockingEnabled(bool enabled) throws (SocketException);

               /**
                * Test if blocking I/O is enabled.
                * @return true if I/O calls may be block, false otherwise
                * @exception SocketException if an error occurred
                */
            public:
               bool isBlockingEnabled() const throws (SocketException);

               /**
                * Set the send buffer size
                * @param size the send buffer size
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setSendBufferSize(UInt32 size) throws (SocketException);

               /**
                * Get the receive buffer size.
                * @return size of the receive buffer
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               UInt32 getSendBufferSize() const throws (SocketException);

               /**
                * Set the receive buffer size.
                * @param size the receive buffer size
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setReceiveBufferSize(UInt32 size) throws (SocketException);

               /**
                * Get the receive buffer size.
                * @return size of the receive buffer
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               UInt32 getReceiveBufferSize() const throws (SocketException);

               /**
                * Clear any errors on this socket.
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void clearErrors() throws (SocketException);

               /**
                * Enable reuse of addresses if there are no connected sockets.
                * @param enabled true if the address can be bound again
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setReuseAddressEnabled(bool enabled) throws (SocketException);

               /**
                * Test if the address reuse option has been enabled
                * @return true if addresses can be reused when binding
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isReuseAddressEnabled() const throws (SocketException);

               /**
                * Enable broadcast for this socket. This option is only
                * available for UDP sockets.
                * @param enabled true if this socket should be able to broadcast
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setBroadcastEnabled(bool enabled) throws (SocketException);

               /**
                * Test if this socket is capable of broadcasting
                * @return true if this socket can broadcast
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isBroadcastEnabled() const throws (SocketException);

               /**
                * Enable  sending  of  keep-alive messages on
                * connection-oriented sockets.
                * @param enabled true if keep alive message are to be sent
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setKeepAliveEnabled(bool enabled) throws (SocketException);

               /**
                * Test if the keep alive option
                * is enabled.
                * @return true if the keep alive option is enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isKeepAliveEnabled() const throws (SocketException);

               /**
                * Enable lingering of this socket after closing it.
                * @param duration the time in seconds that the socket should remain
                * open after it has been closed
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void enableLinger(UInt32 duration) throws (SocketException);

               /**
                * Disable linger.
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void disableLinger() throws (SocketException);

               /**
                * Get the linger duration
                * @return the linger time or 0 if linger is not enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               UInt32 getLingerTime() const throws (SocketException);

               /**
                * Test if linger is enabled.
                * @return true if lingering for this socket is enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isLingerEnabled() const throws (SocketException);

               /*@}*/

               /**
                * @name BSD socket operations.
                * @{
                */

               /**
                * Put this socket in listening mode. Sockets of type
                * UDP do not support this method.
                * @param backlog the number connection requrest that will be
                * queued up before clients receive a "conncection refused"
                * @exception SocketException if this socket does support this method
                * or some other error occurred.
                */
            public:
               void listen(UInt32 backlog) throws (SocketException);

               /**
                * Accept a connection on this socket. The current
                * thread is suspended until a connection is made. Clients
                * are responsible for deleting the returned object.
                * Sockets of type UDP do not support this method.
                * @param peer an address which will contain the peer address upon completion.
                * @return a socket representing the connection to
                * a remote host, or nullptr if no connection was made
                * @throw SocketException if some error occurred or this method
                * is not supported by this socket.
                */
            public:
               ::std::unique_ptr< ::canopy::net::socket::Socket> accept(Address& peer) throws (SocketException);

               /**
                * Bind this socket to the specified end-point address. For
                * obvious reasons, the endpoint address must be describe the
                * current host node.
                * @param address an end-point address.
                * @exception SocketException if the socket could not be bound
                * to the address and port
                */
            public:
               void bind(const Address& address) throws (SocketException);

               /**
                * Connect this socket to an endpoint at the specified address. If this socket
                * is of type UDP, then no real connection is made, but all
                * datagrams are sent by default to the specified address.
                * @param address an address.
                * @return true if the socket is connected, false if a connection is in progress
                * @exception SocketException if the socket could not be connect
                * to the address and port
                * @exception ConnectionRefused if the connection failed
                */
            public:
               bool connect(const Address& address) throws (SocketException);

               /*@}*/

               /**
                * @name Enquiring about this socket.
                * @{
                */

               /**
                * Get the address to which this socket is bound.
                * @return the inet address to which this socket is bound
                * @throw SocketException if the socket is not bound
                */
            public:
               Address getBoundAddress() const throws (SocketException);

               /**
                * Get the address of the peer socket.
                * @return the inet address of socket to which this socket is connected
                * @throw SocketException if the socket is not connected
                */
            public:
               Address getPeerAddress() const throws (SocketException);

               /*@}*/

               /**
                * Get the descriptor
                * @return the descriptor
                */
            public:
               SocketDescriptor descriptor() const throws ()
               {
                  return _socket->descriptor();
               }

               /** The socket that provides the real implementation */
            private:
               ::std::unique_ptr< ::canopy::net::socket::Socket> _socket;
         };
      }
   }
}
#endif
