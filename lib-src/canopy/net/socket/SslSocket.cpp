#include <canopy/net/socket/SslSocket.h>
#include <canopy/net/socket/PosixSocket.h>
#include <canopy/net/net.h>
#include <canopy/Process.h>
#include <cassert>
#include <sstream>
#include <iostream>
#include <mutex>

// we're using openssl
#if CANOPY_HAVE_OPENSSL==1
#include <openssl/ssl.h>
#include <openssl/err.h>
#else
#include <ssl/ssl.h>
#include <ssl/err.h>
#endif

#if !defined(OPENSSL_THREADS)
#error "OpenSSL not compiled with thread-support"
#endif

using namespace ::std;

namespace canopy {
   namespace net {
      namespace socket {
         namespace {

            static ::std::string getAllErrors()
            {
               ::std::string result;
               BIO *mem = BIO_new(BIO_s_mem());
               ERR_print_errors(mem);
               char* text = nullptr;
               long len = BIO_get_mem_data(mem, &text);
               result.assign(text, len);
               BIO_free(mem);
               return result;
            }

            static bool processSslError(int code, const char* prefix, bool throwOnEOF = true)
            {
               if (code == SSL_ERROR_NONE) {
                  return true;
               }

               ::std::ostringstream sout(prefix);

               switch (code) {
                  case SSL_ERROR_WANT_ACCEPT:
                     sout << "SSL_ERROR_WANT_ACCEPT";
                     break;
                  case SSL_ERROR_ZERO_RETURN:
                     sout << "SSL_ERROR_ZERO_RETURN";
                     break;
                  case SSL_ERROR_WANT_WRITE:
                     sout << "SSL_ERROR_WANT_WRITE";
                     break;
                  case SSL_ERROR_WANT_READ:
                     sout << "SSL_ERROR_WANT_READ";
                     break;
                  case SSL_ERROR_WANT_X509_LOOKUP:
                     sout << "SSL_ERROR_WANT_X509_LOOKUP";
                     break;
                  case SSL_ERROR_WANT_CONNECT:
                     sout << "SSL_ERROR_WANT_CONNECT";
                     break;
                  case SSL_ERROR_SYSCALL:
                     // there's no underlying error, so just print a debug message
                     if (ERR_get_error() == 0) {
                        sout << "Unexpected EOF observed";
                        printDebugMessage(sout.str());
                        if (!throwOnEOF) {
                           return false;
                        }
                     }
                     sout << "SSL_ERROR_SYSCALL" << ": " << ::canopy::getSystemError(nullptr);
                     break;
                  case SSL_ERROR_SSL:
                     sout << "SSL_ERROR_SSL" << ": " << getAllErrors();
                     break;
                  default:
                     sout << "SSL_ERROR_SSL";
                     break;
               }
               ::std::string message(::std::move(sout.str()));
               ::canopy::printDebugMessage(message);
               throw SocketException(::std::move(message));
            }

            static ::std::string viewCertificate(X509* certificate, bool verbose = true)
            {
               ::std::string result;
               BIO *mem = BIO_new(BIO_s_mem());
               if (verbose) {
                  X509_NAME_print_ex(mem, X509_get_subject_name(certificate), 2, XN_FLAG_RFC2253);
                  PEM_write_bio_X509(mem, certificate);
               }
               else {
                  X509_NAME_print_ex(mem, X509_get_subject_name(certificate), 2, XN_FLAG_RFC2253);
               }

               char* text = nullptr;
               long len = BIO_get_mem_data(mem, &text);
               result.assign(text, len);
               BIO_free(mem);
               return result;
            }

            static ::std::string getDefaultCertificatePath()
            {

#if defined(CANOPY_SSL_CERTIFICATES_PATH)
               const char* defaultPath = CANOPY_TO_STRING(CANOPY_SSL_CERTIFICATES_PATH);
#else
               const char* defaultPath = nullptr;
#endif

               return Process::getEnv("CANOPY_SSL_CERTIFICATES_PATH", defaultPath);
            }

            class SslInit
            {
                  SslInit(const SslInit&) = delete;
                  SslInit&operator=(const SslInit&) = delete;

                  SslInit() throws(SocketException)
                  {
                     // validation of client certificate is optional for servers
                     _serverOptions.setPeerCertificateOptional(true);

                     // setup thread-safety here
                     initLocks();
                     try {
                        if (!SSL_library_init()) {
                           ::std::string msg("SslInit: failed to obtain index");
                           ::canopy::printErrorMessage(msg);
                           throw SocketException(msg);
                        }

                        SSL_load_error_strings();

                        socketDataIndex = SSL_get_ex_new_index(0,
                              const_cast< char*>("::canopy::net::socket::SslSocket"), nullptr, nullptr, nullptr);
                        if (socketDataIndex < 0) {
                           ::std::string msg("SslInit: failed to obtain index");
                           ::canopy::printErrorMessage(msg);
                           canopy::printErrorMessage(msg);
                        }
                     }
                     catch (...) {
                        destroyLocks();
                        throw;
                     }
                  }

                  ~SslInit()
                  {
                     destroyLocks();
                  }

                  static void initLocks()
                  {
                     _locks = new ::std::mutex[CRYPTO_num_locks()];
                     CRYPTO_set_locking_callback(ssl_locking_callback);
                  }

                  static void destroyLocks()
                  {
                     if (_locks != nullptr) {
                        delete[] _locks;
                        _locks = nullptr;
                     }
                  }

                  static void ssl_locking_callback(int mode, int lockid, const char* /*file*/, int /*lineno*/)
                  {
                     if (mode & CRYPTO_LOCK) {
                        _locks[lockid].lock();
                     }
                     else {
                        _locks[lockid].unlock();
                     }
                  }

               public:
                  static SslInit& sslInit() throws(SocketException)
                  {
                     // the way we use the SslInit class, it only one instance is created and
                     // it is used to setup the globals
                     static SslInit __ssl;
                     return __ssl;
                  }

                  /** The a mutex */
               public:
                  ::std::mutex _mutex;
                  SslSocket::Options _clientOptions;
                  SslSocket::Options _serverOptions;

               public:
                  static int socketDataIndex;

                  /** Use raw pointers to make access much fast */
               private:
                  static ::std::mutex* _locks;

            };
            ::std::mutex* SslInit::_locks = nullptr;
            int SslInit::socketDataIndex = -1;
         }

         SslSocket::Options::Options()
throws               ()
               : _maxLength(0), _certificateRepositoryDirectory(getDefaultCertificatePath()), _validationDisabled(
                     false), _peerCertificateOptional(false)
               {
               }

         SslSocket::Options::~Options() throws()
         {
         }

         void SslSocket::Options::setCertificateRepositoryPath(const ::std::string& directory) throws()
         {
            _certificateRepositoryDirectory = directory;
         }

         void SslSocket::Options::setCertificateRepositoryFile(const ::std::string& pem) throws()
         {
            _certificatesRepositoryFile = pem;
         }

         void SslSocket::Options::setOwnCertificateFile(const ::std::string& pemFile) throws()
         {
            _ownCertificateFile = pemFile;
         }

         void SslSocket::Options::setPeerCertificateOptional(bool optional) throws()
         {
            _peerCertificateOptional = optional;
         }

         void SslSocket::Options::setCertificateValidationDisabled(bool disabled) throws()
         {
            _validationDisabled = disabled;
         }

         void SslSocket::Options::setCertificateChainLength(int length) throws()
         {
            if (length <= 0) {
               _maxLength = 0;
            }
            else if (length > MAX_CERTIFICATE_CHAIN_LENGTH) {
               _maxLength = MAX_CERTIFICATE_CHAIN_LENGTH;
            }
            else {
               _maxLength = length;
            }
         }

         void SslSocket::Options::setKeyFile(const ::std::string& file) throws()
         {
            _keyFile = file;
         }

         void SslSocket::Options::setCertificateFile(const ::std::string& file) throws()
         {
            _certificateFile = file;
         }

         void SslSocket::Options::setCertificateVerifier(::std::function< bool()> cb) throws()
         {
            _certificateVerifier = cb;
         }

         struct SslSocket::SocketState
         {

               /** The default constructor */
               SocketState()
                     : _handle(nullptr), _context(nullptr), _peerCertificate(nullptr), _blockingEnabled(false), _connectionState(
                           ConnectionState::UNDEFINED)
               {
               }

               static int verifyCertificate(int preverify_ok, X509_STORE_CTX * ctx)
               {
                  SSL* ssl = static_cast< SSL*>(X509_STORE_CTX_get_ex_data(ctx, SSL_get_ex_data_X509_STORE_CTX_idx()));
                  SocketState* socket = static_cast< SocketState*>(SSL_get_ex_data(ssl, SslInit::socketDataIndex));
                  const Options& options = socket->_options;

                  int depth = X509_STORE_CTX_get_error_depth(ctx);

                  // check the certificate chain length
                  if (preverify_ok && options._maxLength > 0 && depth >= options._maxLength) {
                     X509_STORE_CTX_set_error(ctx, X509_V_ERR_CERT_CHAIN_TOO_LONG);
                     {
                        ::std::ostringstream sout;
                        sout << "Certificate chain too long : " << depth << ", max = " << options._maxLength;
                        ::canopy::printDebugMessage(sout.str());
                     }
                  }

                  // don't call the verification callback if the current certificate is ok
                  if (preverify_ok || (options._certificateVerifier && options._certificateVerifier())) {
                     {
                        ::std::ostringstream sout;
                        sout << "Certificate verified [" << depth << "] : "
                              << viewCertificate(X509_STORE_CTX_get_current_cert(ctx), false);
                        ::canopy::printDebugMessage(sout.str());
                     }
                     return 1;
                  }

                  {
                     ::std::ostringstream sout;
                     int err = X509_STORE_CTX_get_error(ctx);
                     sout << "Certificate error [" << depth << "] : "
                           << viewCertificate(X509_STORE_CTX_get_current_cert(ctx), false) << " : "
                           << X509_verify_cert_error_string(err) << " : " << (preverify_ok ? "OK" : "FAIL");
                     ::canopy::printDebugMessage(sout.str());
                  }

                  return 0;
               }


               /** The SSL handle for this connection */
               mutable SSL* _handle;

               /** The SSL context */
               mutable SSL_CTX* _context;

               /** A pointer to the peer certificate (if it was sent) */
               X509* _peerCertificate;

               /** The client/server options */
               Options _options;

               /** True if blocking is enabled on this socket */
               bool _blockingEnabled;

               /** The current connection state */
               ConnectionState _connectionState;
         };

         SslSocket::SslSocket(::std::unique_ptr< Socket>&& client, const Options& options)
throws               (SocketException) : Proxy(::std::move(client)), _state(new SocketState())
               {
                  if (Proxy::type() != STREAM) {
                     throw SocketException("SslSocket must be a stream socket");
                  }
                  _state->_blockingEnabled=Proxy::isBlockingEnabled();
                  _state->_options=options;
                  SslInit::sslInit();
               }

         SslSocket::SslSocket(const Options& options, const Domain& d, const Protocol& p)
throws               (SocketException)
               : SslSocket(::std::unique_ptr< Socket>(new PosixSocket(STREAM, d, p)), options)
               {
               }

         SslSocket::~SslSocket() throws()
         {
            teardownSSL(true);
         }

         SslSocket::Options SslSocket::getClientOptions() throws()
         {
            Options opts;
            SslInit& ssl = SslInit::sslInit();
            ssl._mutex.lock();
            opts = ssl._clientOptions;
            ssl._mutex.unlock();
            return opts;
         }

         void SslSocket::setClientOptions(const Options& opts) throws()
         {
            SslInit& ssl = SslInit::sslInit();
            ssl._mutex.lock();
            ssl._clientOptions = opts;
            ssl._mutex.unlock();
         }

         SslSocket::Options SslSocket::getServerOptions() throws()
         {
            Options opts;
            SslInit& ssl = SslInit::sslInit();
            ssl._mutex.lock();
            opts = ssl._serverOptions;
            ssl._mutex.unlock();
            return opts;
         }

         void SslSocket::setServerOptions(const Options& opts) throws()
         {
            SslInit& ssl = SslInit::sslInit();
            ssl._mutex.lock();
            ssl._serverOptions = opts;
            ssl._mutex.unlock();
         }

         void SslSocket::close() throws(SocketException)
         {
            teardownSSL(true);
         }

         void SslSocket::initializeSSL() throws (SocketException)
         {
            try {
               _state->_context = SSL_CTX_new(SSLv23_method());
               if (_state->_context == nullptr) {
                  //FIXME: get error
                  throw SocketException(::std::string("Failed to create SSL context: "));
               }
               int sslFlags = 0;
               if (_state->_options._validationDisabled) {
                  sslFlags |= SSL_VERIFY_NONE;
               }
               else {
                  sslFlags |= SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE;
                  // FIXME: not sure we want to do this, so disable for now
                  // sslFlags |=  SSL_VERIFY_CLIENT_ONCE;
               }
               if (!_state->_options._peerCertificateOptional) {
                  sslFlags |= SSL_VERIFY_FAIL_IF_NO_PEER_CERT;
               }

               if (_state->_options._validationDisabled) {
                  SSL_CTX_set_verify(_state->_context, sslFlags, nullptr);
               }
               else {
                  SSL_CTX_set_verify(_state->_context, sslFlags, SocketState::verifyCertificate);
               }

               {
                  const char* pemFile =
                        _state->_options._certificatesRepositoryFile.empty() ? nullptr :
                              _state->  _options._certificatesRepositoryFile.c_str();
                  const char* pemDir =
                        _state->  _options._certificateRepositoryDirectory.empty() ? nullptr :
                              _state->  _options._certificateRepositoryDirectory.c_str();
                  if (pemFile != nullptr || pemDir != nullptr) {
                     if (!SSL_CTX_load_verify_locations(_state->_context, pemFile, pemDir)) {
                        throw SocketException(::std::string("Invalid certificate repository specification"));
                     }
                  }
               }

               if (!_state->_options._ownCertificateFile.empty()) {
                  SSL_CTX_use_PrivateKey_file(_state->_context, _state->_options._ownCertificateFile.c_str(), SSL_FILETYPE_PEM);
                  SSL_CTX_use_certificate_file(_state->_context,_state->_options._ownCertificateFile.c_str(), SSL_FILETYPE_PEM);
               }

               if (!_state->_options._keyFile.empty()) {
                  SSL_CTX_use_PrivateKey_file(_state->_context,_state-> _options._keyFile.c_str(), SSL_FILETYPE_PEM);
               }
               if (!_state->_options._certificateFile.empty()) {
                  SSL_CTX_use_certificate_file(_state->_context, _state->_options._certificateFile.c_str(), SSL_FILETYPE_PEM);
               }

               _state->_handle = SSL_new(_state->_context);
               SSL_check_private_key (_state->_handle);
               if (_state->_handle == nullptr) {
                  throw SocketException(::std::string("Failed to create SSL handle: "));
               }
               // this should properly deal with overflow
               if (_state->_options._maxLength > 0) {
                  SSL_set_verify_depth(_state->_handle, _state->_options._maxLength + 1);
               }

               // can we do this before connecting?
               if (!SSL_set_fd(_state->_handle, Proxy::descriptor())) {
                  throw SocketException("Failed to associate SSL with socket :");
               }

               SSL_set_ex_data(_state->_handle, SslInit::socketDataIndex, _state.get());
            }
            catch (const SocketException& e) {
               teardownSSL(true);
               throw;
            }
         }
         void SslSocket::setBlockingEnabled(bool block) throws (SocketException)
         {
            _state->_blockingEnabled = block;
            Proxy::setBlockingEnabled (_state->_blockingEnabled);
         }

         bool SslSocket::isBlockingEnabled() const throws(SocketException)
         {
            return _state->_blockingEnabled;
         }

         void SslSocket::setBlockingState(bool block) const throws (SocketException)
         {
            SslSocket* THIS = const_cast< SslSocket*>(this);
            THIS->Proxy::setBlockingEnabled(block && _state->_blockingEnabled);
         }

         void SslSocket::teardownSSL(bool closeSocket) throws()
         {
            _state->_connectionState = ConnectionState::UNDEFINED;
            if (_state->_peerCertificate) {
               X509_free (_state->_peerCertificate);
               _state-> _peerCertificate = nullptr;
            }
            if (_state->_handle) {
               SSL_shutdown (_state->_handle);
               SSL_free(_state->_handle);
               _state->      _handle = nullptr;
            }
            if (_state->_context) {
               SSL_CTX_free (_state->_context);
               _state->    _context = nullptr;
            }
            if (closeSocket) {
               try {
                  // careful: don't invoke this->close()
                  Proxy::close();
               }
               catch (const SocketException& e) {
                  ::canopy::printDebugMessage("Failed to close socket");
               }
            }
         }

         bool SslSocket::verifyPeerCertificate() throws()
         {
            bool result;

            _state->_peerCertificate = SSL_get_peer_certificate(_state->_handle);

            if (_state->_options._validationDisabled) {
               result = true;
            }
            else if (_state->_peerCertificate == nullptr && _state->_options._peerCertificateOptional) {
               result = true;
            }
            else if (_state->_peerCertificate != nullptr && SSL_get_verify_result(_state->_handle) == X509_V_OK) {
               result = true;
            }
            else {
               result = false;
            }

            {
               ::std::ostringstream sout;
               sout << "verifyPeerCertificate : [" << (_state->_peerCertificate != nullptr ? "Y" : "N") << "] "
                     << X509_verify_cert_error_string(SSL_get_verify_result(_state->_handle));
               ::canopy::printDebugMessage(sout.str());
            }

            return result;
         }

         bool SslSocket::ensureConnected() throws(SocketException)
         {
            try {
               int result;
               switch (_state->_connectionState) {
                  case ConnectionState::CONNECTED:
                     return true;
                  case ConnectionState::ACCEPT:
                     result = ::SSL_accept(_state->_handle);
                     break;
                  case ConnectionState::CONNECT:
                     result = ::SSL_connect(_state->_handle);
                     break;
                  case ConnectionState::UNDEFINED:
                  default:
                     throw SocketException("SslSocket not connected");
               }
               int code = ::SSL_get_error(_state->_handle, result);
               switch (code) {
                  case SSL_ERROR_NONE:
                     _state->_connectionState = ConnectionState::CONNECTED;

                     if (!verifyPeerCertificate()) {
                        ::canopy::printDebugMessage("SslSocket:invalid certificate");
                        throw SocketException("Invalid certificate");
                     }
                     return true;
                  case SSL_ERROR_WANT_READ:
                  case SSL_ERROR_WANT_WRITE:
                     // this means that the protocol between client and server is
                     // still going on
                     return false;
                  case SSL_ERROR_WANT_ACCEPT:
                     // this one only is returned if the current state is also ACCEPT
                     assert(_state->_connectionState == ConnectionState::ACCEPT);
                     ::canopy::printDebugMessage("SslSocket: accept has not yet completed");
                     return false;
                  case SSL_ERROR_WANT_CONNECT:
                     assert(_state->_connectionState == ConnectionState::CONNECT);
                     ::canopy::printDebugMessage("SslSocket: connect has not yet completed");
                     return false;
                  default:
                     _state->   _connectionState = ConnectionState::UNDEFINED;
                     processSslError(code, "SslSocket::ensureConnected");
                     return false;
               }
            }
            catch (...) {
               teardownSSL(true);
               throw;
            }
         }

         ::std::unique_ptr< Socket> SslSocket::accept(Address& addr) throws (SocketException)
         {
            ::std::unique_ptr< Socket> client = Proxy::accept(addr);
            if (!client) {
               return client;
            }

            try {
               ::std::unique_ptr< SslSocket> ssl(new SslSocket(::std::move(client), _state->_options));
               ssl->initializeSSL();
               ssl->_state->_connectionState = ConnectionState::ACCEPT;
               ssl->ensureConnected();
               return ::std::move(ssl);
            }
            catch (const SocketException& e) {
               ::std::cerr << "Socket exception on SslSocket::ACCEPT" << ::std::endl;
               teardownSSL();
               throw;
            }
         }

         bool SslSocket::connect(const Address& addr) throws(SocketException)
         {
            try {
               bool connected = Proxy::connect(addr);
               initializeSSL();
               _state->_connectionState = ConnectionState::CONNECT;
               if (!connected) {
                  return false;
               }
               ensureConnected();
               return true;
            }
            catch (const SocketException&) {
               teardownSSL();
               throw;
            }
         }

         size_t SslSocket::writeTo(const char*, size_t, const Address&, const Timeout&) throws(SocketException,InterruptedException)
         {
            throw SocketException("SslSocket::writeTo : Unsupported operation");
         }

         ssize_t SslSocket::readFrom(char*, size_t, Address&, const Timeout&) throws(SocketException,InterruptedException)
         {
            throw SocketException("SslSocket::readFrom : Unsupported operation");
         }

         ssize_t SslSocket::peek(char* buffer, size_t bufSize, const Timeout& timeout) const throws (SocketException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }
            setBlockingState(::canopy::io::isBlockingTimeout(timeout));
            // if we're not connected, then the socket must be non-blocking
            if (!const_cast< SslSocket*>(this)->ensureConnected()) {
               return 0;
            }

            const int result = ::SSL_peek(_state->_handle, buffer, bufSize);
            const int code = ::SSL_get_error(_state->_handle, result);
            switch (code) {
               case SSL_ERROR_NONE:
                  return result;
               case SSL_ERROR_ZERO_RETURN:
                  return -1;
               case SSL_ERROR_WANT_READ:
                  return 0;
               default:
                  processSslError(code, "SslSocket::peek", false);
                  return -1;
            }
         }

         ssize_t SslSocket::read(char* buffer, size_t bufSize, const Timeout& timeout) throws (SocketException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }
            setBlockingState(::canopy::io::isBlockingTimeout(timeout));
            // if we're not connected, then the socket must be non-blocking
            if (!ensureConnected()) {
               return 0;
            }

            const int result = ::SSL_read(_state->_handle, buffer, bufSize);
            const int code = ::SSL_get_error(_state->_handle, result);

            switch (code) {
               case SSL_ERROR_NONE:
                  return result;
               case SSL_ERROR_ZERO_RETURN:
                  return -1;
               case SSL_ERROR_WANT_READ:
                  return 0;
               default:
                  processSslError(code, "SslSocket::read", false);
                  return -1;
            }
         }

         size_t SslSocket::write(const char* buffer, size_t bufSize, const Timeout& timeout) throws (SocketException,InterruptedException)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }
            setBlockingState(::canopy::io::isBlockingTimeout(timeout));
            // if we're not connected, then the socket must be non-blocking
            if (!ensureConnected()) {
               return 0;
            }

            const int result = ::SSL_write(_state->_handle, buffer, bufSize);
            const int code = ::SSL_get_error(_state->_handle, result);
            switch (SSL_get_error(_state->_handle, result)) {
               case SSL_ERROR_NONE:
                  return result;
               case SSL_ERROR_ZERO_RETURN:
                  return -1;
               case SSL_ERROR_WANT_WRITE:
                  return 0;
               default:
                  processSslError(code, "SslSocket::write", false);
                  return -1;
            }
         }

      }
   }
}
