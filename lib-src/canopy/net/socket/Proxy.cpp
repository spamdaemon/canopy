#include <canopy/net/socket/Proxy.h>
#include <canopy/net/socket/PosixSocket.h>
#include <canopy/net/net.h>
#include <iostream>
#include <cassert>

using namespace ::std;

namespace canopy {
   namespace net {
      namespace socket {

         Proxy::Proxy(::std::unique_ptr< ::canopy::net::socket::Socket> socket)  throws (::std::invalid_argument)
               : _socket(::std::move(socket))
         {
            if(!_socket) {
               throw ::std::invalid_argument("Null pointer");
            }
         }

         Proxy::~Proxy() throws()
         {
         }

         void Proxy::close() throws(::canopy::io::IOException)
         {
            _socket->close();
         }

         bool Proxy::isClosed() const throws()
         {
            return _socket->isClosed();
         }
         ::canopy::io::IOEvents Proxy::block(const ::canopy::io::IOEvents& events,
               const Timeout& timeout) const throws (::canopy::io::IOException, InterruptedException)
         {
            return _socket->block(events, timeout);
         }

         void Proxy::shutdownWrite() throws(SocketException)
         {
            _socket->shutdownWrite();
         }

         void Proxy::shutdownRead() throws(SocketException)
         {
            _socket->shutdownRead();
         }

         size_t Proxy::writeTo(const char* buffer, size_t bufSize, const Address& to,
               const Timeout& timeout) throws(SocketException,InterruptedException)
         {
            return _socket->writeTo(buffer, bufSize, to, timeout);
         }

         ssize_t Proxy::readFrom(char* buffer, size_t bufSize, Address& from,
               const Timeout& timeout) throws(SocketException,InterruptedException)
         {
            return _socket->readFrom(buffer, bufSize, from, timeout);
         }

         ssize_t Proxy::peek(char* buffer, size_t bufSize,
               const Timeout& timeout) const throws (::canopy::io::IOException,InterruptedException)
         {
            return _socket->peek(buffer, bufSize, timeout);
         }

         ssize_t Proxy::read(char* buffer, size_t bufSize,
               const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
         {
            return _socket->read(buffer, bufSize, timeout);
         }

         size_t Proxy::write(const char* buffer, size_t bufSize,
               const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
         {
            return _socket->write(buffer, bufSize, timeout);
         }

         SocketType Proxy::type() const throws ()
         {
            return _socket->type();
         }

         Domain Proxy::domain() const throws ()
         {
            return _socket->domain();
         }

         Protocol Proxy::protocol() const throws ()
         {
            return _socket->protocol();
         }

         void Proxy::setBlockingEnabled(bool enabled) throws (SocketException)
         {
            _socket->setBlockingEnabled(enabled);
         }

         bool Proxy::isBlockingEnabled() const throws (SocketException)
         {
            return _socket->isBlockingEnabled();
         }

         void Proxy::setSendBufferSize(UInt32 size) throws (SocketException)
         {
            _socket->setSendBufferSize(size);
         }

         UInt32 Proxy::getSendBufferSize() const throws (SocketException)
         {
            return _socket->getSendBufferSize();
         }

         void Proxy::setReceiveBufferSize(UInt32 size) throws (SocketException)
         {
            _socket->setReceiveBufferSize(size);
         }

         UInt32 Proxy::getReceiveBufferSize() const throws (SocketException)
         {
            return _socket->getReceiveBufferSize();
         }

         void Proxy::clearErrors() throws (SocketException)
         {
            _socket->clearErrors();
         }

         bool Proxy::connect(const Address& addr) throws(SocketException)
         {
            return _socket->connect(addr);
         }

         void Proxy::bind(const Address& addr) throws(SocketException)
         {
            _socket->bind(addr);
         }

         bool Proxy::isServerSocket() const throws(SocketException)
         {
            return _socket->isServerSocket();
         }

         void Proxy::setReuseAddressEnabled(bool enabled) throws(SocketException)
         {
            _socket->setReuseAddressEnabled(enabled);
         }

         bool Proxy::isReuseAddressEnabled() const throws(SocketException)
         {
            return _socket->isReuseAddressEnabled();
         }

         void Proxy::setBroadcastEnabled(bool enabled) throws(SocketException)
         {
            _socket->setBroadcastEnabled(enabled);
         }

         bool Proxy::isBroadcastEnabled() const throws(SocketException)
         {
            return _socket->isBroadcastEnabled();
         }

         void Proxy::setKeepAliveEnabled(bool enabled) throws(SocketException)
         {
            _socket->setKeepAliveEnabled(enabled);
         }

         bool Proxy::isKeepAliveEnabled() const throws(SocketException)
         {
            return _socket->isKeepAliveEnabled();
         }

         void Proxy::enableLinger(UInt32 duration) throws(SocketException)
         {
            _socket->enableLinger(duration);
         }

         void Proxy::disableLinger() throws(SocketException)
         {
            _socket->disableLinger();
         }

         UInt32 Proxy::getLingerTime() const throws(SocketException)
         {
            return _socket->getLingerTime();
         }

         bool Proxy::isLingerEnabled() const throws(SocketException)
         {
            return _socket->isLingerEnabled();
         }

         Address Proxy::getBoundAddress() const throws(SocketException)
         {
            return _socket->getBoundAddress();
         }

         Address Proxy::getPeerAddress() const throws(SocketException)
         {
            return _socket->getPeerAddress();
         }

         void Proxy::listen(UInt32 backlog) throws (SocketException)
         {
            _socket->listen(backlog);
         }

         ::std::unique_ptr< Socket> Proxy::accept(Address& addr) throws (SocketException)
         {
            return _socket->accept(addr);
         }

      }
   }
}
