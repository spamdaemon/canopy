#ifndef _CANOPY_NET_SOCKET_SSLSOCKET_H
#define _CANOPY_NET_SOCKET_SSLSOCKET_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_NET_SOCKET_PROXY_H
#include <canopy/net/socket/Proxy.h>
#endif

#include <memory>
#include <functional>

namespace canopy {
   namespace net {
      namespace socket {

         /**
          * This is an implementation of a socket using the standard Ssl / BSD api.
          * This implementation supports UDP and TCP sockets and any sockets that
          * can be created via this class.
          */
         class SslSocket : public Proxy
         {
               SslSocket() = delete;
               CANOPY_BOILERPLATE_PREVENT_COPYING(SslSocket);


               /** Forward declare information containing SSL data structures */
               struct SocketState;

               /** The asynchronous socket state.
                */
            private:
               enum class ConnectionState
               {

                  /** The socket is already connected */
                  CONNECTED = 0,
                  /** A connect call must be performed */
                  CONNECT = 1,

                  /** An accept call must be performed */
                  ACCEPT = 2,

                  /** The undefined state */
                  UNDEFINED = 4
               };

               /**
                * An options object.
                */
            public:
               class Options
               {
                     friend class SslSocket;

                     /** The maximum certificate chain length */
                  public:
                     static const int MAX_CERTIFICATE_CHAIN_LENGTH = 5;

                     /**
                      * The default certificate path for the option is defined
                      * by the environment variable <em>CANOPY_SSL_CERTIFICATES_PATH</em>
                      * or the compiler defined variable <em>CANOPY_SSL_CERTIFICATES_PATH</em>.
                      */
                  public:
                     Options() throws();

                     /**
                      * Destructor
                      */
                  public:
                     ~Options() throws();

                     /**
                      * Disable all certificate validation. This parameter takes precendence over any other
                      * parameters.
                      * @param disable true to disable validation, false to enable
                      */
                  public:
                     void setCertificateValidationDisabled(bool disable) throws();

                     /**
                      * Set the maximum size of a certificate chain. The maximum size of the certificate
                      * chain is MAX_CERTIFICATE_CHAIN_LENGTH.
                      * @param length the maximum length of a certificate chain or <=0 for a default
                      */
                  public:
                     void setCertificateChainLength(int length) throws();

                     /**
                      * Set the key file.
                      * @param file the key file
                      */
                  public:
                     void setKeyFile(const ::std::string& file) throws();

                     /**
                      * Set the certificate file.
                      * @param file the certificate file
                      */
                  public:
                     void setCertificateFile(const ::std::string& file) throws();

                     /**
                      * Callback how to deal with a certificate that isn't valid. This callback doesn't do
                      * much yet.
                      * FIXME: implement this properly
                      * @param cb the callback
                      */
                  private:
                     void setCertificateVerifier(::std::function< bool()> cb) throws();

                     /**
                      * Indicate that a peer certificate must be provided for the connection
                      * to be established.
                      * @param optional true if the peer certificate is optional, false if it is required.
                      */
                  public:
                     void setPeerCertificateOptional(bool optional) throws();

                     /**
                      * Set the certificates repository path. This path is a folder that contains
                      * the a list of PEM files that provide the necessary certificates to verify
                      * peer certificates.
                      * @param pemDirectory a directory containing PEM files
                      */
                  public:
                     void setCertificateRepositoryPath(const ::std::string& pemDirectory) throws();

                     /**
                      * Set the certificates repository file. This file contains multiple different
                      * certificates in PEM format and these certificates are used to verify
                      * peer certificates.
                      * @param pemFile a PEM file
                      */
                  public:
                     void setCertificateRepositoryFile(const ::std::string& pemFile) throws();

                     /**
                      * Set the server or client certificate. This is the certificate that will be
                      * presented to peer.
                      * @param pemFile the own certificate
                      */
                  public:
                     void setOwnCertificateFile(const ::std::string& pemFile) throws();

                     /** The maximum length */
                  private:
                     int _maxLength;

                     /** The verification callback */
                  private:
                     ::std::function< bool()> _certificateVerifier;

                     /**
                      * The certificate search path.
                      */
                  private:
                     ::std::string _certificateRepositoryDirectory;

                     /**
                      * A PEM file containing certificates.
                      */
                  private:
                     ::std::string _certificatesRepositoryFile;

                     /** The own certificate */
                  private:
                     ::std::string _ownCertificateFile;

                     /** The key file */
                  private:
                     ::std::string _keyFile;

                     /** The certificate file */
                  private:
                     ::std::string _certificateFile;

                     /** True if certificate validation is disabled */
                  private:
                     bool _validationDisabled;

                     /** True if a peer certificate is required */
                  public:
                     bool _peerCertificateOptional;
               };

               /**
                * Constructor with a socket
                * @param socket a socket
                * @param options ssl options
                * @throws SocketException if the socket could not be set up
                */
            private:
               SslSocket(::std::unique_ptr< Socket>&& socket, const Options& options =
                     Options()) throws(SocketException);

               /**
                * Open a new STREAM socket utilizing SSL.
                * @param options ssl options
                * @param d the socket domain
                * @param p the protocol
                * @throws a socket exception if the socket could not be opened
                */
            public:
               SslSocket(const Options& options = Options(), const Domain& d = INET_4, const Protocol& p =
                     Protocol()) throws(SocketException);

               /**
                * Destructor.
                */
            public:
               ~SslSocket() throws();

               /**
                * Get the default client options. These are options used only for a client connection.
                * @return the options for a client.
                */
            public:
               static Options getClientOptions() throws();

               /**
                * Get the default client options. These are options used only for a client connection.
                * @param opts the options to return for a getClientOptions() call.
                * @return the options for a client.
                */
            public:
               static void setClientOptions(const Options& opts) throws();

               /**
                * Get the default server options. These are options used only for a server connection.
                * @return the options for a client.
                */
            public:
               static Options getServerOptions() throws();

               /**
                * Get the default server options. These are options used only for a server connection.
                * @param opts the options to return for a getServerOptions() call.
                * @return the options for a client.
                */
            public:
               static void setServerOptions(const Options& opts) throws();

               /**
                * Close this socket.
                * @note this method will change the socket's id
                * @throws Exception if the socket could not be closed
                */
            public:
               void close() throws(SocketException);

               /**
                * Peek at the buffer in the socket. This method works just like read, but does not
                * consume the data on the socket. This method is ideal for checking if a socket has been
                * closed in an orderly fashion, as this method will return 0 in that case.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t peek(char* buffer, size_t bufSize,
                     const Timeout& timeout) const throws(SocketException, InterruptedException);

               /**
                * Read a number of bytes from this socket. This method blocks
                * unless the socket is a non-blocking socket or there is already
                * data to be read. This method must return 0 to indicate that the socket
                * buffer is full.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t read(char* buffer, size_t bufSize,
                     const Timeout& timeout) throws(SocketException, InterruptedException);

               /**
                * Write a number of bytes to this socket. This method blocks
                * until all bytes have been written. If this socket is in non-blocking mode
                * and no bytes can currently be written (because the buffer is full), then 0 is returned.
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
                * @exception SocketException if an error occurred during writing
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               size_t write(const char* buffer, size_t bufSize,
                     const Timeout& timeout) throws(SocketException, InterruptedException);

               /**
                * @name UDP socket operations
                * @{
                */

               /**
                * Receive data on this socket. This method must be called
                * on an unconnected UDP socket. The address of the sender is
                * returned in the address.<p>
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param from the source address of the data
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t readFrom(char* buffer, size_t bufSize, Address& from,
                     const Timeout& timeout) throws(SocketException, InterruptedException);

               /**
                * Write a number of bytes via this socket to the specified destination.
                * This method must be called on an unconnected UDP socket.
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param to the destination address
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written or 0 if nothing could be written.
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               size_t writeTo(const char* buffer, size_t bufSize, const Address& to,
                     const Timeout& timeout) throws(SocketException, InterruptedException);

               /*@}*/

               /**
                * @name BSD socket operations.
                * @{
                */

               /**
                * Accept a connection on this socket. The current
                * thread is suspended until a connection is made. Clients
                * are responsible for deleting the returned object.
                * Sockets of type UDP do not support this method.
                * @param peer an address which will contain the peer address upon completion.
                * @return a socket representing the connection to
                * a remote host, or nullptr if no connection was made
                * @throw SocketException if some error occurred or this method
                * is not supported by this socket.
                */
            public:
               ::std::unique_ptr< Socket> accept(Address& peer) throws(SocketException);

               /**
                * Connect this socket to an endpoint at the specified address. If this socket
                * is of type UDP, then no real connection is made, but all
                * datagrams are sent by default to the specified address.
                * @param address an address.
                * @exception SocketException if the socket could not be connect
                * to the address and port
                * @exception ConnectionRefused if the connection failed
                */
            public:
               bool connect(const Address& address) throws(SocketException);

               /*@}*/

               /**
                * @name Enquiring about this socket.
                * @{
                */

               /*@}*/

               /**
                * Set the socket to block on I/O operations.
                * @param enabled if true, then I/O operations may block
                * @exception SocketException if an error occurred
                */
            public:
               void setBlockingEnabled(bool enabled) throws(SocketException);

               /**
                * Test if blocking I/O is enabled.
                * @return true if I/O calls may be block, false otherwise
                * @exception SocketException if an error occurred
                */
            public:
               bool isBlockingEnabled() const throws(SocketException);

               /**
                * Set the socket's blocking state
                * @param block the blocking state
                */
            private:
               void setBlockingState(bool block) const throws(SocketException);

                /**
                * Connect this socket to the SSL framework.
                */
            private:
               void initializeSSL() throws(SocketException);

               /**
                * Perform a disconnect
                * @param closeSocket true to close the socket as well
                */
            private:
               void teardownSSL(bool closeSocket = false) throws();

               /**
                * Verify the peer in the ssl connnection.
                */
            private:
               bool verifyPeerCertificate() throws();

               /**
                * Ensure connected.
                * @return true if the socket is now connected, false otherwise
                */
            private:
               bool ensureConnected() throws(SocketException);

               /** The SSL state */
            private:
               ::std::unique_ptr<SocketState> _state;
         }
         ;
      }
   }
}
#endif
