#include <canopy/net/socket/MulticastSocket.h>
#include <canopy/net/socket/PosixSocket.h>
#include <memory>

#include <netinet/in.h>

namespace canopy {
   namespace net {
      namespace socket {

         MulticastSocket::Version MulticastSocket::version(Domain d) throws (SocketException)
         {
            if (d == INET_6) {
               return Version::V6;
            }
            else if (d == INET_4) {
               return Version::V4;
            }
            else {
               throw SocketException("Socket must be IPv4 or IPv6 socket");
            }

         }

         MulticastSocket::MulticastSocket(::std::unique_ptr< Socket>&& client) throws (SocketException)
               : Proxy(::std::move(client))
         {
            if (Proxy::type() != DATAGRAM || Proxy::type() == RAW) {
               throw SocketException("MulticastSocket must be a datagram or raw socket");
            }
            _version = version(Proxy::domain());
         }

         MulticastSocket::MulticastSocket(const Domain& d, const Protocol& p) throws(SocketException)
               : MulticastSocket(::std::unique_ptr< Socket>(new PosixSocket(DATAGRAM, d, p)))
         {
            setMulticastLoopEnabled(true);
         }

         MulticastSocket::~MulticastSocket()
         throws()
         {
         }

         ::std::unique_ptr< MulticastSocket> MulticastSocket::createConnectedSocket(
               const Address& addr) throws (SocketException)
         {
            Endpoint ep = Endpoint::createAnyEndpoint(addr.serviceName().c_str(), addr.type(), addr.domain(),
                  addr.protocol());
            ::std::unique_ptr< MulticastSocket> s(new MulticastSocket(addr.domain(), addr.protocol()));

            s->setReuseAddressEnabled(true);
            s->bind(ep.address());
            s->connect(addr);
            return ::std::move(s);
         }

         ::std::unique_ptr< MulticastSocket> MulticastSocket::createConnectedSocket(
               const Endpoint& group) throws (SocketException)
         {

            for (const Address& addr : group.filter(DATAGRAM)) {
               try {
                  return createConnectedSocket(addr);
               }
               catch (const SocketException&) {
               }
            }
            throw SocketException("No usable address found");
         }

         ::std::unique_ptr< MulticastSocket> MulticastSocket::joinGroup(const Address& addr) throws (SocketException)
         {
            if (!addr.isMulticastAddress()) {
               throw SocketException("Not a multicast address");
            }
            ::std::unique_ptr< MulticastSocket> s(new MulticastSocket(addr.domain(), addr.protocol()));
            s->setReuseAddressEnabled(true);
            s->bind(addr);
            s->join(addr);
            return ::std::move(s);
         }

         ::std::unique_ptr< MulticastSocket> MulticastSocket::joinGroup(const Endpoint& group) throws (SocketException)
         {
            for (const Address& addr : group.filter(DATAGRAM)) {
               try {
                  if (addr.isMulticastAddress()) {
                     return joinGroup(addr);
                  }
               }
               catch (const SocketException&) {
               }
            }
            throw SocketException("No usable multicast address found");
         }

         void MulticastSocket::join(const Address& address) throws (SocketException)
         {
            if (!address.isMulticastAddress()) {
               throw SocketException("Not a multicast address");
            }
            int status = 1;
            switch (version(address.domain())) {
               case Version::V4: {
                  const sockaddr_in* in_addr = reinterpret_cast< const sockaddr_in*>(address.sockaddr());
                  struct ip_mreq mreq = { in_addr->sin_addr, INADDR_ANY };
                  status = ::setsockopt(descriptor(), IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
                  break;
               }
               case Version::V6: {
                  const sockaddr_in6* in6_addr = reinterpret_cast< const sockaddr_in6*>(address.sockaddr());
                  struct ipv6_mreq mreq = { in6_addr->sin6_addr, 0 };
                  status = ::setsockopt(descriptor(), IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
                  break;
               }
               default:
                  break;
            };

            if (status != 0) {
               throw SocketException("Failed to join");
            }
         }

         void MulticastSocket::leave(const Address& address) throws (SocketException)
         {
            if (!address.isMulticastAddress()) {
               throw SocketException("Not a multicast address");
            }
            int status = 1;
            switch (version(address.domain())) {
               case Version::V4: {
                  const sockaddr_in* in_addr = reinterpret_cast< const sockaddr_in*>(address.sockaddr());
                  struct ip_mreq mreq = { in_addr->sin_addr, INADDR_ANY };
                  status = ::setsockopt(descriptor(), IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq));
                  break;
               }
               case Version::V6: {
                  const sockaddr_in6* in6_addr = reinterpret_cast< const sockaddr_in6*>(address.sockaddr());
                  struct ipv6_mreq mreq = { in6_addr->sin6_addr, 0 };
                  status = ::setsockopt(descriptor(), IPPROTO_IPV6, IPV6_DROP_MEMBERSHIP, &mreq, sizeof(mreq));
                  break;
               }
               default:
                  break;
            };

            if (status != 0) {
               throw SocketException("Failed to leave");
            }
         }

         void MulticastSocket::setMulticastTTL(UInt32 ttl) throws (SocketException)
         {
            int status = 1;
            switch (_version) {
               case Version::V4:
                  status = ::setsockopt(descriptor(), IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));
                  break;
               case Version::V6:
                  status = ::setsockopt(descriptor(), IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &ttl, sizeof(ttl));
                  break;
               default:
                  break;
            }
            if (status != 0) {
               throw SocketException("Failed setMulticastTTL");
            }
         }

         UInt32 MulticastSocket::getMulticastTTL() const throws(SocketException)
         {
            UInt32 ttl;
            ::socklen_t len = sizeof(ttl);
            int status = 1;
            switch (_version) {
               case Version::V4:
                  status = ::getsockopt(descriptor(), IPPROTO_IP, IP_MULTICAST_TTL, &ttl, &len);
                  break;
               case Version::V6:
                  status = ::getsockopt(descriptor(), IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &ttl, &len);
                  break;
               default:
                  break;
            }
            if (status != 0) {
               throw SocketException("Failed getMulticastTTL");
            }
            return ttl;
         }

         void MulticastSocket::setMulticastLoopEnabled(bool enable) throws (SocketException)
         {
            UInt32 mc_loop = enable ? 1 : 0;
            int status = 1;
            switch (_version) {
               case Version::V4:
                  status = ::setsockopt(descriptor(), IPPROTO_IP, IP_MULTICAST_LOOP, &mc_loop, sizeof(mc_loop));
                  break;
               case Version::V6:
                  status = ::setsockopt(descriptor(), IPPROTO_IPV6, IPV6_MULTICAST_LOOP, &mc_loop, sizeof(mc_loop));
                  break;
               default:
                  break;
            }

            if (status != 0) {
               throw SocketException("Failed setMulticastLoopEnabled");
            }

         }

         bool MulticastSocket::isMulticastLoopEnabled() const throws (SocketException)
         {
            UInt32 mc_loop;
            ::socklen_t len = sizeof(mc_loop);
            int status = 1;
            switch (_version) {
               case Version::V4:
                  status = ::getsockopt(descriptor(), IPPROTO_IP, IP_MULTICAST_LOOP, &mc_loop, &len);
                  break;
               case Version::V6:
                  status = ::getsockopt(descriptor(), IPPROTO_IPV6, IPV6_MULTICAST_LOOP, &mc_loop, &len);
                  break;
               default:
                  break;
            }
            if (status != 0) {
               throw SocketException("Failed isMulticastLoopEnabled");
            }
            return mc_loop == 1;
         }

      }
   }
}
