#ifndef _CANOPY_NET_SOCKET_MULTICASTSOCKET_H
#define _CANOPY_NET_SOCKET_MULTICASTSOCKET_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_NET_SOCKET_PROXY_H
#include <canopy/net/socket/Proxy.h>
#endif

#ifndef _CANOPY_NET_ENDPOINT_H
#include <canopy/net/Endpoint.h>
#endif

namespace canopy {
   namespace net {
      namespace socket {

         /**
          * This class provides an implementation of multicast sockets.
          */
         class MulticastSocket : public Proxy
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(MulticastSocket);

               enum class Version
               {
                  V4, V6
               };

               /**
                * Constructor with a socket
                * @param socket a socket
                * @param options MC options
                * @throws SocketException if the socket could not be set up
                */
            private:
               MulticastSocket(::std::unique_ptr< Socket>&& socket) throws (SocketException);

               /**
                * Open a new Datagram socket for multicast.
                * @param d the socket domain
                * @param p the protocol
                * @throws a socket exception if the socket could not be opened
                */
            public:
               MulticastSocket(const Domain& d = INET_4, const Protocol& p = Protocol()) throws(SocketException);

               /**
                * Destructor.
                */
            public:
               ~MulticastSocket() throws();

               /**
                * Create a multicast socket.
                * @param group a multicast group
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> joinGroup(const Address& group) throws (SocketException);

               /**
                * Create a multicast socket.
                * @param group a multicast group
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> joinGroup(const Endpoint& group) throws (SocketException);

               /**
                * Create a multicast socket that is connected to the given multicast address.
                * @param group multicast address and port
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> createConnectedSocket(
                     const Address& group) throws (SocketException);

               /**
                * Create a multicast socket that is connected to the given multicast address.
                * @param group multicast address and port
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> createConnectedSocket(
                     const Endpoint& group) throws (SocketException);

               /**
                * Join the specified multicast address.
                * @param address an endpoint
                * @throws  a socket exception if the the socket could not join the given address
                */
            public:
               void join(const Address& address) throws (SocketException);

               /**
                * Leave the current multicast group.
                * @param address
                * @throws  a socket exception if the the socket could not join the given address
                */
            public:
               void leave(const Address& address) throws (SocketException);

               /**
                * Enable looping on the multicast connection.
                * This option is enabled by default.
                * @param enable enable loop on the multicast connection
                */
            public:
               void setMulticastLoopEnabled(bool enable) throws (SocketException);

               /**
                * Determine if multicast loop is enabled.
                * @return true if multicast loop is enabled
                */
            public:
               bool isMulticastLoopEnabled() const throws (SocketException);

               /**
                * Set the multicast ttl or hops.
                * @param ttl the new ttl/hop value
                * @throws SocketException on error
                */
            public:
               void setMulticastTTL(UInt32 ttl) throws (SocketException);

               /**
                * Get the multicast TTL.
                * @return the multicast ttl
                */
            public:
               UInt32 getMulticastTTL() const throws(SocketException);

               /**
                * Map a domain to a version.
                * @param d a domain
                * @return a version
                * @throws SocketException if the domain is a valid version
                */
            private:
               static Version version(Domain d) throws (SocketException);

               /**
                * The IP version
                */
            private:
               Version _version;
         }
         ;
      }
   }
}
#endif
