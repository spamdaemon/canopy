#include <canopy/net/ConnectionRefused.h>

namespace canopy {
  namespace net {
    ConnectionRefused::ConnectionRefused() throws()
    : SocketException(CONNECTION_REFUSED) {}

    ConnectionRefused::~ConnectionRefused() throw()
    {}
  }
}
