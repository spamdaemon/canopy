#ifndef _CANOPY_NET_NETEXCEPTION_H
#define _CANOPY_NET_NETEXCEPTION_H

#ifndef _CANOPY_IO_IOEXCEPTION_H
#include <canopy/io/IOException.h>
#endif

namespace canopy {
   namespace net {

      /**
       * This exception is thrown by classes in the net package. For convenience
       * sake, this exception derives from IOException, but it may be raised for
       * problems not necessarily involving an IO operation.
       */
      class NetException : public ::canopy::io::IOException
      {
            /** No default constructor */
         private:
            NetException() = delete;

            /**
             * Create an exception with the specified error string.
             * The exception code is set to OTHER.
             * @param msg an error message
             */
         public:
            NetException(::std::string) throws ();

            /** Destructor */
         public:
            ~NetException() throws();
      };
   }
}

#endif
