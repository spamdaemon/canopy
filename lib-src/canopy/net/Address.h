#ifndef _CANOPY_NET_ADDRESS_H
#define _CANOPY_NET_ADDRESS_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_NET_H
#include <canopy/net/net.h>
#endif

#ifndef _CANOPY_NET_ADDRESSEXCEPTION_H
#include <canopy/net/AddressException.h>
#endif

#ifndef _CANOPY_RESOURCEUNAVAILABLE_H
#include <canopy/ResourceUnavailable.h>
#endif

#ifndef _CANOPY_NET_H
#include <canopy/net/net.h>
#endif

#include <string>
#include <stdexcept>
#include <vector>
#include <iosfwd>

#include <sys/types.h>
#include <sys/socket.h>

namespace canopy {
   namespace net {
      class Socket;
      namespace socket {
         class PosixSocket;
         class MulticastSocket;
      }

      /**
       * An address defines a unique name for an Endpoint. Addresses are defined
       * for different combinations of a domain, a type, and a protocol.
       *
       * Usually, an address is obtained from an Endpoint and are not created directly
       * by outside packages.
       */
      class Address
      {

            /** The posix socket is a friend because it needs the address implementation */
            friend class ::canopy::net::socket::PosixSocket;

            /** The posix socket is a friend because it needs the address implementation */
            friend class ::canopy::net::socket::MulticastSocket;

            /** The endpoint must also be a friend */
            friend class Endpoint;

            /**
             * Create an object representing an uninitialized address.
             */
         public:
            Address() throws();

            /**
             * Create a new address
             * @param t the socket type
             * @param p the protocol
             * @param addrlen the size of the address storage
             * @param addr the optional address
             * @throws ::std::exception if addr length is too large
             */
         private:
            Address(SocketType t, Protocol p, size_t addrlen, const ::sockaddr_storage& addr) throws(::std::exception);

            /** Destructor */
         public:
            ~Address() throws();

            /**
             * Get the domain for this address
             * @return the address domain
             */
         public:
            inline Domain domain() const throws()
            {
               return _domain;
            }

            /**
             * Get the socket type
             * @return the socket type
             */
         public:
            inline SocketType type() const throws()
            {
               return _type;
            }

            /**
             * Get the protocol
             * @return the protocol
             */
         public:
            inline Protocol protocol() const throws()
            {
               return _protocol;
            }

            /**
             * Get the numeric address of the node represented by this address. This will
             * be a <em>numeric</em> address.
             * @return a <em>numeric</em> string representing the host.
             * @throws ::std::exception if the address could not be converted correctly
             */
         public:
            ::std::string nodeID() const throws ();

            /**
             * Get the name of this node represented by this address.  This
             * will retrieve a meaningful name rather than a numeric address. If
             * a name cannot be retrieved, then the numeric address will be attempted.
             * @return a string representing the host.
             * @throws ::std::exception if the address could not be converted correctly
             */
         public:
            ::std::string nodeName() const throws ();

            /**
             * Get the name of the service.  For example, instead return 80, this method
             * may return http.
             * @return a string representing the host.
             * @throws ::std::exception if the address could not be converted correctly
             */
         public:
            ::std::string serviceName() const throws ();

            /**
             * Get the numeric id of the service.
             * @return a string representing the host.
             * @throws ::std::exception if the address could not be converted correctly
             */
         public:
            ::std::string serviceID() const throws ();

            /**
             * Get the node service object for this address. This is function
             * is short for NodeService(nodeID(),serviceID()).
             * @return a node service object
             */
         public:
            NodeService getNodeServiceID() const throws();

            /**
             * Get the node service object for this address. This is function
             * is short for NodeService(nodeName(),serviceName()).
             * @return a node service object
             */
         public:
            NodeService getNodeServiceName() const throws();

            /**
             * Determine if the address is a multicast address.
             * @return true if this address is a multicast address.
             */
         public:
            bool isMulticastAddress() const throws();

            /**
             * Get the sockaddr object
             * @return a pointer to the socket address
             */
         private:
            struct sockaddr* sockaddr() throws()
            {
               return reinterpret_cast< struct sockaddr*>(&_address);
            }
            const struct sockaddr* sockaddr() const throws()
            {
               return reinterpret_cast< const struct sockaddr*>(&_address);
            }

            /** The socket type */
         private:
            SocketType _type;

            /** The socket type */
         private:
            Domain _domain;

            /** The protocol */
         private:
            Protocol _protocol;

            /** The length of the address in bytes */
         private:
            ::socklen_t _addrlen;

            /** The address itself (we're treating it as an opaque quantity) */
         private:
            ::sockaddr_storage _address;
      };
   }
}

/**
 * Print some representation of this address
 * @param out a stream
 * @param addr an address
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::Address& addr);

#endif
