#ifndef _CANOPY_NET_SOCKET_H
#define _CANOPY_NET_SOCKET_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_HASH_H
#include <canopy/hash.h>
#endif

#ifndef _CANOPY_INTERRUPTEDEXCEPTION_H
#include <canopy/InterruptedException.h>
#endif

#ifndef _CANOPY_IO_IODESCRIPTOR_H
#include <canopy/io/IODescriptor.h>
#endif

#ifndef _CANOPY_IO_IOEXCEPTION_H
#include <canopy/io/IOException.h>
#endif

#ifndef _CANOPY_NET_SOCKETEXCEPTION_H
#include <canopy/net/SocketException.h>
#endif

#ifndef _CANOPY_NET_CONNECTIONREFUSED_H
#include <canopy/net/ConnectionRefused.h>
#endif

#ifndef _CANOPY_NET_H
#include <canopy/net/net.h>
#endif

#ifndef _CANOPY_NET_ADDRESS_H
#include <canopy/net/Address.h>
#endif

#ifndef _CANOPY_NET_ENDPOINT_H
#include <canopy/net/Endpoint.h>
#endif

#ifndef _CANOPY_NET_SOCKET_SOCKET_ENDPOINT_H
#include <canopy/net/socket/Socket.h>
#endif

#include <memory>
#include <atomic>

namespace canopy {
   namespace net {

      /**
       * This is the baseclass for all socket implementations
       * that are based on a SocketDescriptor. The socket descriptor
       * is accessible via the protected method descriptor().
       * Each instance of this class is given a name, which can be
       * retrieved with the name() method. Basic operations on sockets
       * are provided by this class, such as write(), read(), close(), etc.
       *
       * @note Sockets are reference counted in a thread-safe way, so that they may
       *       be passed around threads.
       * @note Socket functions are not guaranteed to be thread-safe and users should
       *       assume that all sockets support concurrent reads, writes, or read-writes.
       */
      class Socket
      {
            /** The socket impl */
         public:
            typedef ::canopy::net::socket::Socket SocketProvider;

            /** The socket descriptor */
         public:
            typedef SocketProvider::SocketDescriptor SocketDescriptor;

            /**
             * Create a new socket with the specified socket descriptor.
             * @param d the socket's domain
             * @param t the socket's type
             * @param p the socket's protocol
             * @param sd the socket descriptor
             */
         public:
            explicit Socket(::std::shared_ptr< SocketProvider> socket) throws (SocketException);

            /** The default constructor */
         public:
            Socket() throws ();

            /**
             * Open a new socket.
             * @param d the socket domain
             * @param t the socket type
             * @param p the protocol
             * @throws a socket exception if the socket could not be opened
             */
         public:
            Socket(const SocketType& t, const Domain& d, const Protocol& p = Protocol()) throws (SocketException);

            /**
             * Create a copy of the socket.
             * @param s a socket
             */
         public:
            Socket(const Socket& s) throws ();

            /**
             * A move operator.
             * @param s a socket
             */
         public:
            Socket(Socket&& s) throws ();

            /**
             * Create a socket from descriptor. If the NativeDescriptor associated
             * with the IODescriptor cannot be cast to a socket provider, then
             * an invalid socket is created.
             * @param d a descriptor (possibly invalid)
             */
         public:
            Socket(const ::canopy::io::IODescriptor& d) throws ();

            /**
             * Destroy this socket reference. If this is the last
             * reference to the socket, then it is destroyed.
             * If the socket cannot be destroyed properly, then an error
             * message is printed to cerr
             */
         public:
            ~Socket() throws ();

            /**
             * Create a socket that is bound the specified end-point. This will bind
             * the socket to the <em>first</em> address known to the end-point.
             * Currently, this method does not allow setting of generic socket options
             * before binding.
             *
             * @note Use getBoundAddress() to obtain the address used for binding.
             *
             * @param ep an end-point.
             * @param reuseAddr reuse an address if possible (if true)
             * @return a socket that was bound to an address defined by the end-point.
             * @exception SocketException if the socket could not be bound
             * to the address and port
             */
         public:
            static Socket createBoundSocket(const Endpoint& ep, bool reuseAddr = true) throws (SocketException);

            /**
             * Create a socket that will connect to the specified endpoint.
             * @param dest the destination
             * @return a connected socket
             * @exception ConnectionRefused if the connection failed
             */
         public:
            static Socket createConnectedSocket(const Endpoint& dest) throws (SocketException);

            /**
             * Assign the specified socket. An exception will be thrown
             * if this socket is the last reference and cannot be closed.
             * @param s a socket
             * @return *this
             */
         public:
            Socket& operator=(const Socket& s) throws (SocketException);

            /**
             * The move operator.
             * @param s a socket
             * @return *this
             */
         public:
            Socket& operator=(Socket&& s) throws (SocketException);

            /**
             * Get the descriptor
             * @return the descriptor
             */
         public:
            inline ::canopy::io::IODescriptor descriptor() const throws ()
            {
               return _sd->ioDescriptor();
            }

            /**
             * Get the id for this socket.
             * @note the id is not constant for the lifetime of this socket.
             * Calling close() on the socket will change the id.
             * @return the unique id.
             */
         public:
            inline Int32 id() const throws ()
            {
               // use the socket descirptor as an id
               return _sd->id();
            }

            /**
             * Test if this is an invalid socket.
             * @return true if this socket is <em>invalid</em>
             */
         public:
            inline bool operator!() const throws ()
            {
               return _sd->invalid();
            }

            /**
             * Get the socket type.
             * @return the type of this socket
             * @throw SocketException if the type could not be established
             */
         public:
            inline SocketType type() const throws ()
            {
               return _sd->type();
            }

            /**
             * Get the socket domain.
             * @return the domain of this socket
             * @throw SocketException if the domain could not be established
             */
         public:
            inline Domain domain() const throws ()
            {
               return _sd->domain();
            }

            /**
             * Get the socket protcol.
             * @return the protocol used by this socket.
             * @throw SocketException if the domain could not be established
             */
         public:
            inline Protocol protocol() const throws ()
            {
               return _sd->protocol();
            }

            /**
             * Close this socket.
             * @note this method will change the socket's id
             * @throws Exception if the socket could not be closed
             */
         public:
            inline void close() throws (::canopy::io::IOException)
            {
               _sd->close();
            }

            /**
             * Test if this socket has been closed. If this
             * method returns false, then this does not mean
             * that the next method invocation for this socket
             * will succeed.
             * @return true if this socket can no longer be used, false
             * if the socket currently not closed
             */
         public:
            bool isClosed() const throws ();

            /**
             * Shut this socket down for any writes.
             */
         public:
            void shutdownWrite() throws (SocketException);

            /**
             * Shut this socket down for any more reads.
             */
         public:
            void shutdownRead() throws (SocketException);

            /**
             * Peek at the buffer in the socket. This method works just like read, but does not
             * consume the data on the socket. This method is ideal for checking if a socket has been
             * closed in an orderly fashion, as this method will return 0 in that case.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            ssize_t peek(char* buffer, size_t bufSize,
                  const Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) const throws (::canopy::io::IOException,InterruptedException);

            /**
             * Read a number of bytes from this socket. This method blocks
             * unless the socket is a non-blocking socket or there is already
             * data to be read. This method must return 0 to indicate that the socket
             * buffer is full.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            ssize_t read(char* buffer, size_t bufSize,
                  const Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) throws (::canopy::io::IOException,InterruptedException);

            /**
             * Write a number of bytes to this socket. This method blocks
             * until all bytes have been written. If this socket is in non-blocking mode
             * and no bytes can currently be written (because the buffer is full), then 0 is returned.
             * Even if blocking is enabled, this call may still return 0 if the socket
             * itself has been set to be non-blocking!
             * @note if this is a UDP socket and the destination does not exist, then
             *       a CONNECTION_REFUSED exception may be raised.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to write
             * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
             * @exception SocketException if an error occurred during writing
             * @exception InterruptedException if the call was interrupted
             * @exception ConnectionRefused may be thrown if the destination does not exist
             */
         public:
            size_t write(const char* buffer, size_t bufSize,
                  const Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) throws (::canopy::io::IOException,InterruptedException);

            /**
             * @name UDP socket operations
             * @{
             */

            /**
             * Receive data on this socket. This method must be called
             * on an unconnected UDP socket. The address of the sender is
             * returned in the address.<p>
             * Even if blocking is enabled, this call may still return 0 if the socket
             * itself has been set to be non-blocking!
             * @param buffer the data buffer to be written
             * @param bufSize the number of characters in the buffer to write
             * @param from the source address of the data
             * @param block if true (default), then this method blocks until there is at least 1 byte to write
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception SocketException if an error occurred
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            ssize_t readFrom(char* buffer, size_t bufSize, Address& from, const Timeout& timeout =
                  ::canopy::io::BLOCKING_TIMEOUT) throws (SocketException,InterruptedException);

            /**
             * Write a number of bytes via this socket to the specified destination.
             * This method must be called on an unconnected UDP socket.
             * @note if this is a UDP socket and the destination does not exist, then
             *       a CONNECTION_REFUSED exception may be raised.
             * @param buffer the data buffer to be written
             * @param bufSize the number of characters in the buffer to write
             * @param to the destination address
             * @param block if true (default), then this method blocks until there is at least 1 byte to write
             * @return the number of bytes written or 0 if nothing could be written.
             * @exception SocketException if an error occurred
             * @exception InterruptedException if the call was interrupted
             * @exception ConnectionRefused may be thrown if the destination does not exist
             */
         public:
            size_t writeTo(const char* buffer, size_t bufSize, const Address& to, const Timeout& timeout =
                  ::canopy::io::BLOCKING_TIMEOUT) throws (SocketException,InterruptedException);

            /*@}*/

            /**
             * Determine if this socket is a server socket. A server socket is one
             * for which listen() has been invoked.
             * @return true if listen has been invoked
             * @exception SocketException if an error occurred
             */
         public:
            inline bool isServerSocket() const throws (SocketException)
            {
               return _sd->isServerSocket();
            }

            /**
             * Set the socket to block on I/O operations.
             * @param enabled if true, then I/O operations may block
             * @exception ::canopy::io::IOException if an error occurred
             */
         public:
            inline void setBlockingEnabled(bool enabled) throws (::canopy::io::IOException)
            {
               _sd->setBlockingEnabled(enabled);
            }

            /**
             * Test if blocking I/O is enabled.
             * @return true if I/O calls may be block, false otherwise
             * @exception ::canopy::io::IOException if an error occurred
             */
         public:
            inline bool isBlockingEnabled() const throws (::canopy::io::IOException)
            {
               return _sd->isBlockingEnabled();
            }

            /**
             * Set the send buffer size
             * @param size the send buffer size
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void setSendBufferSize(UInt32 size) throws (SocketException)
            {
               _sd->setSendBufferSize(size);
            }

            /**
             * Get the receive buffer size.
             * @return size of the receive buffer
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline UInt32 getSendBufferSize() const throws (SocketException)
            {
               return _sd->getSendBufferSize();
            }

            /**
             * Set the receive buffer size.
             * @param size the receive buffer size
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void setReceiveBufferSize(UInt32 size) throws (SocketException)
            {
               _sd->setReceiveBufferSize(size);
            }

            /**
             * Get the receive buffer size.
             * @return size of the receive buffer
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline UInt32 getReceiveBufferSize() const throws (SocketException)
            {
               return _sd->getReceiveBufferSize();
            }

            /**
             * Clear any errors on this socket.
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void clearErrors() throws (SocketException)
            {
               _sd->clearErrors();
            }

            /**
             * Enable reuse of addresses if there are no connected sockets.
             * @param enabled true if the address can be bound again
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void setReuseAddressEnabled(bool enabled) throws (SocketException)
            {
               _sd->setReuseAddressEnabled(enabled);
            }

            /**
             * Test if the address reuse option has been enabled
             * @return true if addresses can be reused when binding
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline bool isReuseAddressEnabled() const throws (SocketException)
            {
               return _sd->isReuseAddressEnabled();
            }

            /**
             * Enable broadcast for this socket. This option is only
             * available for UDP sockets.
             * @param enabled true if this socket should be able to broadcast
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void setBroadcastEnabled(bool enabled) throws (SocketException)
            {
               _sd->setBroadcastEnabled(enabled);
            }

            /**
             * Test if this socket is capable of broadcasting
             * @return true if this socket can broadcast
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline bool isBroadcastEnabled() const throws (SocketException)
            {
               return _sd->isBroadcastEnabled();
            }

            /**
             * Enable  sending  of  keep-alive messages on
             * connection-oriented sockets.
             * @param enabled true if keep alive message are to be sent
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void setKeepAliveEnabled(bool enabled) throws (SocketException)
            {
               _sd->setKeepAliveEnabled(enabled);
            }

            /**
             * Test if the keep alive option
             * is enabled.
             * @return true if the keep alive option is enabled
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline bool isKeepAliveEnabled() const throws (SocketException)
            {
               return _sd->isKeepAliveEnabled();
            }

            /**
             * Enable lingering of this socket after closing it.
             * @param duration the time in seconds that the socket should remain
             * open after it has been closed
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void enableLinger(UInt32 duration) throws (SocketException)
            {
               _sd->enableLinger(duration);
            }

            /**
             * Disable linger.
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline void disableLinger() throws (SocketException)
            {
               return _sd->disableLinger();
            }

            /**
             * Get the linger duration
             * @return the linger time or 0 if linger is not enabled
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline UInt32 getLingerTime() const throws (SocketException)
            {
               return _sd->getLingerTime();
            }

            /**
             * Test if linger is enabled.
             * @return true if lingering for this socket is enabled
             * @exception SocketException if this operation could not
             * be performed on this socket.
             */
         public:
            inline bool isLingerEnabled() const throws (SocketException)
            {
               return _sd->isLingerEnabled();
            }

            /*@}*/

            /**
             * @name BSD socket operations.
             * @{
             */

            /**
             * Put this socket in listening mode. Sockets of type
             * UDP do not support this method.
             * @param backlog the number connection requrest that will be
             * queued up before clients receive a "conncection refused"
             * @exception SocketException if this socket does support this method
             * or some other error occurred.
             */
         public:
            inline void listen(UInt32 backlog) throws (SocketException)
            {
               _sd->listen(backlog);
            }

            /**
             * Accept a connection on this socket. The current
             * thread is suspended until a connection is made. Clients
             * are responsible for deleting the returned object.
             * Sockets of type UDP do not support this method.
             * @param peer an address which will contain the peer address upon completion.
             * @return a socket representing the connection to
             * a remote host, or 0 if no connection was made
             * @throw SocketException if some error occurred or this method
             * is not supported by this socket.
             */
         public:
            Socket accept(Address& peer) throws (SocketException);

            /**
             * Accept a connection on this socket. The current
             * thread is suspended until a connection is made. Clients
             * are responsible for deleting the returned object.
             * Sockets of type UDP do not support this method.
             * @return a socket representing the connection to
             * a remote host, or 0 if no connection was made
             * @throw SocketException if some error occurred or this method
             * is not supported by this socket.
             */
         public:
            Socket accept() throws (SocketException);

            /**
             * Bind this socket to the specified end-point address. For
             * obvious reasons, the endpoint address must be describe the
             * current host node.
             * @param address an end-point address.
             * @exception SocketException if the socket could not be bound
             * to the address and port
             */
         public:
            void bind(const Address& address) throws (SocketException);

            /**
             * Bind this socket to the first address defined by this end-point.
             * @param ep an end-point
             * @exception SocketException if the socket could not be bound to the end-point
             */
         public:
            void bind(const Endpoint& ep) throws (SocketException);

            /**
             * Connect this socket to an endpoint at the specified address. If this socket
             * is of type UDP, then no real connection is made, but all
             * datagrams are sent by default to the specified address.
             * @return true if the socket was connected, false if the socket is non-blocking and a connection is in-progress
             * @param address an address.
             * @exception SocketException if the socket could not be connect
             * to the address and port
             * @exception ConnectionRefused if the connection failed
             */
         public:
            bool connect(const Address& address) throws (SocketException);

            /**
             * Connect this socket to an endpoint via one of its addresses. If this socket
             * is of type UDP, then no real connection is made, but all
             * datagrams are sent by default to the specified address.
             * @param ep an end-point
             * @return true if the socket was connected, false if the socket is non-blocking and a connection is in-progress
             * @exception SocketException if the socket could not be connected to the end-point
             */
         public:
            bool connect(const Endpoint& ep) throws (SocketException);
            /*@}*/

            /**
             * @name Enquiring about this socket.
             * @{
             */

            /**
             * Get the address to which this socket is bound.
             * @return the inet address to which this socket is bound
             * @throw canopy::io::IOException if the socket is not bound
             */
         public:
            inline Address getBoundAddress() const throws (SocketException)
            {
               return _sd->getBoundAddress();
            }

            /**
             * Get the address of the peer socket.
             * @return the inet address of socket to which this socket is connected
             * @throw canopy::io::IOException if the socket is not connected
             */
         public:
            inline Address getPeerAddress() const throws (SocketException)
            {
               return _sd->getPeerAddress();
            }

            /*@}*/

            /**
             * Test for equality. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             */
         public:
            inline bool operator==(const Socket& s) const throws ()
            {
               return _sd == s._sd;
            }

            /**
             * Test for inequality. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             */
         public:
            inline bool operator!=(const Socket& s) const throws ()
            {
               return _sd != s._sd;
            }

            /**
             * Get a partial ordering of two sockets. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             * @return true if this socket is before the specified socket.
             */
         public:
            inline bool operator<(const Socket& s) const throws ()
            {
               return _sd < s._sd;
            }

            /**
             * Swap two sockets.
             * @param s a socket
             * @note this method is not threadsafe!!!
             */
         public:
            inline void swap(Socket& s) throws()
            {
               ::std::swap(_sd, s._sd);
            }

            /**
             * Get a hash code
             * @param socket a socket
             * @return a hash code
             */
         public:
            CANOPY_BOILERPLATE_DECLARE_HASH;

            /** The socket descriptor for this socket */
         private:
            ::std::shared_ptr< SocketProvider> _sd;
      };
   }
}

inline CANOPY_BOILERPLATE_DEFINE_HASH(::canopy::net::Socket, ::std::hash<::std::shared_ptr< SocketProvider>>()(_sd))

CANOPY_BOILERPLATE_HASH(::canopy::net::Socket)
#endif
