#ifndef _CANOPY_NET_H
#define _CANOPY_NET_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_IO_H
#include <canopy/io/io.h>
#endif

#include <chrono>
#include <iosfwd>

#include <sys/types.h>
#include <sys/socket.h>

namespace canopy {
   namespace net {

      /** A timeout for IO operations is always in nanoseconds */
      using ::canopy::io::Timeout;

      /** Use the make timeout function */
      using ::canopy::io::makeTimeout;

      /** A tuple to denote a host and a port */
      struct HostPort
      {
            HostPort(const ::std::string& h, ::std::uint32_t p);

            HostPort();

            ::std::string host;
            ::std::uint32_t port;
      };

      /** A tuple to denote a host service */
      struct NodeService
      {
            NodeService();
            NodeService(const ::std::string& n, const ::std::string& s);

            ::std::string node;
            ::std::string service;
      };

      /**
       * Parse a host-port.
       * @param str a string
       * @param def a default hiost port
       * @return a host and port combo
       * @throws ::std::invalid_argument if the string could not be parsed
       */
      HostPort parseHostPort(const ::std::string& str, const HostPort& def = HostPort()) throws(::std::exception);

      /**
       * Parse a node service.
       * @param str a string
       * @param def a default hiost port
       * @return a host and port combo
       * @throws ::std::invalid_argument if the string could not be parsed
       */
      NodeService parseNodeService(const ::std::string& str, const NodeService& def =
            NodeService()) throws(::std::exception);

      /** The supported address domains (aka families) */
      class Domain
      {

            /** Create a new domain */
         public:
            constexpr explicit Domain(Int32 xid)
                  : _id(xid)
            {
            }

            /** Create a new domain */
         public:
            constexpr Domain()
                  : _id(0)
            {
            }

            /**
             * Get the domain id
             * @return the domain id
             */
         public:
            constexpr Int32 id() const
            {
               return _id;
            }

            /**
             * @name Comparison functions.
             * @{
             */
         public:
            inline bool operator==(const Domain& t) const throws()
            {
               return id() == t.id();
            }
            inline bool operator!=(const Domain& t) const throws()
            {
               return id() != t.id();
            }
            inline bool operator<(const Domain& t) const throws()
            {
               return id() < t.id();
            }

            /*@}*/

            /** The domain id */
         private:
            Int32 _id;
      };

      /** The supported socket types */
      class SocketType
      {

            /** Create a new socket type id */
         public:
            constexpr explicit SocketType(Int32 xid)
                  : _id(xid)
            {
            }

            /** Create a new socket type id */
         public:
            constexpr SocketType()
                  : _id(0)
            {
            }

            /**
             * Get the socket type id
             * @return the socket type id
             */
         public:
            constexpr Int32 id() const
            {
               return _id;
            }

            /**
             * @name Comparison functions.
             * @{
             */
         public:
            inline bool operator==(const SocketType& t) const throws()
            {
               return id() == t.id();
            }
            inline bool operator!=(const SocketType& t) const throws()
            {
               return id() != t.id();
            }
            inline bool operator<(const SocketType& t) const throws()
            {
               return id() < t.id();
            }

            /** The socket type id */
         private:
            Int32 _id;
      };

      /** The supported protocols */
      class Protocol
      {

            /** Create a new protocol */
         public:
            constexpr explicit Protocol(Int32 xid)
                  : _id(xid)
            {
            }

            /** Create a new protocol */
         public:
            constexpr Protocol()
                  : _id(0)
            {
            }

            /**
             * Get the protocol id
             * @return the protocol id
             */
         public:
            constexpr Int32 id() const
            {
               return _id;
            }

            /**
             * @name Comparison functions.
             * @{
             */
         public:
            inline bool operator==(const Protocol& t) const throws()
            {
               return id() == t.id();
            }
            inline bool operator!=(const Protocol& t) const throws()
            {
               return id() != t.id();
            }
            inline bool operator<(const Protocol& t) const throws()
            {
               return id() < t.id();
            }

            /** The protocol id */
         private:
            Int32 _id;
      };

      /**
       * The unix domain
       */
      constexpr Domain UNIX(PF_UNIX);

      /** The IPv4 domain */
      constexpr Domain INET_4(PF_INET);

      /** The IPv6 domain */
      constexpr Domain INET_6(PF_INET6);

      /** The  local domain */
      constexpr Domain PACKET(PF_PACKET);

      /**
       * The supported socket types
       */
      constexpr SocketType STREAM(SOCK_STREAM);

      /** The datagram socket */
      constexpr SocketType DATAGRAM(SOCK_DGRAM);

      /** The raw socket type */
      constexpr SocketType RAW(SOCK_RAW);

      /** The seqential packet socket type */
      constexpr SocketType SEQPACKET(SOCK_SEQPACKET);

   }
}

/**
 * Print a nodeservice
 * @param out an output stream
 * @param srv the service object
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::NodeService& srv);

/**
 * Print a hostport
 * @param out an output stream
 * @param srv the service object
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::HostPort& srv);

/**
 * Print the domain.
 * @param out an output stream
 * @param x a domain
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::Domain& x);

/**
 * Print the domain.
 * @param out an output stream
 * @param x a protocol
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::Protocol& x);

/**
 * Print the domain.
 * @param out an output stream
 * @param x a socket type
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::SocketType& x);

#endif
