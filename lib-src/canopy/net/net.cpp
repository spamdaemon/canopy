#include <canopy/net/net.h>
#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>

namespace canopy {
   namespace net {

      HostPort::HostPort()
            : port(0)
      {
      }

      HostPort::HostPort(const ::std::string& h, ::std::uint32_t p)
            : host(h), port(p)
      {
      }

      NodeService::NodeService()
      {
      }

      NodeService::NodeService(const ::std::string& n, const ::std::string& s)
            : node(n), service(s)
      {
      }

      NodeService parseNodeService(const ::std::string& str, const NodeService& def) throws(::std::exception)
      {
         if (str.empty()) {
            return def;
         }
         ::std::string::size_type pos = str.rfind(':');
         if (pos == ::std::string::npos) {
            return NodeService { str, def.service };
         }
         if (pos > 0 && pos < str.length() - 1) {
            return NodeService { str.substr(0, pos), str.substr(pos + 1) };
         }
         if (pos == 0 && pos == str.length() - 1) {
            // test for str=":"
            return def;
         }

         if (pos == str.length() - 1) {
            return NodeService { str.substr(0, pos), def.service };
         }
         assert(pos == 0);
         return NodeService { def.node, str.substr(1) };
      }

      HostPort parseHostPort(const ::std::string& str, const HostPort& def) throws(::std::exception)
      {
         const NodeService ns = parseNodeService(str, { def.host, ::std::to_string(def.port) });

         // convert the service back to a number
         size_t pos = 0;
         int port = ::std::stoi(ns.service, &pos, 10);
         if (pos != ns.service.length() || port < 0) {
            throw ::std::invalid_argument("Not a valid port number " + ns.service);
         }
         return HostPort(ns.node, port);
      }

   }
}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::NodeService& srv)
{
   out << srv.node << ':' << srv.service;
   return out;
}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::HostPort& srv)
{
   out << srv.host << ':' << srv.port;
   return out;
}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::Domain& x)
{
   switch (x.id()) {
      case ::canopy::net::UNIX.id():
         out << "UNIX";
         break;
      case ::canopy::net::INET_4.id():
         out << "INET4";
         break;
      case ::canopy::net::INET_6.id():
         out << "INET6";
         break;
      case ::canopy::net::PACKET.id():
         out << "PACKET";
         break;
      default:
         out << x.id();
   }
   return out;
}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::Protocol& x)
{
   return out << x.id();

}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::net::SocketType& x)
{
   switch (x.id()) {
      case ::canopy::net::STREAM.id():
         out << "STREAM";
         break;
      case ::canopy::net::DATAGRAM.id():
         out << "DATAGRAM";
         break;
      case ::canopy::net::RAW.id():
         out << "RAW";
         break;
      case ::canopy::net::SEQPACKET.id():
         out << "SEQPACKET";
         break;
      default:
         out << x.id();
   }
   return out;

}
