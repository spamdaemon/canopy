#ifndef _CANOPY_NET_CONNECTIONREFUSED_H
#define _CANOPY_NET_CONNECTIONREFUSED_H

#ifndef _CANOPY_NET_SOCKETEXCEPTION_H
#include <canopy/net/SocketException.h>
#endif

namespace canopy {
  namespace net {

    /**
     * This exception may thrown by operations that work
     * no connected sockets.
     */
    class ConnectionRefused : public SocketException {
      /** Default constructor */
    public:
      ConnectionRefused() throws();

      /** Destructor */
    public:
      ~ConnectionRefused() throw();
    };
  }
}

#endif
