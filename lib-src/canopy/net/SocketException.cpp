#include <canopy/net/SocketException.h>

namespace canopy {
   namespace net {
      namespace {
         static const char* mapCode(SocketException::ExceptionCode code) throws()
         {
            switch (code) {
               case SocketException::INVALID_ADDRESS:
                  return "invalid address";
               case SocketException::DENIED:
                  return "denied";
               case SocketException::ALREADY_BOUND:
                  return "already bound";
               case SocketException::ALREADY_USED:
                  return "already used";
               case SocketException::CONNECTION_ABORTED:
                  return "connection aborted";
               case SocketException::NO_RESOURCES:
                  return "no resources";
               case SocketException::UNSUPPORTED_OPERATION:
                  return "unsupported operation";
               case SocketException::ALREADY_CONNECTED:
                  return "already connected";
               case SocketException::CONNECTION_REFUSED:
                  return "connection refused";
               case SocketException::CONNECTION_TIMED_OUT:
                  return "connection timed out";
               case SocketException::NETWORK_UNREACHABLE:
                  return "network unreachable";
               case SocketException::CONNECTION_IN_PROGRESS:
                  return "connection in progress";
               case SocketException::CONNECTION_RESET:
                  return "connection reset";
               case SocketException::CONNECTION_BROKEN:
                  return "connection broken";
               default:
                  return "reason unknown";
            }
         }
      }

      SocketException::SocketException(ExceptionCode code) throws()
            : NetException(mapCode(code)), _code(code)
      {
      }

      SocketException::SocketException(::std::string s) throws()
            : NetException(::std::move(s)), _code(OTHER)
      {
      }

      SocketException::~SocketException() throws()
      {
      }

   }
}
