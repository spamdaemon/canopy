#include <canopy/net/Socket.h>
#include <canopy/net/net.h>
#include <canopy/net/socket/PosixSocket.h>

using namespace ::std;

namespace canopy {
   namespace net {

      Socket::Socket(::std::shared_ptr< SocketProvider> socket)
throws            (SocketException)
            : _sd(::std::move(socket))
            {
               if (!_sd) {
                  _sd = ::std::make_shared<::canopy::net::socket::PosixSocket>();
               }
            }

      Socket::Socket(const ::canopy::io::IODescriptor& d)
throws            ()
            : Socket(d.cast< SocketProvider>())
            {
            }

      Socket::~Socket() throws()
      {
      }

      Socket::Socket()
throws            ()
            : Socket(nullptr)
            {
            }

      Socket::Socket(const SocketType& t, const Domain& d, const Protocol& p)
throws            (SocketException)
            : _sd(::std::make_shared<::canopy::net::socket::PosixSocket>(t, d, p))
            {

            }

      Socket::Socket(const Socket& s)
throws            ()
            : _sd(s._sd)
            {
            }

      Socket::Socket(Socket&& s)
throws            ()
            : _sd(::std::move(s._sd))
            {
               s._sd = ::std::make_shared<::canopy::net::socket::PosixSocket>();
            }

      Socket& Socket::operator=(const Socket& s) throws (SocketException)
      {
         _sd = s._sd;
         return *this;
      }

      Socket& Socket::operator=(Socket&& s) throws (SocketException)
      {
         if (_sd != s._sd) {
            _sd = ::std::move(s._sd);
            s._sd = ::std::make_shared< ::canopy::net::socket::PosixSocket>();
         }
         return *this;
      }

      bool Socket::isClosed() const throws ()
      {
         return _sd->isClosed();
      }

      void Socket::shutdownWrite() throws (SocketException)
      {
         _sd->shutdownWrite();
      }

      void Socket::shutdownRead() throws (SocketException)
      {
         _sd->shutdownRead();
      }

      bool Socket::connect(const Endpoint& ep) throws(SocketException)
      {
         const Endpoint xep = ep.filter(type(), domain(), protocol());
         for (const Address& addr : xep) {
            try {
               return connect(addr);
            }
            catch (const SocketException&) {
            }
         }
         throw SocketException("Endpoint has no usable addresses");
      }

        void Socket::bind(const Address& address) throws (SocketException)
      {
         _sd->bind(address);
      }

      void Socket::bind(const Endpoint& ep) throws(SocketException)
      {
         const Endpoint xep = ep.filter(type(), domain(), protocol());
         for (const Address& addr : xep) {
            try {
               bind(addr);
            }
            catch (const SocketException&) {
            }
         }
         throw SocketException("Endpoint has no usable addresses");
      }

      Socket Socket::accept(Address& peer) throws (SocketException)
      {
         return Socket(_sd->accept(peer));
      }

      Socket Socket::accept() throws (SocketException)
      {
         Address addr;
         return accept(addr);
      }

      bool Socket::connect(const Address& address) throws (SocketException)
      {
         return _sd->connect(address);
      }

      Socket Socket::createConnectedSocket(const Endpoint& ep) throws (SocketException)
      {
         for (const Address& addr : ep) {
            try {
               Socket s(addr.type(), addr.domain(), addr.protocol());
               s.connect(addr);
               return s;
            }
            catch (const SocketException&) {
               // check the next address
            }
         }

         throw SocketException("Endpoint has no usable addresses");
      }

      Socket Socket::createBoundSocket(const Endpoint& ep, bool reuseAddr) throws (SocketException)
      {
         for (const Address& addr : ep) {
            try {
               Socket s(addr.type(), addr.domain(), addr.protocol());
               s.setReuseAddressEnabled(reuseAddr);
               s.bind(addr);
               return s;
            }
            catch (const SocketException&) {
               // check the next address
            }
         }
         // loop over all addresses for the endpoint, until we find one which can be bound
         throw SocketException("Endpoint has no usable addresses");
      }

      ssize_t Socket::peek(char* buffer, size_t bufSize, const Timeout& timeout) const throws (::canopy::io::IOException,InterruptedException)
      {
         return _sd->peek(buffer, bufSize, timeout);
      }

      ssize_t Socket::read(char* buffer, size_t bufSize, const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
      {
         return _sd->read(buffer, bufSize, timeout);
      }
      ssize_t Socket::readFrom(char* buffer, size_t bufSize, Address& from, const Timeout& timeout) throws (SocketException,InterruptedException)
      {
         return _sd->readFrom(buffer, bufSize, from, timeout);
      }

      size_t Socket::write(const char* buffer, size_t bufSize, const Timeout& timeout) throws (::canopy::io::IOException,InterruptedException)
      {
         return _sd->write(buffer, bufSize, timeout);
      }
      size_t Socket::writeTo(const char* buffer, size_t bufSize, const Address& to, const Timeout& timeout ) throws (SocketException,InterruptedException)
                  {
                     return _sd->writeTo(buffer, bufSize, to, timeout);
                  }
   }
}
