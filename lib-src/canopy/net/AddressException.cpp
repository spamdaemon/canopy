#include <canopy/net/AddressException.h>
#include <memory>


namespace canopy {
  namespace net {
    
    AddressException::AddressException (  ::std::string s) throws()
    : NetException(::std::move(s))
    {}

  }
}
