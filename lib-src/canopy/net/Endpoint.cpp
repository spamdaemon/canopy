#include <canopy/net/Endpoint.h>
#include <canopy/SystemError.h>

#include <cassert>
#include <sstream>
#include <cstring>
#include <cstdlib>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

using namespace ::std;

namespace canopy {
   namespace net {

      Endpoint::~Endpoint() throws()
      {
      }

      Endpoint::Endpoint() throws ()
      {
      }

      Endpoint::Endpoint(const Address& addr) throws()
      {
         _addresses.reserve(1);
         _addresses.push_back(addr);
      }

      Endpoint::Endpoint(const string& node, const char* service, SocketType t, Domain d,
            Protocol p) throws (exception,AddressException)
      {
         lookupAddresses(node.c_str(), service, t, d, p, false);
      }

      Endpoint::Endpoint(const string& node, UInt32 service, SocketType t, Domain d,
            Protocol p) throws (exception,AddressException)
      {
         lookupAddresses(node.c_str(), service, t, d, p, false);
      }

      Endpoint::Endpoint(const HostPort& hostport, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
            : Endpoint(hostport.host,hostport.port, t, d, p)
      {
      }

      Endpoint::Endpoint(const NodeService& nodeservice, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
            : Endpoint(nodeservice.node,nodeservice.service.c_str(), t, d, p)
      {
      }

      Endpoint Endpoint::filter(SocketType t, Domain d, Protocol p) const throws()
      {
         Endpoint ep;
         for (const Address& addr : _addresses) {
            if (t.id() != 0 && addr.type() != t) {
               continue;
            }
            if (d.id() != 0 && addr.domain() != d) {
               continue;
            }
            if (p.id() != 0 && addr.protocol() != p) {
               continue;
            }
            ep._addresses.push_back(addr);
         }
         return ep;
      }

      Address Endpoint::address() const throws (::std::exception)
      {
         if (_addresses.empty()) {
            throw ::std::runtime_error("Endpoint has no addresses");
         }
         return _addresses[0];
      }

      Endpoint Endpoint::createEndpoint(const char* hostport, const char* defService, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         // find a colon
         const char* sep = hostport == nullptr ? (const char*) 0 : rindex(hostport, ':');

         UInt32 port;
         const char* service = nullptr;
         if (sep == nullptr || *(sep + 1) == '\0') {
            // use the default port
            service = defService;
            if (defService == nullptr || strlen(defService) == 0) {
               throw ::std::invalid_argument("No port specified and no default service known");
            }
         }
         else {
            // assign the port string
            ::std::string tstr(sep + 1);
            ::std::istringstream in(tstr);
            in >> port;
            if (in.fail() || !in.eof()) {
               throw ::std::invalid_argument("Invalid port " + tstr);
            }
         }

         string host;
         if (hostport) {
            if (sep) {
               host.assign(hostport, sep - hostport);
            }
            else {
               host = hostport;
            }
         }
         if (host.empty()) {
            host = "127.0.0.1";
         }
         if (service != nullptr) {
            return Endpoint(host, service, t, d, p);
         }
         else {
            return Endpoint(host, port, t, d, p);
         }
      }

      Endpoint Endpoint::createEndpoint(const char* hostport, UInt32 defPort, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         // find a colon
         const char* sep = hostport == nullptr ? nullptr : rindex(hostport, ':');

         UInt32 port;
         if (sep == nullptr || *(sep + 1) == '\0') {
            // use the default port
            port = defPort;
         }
         else {
            // assign the port string
            ::std::string tstr(sep + 1);
            ::std::istringstream in(::std::string(sep + 1));
            in >> port;
            if (in.fail() || !in.eof()) {
               throw ::std::invalid_argument("Invalid port " + tstr);
            }
         }

         string host;
         if (hostport) {
            if (sep) {
               host.assign(hostport, sep - hostport);
            }
            else {
               host = hostport;
            }
         }
         if (host.empty()) {
            host = "127.0.0.1";
         }
         return Endpoint(host, port, t, d, p);
      }

      Endpoint Endpoint::createEndpoint(const char* hostport, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         // call createEndpoint with an incorrect default service
         return createEndpoint(hostport, nullptr, t, d, p);
      }

      Endpoint Endpoint::createAnyEndpoint(const char* service, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         Endpoint res;
         res.lookupAddresses(nullptr, service, t, d, p, true);
         return res;
      }

      Endpoint Endpoint::createAnyEndpoint(UInt32 service, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         Endpoint res;
         res.lookupAddresses(nullptr, service, t, d, p, true);
         return res;
      }

      Endpoint Endpoint::createLoopbackEndpoint(const char* service, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         Endpoint res;
         res.lookupAddresses(nullptr, service, t, d, p, false);
         return res;
      }

      Endpoint Endpoint::createLoopbackEndpoint(UInt32 service, SocketType t, Domain d,
            Protocol p) throws (::std::exception,AddressException)
      {
         Endpoint res;
         res.lookupAddresses(nullptr, service, t, d, p, false);
         return res;
      }

      void Endpoint::lookupAddresses(const char* node, UInt32 service, SocketType t, Domain d, Protocol p,
            bool bindable) throws (::std::exception,AddressException)
      {
         ostringstream str;
         str << service;
         lookupAddresses(node, str.str().c_str(), t, d, p, bindable);
      }

      void Endpoint::lookupAddresses(const char* node, const char* service, SocketType t, Domain d, Protocol p,
            bool bindable) throws (::std::exception,AddressException)
      {
         if (node == nullptr && service == nullptr) {
            throw ::std::invalid_argument("At least one of node or service must be specified");
         }

         struct ::addrinfo hints;
         struct ::addrinfo* info = nullptr;
         memset(&hints, 0, sizeof(struct ::addrinfo));

         hints.ai_flags = AI_ALL;
         if (bindable) {
            hints.ai_flags |= AI_PASSIVE;
         }
         hints.ai_family = d.id();
         hints.ai_socktype = t.id();
         hints.ai_protocol = p.id();

         int errCode = ::getaddrinfo(node, service, &hints, &info);
         if (errCode != 0) {
            string errorString = ::gai_strerror(errCode);
            errorString += " : ";
            errorString += node;

            switch (errCode) {
               case EAI_NONAME:
                  throw AddressException(errorString);
               case EAI_SERVICE:
                  throw AddressException(errorString);
               case EAI_AGAIN:
                  throw ResourceUnavailable(errorString, true);
               case EAI_ADDRFAMILY:
                  throw AddressException(errorString);
               case EAI_SYSTEM:
                  throw SystemError(errorString);
               default:
                  throw AddressException(errorString);
            }
         }
         Destructor destructor([info] { ::freeaddrinfo(info);});

         // for now, use the first address that pops up; we actually need a way to connect
         // using all possible addresses that mapped to the name

         for (struct ::addrinfo* i = info; i != 0; i = i->ai_next) {
            assert(i->ai_addr->sa_family==i->ai_family && "Socket families to do not match");
            // this is a safe transform
            const ::sockaddr_storage* saddr = reinterpret_cast<const ::sockaddr_storage*>(i->ai_addr);
            Address addr(SocketType(i->ai_socktype), Protocol(i->ai_protocol), i->ai_addrlen, *saddr);
            _addresses.push_back(::std::move(addr));
         }
      }

   }
}

