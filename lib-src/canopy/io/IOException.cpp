#include <canopy/io/IOException.h>
#include <memory>

namespace canopy {
   namespace io {


      IOException::IOException(::std::string s) throws()
            : ::std::runtime_error(::std::move(s))
      {
      }

      IOException::~IOException() throws()
      {}
   }
}
