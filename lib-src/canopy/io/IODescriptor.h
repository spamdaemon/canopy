#ifndef _CANOPY_IO_IODESCRIPTOR_H
#define _CANOPY_IO_IODESCRIPTOR_H

#ifndef _CANOPY_IO_NATIVEDESCRIPTOR_H
#include <canopy/io/NativeDescriptor.h>
#endif

namespace canopy {
   namespace io {
      /**
       * This is the smart pointer used to maintain references to a Native IO Descriptor.
       */
      class IODescriptor
      {
            /** The socket descriptor */
         public:
            typedef NativeDescriptor::Descriptor Descriptor;

            /** The Native descriptor is a friend */
         public:
            friend class NativeDescriptor;

            /**
             * Create a descriptor an invalid descriptor.
             */
         public:
            IODescriptor() throws();

            /**
             * A copy constructor.
             * @param src a source
             */
         public:
            IODescriptor(const IODescriptor& d) throws();

            /**
             * A move constructor.
             * @param src a source
             */
         public:
            IODescriptor(IODescriptor&& d) throws();

            /**
             * Create a new descriptor. This method will increase the reference
             * count of the passed pointer.
             * @param ptr a pointer to an descriptor (can be nullptr)
             */
         public:
            explicit IODescriptor(::std::shared_ptr< NativeDescriptor> ptr) throws();

            /**
             * The desctructor
             */
         public:
            ~IODescriptor() throws();

            /**
             * Copy operator
             */
         public:
            IODescriptor& operator=(const IODescriptor& obj) throws();

            /**
             * Move operator.
             */
         public:
            IODescriptor& operator=(IODescriptor&& obj) throws();

            /**
             * The Unix descriptor OS descriptor
             * @return the descriptor or a null descriptor
             */
         public:
            inline Descriptor descriptor() const throws ()
            {
               return _native->descriptor();
            }

            /**
             * Test if this is an invalid socket.
             * @return true if this socket is <em>invalid</em>
             */
         public:
            inline bool operator!() const throws ()
            {
               return _native->invalid();
            }

            /**
             * Test for equality. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             */
         public:
            bool operator==(const IODescriptor& s) const throws ()
            {
               return _native == s._native;
            }

            /**
             * Test for inequality. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             */
         public:
            inline bool operator!=(const IODescriptor& s) const throws ()
            {
               return _native != s._native;
            }

            /**
             * Get a partial ordering of two sockets. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             * @return true if this socket is before the specified socket.
             */
         public:
            inline bool operator<(const IODescriptor& s) const throws ()
            {
               return _native < s._native;
            }
            /**
             * Cast the native descriptor to the specified specialized descriptor.
             * Non-canopy classes should not be accessing this method.
             * @return true if the native descriptor implements the template type
             */
         public:
            template<class T>
            inline ::std::shared_ptr< T> cast() const throws()
            {
               return ::std::dynamic_pointer_cast< T>(_native);
            }

            /**
             * Determine if the native descriptor implements the given type.
             * @return true if the native descriptor implements the template type
             */
         public:
            template<class T>
            inline bool implements() const throws()
            {
               return cast< T>() != nullptr;
            }

            /** The socket descriptor for this socket */
         protected:
            mutable ::std::shared_ptr< NativeDescriptor> _native;
      };
   }
}
#endif
