#include <canopy/io/IOEvents.h>

namespace canopy {
   namespace io {

      constexpr int IOEvents::ALL;

      ::std::ostream& operator<<(::std::ostream& stream, const IOEvents& e)
      {
         stream << '{';
         const char* sep = "";
         if (e.test(IOEvents::READ)) {
            stream << "READ";
            sep = "|";
         }
         if (e.test(IOEvents::WRITE)) {
            stream << sep << "WRITE";
            sep = "|";
         }
         if (e.test(IOEvents::ERROR)) {
            stream << sep << "ERROR";
            sep = "|";
         }
         stream << '}';
         return stream;
      }
   }
}
