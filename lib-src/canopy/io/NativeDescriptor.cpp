#include <canopy/io/NativeDescriptor.h>
#include <canopy/io/IODescriptor.h>

namespace canopy {
   namespace io {

      const NativeDescriptor::Descriptor NativeDescriptor::INVALID_DESCRIPTOR;

      NativeDescriptor::NativeDescriptor() throws()
      {
      }

      NativeDescriptor::~NativeDescriptor() throws()
      {
      }

      IODescriptor NativeDescriptor::ioDescriptor() const throws()
      {
         return IODescriptor(::std::const_pointer_cast< NativeDescriptor>(shared_from_this()));
      }

   }
}
