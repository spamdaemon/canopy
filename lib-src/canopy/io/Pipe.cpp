#include <canopy/io/Pipe.h>
#include <cstring>

namespace canopy {
   namespace io {
      const size_t Pipe::BUFFER_SIZE;

      Pipe::Pipe() throws()
            : _outputClosed(true), _inputClosed(true), _bufferSize(0), _nRead(0), _nWritten(0)
      {
      }

      Pipe::Pipe(const FileDescriptor& r, const FileDescriptor& w, size_t size) throws()
            : _reader(r), _writer(w), _outputClosed(false), _inputClosed(false), _bufferSize(
                  size == 0 ? BUFFER_SIZE : size), _nRead(0), _nWritten(0)
      {
         _buffer.reset(new char[_bufferSize]);
      }

      Pipe::Pipe(const Pipe& p) throws()
            : _reader(p._reader), _writer(p._writer), _outputClosed(p._outputClosed), _inputClosed(p._inputClosed), _buffer(
                  new char[p._bufferSize]), _bufferSize(p._bufferSize), _nRead(p._nRead), _nWritten(p._nWritten)
      {
         ::std::memcpy(_buffer.get(), p._buffer.get(), _bufferSize);
      }

      Pipe::~Pipe() throw()
      {
      }

      Pipe& Pipe::operator=(const Pipe& p) throws()
      {
         if (&p != this) {
            _reader = p._reader;
            _writer = p._writer;
            _outputClosed = p._outputClosed;
            _inputClosed = p._inputClosed;
            _buffer.reset(new char[p._bufferSize]);
            _bufferSize = p._bufferSize;
            _nRead = p._nRead;
            _nWritten = p._nWritten;
            ::std::memcpy(_buffer.get(), p._buffer.get(), _bufferSize);
         }
         return *this;
      }

      void Pipe::closeReader() throws (IOException)
      {
         if (!_inputClosed) {
            _inputClosed = true;
            _reader.close();
         }
      }

      void Pipe::closeWriter() throws (IOException)
      {
         if (!_outputClosed) {
            _outputClosed = true;
            _writer.close();
         }
      }

      void Pipe::close() throws (IOException)
      {
         bool caught = false;
         try {
            closeReader();
         }
         catch (const IOException& e) {
            caught = true;
         }
         try {
            closeWriter();
         }
         catch (const IOException& e) {
            caught = true;
         }
         if (caught) {
            throw IOException("Failed to close pipe");
         }
      }

      void Pipe::clear() throws()
      {
         _nWritten = _nRead = 0;
      }

      void Pipe::wrap() throws()
      {
         if (_nWritten == _nRead) {
            _nWritten = _nRead = 0;
         }
         else if (_nWritten == _bufferSize) {
            // we can always wrap around if we've written an entire buffer
            _nWritten = 0;
         }
         else if (_nWritten > 1 && _nRead == _bufferSize) {
            // wrap around for the reads
            _nRead = 0;
         }
      }

      bool Pipe::determineEvents(IOEvents& revents, IOEvents& wevents) const throws()
      {
         wevents = revents = IOEvents();
         if (!_inputClosed) {
            if (_nRead < _nWritten) {
               if (_nWritten > 1 && _nRead < _nWritten - 1) {
                  revents.addEvents(IOEvents::READ);
               }
            }
            else if (_nRead < _bufferSize) {
               revents.addEvents(IOEvents::READ);
            }
         }
         if (!_outputClosed) {
            if (_nWritten < _nRead) {
               wevents.addEvents(IOEvents::WRITE);
            }
            else if (_nRead < _nWritten && _nWritten < _bufferSize) {
               wevents.addEvents(IOEvents::WRITE);
            }
         }
         return !revents.empty() || !wevents.empty();
      }

      size_t Pipe::transfer(const IOEvents& revents, const IOEvents& wevents) throws (IOException)
      {
         ssize_t nWritten = 0;
         ssize_t nRead = 0;

         ssize_t ioCount = 0;

         char* buffer = _buffer.get();

         // now, read as much as possible
         if (!_inputClosed && revents.test(IOEvents::READ)) {
            ioCount = 0;
            if (_nRead < _nWritten) {
               if (_nWritten > 1) {
                  ioCount = _nWritten - 1 - _nRead;
               }
            }
            else if (_nRead < _bufferSize) {
               // fill up the buffer
               ioCount = _bufferSize - _nRead;
            }
            if (ioCount > 0) {
               nRead = _reader.read(&buffer[_nRead], ioCount, BLOCKING_TIMEOUT);
            }
            if (nRead > 0) {
               _nRead += nRead;
            }
            else if (nRead < 0) {
               closeReader();
               nRead = 0;
            }
         }
         else if (revents.test(IOEvents::ERROR)) {
            // close the input stream
            closeReader();
         }

         if (!_outputClosed && wevents.test(IOEvents::WRITE)) {
            ioCount = 0;
            if (_nWritten < _nRead) {
               ioCount = _nRead - _nWritten;
            }
            else if (_nRead < _nWritten) {
               ioCount = _bufferSize - _nWritten;
            }
            if (ioCount > 0) {
               nWritten = _writer.write(&buffer[_nWritten], ioCount, BLOCKING_TIMEOUT);
            }
            if (nWritten > 0) {
               _nWritten += nWritten;
            }
            else if (nWritten < 0) {
               closeWriter();
               nWritten = 0;
            }
            if (_nWritten == _nRead) {
               _nWritten = _nRead = 0;
            }
            if (_nWritten == _bufferSize && _nRead < _nWritten) {
               _nWritten = 0;
            }
         }
         else if (wevents.test(IOEvents::ERROR)) {
            // close the input stream
            closeWriter();
         }

         // we can wrap around no the next read, but we can only do that
         // if we've written at least 1 character
         wrap();

         return nWritten > nRead ? nWritten : nRead;
      }

   }
}
