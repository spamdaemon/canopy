#ifndef _CANOPY_IO_SELECTOR_H
#define _CANOPY_IO_SELECTOR_H

#ifndef _CANOPY_INTERRUPTEDEXCEPTION_H
#include <canopy/InterruptedException.h>
#endif

#ifndef _CANOPY_IO_H
#include <canopy/io/io.h>
#endif

#ifndef _CANOPY_IO_IOEVENTS_H
#include <canopy/io/IOEvents.h>
#endif

#ifndef _CANOPY_IO_IODESCRIPTOR_H
#include <canopy/io/IODescriptor.h>
#endif

#ifndef _CANOPY_IO_SELECTIONEXCEPTION_H
#include <canopy/io/SelectionException.h>
#endif

#ifndef _CANOPY_MT_MUTEX_H
#include <canopy/mt/Mutex.h>
#endif

#include <stdexcept>
#include <map>
#include <vector>

namespace canopy {
   namespace io {

      /**
       * The Selector allows threads to manage multiple descriptors at the same
       * time for both reading and writing. Clients register a descriptor for
       * certain I/O events via the the add() method. Two types of events
       * are currently supported by the selector:
       * <dl>
       * <dt>READ
       * <dd>This event indicates that a descriptor is ready for reading,
       * and a call to descriptor::read() will return immediately.
       * <dt>WRITE
       * <dd>This event indicates that a descriptor has no outstanding
       * data to be written.
       * <dt>ERROR
       * <dd>This event indicates that either an error has occurred, or the
       * descriptor was closed.
       * </dl>
       * Each call to select() blocks the calling thread until one of the following
       * occurs:
       * <ul>
       * <li>an event on at least one descriptor
       * <li>the user specified timeout expires
       * <li>wakeup() is called
       * <li>add() or remove() is called and auto-interrupt is enabled.
       * </ul>
       * The select() call will return set of descriptors for which some event
       * occured. descriptors which were cancelled or closed, are removed during
       * the select() call.<p>
       * All methods of this class are thread-safe, but select() may not be
       * invoked concurrently. If select() is invoked while a selection is
       * in progress, then a ConcurrencyException is thrown.
       * <p>
       * This class is not meant to be subclassed.
       */
      class Selector
      {
         private:
            Selector(const Selector&s);
            Selector&operator=(const Selector&);

            /** The descriptor  type */
         private:
            typedef ::canopy::io::IODescriptor Descriptor;

             /** A descriptor descriptor */
         public:
            typedef Descriptor::Descriptor descriptorDescriptor;

            /** A descriptor event map */
         public:
            typedef ::std::map< Descriptor, IOEvents> EventMap;

            /** A descriptor event */
         public:
            typedef ::std::pair< Descriptor, IOEvents> DescriptorEvent;

            /** A descriptor event map */
         public:
            typedef ::std::vector< DescriptorEvent> Events;

            /**
             * Create a selector.
             * @throw ::std::runtime_error if underlying system cannot allocate resources
             */
         public:
            Selector() throws (::std::runtime_error);

            /**
             * Destroys this selector. All registered
             * descriptors are removed.
             */
         public:
            ~Selector() throws ();

            /**
             * The number of descriptors
             * @return the number of descriptors that have been registered
             */
         public:
            size_t size() const throws ();

            /**
             * Replace the events for a descriptor. If the
             * event is 0, then the descriptor is added. <code>ERROR</code>
             * is not a valid value for the event. If the descriptor does not exist,
             * then this method will return false.
             * @param descriptor a descriptor
             * @param events the set of events that select this descriptor
             * @pre REQUIRE_CONDITION(events,(event & ERROR)==0)
             * @note this method is thread-safe
             * @throw ConcurrencyException if another thread is suspended in a call to select()
             * @return true if the descriptor was replaced or removed.
             */
         public:
            bool replace(Descriptor descriptor, IOEvents events) throws ();

            /**
             * Add a descriptor-event pair to this set. If the
             * event is 0, then the descriptor is not added. <code>ERROR</code>
             * is not a valid value for the event. The additional events
             * will be or'ed into the current set of events for the descriptor.
             * @param descriptor a descriptor
             * @param events the set of events that select this descriptor
             * @pre REQUIRE_CONDITION(events,(event & ERROR)==0)
             * @note this method is thread-safe
             * @throw ConcurrencyException if another thread is suspended in a call to select()
             */
         public:
            void add(Descriptor descriptor, IOEvents events) throws ();

            /**
             * Remove a descriptor-event pair. To remove a descriptor completely,
             * use @code remove(descriptor,ALL) @endcode.
             * @note this method is thread-safe
             * @param descriptor a descriptor
             * @param events the events to be removed
             * @return false if there is at least one more
             * descriptor event pair for the descriptor, true otherwise
             * @throw ConcurrencyException if another thread is suspended in a call to select()
             */
         public:
            bool remove(Descriptor descriptor, IOEvents events) throws ();

            /**
             * Remove all descriptors from this selector.
             * @note this method is thread-safe
             * @throw ConcurrencyException if another thread is suspended in a call to select()
             */
         public:
            void clear() throws ();

            /**
             * Enable automatic wakeup of selection when adding,
             * removing, or clearing the selector.
             * @param enable to enable this functionality
             */
         public:
            inline void setAutoWakeupEnabled(bool enable) throws ()
            {
               _wakeupEnabled = enable;
            }

            /**
             * Test if automatic wakeup is turned on.
             * @return true if autowakeup is enabled.
             */
         public:
            inline bool isAutoWakeupEnabled() const throws ()
            {
               return _wakeupEnabled;
            }

            /**
             * Wakeup the selector if it is currently selecting. The
             * selector may wakeup before this method exits.
             */
         public:
            void wakeup() throws ();

            /**
             * Clear any pending wakeup.
             * @return true if a wakeup was pending, false otherwise
             */
         public:
            bool clearPendingWakeup() throws ();

            /**
             * Test if the specified descriptor is being selected.
             * @param descriptor a descriptor
             * @return true if the descriptor is ready for selection, false otherwise
             */
         public:
            bool isSelectable(Descriptor descriptor) const throws ();

            /**
             * Return the events that are registered with the specified
             * descriptor.
             * @note this method is thread-safe
             * @param descriptor a descriptor
             * @return the events registered with the specified descriptor or if the descriptor is no not registered
             */
         public:
            IOEvents getRegisteredEvents(Descriptor descriptor) const throws ();

            /**
             * Suspend the calling thread until an event occurs for
             * at least one descriptor, or the specified time has elapsed, or
             * the selector was woken up. Note that if a descriptor is closed,
             * after it is being selected then this method may wait as long
             * as the specified timeout.
             * @note this method is thread-safe
             * @param timeout a timeout in nanoseconds or -1 for infinite timeout
             * @param result whose contents are transfered to the select function
             * @return the result of the selection
             * @throw InterruptedException if the call was interrupted
             * @throw SelectionException if some error occurred
             */
         public:
            EventMap select(const Timeout& timeout, EventMap&& result) throws (SelectionException,InterruptedException);

            /**
             * Suspend the calling thread until an event occurs for
             * at least one descriptor, or the specified time has elapsed, or
             * the selector was woken up. Note that if a descriptor is closed,
             * after it is being selected then this method may wait as long
             * as the specified timeout.
             * @note this method is thread-safe
             * @param timeout a timeout in nanoseconds or -1 for infinite timeout
             * @param result whose contents are transfered to the select function
             * @return the result of the selection
             * @throw InterruptedException if the call was interrupted
             * @throw SelectionException if some error occurred
             */
         public:
            Events select(const Timeout& timeout, Events&& result) throws (SelectionException,InterruptedException);

            /**
             * Suspend the calling thread until at least one of the specified
             * events occurs for the given descriptor or the the specified
             * time expires.
             * @param descriptor a descriptor
             * @param events the event to select for the descriptor
             * @param timeout a timeout in nanoseconds
             * @pre assertNotNull(descriptor)
             * @return the events that occurred on the descriptor or 0 if
             * none occurred
             * @throw SelectionException if some error occurred
             * @throw InterruptedException if the call was interrupted
             */
         public:
            static IOEvents select(Descriptor descriptor, IOEvents events,
                  const Timeout& timeout) throws (SelectionException,InterruptedException);

            /**
             * Autonotify the select. Requires that the mutex is already locked
             * by the calling process.
             */
         private:
            void autoWakeup() volatile throws ();

            /** A mutex protecting access to this selector */
         private:
            ::canopy::mt::Mutex _mutex;

            /** True if the selector is currently selecting */
         private:
            bool _selecting;

            /** The event map */
         private:
            EventMap _events;

            /** The modification flag */
         private:
            bool _modified;

            /** The capacity */
         private:
            size_t _capacity;

            /** The auto wakeup flag */
         private:
            bool _wakeupEnabled;

            /** A true if a notification is pending */
         private:
            bool _wakeupPending;

            /** A pointer to some data structure */
         private:
            void* _data;

            /** A couple of filedescriptors */
         private:
            Int32 _fds[2];

            /** An array of descriptors */
         private:
            Descriptor* _descriptors;
      };
   }
}
#endif
