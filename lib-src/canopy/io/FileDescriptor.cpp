#include <canopy/io/FileDescriptor.h>
#include <canopy/io/FileDescriptorImpl.h>
#include <canopy/io/NativeDescriptor.h>

namespace canopy {
   namespace io {

      FileDescriptor::FileDescriptor(::std::shared_ptr< NativeFileDescriptor> fd)
throws            (IOException)
            : _sd(fd )
            {
               if (!_sd) {
                  _sd = ::std::make_shared<FileDescriptorImpl>();
               }
            }

      FileDescriptor::FileDescriptor(const ::canopy::io::IODescriptor& d)
throws            ()
            : FileDescriptor(d.cast< NativeFileDescriptor>())
            {
            }

      FileDescriptor::~FileDescriptor() throws()
      {
      }

      FileDescriptor::FileDescriptor()
throws            ()
            : FileDescriptor(nullptr)
            {
            }

      FileDescriptor& FileDescriptor::operator=(const FileDescriptor& s) throws (IOException)
      {
         _sd = s._sd;
         return *this;
      }

      FileDescriptor FileDescriptor::stdin() throws (IOException)
      {
         return FileDescriptor(::std::make_shared< FileDescriptorImpl>(0, false));
      }
      FileDescriptor FileDescriptor::stdout() throws (IOException)
      {
         return FileDescriptor(::std::make_shared< FileDescriptorImpl>(1, false));
      }
      FileDescriptor FileDescriptor::stderr() throws (IOException)
      {
         return FileDescriptor(::std::make_shared< FileDescriptorImpl>(2, false));
      }
   }
}
