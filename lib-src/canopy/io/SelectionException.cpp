#include <canopy/io/SelectionException.h>
#include <memory>

namespace canopy {
   namespace io {
      namespace {
         static const char* mapCode(SelectionException::ExceptionCode code) throws()
         {
            switch (code) {
               case SelectionException::CONCURRENT_MODIFICATION:
                  return "selection in progress";
               default:
                  return "reason unknown";
            }
         }
      }

      SelectionException::SelectionException(ExceptionCode code) throws()
            : IOException(mapCode(code)), _code(code)
      {
      }

      SelectionException::SelectionException(  ::std::string s) throws()
            : IOException(::std::move(s)), _code(OTHER)
      {
      }

      SelectionException::~SelectionException() throws()
      {
      }
   }
}
