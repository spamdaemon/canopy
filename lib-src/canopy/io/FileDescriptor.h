#ifndef _CANOPY_IO_FILEDESCRIPTOR_H
#define _CANOPY_IO_FILEDESCRIPTOR_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_IO_NATIVEFILEDESCRIPTOR_H
#include <canopy/io/NativeFileDescriptor.h>
#endif

#ifndef _CANOPY_INTERRUPTEDEXCEPTION_H
#include <canopy/InterruptedException.h>
#endif

#ifndef _CANOPY_IO_IODESCRIPTOR_H
#include <canopy/io/IODescriptor.h>
#endif

#include <memory>
#include <atomic>

namespace canopy {
   namespace io {

      /**
       * This is a wrapper for NativeFileDescriptor objects.
       */
      class FileDescriptor
      {
            /**
             * Create a new socket with the specified socket descriptor.
             * @param d a descriptor
             */
         public:
            explicit FileDescriptor(::std::shared_ptr<NativeFileDescriptor> d) throws(IOException);

            /** The default constructor */
         public:
            FileDescriptor() throws ();

            /**
             * Create a copy of the socket.
             * @param s a socket
             */
         public:
            inline FileDescriptor(const FileDescriptor& s) throws ()
                  : FileDescriptor(s._sd)
            {
            }

            /**
             * Create a socket from descriptor. If the NativeDescriptor associated
             * with the IODescriptor cannot be cast to a socket provider, then
             * an invalid socket is created.
             * @param d a descriptor (possibly invalid)
             */
         public:
            FileDescriptor(const ::canopy::io::IODescriptor& d) throws ();

            /**
             * Destroy this socket reference. If this is the last
             * reference to the socket, then it is destroyed.
             * If the socket cannot be destroyed properly, then an error
             * message is printed to cerr
             */
         public:
            ~FileDescriptor() throws ();

            /**
             * Assign the specified socket. An exception will be thrown
             * if this socket is the last reference and cannot be closed.
             * @param s a socket
             * @return *this
             */
         public:
            FileDescriptor& operator=(const FileDescriptor& s) throws (IOException);

            /**
             * Get the file descriptor corresponding to this process's stdin
             * @return the stdin file descriptor
             */
         public:
            static FileDescriptor stdin() throws();

            /**
             * Get the file descriptor corresponding to this process's stderr
             * @return the stderr file descriptor
             */
         public:
            static FileDescriptor stderr() throws();

            /**
             * Get the file descriptor corresponding to this process's stdout
             * @return the stdout file descriptor
             */
         public:
            static FileDescriptor stdout() throws();

            /**
             * Get the descriptor
             * @return the descriptor
             */
         public:
            inline ::canopy::io::IODescriptor descriptor() const throws ()
            {
               return _sd->ioDescriptor();
            }

            /**
             * Test if this is an invalid socket.
             * @return true if this socket is <em>invalid</em>
             */
         public:
            inline bool operator!() const throws ()
            {
               return _sd->invalid();
            }

            /**
             * Close this socket.
             * @note this method will change the socket's id
             * @throws Exception if the socket could not be closed
             */
         public:
            inline void close() throws (IOException)
            {
               _sd->close();
            }

            /**
             * Test if this socket has been closed. If this
             * method returns false, then this does not mean
             * that the next method invocation for this socket
             * will succeed.
             * @return true if this socket can no longer be used, false
             * if the socket currently not closed
             */
         public:
            inline bool isClosed() const throws ()
            {
               return _sd->isClosed();
            }

            /**
             * Peek at the buffer in the socket. This method works just like read, but does not
             * consume the data on the socket. This method is ideal for checking if a socket has been
             * closed in an orderly fashion, as this method will return 0 in that case.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            inline ssize_t peek(char* buffer, size_t bufSize,
                  const Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) const throws (IOException,InterruptedException)
            {
               return _sd->peek(buffer, bufSize, timeout);
            }

            /**
             * Read a number of bytes from this socket. This method blocks
             * unless the socket is a non-blocking socket or there is already
             * data to be read. This method must return 0 to indicate that the socket
             * buffer is full.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            inline ssize_t read(char* buffer, size_t bufSize,
                  const Timeout& timeout = ::canopy::io::BLOCKING_TIMEOUT) throws (IOException,InterruptedException)
            {
               return _sd->read(buffer, bufSize, timeout);
            }

            /**
             * Write a number of bytes to this socket. This method blocks
             * until all bytes have been written. If this socket is in non-blocking mode
             * and no bytes can currently be written (because the buffer is full), then 0 is returned.
             * Even if blocking is enabled, this call may still return 0 if the socket
             * itself has been set to be non-blocking!
             * @note if this is a UDP socket and the destination does not exist, then
             *       a CONNECTION_REFUSED exception may be raised.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to write
             * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
             * @exception SocketException if an error occurred during writing
             * @exception InterruptedException if the call was interrupted
             * @exception ConnectionRefused may be thrown if the destination does not exist
             */
         public:
            inline size_t write(const char* buffer, size_t bufSize, const Timeout& timeout =
                  ::canopy::io::BLOCKING_TIMEOUT) throws (IOException,InterruptedException)
            {
               return _sd->write(buffer, bufSize, timeout);
            }

            /**
             * Set the socket to block on I/O operations.
             * @param enabled if true, then I/O operations may block
             * @exception SocketException if an error occurred
             */
         public:
            inline void setBlockingEnabled(bool enabled) throws (IOException)
            {
               _sd->setBlockingEnabled(enabled);
            }

            /**
             * Test if blocking I/O is enabled.
             * @return true if I/O calls may be block, false otherwise
             * @exception SocketException if an error occurred
             */
         public:
            inline bool isBlockingEnabled() const throws (IOException)
            {
               return _sd->isBlockingEnabled();
            }

            /**
             * Test for equality. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             */
         public:
            inline bool operator==(const FileDescriptor& s) const throws ()
            {
               return _sd == s._sd;
            }

            /**
             * Test for inequality. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             */
         public:
            inline bool operator!=(const FileDescriptor& s) const throws ()
            {
               return _sd != s._sd;
            }

            /**
             * Get a partial ordering of two sockets. The test is NOT based on the socket's id
             * as it is not constant for the life-time of the socket.
             * @return true if this socket is before the specified socket.
             */
         public:
            inline bool operator<(const FileDescriptor& s) const throws ()
            {
               return _sd < s._sd;
            }

            /**
             * Swap two sockets.
             * @param s a socket
             * @note this method is not threadsafe!!!
             */
         public:
            inline void swap(FileDescriptor& s) throws()
            {
               ::std::swap(_sd, s._sd);
            }

            /** The socket descriptor for this socket */
         private:
            ::std::shared_ptr<NativeFileDescriptor> _sd;
      };
   }
}
#endif
