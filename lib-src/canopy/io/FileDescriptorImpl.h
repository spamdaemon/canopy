#ifndef _CANOPY_IO_FILEDESCRIPTORIMPL_H
#define _CANOPY_IO_FILEDESCRIPTORIMPL_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_IO_NATIVEFILEDESCRIPTOR_H_H
#include <canopy/io/NativeFileDescriptor.h>
#endif

#include <memory>
#include <atomic>

namespace canopy {
   namespace io {

      /**
       * This is a simple native file descriptor 
       */
      class FileDescriptorImpl : public NativeFileDescriptor
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(FileDescriptorImpl);

            /**
             * Create an invalid file descriptor */
         public:
            FileDescriptorImpl() throws ();

            /**
             *
             * Create a file descriptor.
             * @param fd a unix file descriptor
             * @param close the descriptor when the destructor is invoked
             */
         public:
            FileDescriptorImpl(Descriptor fd, bool closeWhenDone) throws ();

            /**
             * Destroy this socket reference. If this is the last
             * reference to the socket, then it is destroyed.
             * If the socket cannot be destroyed properly, then an error
             * message is printed to cerr
             */
         public:
            ~FileDescriptorImpl() throws ();

            /**
             * Get the id for this socket.
             * @note the id is not constant for the lifetime of this socket.
             * Calling close() on the socket will change the id.
             * @return the unique id.
             */
         public:
            inline Int32 id() const throws ()
            {
               // use the socket descirptor as an id
               return descriptor();
            }

            /**
             * Get the descriptor
             * @return the descriptor
             */
         public:
            Descriptor descriptor() const throws ()
            {
               return _sd.load();
            }

            /**
             * Test if this is an invalid socket.
             * @return true if this socket is <em>invalid</em>
             */
         public:
            bool invalid() const throws ();

            /**
             * Close this socket.
             * @note this method will change the socket's id
             * @throws Exception if the socket could not be closed
             */
         public:
            void close() throws (IOException);

            /**
             * Test if this socket has been closed. If this
             * method returns false, then this does not mean
             * that the next method invocation for this socket
             * will succeed.
             * @return true if this socket can no longer be used, false
             * if the socket currently not closed
             */
         public:
            bool isClosed() const throws ();

         public:
            IOEvents block(const IOEvents& events,
                  const Timeout& timeout) const throws (IOException, InterruptedException);

            /**
             * Peek at the buffer in the socket. This method works just like read, but does not
             * consume the data on the socket. This method is ideal for checking if a socket has been
             * closed in an orderly fashion, as this method will return 0 in that case.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            ssize_t peek(char* buffer, size_t bufSize, const Timeout& timeout) const throws (IOException,InterruptedException);

            /**
             * Read a number of bytes from this socket. This method blocks
             * unless the socket is a non-blocking socket or there is already
             * data to be read. This method must return 0 to indicate that the socket
             * buffer is full.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            ssize_t read(char* buffer, size_t bufSize, const Timeout& timeout) throws (IOException,InterruptedException);

            /**
             * Write a number of bytes to this socket. This method blocks
             * until all bytes have been written. If this socket is in non-blocking mode
             * and no bytes can currently be written (because the buffer is full), then 0 is returned.
             * Even if blocking is enabled, this call may still return 0 if the socket
             * itself has been set to be non-blocking!
             * @note if this is a UDP socket and the destination does not exist, then
             *       a CONNECTION_REFUSED exception may be raised.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to write
             * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
             * @exception IOException if an error occurred during writing
             * @exception InterruptedException if the call was interrupted
             * @exception ConnectionRefused may be thrown if the destination does not exist
             */
         public:
            size_t write(const char* buffer, size_t bufSize, const Timeout& timeout) throws (IOException,InterruptedException);

            /**
             * Set the socket to block on I/O operations.
             * @param enabled if true, then I/O operations may block
             * @exception IOException if an error occurred
             */
         public:
            void setBlockingEnabled(bool enabled) throws (IOException);

            /**
             * Test if blocking I/O is enabled.
             * @return true if I/O calls may be block, false otherwise
             * @exception IOException if an error occurred
             */
         public:
            bool isBlockingEnabled() const throws (IOException);

            /** The descriptor itself */
         private:
            mutable ::std::atomic< Descriptor> _sd;

            /** Close the descriptor when done */
         private:
            const bool _closeDescriptor;
      };
   }
}
#endif
