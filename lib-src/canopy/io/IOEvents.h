#ifndef _CANOPY_IO_IOEVENTS_H
#define _CANOPY_IO_IOEVENTS_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <iostream>

namespace canopy {
   namespace io {

      /** 
       * An IOEvent.
       */
      struct IOEvents
      {
            /** The event set type is just an integer */
            typedef int Set;

            enum EventType
            {
               READ = 1, WRITE = 2, ERROR = 4
            };


            /** The value for all possible events */
            static constexpr int ALL = READ | WRITE | ERROR;

            /**
             * Default constructor
             */
            inline IOEvents()
                  : _events(0)
            {
            }

            /**
             * Default constructor
             */
            inline IOEvents(const Set& initialSet)
                  : _events(initialSet & ALL)
            {
            }

            inline bool operator==(const IOEvents& e) const throws()
            {
               return _events == e._events;
            }
            inline bool operator!=(const IOEvents& e) const throws()
            {
               return _events != e._events;
            }
            inline IOEvents operator|(const IOEvents& e) const throws()
            {
               return IOEvents(_events | e._events);
            }
            inline IOEvents operator&(const IOEvents& e) const throws()
            {
               return IOEvents(_events & e._events);
            }
            inline IOEvents operator~() const throws()
            {
               return IOEvents((~_events) & ALL);
            }

            /**
             * Add the specified to this event set.
             * @param e an event type
             */
            inline void addEvents(Set e)
            {
               _events |= (e & ALL);
            }

            /**
             * Remove the specified events.
             * @param type an event type
             */
            inline void removeEvents(Set e)
            {
               _events &= ~e;
            }

            /**
             * Remove the specified events.
             * @param type an event type
             */
            inline void clearEvents()
            {
               _events  = 0;
            }

            /**
             * Get the events
             * @return the events
             */
            inline Set events() const
            {
               return _events;
            }

            /**
             * Test if any of the specified events is set.
             * @param events
             * @return the events that are set or 0
             */
            inline Set test(Set e) const
            {
               return _events & e;
            }

            /**
             * Test if no events are selected.
             * @return true if no events are selected
             */
            inline bool empty() const
            {
               return _events == 0;
            }

            /** The event set */
         private:
            Set _events;
      }
      ;

      ::std::ostream& operator<<(::std::ostream& stream, const IOEvents& e);
   }
}

#endif
