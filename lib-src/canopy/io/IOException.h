#ifndef _CANOPY_IO_IOSOCKETEXCEPTION_H
#define _CANOPY_IO_IOEXCEPTION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <stdexcept>
#include <string>

namespace canopy {
   namespace io {

      /**
       * This exception may thrown by operations that work
       * no connected sockets.
       */
      class IOException : public ::std::runtime_error
      {
         private:
            IOException() = delete;

            /**
             * Create an exception with the specified error string.
             * The exception code is set to OTHER.
             * @param msg an error message
             */
         public:
            IOException(  ::std::string msg) throws ();

            /** Destructor */
         public:
            ~IOException() throws();
      };
   }
}

#endif
