#ifndef _CANOPY_IO_NATIVEFILEFILEDESCRIPTOR_H
#define _CANOPY_IO_NATIVEFILEFILEDESCRIPTOR_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_IO_NATIVEDESCRIPTOR_H
#include <canopy/io/NativeDescriptor.h>
#endif

#ifndef _CANOPY_INTERRUPTEDEXCEPTION_H
#include <canopy/InterruptedException.h>
#endif

#ifndef _CANOPY_IO_IOEXCEPTION_H
#include <canopy/io/IOException.h>
#endif

namespace canopy {
   namespace io {
      /**
       * This class provides to C++ objects to OS system objects.
       */
      class NativeFileDescriptor : public NativeDescriptor
      {
            /**
             * A default constructor
             */
         protected:
            inline NativeFileDescriptor() throw()
            {
            }

            /**
             * Virtual destructor
             */
         public:
            ~NativeFileDescriptor() throws();

            /**
             * Test if this is an invalid socket.
             * @return true if this socket is <em>invalid</em>
             */
         public:
            virtual bool invalid() const throws () = 0;

            /**
             * Close this socket.
             * @note this method will change the socket's id
             * @throws Exception if the socket could not be closed
             */
         public:
            virtual void close() throws (IOException) = 0;

            /**
             * Test if this socket has been closed. If this
             * method returns false, then this does not mean
             * that the next method invocation for this socket
             * will succeed.
             * @return true if this socket can no longer be used, false
             * if the socket currently not closed
             */
         public:
            virtual bool isClosed() const throws () = 0;

            /**
             * Set the socket to block on I/O operations.
             * @param enabled if true, then I/O operations may block
             * @exception SocketException if an error occurred
             */
         public:
            virtual void setBlockingEnabled(bool enabled) throws (IOException) = 0;

            /**
             * Test if blocking I/O is enabled.
             * @return true if I/O calls may be block, false otherwise
             * @exception SocketException if an error occurred
             */
         public:
            virtual bool isBlockingEnabled() const throws (IOException) = 0;

            /**
             * Peek at the buffer in the socket. This method works just like read, but does not
             * consume the data on the socket. This method is ideal for checking if a socket has been
             * closed in an orderly fashion, as this method will return 0 in that case.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            virtual ssize_t peek(char* buffer, size_t bufSize,
                  const Timeout&  block) const throws (IOException,InterruptedException) = 0;

            /**
             * Read a number of bytes from this socket. This method blocks
             * unless the socket is a non-blocking socket or there is already
             * data to be read. This method must return 0 to indicate that the socket
             * buffer is full.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to read
             * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
             * @exception ConnectionException if an error occurred during reading
             * @exception InterruptedException if the call was interrupted
             * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
             */
         public:
            virtual ssize_t read(char* buffer, size_t bufSize,
                  const Timeout&  block) throws (IOException,InterruptedException) = 0;

            /**
             * Write a number of bytes to this socket. This method blocks
             * until all bytes have been written. If this socket is in non-blocking mode
             * and no bytes can currently be written (because the buffer is full), then 0 is returned.
             * Even if blocking is enabled, this call may still return 0 if the socket
             * itself has been set to be non-blocking!
             * @note if this is a UDP socket and the destination does not exist, then
             *       a CONNECTION_REFUSED exception may be raised.
             * @param buffer a buffer
             * @param bufSize the size of the buffer
             * @param block if true (default), then this method blocks until there is at least 1 byte to write
             * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
             * @exception SocketException if an error occurred during writing
             * @exception InterruptedException if the call was interrupted
             * @exception ConnectionRefused may be thrown if the destination does not exist
             */
         public:
            virtual size_t write(const char* buffer, size_t bufSize,
                  const Timeout&  block) throws (IOException,InterruptedException) = 0;

      };
   }
}
#endif
