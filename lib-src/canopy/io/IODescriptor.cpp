#include <canopy/io/IODescriptor.h>
#include <canopy/io/FileDescriptorImpl.h>

namespace canopy {
   namespace io {

      namespace {
         static ::std::shared_ptr< NativeDescriptor> getInvalidDescriptor() throws()
         {
            static ::std::shared_ptr< NativeDescriptor> STATIC(new FileDescriptorImpl());
            return STATIC;
         }
      }

      IODescriptor::IODescriptor(::std::shared_ptr< NativeDescriptor> ptr)
throws            ()
            : _native(ptr ? ptr : getInvalidDescriptor())
            {
            }

      IODescriptor::IODescriptor()
throws            ()
            : _native(getInvalidDescriptor())
            {
            }

      IODescriptor::IODescriptor(const IODescriptor& obj)
throws            ()
            : _native(obj._native)
            {
            }

      IODescriptor::IODescriptor(IODescriptor&& obj)
throws            ()
            : _native(obj._native)
            {
               obj._native = getInvalidDescriptor();
            }

      IODescriptor::~IODescriptor() throws()
      {
      }

      IODescriptor& IODescriptor::operator=(const IODescriptor& obj) throws()
      {
         _native = obj._native;
         return *this;
      }

      IODescriptor& IODescriptor::operator=(IODescriptor&& obj) throws()
      {
         _native = obj._native;
         obj._native = getInvalidDescriptor();
         return *this;
      }

   }
}
