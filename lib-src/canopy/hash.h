#ifndef _CANOPY_HASH_H
#define _CANOPY_HASH_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <functional>

namespace canopy {

  /** 
   * Hash a value. This function uses the new hash type 
   * defined in standard c++.
   * @param x a value
   * @return an integer 
   */
  template <class T> 
    inline size_t hashValue(const T& x) throws()
    { return ::std::hash<T>()(x); }
  
  /**
   * Compute the hashvalue for a bounded string.
   * @param str a string
   * @param length the length of the string
   * @return a hashvalue for the bounded string
   */
  size_t hashString (const char* str, size_t length) throws();

  /**
   * Compute the hashvalue for the 0-terminated string.
   * @param str a null-terminated string
   * @return a hashvalue for the string
   */
  size_t hashString (const char* str) throws();

  /**
   * Compute the hashvalue for a bounded string, but ignore the case
   * @param str a string
   * @param length the length of the string
   * @return a hashvalue for the bounded string
   */
  size_t hashStringIgnoreCase (const char* str, size_t length) throws();

  /**
   * Compute the hashvalue for the 0-terminated string, but ignore the case.
   * @param str a null-terminated string
   * @return a hashvalue for the string
   */
  size_t hashStringIgnoreCase (const char* str) throws();
}

#endif
