# this fragment defines required C compiler options
# there are 3 types of options:
#C_WARNING_OPTS = warnings that are to be turned on
#C_STANDARD_OPTS = common c options for the compiler being used (these options are used in both debug and optimized mode)
#C_DEBUG_OPTS = debug options 
#C_OPTIMIZE_OPTS = optimization options
#C_DEFINES = definitions for c compilation

C_WARNING_OPTS  := -Wall -W -Wshadow -Winline -Wredundant-decls -Wpointer-arith -Wlogical-op
# we're going to compile only for 486, nothing before that!
C_STANDARD_OPTS := -std=iso9899:199x -mfpmath=387 -std=c11 -fstrict-aliasing
C_DEBUG_OPTS    := -g3
C_OPTIMIZE_OPTS := -DNDEBUG -O2 -finline-functions  -fstrength-reduce -funroll-loops -finline-limit=1200
C_DEFINES := CANOPY_HAVE_ENV __STDC_CONSTANT_MACROS __STDC_LIMIT_MACROS _REENTRANT _XOPEN_SOURCE=500 _DEFAULT_SOURCE _PTHREADS $(C_DEFINES) 

