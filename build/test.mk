# this makefile fragment defines how to execute a test
# it requires a definition of the test procedure. 
# Normally, test procedure are either running a test
# defined with int main(), or running a test with inquest

# if testing with inquest, we need to know where inquest executable is
# and we'd also like to use a c++ filter to demangle the c++ names
# the inquest program
INQUEST   := $(PROJECT_DIR)/../inquest/bin/inquest
INQUEST_PREFIX := utest_
INQUEST_FAIL_PREFIX := failtest_
INQUEST_CPPFILT := $(addprefix -c++filt ,$(shell which c++filt))

# the test procedure should use 
# $< as the name of the object to be tested object
# 
#define doTest
#	$(INQUEST)  -substr-pass $(INQUEST_PREFIX) -substr-fail $(INQUEST_FAIL_PREFIX) -fail-on-error  $(INQUEST_CPPFILT) $<
#endef

# this test simply runs the program
# you will also have to change link.mk to modify the link stage for the test
# to not create a dynamic object
define doTest
 $<
endef
