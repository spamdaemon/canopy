# this makefile fragment defines the external libraries
# for the libraries, examples, tests, and binaries
# 
# the following definitions must be output from this file:
# EXT_LIB_SEARCH_PATH : the search paths for libraries
# EXT_LINK_LIBS       : the libraries with which to link (in the proper link order)
# EXT_INC_SEARCH_PATH : the search paths for include files
EXT_LIB_SEARCH_PATH :=
EXT_LINK_LIBS       :=
EXT_INC_SEARCH_PATH :=
EXT_COMPILER_DEFINES := 
EXT_CPP_OPTS := 
EXT_C_OPTS := 
EXT_FORTRAN_OPTS := 
# libunwind
#LIBUNWIND_BASE := 
#EXT_INC_SEARCH_PATH += $(LIBUNWIND_BASE)/include
EXT_LINK_LIBS += -lunwind-x86_64
EXT_COMPILER_DEFINES += CANOPY_HAVE_LIBUNWIND=1

# libelf and standard libs
#EXT_INC_SEARCH_PATH += /usr/include/libelf
EXT_LINK_LIBS += -lv4lconvert
EXT_LINK_LIBS += -ldl
EXT_LINK_LIBS += -lelf
EXT_LINK_LIBS += -lpthread 
EXT_LINK_LIBS += -lm

# the location of gmp
EXT_INC_SEARCH_PATH += /local/gmp/include
EXT_LIB_SEARCH_PATH += /local/gmp/lib
EXT_LINK_LIBS += -lgmp

# the location of ssl
#EXT_INC_SEARCH_PATH += 
#EXT_LIB_SEARCH_PATH += 
EXT_LINK_LIBS += -lssl
EXT_COMPILER_DEFINES += CANOPY_HAVE_OPENSSL=1
EXT_COMPILER_DEFINES += CANOPY_SSL_CERTIFICATES_PATH=/etc/ssl/certs


# this is an optional definition
# EXT_COMPILER_DEFINES : definitions passed to the compiler (without the -D option)
#EXT_CPP_OPTS := compiler options for c++ compilation
#EXT_C_OPTS := compiler options for c compilation
#EXT_FORTRAN_OPTS := compiler options for fortran compilation

EXT_COMPILER_DEFINES += CANOPY_HAVE_LIBELF=1
EXT_COMPILER_DEFINES += CANOPY_HAVE_GELF=1
