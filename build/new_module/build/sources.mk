# this makefile generates list of source files for
# binaries, examples, libraries, tests, and modules
# it also produces headers
# The input variables to this file are
# BASE_DIR
# BASE_SOURCE_DIRS
#
# The output of this stage is:
# BASE_OBJECT_FILES : the list of object that need to be linked into a module
# OBJECT_DIR : the directory where object files will be stored
# SOURCE_DIRS : the list of source directories

# the header source types are those that need not be compiled
# these are normally c/c++ header files and template files
HEADER_TYPES := .h .tpl

# the object types are those that will produce objects when compiled
OBJECT_TYPES := .cpp .c .f

# all objects are placed into this directory
OBJECT_DIR := $(BASE_DIR)/obj

# we need to build the source query: our query will be a simple 
# find command in each directory
SOURCE_QUERY := $(addprefix -o -name \*, $(OBJECT_TYPES) $(HEADER_TYPES))

# since the source query starts with -o, we need to run find with 
# an initial test which has to return false for the file
SOURCE_FILES := $(shell find $(BASE_SOURCE_DIRS) -false $(SOURCE_QUERY) 2> /dev/null )

# find the sources that need to be compiled into object files
OBJECT_SOURCE_FILES := $(filter $(addprefix %,$(OBJECT_TYPES)),$(SOURCE_FILES))

# get the object files for each type that will result during compilation

OBJECT_FILES := $(foreach ext, $(OBJECT_TYPES), \
	$(filter %.o,$(patsubst %$(ext),$(OBJECT_DIR)/%.o,$(OBJECT_SOURCE_FILES))))

BASE_OBJECT_FILES := $(filter $(OBJECT_DIR)/module-src/%,$(OBJECT_FILES))
TEST_OBJECT_FILES := $(filter $(OBJECT_DIR)/tests-src/%.o, $(OBJECT_FILES))

# the tests to be run
TESTS := $(patsubst $(OBJECT_DIR)/tests-src/%.o, $(BASE_DIR)/tests/%, $(TEST_OBJECT_FILES))

