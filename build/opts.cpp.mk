# this fragment defines required C++ compiler options
# there are 3 types of options:
#CPP_WARNING_OPTS = warnings that are to be turned on
#CPP_STANDARD_OPTS = common c++ options for the compiler being used (these options are used in both debug and optimized mode)
#CPP_DEBUG_OPTS = debug options 
#CPP_OPTIMIZE_OPTS = optimization options
#CPP_DEFINES = compiler options when compiling c++ code

CPP_WARNING_OPTS  := -Wall -Woverloaded-virtual -W -Wshadow -Winline -Wredundant-decls -Wpointer-arith -Wnon-virtual-dtor -Wno-ctor-dtor-privacy -Wlogical-op
# only support 486 and newer
CPP_STANDARD_OPTS :=  -std=c++17 -mfpmath=387 -fno-nonansi-builtins  -fstrict-aliasing
CPP_DEBUG_OPTS    := -g3
CPP_OPTIMIZE_OPTS := -DNDEBUG -O2 -finline-functions -felide-constructors -fstrength-reduce -funroll-loops -finline-limit=1200
CPP_DEFINES := CANOPY_HAVE_ENV __STDC_CONSTANT_MACROS __STDC_LIMIT_MACROS _REENTRANT _XOPEN_SOURCE=500 _DEFAULT_SOURCE _PTHREADS $(CPP_DEFINES)

