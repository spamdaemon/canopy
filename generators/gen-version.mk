
$(GEN_DIR)/canopy/version.h: $(GEN_DIR)/version.txt
	@mkdir -p "$(@D)";
	@$(BASE_DIR)/build/tools/gen-cpp-version -n canopy < "$<" > "$@";

GEN_LIB_SOURCE_FILES += canopy/version.h
