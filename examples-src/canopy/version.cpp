#include <canopy/canopy.h>
#include <iostream>

int main()
{
   ::std::cout << ::canopy::VERSION << ::std::endl;
   ::std::cout << ::canopy::DATE << ::std::endl;
   return 0;
}
