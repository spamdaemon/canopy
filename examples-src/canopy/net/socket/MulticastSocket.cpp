#include <canopy/mt.h>
#include <canopy/net/socket/MulticastSocket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/time/Time.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::mt;
using namespace ::canopy::io;
using namespace ::canopy::net;
using namespace ::canopy::net::socket;
using namespace ::canopy::time;

static const char* ADDRESS = "224.0.0.2";
static int PORT = 12345;

void write_socket(::canopy::net::socket::Socket& s, const ::std::string& str)
{
   int totalWritten = s.write(str.c_str(), str.length(), BLOCKING_TIMEOUT);
   if (totalWritten <= 0) {
      // ::std::cerr << "Nothing read" << ::std::endl;
      ::canopy::mt::Thread::suspend(::std::chrono::milliseconds(20));
      return;
   }
   ::std::cout << str << ::std::endl;
}

::std::string read_socket(::canopy::net::socket::Socket& s)
{
   char buf[128];
   ::std::ostringstream sbuf;
   int totalRead = s.read(buf, sizeof(buf), BLOCKING_TIMEOUT);
   if (totalRead == 0) {
      // ::std::cerr << "Nothing read" << ::std::endl;
      ::canopy::mt::Thread::suspend(::std::chrono::milliseconds(20));
      return ::std::string();
   }

   if (totalRead < 0) {
      return ::std::string();
   }
   sbuf.write(buf, totalRead);
   ::std::cout.write(buf, totalRead);
   ::std::cout.flush();
   ::std::cout << ::std::endl;
   return sbuf.str();
}

int main(int argc, char* const * argv)
{
   Endpoint ep(ADDRESS, PORT);
   bool send = true;

   if (argc < 2) {

   }
   else {
      send = ::std::string(argv[1]) == "send";
      if (argc < 3) {
      }
      else if (argc < 4) {
         ep = Endpoint(argv[2], PORT);
      }
      else {
         ep = Endpoint(argv[2], argv[3]);
      }
   }
   try {
      if (send) {
         auto socket = ::std::move(MulticastSocket::createConnectedSocket(ep));
         while (true) {
            write_socket(*socket, "Hello, World!!");
            Thread::suspend(Time::ONE_SECOND);
         }
      }
      else {
         auto socket = ::std::move(MulticastSocket::joinGroup(ep));
         while (true) {
            read_socket(*socket);
         }
      }
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
