#include <canopy/mt.h>
#include <canopy/net/socket/SslSocket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/mt/Thread.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::mt;
using namespace ::canopy::net;
using namespace ::canopy::net::socket;

static SslSocket::Options createServerOptions()
{
   SslSocket::Options opts;

//   opts.setKeyFile("/home/ray/git/canopy/test-data/ssl/ssl.key");
//   opts.setCertificateFile("/home/ray/git/canopy/test-data/ssl/ssl.crt");
   opts.setOwnCertificateFile("/home/ray/git/canopy/test-data/ssl/server.pem");

   opts.setCertificateRepositoryFile("/home/ray/git/canopy/test-data/ssl/client.crt");
   return opts;
}

static ::std::string read_socket(::canopy::net::socket::Socket& tcp)
{
   char buf[128];
   int totalRead = 0;
   ::std::ostringstream sbuf;
   while ( totalRead ==0) {
      int n = tcp.read(buf, sizeof(buf), ::canopy::io::BLOCKING_TIMEOUT);
      if (n == 0) {
         ::std::cerr << "Nothing read" << ::std::endl;
         ::canopy::mt::Thread::suspend(::std::chrono::milliseconds(20));
         break;
      }
      if (n < 0) {
         break;
      }
      sbuf.write(buf, n);
      totalRead += n;
      ::std::cout.write(buf, n);
      ::std::cout.flush();
   }
   ::std::cout << ::std::endl;
   return sbuf.str();
}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {
      Endpoint ep("localhost", (int) 55453, STREAM, INET_4);
      SslSocket::Options opts = createServerOptions();
      opts.setCertificateValidationDisabled(true);
      opts.setPeerCertificateOptional(true);
      SslSocket server(opts, INET_4);
      server.setReuseAddressEnabled(true);
      server.bind(ep.address());
      server.listen(1);
      server.setBlockingEnabled(false);
      Address remote;
      while (true) {
         ::std::unique_ptr< ::canopy::net::socket::Socket> client;
         while (client.get() == nullptr) {
            client = server.accept(remote);
            Thread::suspend(100000);
         }
         cerr << "Connection accepted" << ::std::endl;
         client->setBlockingEnabled(true);
         string line = read_socket(*client);
         ::std::cerr << "Read " << line << ::std::endl;
         client->close();
      }
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
