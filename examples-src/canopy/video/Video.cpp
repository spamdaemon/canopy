#include <canopy/video/VideoSource.h>
#include <canopy/video/FrameGrabber.h>
#include <canopy/video/Format.h>
#include <canopy/time/Time.h>
#include <canopy/ByteOrder.h>

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>

using namespace std;
using namespace canopy;
using namespace canopy::video;
using namespace canopy::time;

struct SunRaster
{
      Int32 MagicNumber; /* Magic (identification) number */
      Int32 Width; /* Width of image in pixels */
      Int32 Height; /* Height of image in pixels */
      Int32 Depth; /* Number of bits per pixel */
      Int32 Length; /* Size of image data in bytes */
      Int32 Type; /* Type of raster file */
      Int32 ColorMapType; /* Type of color map */
      Int32 ColorMapLength; /* Size of the color map in bytes */
};

static inline void YUV444_to_RGB888(int y, int u, int v, char* r, char* g, char* b)
{
   *r = (char) ::std::max(::std::min(255, (int) (y + 1.402 * (v - 128))), 0);
   *g = (char) ::std::max(::std::min(255, (int) (y - 0.34414 * (u - 128) - 0.71414 * (v - 128))), 0);
   *b = (char) ::std::max(::std::min(255, (int) (y + 1.772 * (u - 128))), 0);
}

struct MyCallback
{
      MyCallback(size_t nFrames)
            : _remaining(nFrames)
      {
      }

      bool operator()(const FrameGrabber::FrameInfo& info)
      {
         ::std::cerr << "Format : " << info.format.string() << ::std::endl;
         ::std::cerr << "Image size : " << info.w << " X " << info.h << ::std::endl;
         ::std::cerr << "Buffer Size : " << info.bufSize << ::std::endl;
         {
            ostringstream ssout;
            ssout << "frame-" << info.frameNo << ".raw";
            ofstream fout(ssout.str().c_str(), ios::binary);
            fout.write(info.buf, info.bufSize);
         }
         if (info.format.encoding() == Format::MJPEG) {
            ostringstream ssout;
            ssout << "frame-" << info.frameNo << ".mjpeg";
            ofstream fout(ssout.str().c_str(), ios::binary);

            fout.write(info.buf, info.bufSize);
         }
         else if (info.format.encoding() == Format::JPEG) {
            ostringstream ssout;
            ssout << "frame-" << info.frameNo << ".jpeg";
            ofstream fout(ssout.str().c_str(), ios::binary);

            fout.write(info.buf, info.bufSize);
         }
         else if (info.format.encoding() == Format::YUYV) {
            ostringstream ssout;
            ssout << "frame-" << info.frameNo << ".ras";
            ofstream fout(ssout.str().c_str(), ios::binary);

            SunRaster raster;
            raster.MagicNumber = toNetworkByteOrder(UInt32(0x59a66a95));
            raster.Width = toNetworkByteOrder(UInt32(info.w));
            raster.Height = toNetworkByteOrder(UInt32(info.h));
            raster.Depth = toNetworkByteOrder(UInt32(24));
            raster.Length = toNetworkByteOrder(UInt32(info.w * info.h * 3));
            raster.Type = toNetworkByteOrder(0);
            raster.ColorMapType = toNetworkByteOrder(0);
            raster.ColorMapLength = toNetworkByteOrder(0);

            fout.write((char*) &raster, sizeof(raster));

            char* rgb = new char[info.w * 3];
            try {
               for (UInt32 i = 0; i < info.h; ++i) {
                  for (UInt32 j = 0; j < info.w - 1; j += 2) {
                     const char* start = info.buf + 2 * (i * info.w + j);
                     // first get the actual YUV values
                     int Y0 = (unsigned char) *(start);
                     int U = (unsigned char) *(start + 1);
                     int Y1 = (unsigned char) *(start + 2);
                     int V = (unsigned char) *(start + 3);

                     {
                        char* b = rgb + 3 * j;
                        char* g = rgb + 3 * j + 1;
                        char* r = rgb + 3 * j + 2;
                        YUV444_to_RGB888(Y0, U, V, r, g, b);
                     }

                     {
                        char* b = rgb + 3 * (j + 1);
                        char* g = rgb + 3 * (j + 1) + 1;
                        char* r = rgb + 3 * (j + 1) + 2;
                        YUV444_to_RGB888(Y1, U, V, r, g, b);
                     }
                  }
                  fout.write(rgb, info.w * 3);
               }
               fout.flush();
               if (fout.bad()) {
                  return false;
               }
            }
            catch (...) {
               delete[] rgb;
               throw;
            }
            delete[] rgb;
         }
         else if (info.format.encoding() == Format::YUV420) {
            ostringstream ssout;
            ssout << "frame-" << info.frameNo << ".ras";
            ofstream fout(ssout.str().c_str(), ios::binary);

            SunRaster raster;
            raster.MagicNumber = toNetworkByteOrder(UInt32(0x59a66a95));
            raster.Width = toNetworkByteOrder(UInt32(info.w));
            raster.Height = toNetworkByteOrder(UInt32(info.h));
            raster.Depth = toNetworkByteOrder(UInt32(24));
            raster.Length = toNetworkByteOrder(UInt32(info.w * info.h * 3));
            raster.Type = toNetworkByteOrder(0);
            raster.ColorMapType = toNetworkByteOrder(0);
            raster.ColorMapLength = toNetworkByteOrder(0);

            fout.write((char*) &raster, sizeof(raster));

            char* rgb = new char[info.w * 3];
            try {
               const char* yStart = info.buf;
               const char* uStart = yStart + (info.w * info.h);
               const char* vStart = uStart + (info.w * info.h) / 4;
               for (UInt32 i = 0; i < info.h; ++i) {
                  for (UInt32 j = 0; j < info.w; ++j) {

                     // first get the actual YUV values
                     int Y = (unsigned char) yStart[i * info.w + j];
                     int U = (unsigned char) uStart[(i / 2) * (info.w / 2) + (j / 2)];
                     int V = (unsigned char) vStart[(i / 2) * (info.w / 2) + (j / 2)];
                     char* b = rgb + 3 * j;
                     char* g = rgb + 3 * j + 1;
                     char* r = rgb + 3 * j + 2;
                     YUV444_to_RGB888(Y, U, V, r, g, b);
                  }
                  fout.write(rgb, info.w * 3);
               }
               fout.flush();
               if (fout.bad()) {
                  return false;
               }
            }
            catch (...) {
               delete[] rgb;
               throw;
            }
            delete[] rgb;
         }
         ::std::cerr << "Captured frame " << info.frameNo << ::std::endl;
         return (--_remaining) > 0;
      }

   private:
      size_t _remaining;
};

int main(int argc, const char** argv)
{
   const char* source = 0;
   for (int arg = 1; arg < argc; ++arg) {
      if (string(argv[arg]) == "-device") {
         if (++arg == argc) {
            cerr << "Missing device name" << endl;
            exit(1);
         }
         source = argv[arg];
      }
   }

   unique_ptr< VideoSource> vid;
   try {
      vid = VideoSource::open(source);
      if (vid.get() == 0) {
         return 0;
      }
      cerr << "Successfully opened the video device" << endl;
      FrameGrabber& fg = vid->frameGrabber();
      cerr << "Successfully obtained the frame grabber" << endl;
      size_t nFrames = 1;
      Time start = Time::now();
      FrameGrabber::FrameCallback cb = MyCallback(nFrames);
      fg.captureFrames(cb);
      Time end = Time::now();
      Time::Duration delta = end.time() - start.time();

      ::std::cerr << "Grabbed " << nFrames << " at " << ((UINT64_C(1000000000) * nFrames) / delta.count()) << "fps"
            << endl;
   }
   catch (const exception& e) {
      cerr << "Error openening the video device : " << e.what() << endl;
      return 1;
   }
   return 0;
}
